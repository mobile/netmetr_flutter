netmetr_flutter
===============

A new Flutter application.

## Getting Started

For help getting started with Flutter, view our online
[documentation](https://flutter.io/).

Open-RMBT
===============

*Open-RMBT* is an open source, multi-threaded bandwidth test written in Java and
C, consisting of:

- command line client
- Java Applet client
- Android client
- control Servlet based on Restlet
- map Servlet based on Restlet
- statistics Servlet based on Restlet
- test server (written in C)
- qos test server

*Open-RMBT* is released under the [Apache License, Version 2.0]. It was developed
by [alladin-IT GmbH] and [SPECURE GmbH] and financed by the
[Austrian Regulatory Authority for Broadcasting and Telecommunications (RTR-GmbH)] and the [Agency for communication networks and services of the Republic of Slovenia].

 [alladin-IT GmbH]: https://alladin.at/
 [SPECURE GmbH]: https://www.specure.com/
 [Austrian Regulatory Authority for Broadcasting and Telecommunications (RTR-GmbH)]: https://www.rtr.at/
 [Agency for communication networks and services of the Republic of Slovenia]: http://www.akos-rs.si/
 [Apache License, Version 2.0]: http://www.apache.org/licenses/LICENSE-2.0

The following Eclipse projects are distributed in this release:

- **RMBTUtil** - common libraries and classes
- **RMBTClient** - client code used by *RMBTAndroid*, the command line client and the Applet


## Open-RMBT iOS App

OpenRMBT is an open source, multi-threaded bandwidth test used in [Open Nettest]. This repository contains the sources for the iOS App. For server and Android App sources, see https://github.com/SPECURE/ont-ios-client.
OpenRMBT is released under the [Apache License, Version 2.0]. The iOS App was developed by appscape and financed by the [Austrian Regulatory Authority for Broadcasting and Telecommunications (RTR)].

[Open Nettest]: https://nettest.org/en/map
[Apache License, Version 2.0]: http://www.apache.org/licenses/LICENSE-2.0
[Austrian Regulatory Authority for Broadcasting and Telecommunications (RTR)]: https://www.rtr.at/

## Third-party Libraries

In addition Google Maps iOS SDK, OpenRMBT iOS App uses several open source 3rd-party libraries that are under terms of a separate license:

- [CocoaAsyncSocket], public domain license
- [AFNetworking], MIT license
- [BlocksKit], MIT license
- [GCNetworkReachability], MIT license

[CocoaAsyncSocket]: https://github.com/robbiehanson/CocoaAsyncSocket
[AFNetworking]: https://github.com/AFNetworking/AFNetworking
[BlocksKit]: https://github.com/BlocksKit/BlocksKit
[GCNetworkReachability]: https://github.com/GlennChiu/GCNetworkReachability

For details, see `Pods/Pods-acknowledgements.markdown`.

Dependencies
===============

The following third party libraries are required dependencies:

### dnsjava ###

- BSD License
- available at <http://www.xbill.org/dnsjava/>
- copy as "RMBTClient/lib/dnsjava-2.1.8.jar"


### JSON in Java ###

- MIT License (+ "The Software shall be used for Good, not Evil.")
- available at <http://www.json.org/java/index.html>
- copy as  RMBTClient/lib/org.json.jar"


### Simple Logging Facade for Java (SLF4J) ###

- MIT License
- available at <http://www.slf4j.org/>
- copy as "android/libs/slf4j-android-1.5.8.jar"


### JOpt Simple ###

- MIT License
- available at <http://pholser.github.com/jopt-simple/>
- copy as "RMBTClient/lib/jopt-simple-3.2.jar"


### Apache Commons ###

- Apache 2.0 License
- available at <http://commons.apache.org/>
- copy as:
 - "RMBTClient/lib/commons-logging-1.1.1.jar"
 - "RMBTClient/lib/org.apache.httpclient.jar"
 - "RMBTClient/lib/org.apache.httpcore.jar"

  
Building
===============


Before building the application, set build parameters.

## Build parameters
File with build parameters should be located in `~/.gradle/gradle.properies`
Following parameters are used for build release/debug version of Netmetr Application.

<br />
<b>RMBT_CLIENT_SECRET_RELEASE/RMBT_CLIENT_SECRET_DEV</b>

Password for the server

<br />
<b>RMBT_CONTROL_HOST_RELEASE/RMBT_CONTROL_HOST_DEV</b>

Server host

<br />
<b>RMBT_CONTROL_IPV4_HOST_RELEASE/RMBT_CONTROL_IPV4_HOST_DEV</b>

Server host IPv4

<br />
<b>RMBT_CONTROL_IPV6_HOST_RELEASE/RMBT_CONTROL_IPV6_HOST_DEV</b>

Server host IPv6

<br />
<b>RMBT_CONTROL_IPV4_CHECK_HOST_RELEASE/RMBT_CONTROL_IPV4_CHECK_HOST_DEV</b>

Server host IPv4

<br />
<b>RMBT_CONTROL_IPV6_CHECK_HOST_RELEASE/RMBT_CONTROL_IPV6_CHECK_HOST_DEV</b>

Server host IPv6


<br />
<b>RMBT_CONTROL_PORT_RELEASE/RMBT_CONTROL_PORT_DEV</b>

Server port


<br />
<b>RMBT_CONTROL_SSL_RELEASE/RMBT_CONTROL_SSL_DEV</b>

Using SSL protocol

<br />
<b>RMBT_QOS_SSL_RELEASE/RMBT_QOS_SSL_DEV</b>

Using SSL protocol for QoS

<br />
<b>RMBT_SENTRY_DSN_RELEASE/RMBT_SENTRY_DSN_DEV</b>

URL for Sentry server for collecting bugs, release/debug version

<br />
<b>RMBT_MAPBOX_TOKEN</b>
                    
Mapbox access token to associate API requests with your account

## Building Android app

<b>Command to build an Android Netmetr APK file:</b>

```
flutter build apk
```


## Building iOS app
Xcode 9 with iOS 9.0 SDK is required to build the Open-RMBT iOS App.

For build iOS app, you will need following scripts:

<br />
<b>enviroment_script.sh</b>

Script enviroment_script.sh takes data from gradle.properties and use them to set files `ios/Flutter/Debug.xcconfig` (debug) and `ios/Flutter/Release.xcconfig` (release)

<br />
<b>set_build_version.sh</b>

Takes build version from git and use them to set application version in Info.plist. Path to Info.plist used as parameter should be `ios/Runner/Info.plist`

<b>Command to build an iOS Netmetr application bundle (Mac OS X host only):</b>

```
flutter build ios
```