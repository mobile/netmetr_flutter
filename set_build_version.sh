#!/usr/bin/env bash

INFO_PLIST=$1

VERSION_CODE_GIT=$(git log -1 --format=%at)

echo "VERSION CODE:"$VERSION_CODE_GIT

/usr/libexec/PlistBuddy -c "Set :CFBundleVersion $VERSION_CODE_GIT" "$INFO_PLIST"