package at.alladin.rmbt.client.v2.task.service;

public enum TestJitterStatus {
    NOT_STARTED, RUNNING, FINISH, FAIL, DISABLED
}
