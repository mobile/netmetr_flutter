/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package at.alladin.rmbt.client.v2.task;

import org.json.JSONObject;
import org.xbill.DNS.A6Record;
import org.xbill.DNS.AAAARecord;
import org.xbill.DNS.ARecord;
import org.xbill.DNS.CNAMERecord;
import org.xbill.DNS.DClass;
import org.xbill.DNS.MXRecord;
import org.xbill.DNS.Message;
import org.xbill.DNS.Name;
import org.xbill.DNS.Rcode;
import org.xbill.DNS.Record;
import org.xbill.DNS.Resolver;
import org.xbill.DNS.Section;
import org.xbill.DNS.SimpleResolver;
import org.xbill.DNS.TextParseException;
import org.xbill.DNS.Type;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import at.alladin.rmbt.client.QualityOfServiceTest;
import at.alladin.rmbt.client.helper.Dig;
import at.alladin.rmbt.client.v2.task.result.QoSTestResult;
import at.alladin.rmbt.client.v2.task.result.QoSTestResultEnum;

public class DnsTask2 extends AbstractQoSTask {
    private final static long DEFAULT_TIMEOUT = 5000000000L;

    private final String record;

    private final String host;

    private final String resolver;

    private final String port;

    private final long timeout;

    private final String dnsHostname;

    private final static String PARAM_DNS_HOST = "host";

    private final static String PARAM_DNS_RESOLVER = "resolver";

    private final static String PARAM_DNS_RECORD = "record";

    private final static String PARAM_DNS_TIMEOUT = "timeout";

    private final static String RESULT_STATUS = "dns_result_status";

    private final static String RESULT_ENTRY = "dns_result_entries";

    private final static String RESULT_TTL = "dns_result_ttl";

    private final static String RESULT_ADDRESS = "dns_result_address";

    private final static String RESULT_PRIORITY = "dns_result_priority";

    private final static String RESULT_DURATION = "dns_result_duration";

    private final static String RESULT_QUERY = "dns_result_info";

    private final static String RESULT_RESOLVER = "dns_objective_resolver";

    private final static String RESULT_DNS_HOST = "dns_objective_host";

    private final static String RESULT_DNS_RECORD = "dns_objective_dns_record";

    private final static String RESULT_DNS_TIMEOUT = "dns_objective_timeout";

    private final static String RESULT_DNS_ENTRIES_FOUND = "dns_result_entries_found";

    public DnsTask2(QualityOfServiceTest nnTest, TaskDesc taskDesc, int threadId, String dnsHostname){
        super(nnTest, taskDesc, threadId, threadId);

        this.dnsHostname = dnsHostname;
        this.record = (String)taskDesc.getParams().get(PARAM_DNS_RECORD);
        this.host = (String)taskDesc.getParams().get(PARAM_DNS_HOST);
        this.resolver = (String)taskDesc.getParams().get(PARAM_DNS_RESOLVER);
        this.port = (String)taskDesc.getParams().get(PARAM_QOS_TEST_OBJECTIVE_PORT);

        String value = (String) taskDesc.getParams().get(PARAM_DNS_TIMEOUT);
        this.timeout = value != null ? Long.valueOf(value) : DEFAULT_TIMEOUT;
    }

    @Override
    public void initTask() {
        //nothing
    }

    @Override
    public QoSTestResultEnum getTestType() {
        return QoSTestResultEnum.DNS;
    }

    @Override
    public boolean needsQoSControlConnection() {
        return false;
    }

    @Override
    public QoSTestResult call() throws Exception {
        final QoSTestResult testResult = initQoSTestResult(QoSTestResultEnum.DNS);

        try {
            onStart(testResult);

            long start = System.nanoTime();
            List<JSONObject> dnsResult = lookupDns(host, record, dnsHostname, (int)(timeout / 1000000), testResult);
            testResult.getResultMap().put(RESULT_ENTRY, dnsResult);
            long duration = System.nanoTime() - start;

            testResult.getResultMap().put(RESULT_DURATION, duration);
            testResult.getResultMap().put(RESULT_RESOLVER, resolver != null ? resolver : "Standard");
            testResult.getResultMap().put(RESULT_DNS_RECORD, record);
            testResult.getResultMap().put(RESULT_DNS_HOST, host);
            testResult.getResultMap().put(RESULT_DNS_TIMEOUT, timeout);

            if (dnsResult == null || dnsResult.size() <= 0) {
                testResult.getResultMap().put(RESULT_DNS_ENTRIES_FOUND, 0);
            }
            else {
                testResult.getResultMap().put(RESULT_DNS_ENTRIES_FOUND, dnsResult.size());
            }
        }catch (Exception e) {
            throw e;
        }
        finally {
            onEnd(testResult);
        }


        return  testResult;
    }

    public static List<JSONObject> lookupDns(String domainName, String record, String dnsHostname, int timeout, QoSTestResult testResult) {
        List<JSONObject> result = new ArrayList<JSONObject>();


        try {

            Resolver res = new SimpleResolver(dnsHostname);

            Record rec = Record.newRecord(Name.fromString(domainName, Name.root), Type.value(record), DClass.IN);
            Message query = Message.newQuery(rec);

            long startTime = System.currentTimeMillis();
            res.setTimeout(0, timeout);
            Message response = res.send(query);
            long endTime = System.currentTimeMillis();

            Dig.DnsRequest req= new Dig.DnsRequest(response, query, res, endTime - startTime);
            testResult.getResultMap().put(RESULT_QUERY, "OK");
            //dnsLookup = new Lookup(domainName, Type.value(record.toUpperCase()));
            //dnsLookup.setResolver(new SimpleResolver(resolver));
            //Record[] records = dnsLookup.run();
            testResult.getResultMap().put(RESULT_STATUS, Rcode.string(req.getResponse().getRcode()));
            if (req.getRequest().getRcode() == Rcode.NOERROR) {
                Record [] records = req.getResponse().getSectionArray(Section.ANSWER);

                if (records != null && records.length > 0) {
                    for (int i = 0; i < records.length; i++) {
                        JSONObject dnsEntry = new JSONObject();
                        if (records[i] instanceof MXRecord) {
                            dnsEntry.put(RESULT_PRIORITY, String.valueOf(((MXRecord) records[i]).getPriority()));
                            dnsEntry.put(RESULT_ADDRESS, ((MXRecord) records[i]).getTarget().toString());
                        }
                        else if (records[i] instanceof CNAMERecord) {
                            dnsEntry.put(RESULT_ADDRESS, ((CNAMERecord) records[i]).getAlias());
                        }
                        else if (records[i] instanceof ARecord) {
                            dnsEntry.put(RESULT_ADDRESS, ((ARecord) records[i]).getAddress().getHostAddress());
                        }
                        else if (records[i] instanceof AAAARecord) {
                            dnsEntry.put(RESULT_ADDRESS, ((AAAARecord) records[i]).getAddress().getHostAddress());
                        }
                        else if (records[i] instanceof A6Record) {
                            dnsEntry.put(RESULT_ADDRESS, ((A6Record) records[i]).getSuffix().toString());
                        }
                        else {
                            dnsEntry.put(RESULT_ADDRESS, records[i].getName());
                        }

                        dnsEntry.put(RESULT_TTL, String.valueOf(records[i].getTTL()));

                        //result.add(records[i].toString());
                        result.add(dnsEntry);
                        System.out.println("record " + i + " toString: " + records[i].toString());
                    }
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }

        } catch (TextParseException | UnknownHostException e) {
            testResult.getResultMap().put(RESULT_QUERY, "ERROR");
            e.printStackTrace();
            return null;
        } catch (SocketTimeoutException e) {
            testResult.getResultMap().put(RESULT_QUERY, "TIMEOUT");
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            testResult.getResultMap().put(RESULT_QUERY, "ERROR");
            e.printStackTrace();
            return null;
        }


        return result;
    }
}
