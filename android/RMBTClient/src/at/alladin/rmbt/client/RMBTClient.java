/*******************************************************************************
 * Copyright 2013-2015 alladin-IT GmbH
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package at.alladin.rmbt.client;

import java.io.File;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import at.alladin.rmbt.client.helper.*;
import at.alladin.rmbt.client.v2.task.service.TestJitterStatus;
import org.json.JSONException;
import org.json.JSONObject;

import at.alladin.rmbt.client.RMBTTest.CurrentSpeed;
import at.alladin.rmbt.client.v2.task.TaskDesc;
import at.alladin.rmbt.client.v2.task.VoipTask;
import at.alladin.rmbt.client.v2.task.result.QoSResultCollector;
import at.alladin.rmbt.client.v2.task.result.QoSTestResult;
import at.alladin.rmbt.client.v2.task.service.TestMeasurement;
import at.alladin.rmbt.client.v2.task.service.TestSettings;
import at.alladin.rmbt.client.v2.task.service.TrafficService;
import at.alladin.rmbt.util.tools.InformationCollectorTool;

import static at.alladin.rmbt.client.v2.task.VoipTask.*;

public class RMBTClient
{
    private static final ExecutorService COMMON_THREAD_POOL = Executors.newCachedThreadPool();
    
    private final RMBTTestParameter params;
    
    private final long durationInitNano = 2500000000L; // TODO
    private final long durationUpNano;
    private final long durationDownNano;
    
    private final AtomicLong initNano = new AtomicLong(-1);
    private final AtomicLong pingNano = new AtomicLong(-1);
    private final AtomicLong downBitPerSec = new AtomicLong(-1);
    private final AtomicLong upBitPerSec = new AtomicLong(-1);

    /* jitter */
    private TestJitterStatus testJitterStatus = TestJitterStatus.NOT_STARTED;
    private final AtomicLong jitter = new AtomicLong(-1);
    private final AtomicLong packetLossUp = new AtomicLong(-1);
    private final AtomicLong packetLossDown = new AtomicLong(-1);
    
    /* ping status */
    private final AtomicLong pingTsStart = new AtomicLong(-1);
    private final AtomicInteger pingNumDome = new AtomicInteger(-1);
    private final AtomicLong pingTsLastPing = new AtomicLong(-1);
    
    private final static long MIN_DIFF_TIME = 100000000; // 100 ms
    
    private final static int KEEP_LAST_ENTRIES = 20;
    private int lastCounter;
    private final long[][] lastTransfer;
    private final long[][] lastTime;
    
    private final ExecutorService testThreadPool;
    
    private final RMBTTest[] testTasks;
    
    private TotalTestResult result;
    
    private SSLSocketFactory sslSocketFactory;
    
    private RMBTOutputCallback outputCallback;
    private final boolean outputToStdout = true;
    
    private final ControlServerConnection controlConnection;
    
    private final AtomicBoolean aborted = new AtomicBoolean();

    private String dnsHostname = null;

    private final File cacheDir;

    public String getDnsHostname() {
        return dnsHostname;
    }

    public void setDnsHostname(String dnsHostname) {
        this.dnsHostname = dnsHostname;
    }

    private TestSettings qosSettings;

    public void setQosSettings(TestSettings qosSettings) {
        this.qosSettings = qosSettings;
    }

    private String errorMsg = "";


    
    /*------------------------------------
    	V2 tests
    --------------------------------------*/
    
    public final static String TASK_UDP = "udp";
    public final static String TASK_TCP = "tcp";
    public final static String TASK_DNS = "dns";
    public final static String TASK_VOIP = "voip";
    public final static String TASK_JITTER = "jitter";
    public final static String TASK_NON_TRANSPARENT_PROXY = "non_transparent_proxy";
    public final static String TASK_HTTP = "http_proxy";
    public final static String TASK_WEBSITE = "website";
    public final static String TASK_TRACEROUTE = "traceroute";
    
    private List<TaskDesc> taskDescList;

    /*------------------------------------*/
    
    private final AtomicReference<TestStatus> testStatus = new AtomicReference<TestStatus>(TestStatus.WAIT);
    private final AtomicReference<TestStatus> statusBeforeError = new AtomicReference<TestStatus>(null);
    private final AtomicLong statusChangeTime = new AtomicLong();
    
    private TrafficService trafficService;
    
    private InformationCollectorTool informationCollectorTool;
    
    public static ExecutorService getCommonThreadPool()
    {
        return COMMON_THREAD_POOL;
    }
    
    private ConcurrentHashMap<TestStatus, TestMeasurement> measurementMap = new ConcurrentHashMap<TestStatus, TestMeasurement>();
    
    public static RMBTClient getInstance(final String host, final String pathPrefix, final int port,
            final boolean encryption, final ArrayList<String> geoInfo, final String uuid, final String clientType,
            final String clientName, final String clientVersion, final RMBTTestParameter overrideParams,
            final JSONObject additionalValues, final File cacheDir) throws RMBTClientException{
    	
        final JSONObject requestData = createNewConnectionRequestData(geoInfo, uuid, clientType, clientName, clientVersion, additionalValues, true);
        
        return getInstance(host, pathPrefix, port,
                encryption, geoInfo, uuid, clientType,
                clientName, clientVersion, overrideParams,
                requestData, additionalValues, cacheDir, true, true);
    }
    
    public static RMBTClient getInstance(final String host, final String pathPrefix, final int port,
            final boolean encryption, final ArrayList<String> geoInfo, final String uuid, final String clientType,
            final String clientName, final String clientVersion, final RMBTTestParameter overrideParams,
            final JSONObject requestData, JSONObject additionalValues, final File cacheDir, final boolean runJitter, final boolean runQoS) throws RMBTClientException{


    	final ControlServerConnection controlConnection = new ControlServerConnection();
    	controlConnection.setRunJitter(runJitter);
    	controlConnection.setRunQoS(runQoS);
        
        final String error = controlConnection.requestNewTestConnection(host, pathPrefix, port, encryption, uuid, requestData);
        
        if (error != null)
        {
            System.out.println(error);
            throw new RMBTClientException(error);
        }

        //TODO: simple and fast solution; make it better
        final String errorNewTest = controlConnection.requestQoSTestParameters(host, pathPrefix, port, encryption, geoInfo,
        		uuid, clientType, clientName, clientVersion, additionalValues);
        
        if (errorNewTest != null)
        {
            System.out.println(errorNewTest);
            throw new RMBTClientException(errorNewTest);
        }
        
        final RMBTTestParameter params = controlConnection.getTestParameter(overrideParams);


        return new RMBTClient(params, cacheDir, controlConnection);
    }

    public static RMBTClient getInstance(File cacheDir, final RMBTTestParameter params)
    {
        return new RMBTClient(params, cacheDir,null);
    }
    
    private RMBTClient(final RMBTTestParameter params,  File cacheDir, final ControlServerConnection controlConnection)
    {
        this.params = params;
        this.controlConnection = controlConnection;
        this.cacheDir = cacheDir;

        params.check();
        
        if (params.getNumThreads() > 0)
        {
            testThreadPool = Executors.newFixedThreadPool(params.getNumThreads());
            testTasks = new RMBTTest[params.getNumThreads()];
        }
        else
        {
            testThreadPool = null;
            testTasks = null;
        }
        
        durationDownNano = params.getDuration() * 1000000000L;
        durationUpNano = params.getDuration() * 1000000000L;
        
        lastTransfer = new long[params.getNumThreads()][KEEP_LAST_ENTRIES];
        lastTime = new long[params.getNumThreads()][KEEP_LAST_ENTRIES];
        
        this.taskDescList = controlConnection.v2TaskDesc;
        //if (params.isEncryption())
        //    sslSocketFactory = createSSLSocketFactory();

        if(!params.isRunJitter()){
            testJitterStatus = TestJitterStatus.DISABLED;
        }
    }
    
    public static JSONObject createNewConnectionRequestData(
			final ArrayList<String> geoInfo, final String uuid,
			final String clientType, final String clientName, 
			final String clientVersion, final JSONObject additionalValues, final boolean hasSignal) {
    	
    	final JSONObject regData = new JSONObject();
    	try {
			regData.put("uuid", uuid);
			regData.put("client", clientName);
			
			regData.put("condition", hasSignal ? "standard" : "no_signal");
			
			regData.put("version", Config.RMBT_VERSION_NUMBER);
			regData.put("type", clientType);
			regData.put("softwareVersion", clientVersion);
			regData.put("softwareRevision", RevisionHelper.getVerboseRevision());
			regData.put("language", Locale.getDefault().getLanguage());
			regData.put("timezone", TimeZone.getDefault().getID());
			regData.put("time", System.currentTimeMillis());

			if (geoInfo != null) {
				final JSONObject locData = new JSONObject();
				locData.put("time", geoInfo.get(0));
				locData.put("lat", geoInfo.get(1));
				locData.put("long", geoInfo.get(2));
				locData.put("accuracy", geoInfo.get(3));
				locData.put("altitude", geoInfo.get(4));
				locData.put("bearing", geoInfo.get(5));
				locData.put("speed", geoInfo.get(6));
				locData.put("provider", geoInfo.get(7));

				regData.accumulate("location", locData);
			}

			ControlServerConnection.addToJSONObject(regData, additionalValues);

			return regData;

		} catch (final JSONException e1) {
			return null;
		}
	}

    public void setTrafficService(TrafficService trafficService) {
    	this.trafficService = trafficService;
    }
    
    public TrafficService getTrafficService() {
    	return this.trafficService;
    }
    
    public void setInformationCollectorTool(InformationCollectorTool tool) {
    	this.informationCollectorTool = tool;
    }
    
    public InformationCollectorTool getInformationCollectorTool() {
    	return this.informationCollectorTool;
    }

    public void startInformationCollectorTool() {
    	if (this.informationCollectorTool != null && !this.informationCollectorTool.isRunning()) {
    		this.informationCollectorTool.start(COMMON_THREAD_POOL);
    	}
    }
    
    public JSONObject getInformationCollectorToolIntermediateResult(boolean clean) {
    	if (this.informationCollectorTool != null && !this.informationCollectorTool.isRunning()) {
    		return this.informationCollectorTool.getJsonObject(clean);
    	}
    	
    	return null;
    }
    
    public void stopInformationCollectorTool() {
    	if (this.informationCollectorTool != null && this.informationCollectorTool.isRunning()) {
    		this.informationCollectorTool.stop();
    	}    	
    }
    
    private SSLSocketFactory createSSLSocketFactory()
    {
        log("initSSL...");
        try
        {
            final SSLContext sc = getSSLContext(null, null);
            
            final SSLSocketFactory factory = sc.getSocketFactory();
            
            return factory;
        }
        catch (final Exception e)
        {
            setErrorStatus();
            log(e);
        }
        return null;
    }
    
    public static TrustManager getTrustingManager()
    {
        return new javax.net.ssl.X509TrustManager()
        {
            public X509Certificate[] getAcceptedIssuers()
            {
                return new X509Certificate[] {};
            }
            
            public void checkClientTrusted(final X509Certificate[] certs, final String authType)
                    throws CertificateException
            {
                // System.out.println("[TRUSTING] checkClientTrusted: " +
                // Arrays.toString(certs) + " - " + authType);
            }
            
            public void checkServerTrusted(final X509Certificate[] certs, final String authType)
                    throws CertificateException
            {
                // System.out.println("[TRUSTING] checkServerTrusted: " +
                // Arrays.toString(certs) + " - " + authType);
            }
        };
    }
    
    public static SSLContext getSSLContext(final String caResource, final String certResource)
            throws NoSuchAlgorithmException, KeyManagementException
    {
        X509Certificate _ca = null;
        try
        {
            if (caResource != null)
            {
                final CertificateFactory cf = CertificateFactory.getInstance("X.509");
                _ca = (X509Certificate) cf.generateCertificate(RMBTClient.class.getClassLoader().getResourceAsStream(
                        caResource));
            }
        }
        catch (final Exception e)
        {
            e.printStackTrace();
        }
        
        final X509Certificate ca = _ca;
        
        X509Certificate _cert = null;
        try
        {
            if (certResource != null)
            {
                final CertificateFactory cf = CertificateFactory.getInstance("X.509");
                _cert = (X509Certificate) cf.generateCertificate(RMBTClient.class.getClassLoader().getResourceAsStream(
                        certResource));
            }
        }
        catch (final Exception e)
        {
            e.printStackTrace();
        }
        final X509Certificate cert = _cert;
        
        // TrustManagerFactory tmf = null;
        // try
        // {
        // if (cert != null)
        // {
        // final KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
        // ks.load(null, null);
        // ks.setCertificateEntry("crt", cert);
        //
        // tmf =
        // TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        // tmf.init(ks);
        // }
        // }
        // catch (Exception e)
        // {
        // e.printStackTrace();
        // }
        
        final TrustManager tm;
        if (cert == null)
            tm = getTrustingManager();
        else
            tm = new javax.net.ssl.X509TrustManager()
            {
                public X509Certificate[] getAcceptedIssuers()
                {
                    // System.out.println("getAcceptedIssuers");
                    if (ca == null)
                        return new X509Certificate[] { cert };
                    else
                        return new X509Certificate[] { ca };
                }
                
                public void checkClientTrusted(final X509Certificate[] certs, final String authType)
                        throws CertificateException
                {
                    // System.out.println("checkClientTrusted: " +
                    // Arrays.toString(certs) + " - " + authType);
                }
                
                public void checkServerTrusted(final X509Certificate[] certs, final String authType)
                        throws CertificateException
                {
                    // System.out.println("checkServerTrusted: " +
                    // Arrays.toString(certs) + " - " + authType);
                    if (certs == null)
                        throw new CertificateException();
                    for (final X509Certificate c : certs)
                        if (cert.equals(c))
                            return;
                    throw new CertificateException();
                }
            };
        
        final TrustManager[] trustManagers = new TrustManager[] { tm };
        
        javax.net.ssl.SSLContext sc;
        sc = javax.net.ssl.SSLContext.getInstance(Config.RMBT_ENCRYPTION_STRING);
        
        sc.init(null, trustManagers, new java.security.SecureRandom());
        return sc;
    }
    
    public TestResult runTest() throws InterruptedException
    {
    	System.out.println("starting test...");
    	
    	long txBytes = 0;
    	long rxBytes = 0;
    	final long timeStampStart = System.nanoTime();
    	
        if (testStatus.get() != TestStatus.ERROR && testThreadPool != null)
        {
            
        	if (trafficService != null) {
        		txBytes = trafficService.getTotalTxBytes();
        		rxBytes = trafficService.getTotalRxBytes();
        	}
        	
            resetSpeed();
            downBitPerSec.set(-1);
            upBitPerSec.set(-1);
            pingNano.set(-1);
            initNano.set(-1);
            jitter.set(-1);
            packetLossUp.set(-1);
            packetLossDown.set(-1);
            
            final long waitTime = params.getStartTime() - System.currentTimeMillis();
            if (waitTime > 0)
            {
                setStatus(TestStatus.WAIT);
                log(String.format(Locale.US, "we have to wait %d ms...", waitTime));
                Thread.sleep(waitTime);
                log(String.format(Locale.US, "...done.", waitTime));
            }
            else
                log(String.format(Locale.US, "luckily we do not have to wait.", waitTime));
            
            setStatus(TestStatus.INIT);
            statusBeforeError.set(null);
            
            if (testThreadPool.isShutdown())
                throw new IllegalStateException("RMBTClient already shut down");
            log("starting test...");
            
            final int numThreads = params.getNumThreads();
            
            aborted.set(false);
            
            result = new TotalTestResult();
            
            if (params.isEncryption())
                sslSocketFactory = createSSLSocketFactory();
                        
            log(String.format(Locale.US, "Host: %s; Port: %s; Enc: %s", params.getHost(), params.getPort(), params.isEncryption()));
            log(String.format(Locale.US, "starting %d threads...", numThreads));
            
            final CyclicBarrier barrier = new CyclicBarrier(numThreads);
            
            @SuppressWarnings("unchecked")
            final Future<ThreadTestResult>[] results = new Future[numThreads];
            
            final int storeResults = (int) (params.getDuration() * 1000000000L / MIN_DIFF_TIME);
            
            final AtomicBoolean fallbackToOneThread = new AtomicBoolean();

            
            for (int i = 0; i < numThreads; i++)
            {
                testTasks[i] = new RMBTTest(this, params, i, barrier, storeResults, MIN_DIFF_TIME, fallbackToOneThread);
                results[i] = testThreadPool.submit(testTasks[i]);
            }
            
            try
            {
                
                long shortestPing = Long.MAX_VALUE;
                
                // wait for all threads first
                for (int i = 0; i < numThreads; i++)
                    results[i].get();
                
                if (aborted.get())
                    return null;
                
                final long[][] allDownBytes = new long[numThreads][];
                final long[][] allDownNsecs = new long[numThreads][];
                final long[][] allUpBytes = new long[numThreads][];
                final long[][] allUpNsecs = new long[numThreads][];
                
                int realNumThreads = 0;
                log("");
                for (int i = 0; i < numThreads; i++)
                {
                    final ThreadTestResult testResult = results[i].get();
                    
                    if (testResult != null)
                    {
                        realNumThreads++;
                        
                        log(String.format(Locale.US, "Thread %d: Download: bytes: %d time: %.3f s", i,
                                ThreadTestResult.getLastEntry(testResult.down.bytes),
                                ThreadTestResult.getLastEntry(testResult.down.nsec) / 1e9));
                        log(String.format(Locale.US, "Thread %d: Upload:   bytes: %d time: %.3f s", i,
                                ThreadTestResult.getLastEntry(testResult.up.bytes),
                                ThreadTestResult.getLastEntry(testResult.up.nsec) / 1e9));
                        
                        final long ping = testResult.ping_shortest;
                        if (ping < shortestPing)
                            shortestPing = ping;
                        
                        if (!testResult.pings.isEmpty())
                            result.pings.addAll(testResult.pings);
                        
                        allDownBytes[i] = testResult.down.bytes;
                        allDownNsecs[i] = testResult.down.nsec;
                        allUpBytes[i] = testResult.up.bytes;
                        allUpNsecs[i] = testResult.up.nsec;
                        
                        result.totalDownBytes += testResult.totalDownBytes;
                        result.totalUpBytes += testResult.totalUpBytes;
                        
                        // aggregate speedItems
                        result.speedItems.addAll(testResult.speedItems);
                    }
                }
                
                result.calculateDownload(allDownBytes, allDownNsecs);
                result.calculateUpload(allUpBytes, allUpNsecs);
                
                log("");
                log(String.format(Locale.US, "Total calculated bytes down: %d", result.bytes_download));
                log(String.format(Locale.US, "Total calculated time down:  %.3f s", result.nsec_download / 1e9));
                log(String.format(Locale.US, "Total calculated bytes up:   %d", result.bytes_upload));
                log(String.format(Locale.US, "Total calculated time up:    %.3f s", result.nsec_upload / 1e9));
                
                // get Connection Info from thread 1 (one thread must run)
                result.ip_local = results[0].get().ip_local;
                result.ip_server = results[0].get().ip_server;
                result.port_remote = results[0].get().port_remote;
                result.encryption = results[0].get().encryption;
                
                result.num_threads = realNumThreads;
                
                result.ping_shortest = shortestPing;
                
                result.speed_download = result.getDownloadSpeedBitPerSec() / 1e3;
                result.speed_upload = result.getUploadSpeedBitPerSec() / 1e3;
                
                log("");
                log(String.format(Locale.US, "Total Down: %.0f kBit/s", result.getDownloadSpeedBitPerSec() / 1e3));
                log(String.format(Locale.US, "Total UP:   %.0f kBit/s", result.getUploadSpeedBitPerSec() / 1e3));
                log(String.format(Locale.US, "Ping:       %.2f ms", shortestPing / 1e6));
                
                if (controlConnection != null)
                {
                    log("");
                    final String testId = controlConnection.getTestId();
                    final String testUUID = params.getUUID();
                    final long testTime = controlConnection.getTestTime();
                    log(String.format(Locale.US, "uid=%s, time=%s, uuid=%s\n", testId, new SimpleDateFormat(
                            "yyyy-MM-dd HH:mm:ss.SSS", Locale.US).format(new Date(testTime)), testUUID));
                }
                
                downBitPerSec.set(Math.round(result.getDownloadSpeedBitPerSec()));
                upBitPerSec.set(Math.round(result.getUploadSpeedBitPerSec()));
                
                log("end.");
                setStatus(TestStatus.SPEEDTEST_END);
                
            	if (trafficService != null) {
            		txBytes = trafficService.getTotalTxBytes() - txBytes;
            		rxBytes = trafficService.getTotalRxBytes() - rxBytes;
            		result.setTotalTrafficMeasurement(new TestMeasurement(rxBytes, txBytes, timeStampStart, System.nanoTime()));
            		result.setMeasurementMap(measurementMap);
            	}

                
                return result;
            }
            catch (final ExecutionException e)
            {
                log(e);
                abortTest(true);
                return null;
            }
            catch (final InterruptedException e)
            {
                log("RMBTClient interrupted!");
                abortTest(false);
                throw e;
            }
        }
        else {
            setStatus(TestStatus.SPEEDTEST_END);
            
        	return null;
        }
    }

    void performVoipTest() {
        VoipTest voipTest;

        if (!aborted.get()) {
            try {
                QoSResultCollector voipResult;
                //Implementation for Jitter and Packet loss
                TestSettings qosTestSettings = qosSettings;

                boolean onlyVoipTest = true;
                //noinspection ConstantConditions
                voipTest = new JitterTest(this, qosTestSettings, onlyVoipTest);

                //voipReference.set(voipTest);
                setStatus(TestStatus.PACKET_LOSS_AND_JITTER);
                testJitterStatus = TestJitterStatus.RUNNING;

                voipResult = voipTest.call();

                List<QoSTestResult> voipTestRsults = voipResult.getResults();
                if ((voipTestRsults != null) && (!voipTestRsults.isEmpty())) {
                    QoSTestResult qoSTestResult = voipTestRsults.get(0);
                    HashMap<String, Object> resultMap = qoSTestResult.getResultMap();


                   result.voipResult = qoSTestResult;
                   result.jitterStatus = (JitterResult) resultMap.get(RESULT_STATUS);

                   if(result.jitterStatus == JitterResult.OK) testJitterStatus = TestJitterStatus.FINISH;
                   else testJitterStatus = TestJitterStatus.FAIL;


                    final String prefix_out = RESULT_VOIP_PREFIX + RESULT_OUTGOING_PREFIX;
                    final String prefix_in = RESULT_VOIP_PREFIX + RESULT_INCOMING_PREFIX;

                    Long meanJitterOut = (Long) resultMap.get(prefix_out + RESULT_MEAN_JITTER);
                    Long meanJitterIn = (Long) resultMap.get(prefix_in + RESULT_MEAN_JITTER);
                    Long maxJitterIn = (Long) resultMap.get(prefix_in + RESULT_MAX_JITTER);
                    Long maxJitterOut = (Long) resultMap.get(prefix_out + RESULT_MAX_JITTER);

                    if (meanJitterIn != null) result.downJitterMedian.set(meanJitterIn);
                    if (maxJitterIn != null) result.downJitterMax.set(maxJitterIn);
                    if (meanJitterOut != null) result.upJitterMedian.set(meanJitterOut);
                    if (maxJitterOut != null) result.upJitterMax.set(maxJitterOut);

                    if ((meanJitterIn != null) && (meanJitterOut != null)) {
                        Long meanJitter = (meanJitterIn + meanJitterOut) / 2;
                        this.jitter.set(meanJitter);
                    }

                    long callDuration = (Long) resultMap.get(VoipTask.RESULT_CALL_DURATION);
                    long delay = (Long) resultMap.get(VoipTask.RESULT_DELAY);
                    int outPacketsNumber = (Integer) resultMap.get(prefix_out + VoipTask.RESULT_NUM_PACKETS);
                    int inPacketsNumber = (Integer) resultMap.get(prefix_in + VoipTask.RESULT_NUM_PACKETS);

                    int total = ((int) callDuration / (int) delay);

                    int packetLossDown = (int) (100f * ((float) (total - inPacketsNumber) / (float) total));
                    int packetLossUp = (int) (100f * ((float) (total - outPacketsNumber) / (float) total));


                    result.downPackets.set(inPacketsNumber);
                    result.upPackets.set(outPacketsNumber);
                    result.downPacketsTotal.set(total);
                    result.upPacketsTotal.set(total);


                    this.packetLossDown.set(packetLossDown);
                    this.packetLossUp.set(packetLossUp);

                    System.out.println("JITTER: " + jitter + ", PL_DOWN: " + packetLossDown + ", PL_UP: " + packetLossUp);

                }

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("JITTER ERROR " + e.getMessage());
                testJitterStatus = TestJitterStatus.FAIL;
                log(e);
            }
        }
    }
    
    public void testNoSignal() {
    	result = new TotalTestResult();
    	
    	result.totalDownBytes 	= 0;
        result.totalUpBytes 	= 0;
        
        result.ping_shortest 	= -1;
        
        result.speed_download 	= 0;
        result.speed_upload 	= 0;
    }
    
    public boolean abortTest(final boolean error)
    {
        System.out.println("RMBTClient stopTest");
        
        if (error)
            setErrorStatus();
        else
            setStatus(TestStatus.ABORTED);
        aborted.set(true);
        
        if (testThreadPool != null)
            testThreadPool.shutdownNow();
        
        return true;
    }

    public void setAborted(boolean abort){
        aborted.set(abort);
    }
    
    public void shutdown()
    {
        System.out.println("Shutting down RMBT thread pool...");
        if (testThreadPool != null)
            testThreadPool.shutdownNow();
        
        System.out.println("Shutdown finished.");
    }
    
    @Override
    protected void finalize() throws Throwable
    {
        super.finalize();
        if (testThreadPool != null)
            testThreadPool.shutdownNow();
    }
    
    public SSLSocketFactory getSslSocketFactory()
    {
        return sslSocketFactory;
    }
    
    public void setOutputCallback(final RMBTOutputCallback outputCallback)
    {
        this.outputCallback = outputCallback;
    }
    
    private void resetSpeed()
    {
        lastCounter = 0;
    }
    
    private float getTotalSpeed()
    {
        long sumTrans = 0;
        long maxTime = 0;
        
        final CurrentSpeed currentSpeed = new CurrentSpeed();
        
        for (int i = 0; i < params.getNumThreads(); i++)
            if (testTasks[i] != null)
            {
                testTasks[i].getCurrentSpeed(currentSpeed);
                
                if (currentSpeed.time > maxTime)
                    maxTime = currentSpeed.time;
                sumTrans += currentSpeed.trans;
            }
        
        return maxTime == 0f ? 0f : (float) sumTrans / (float) maxTime * 1e9f * 8.0f;
    }
    
    final Map<Integer, List<SpeedItem>> speedMap = new HashMap<Integer, List<SpeedItem>>();
    
    private float getAvgSpeed()
    {
        long sumDiffTrans = 0;
        long maxDiffTime = 0;
        
        final CurrentSpeed currentSpeed = new CurrentSpeed();
        
        final int currentIndex = lastCounter % KEEP_LAST_ENTRIES;
        int diffReferenceIndex = (lastCounter - KEEP_LAST_ENTRIES + 1) % KEEP_LAST_ENTRIES;
        if (diffReferenceIndex < 0)
            diffReferenceIndex = 0;
        
        lastCounter++;
        
        for (int i = 0; i < params.getNumThreads(); i++)
            if (testTasks[i] != null)
            {
                testTasks[i].getCurrentSpeed(currentSpeed);
                
                lastTime[i][currentIndex] = currentSpeed.time;
                lastTransfer[i][currentIndex] = currentSpeed.trans;
                
//                System.out.println("T" + i + ": " + currentSpeed);
                
                List<SpeedItem> speedList = speedMap.get(i);
                if (speedList == null) {
                	speedList = new ArrayList<SpeedItem>();
                	speedMap.put(i, speedList);
                }
                
                speedList.add(new SpeedItem(false, i, currentSpeed.time, currentSpeed.trans));
                
                final long diffTime = currentSpeed.time - lastTime[i][diffReferenceIndex];
                final long diffTrans = currentSpeed.trans - lastTransfer[i][diffReferenceIndex];
                
                if (diffTime > maxDiffTime)
                    maxDiffTime = diffTime;
                sumDiffTrans += diffTrans;
            }
                
        //TotalTestResult totalResult = TotalTestResult.calculateAndGet(lastTransfer, lastTime, false);
        //TotalTestResult totalResult = TotalTestResult.calculateAndGet(speedMap);
        
        final float speedAvg = maxDiffTime == 0f ? 0f : (float) sumDiffTrans / (float) maxDiffTime * 1e9f * 8.0f;
        //final float speedAvg = (float)totalResult.speed_download * 1e3f;
        
//        System.out.println("calculate: bytes=" + totalResult.bytes_download + " speed=" + (totalResult.speed_download * 1e3) 
//        		+ " nsec=" + totalResult.nsec_download + ", simple: diff=" + sumDiffTrans + " avg=" + speedAvg);
        
        return speedAvg;
    }
    
    public IntermediateResult getIntermediateResult(IntermediateResult iResult)
    {
        if (iResult == null)
            iResult = new IntermediateResult();
        iResult.status = testStatus.get();
        iResult.remainingWait = 0;
        final long diffTime = System.nanoTime() - statusChangeTime.get();
        switch (iResult.status)
        {
        case WAIT:
            iResult.progress = 0;
            iResult.remainingWait = params.getStartTime() - System.currentTimeMillis();
            break;
        
        case INIT:
            iResult.progress = (float) diffTime / durationInitNano;
            break;
        
        case PING:
            iResult.progress = getPingProgress();
            break;
        
        case DOWN:
            iResult.progress = (float) diffTime / durationDownNano;
            downBitPerSec.set(Math.round(getAvgSpeed()));
            break;
        
        case INIT_UP:
            iResult.progress = 0;
            break;
        
        case UP:
            iResult.progress = (float) diffTime / durationUpNano;
            upBitPerSec.set(Math.round(getAvgSpeed()));
            break;
        
        case SPEEDTEST_END:
            iResult.progress = 1;
            break;
        
        case ERROR:
        case ABORTED:
            iResult.progress = 0;
            break;
        default:
        	break;
        }
        
        if (iResult.progress > 1)
            iResult.progress = 1;
        
        iResult.initNano = initNano.get();
        iResult.pingNano = pingNano.get();
        iResult.downBitPerSec = downBitPerSec.get();
        iResult.upBitPerSec = upBitPerSec.get();
        iResult.testJitterStatus = testJitterStatus;

        if(result != null) {
            if (result.downJitterMedian.get() != -1) {
                iResult.jitterMeanDown = result.downJitterMedian.get();
            }

            if (result.upJitterMedian.get() != -1) {
                iResult.jitterMeanUp = result.upJitterMedian.get();
            }

            iResult.totalPacketDown = result.downPacketsTotal.get();
            iResult.totalPacketUp = result.upPacketsTotal.get();
            iResult.numPacketDown = result.downPackets.get();
            iResult.numPacketUp = result.upPackets.get();
        }

        iResult.setLogValues();
        
        return iResult;
    }

    public TotalTestResult getResult(){
        return result;
    }
    
    public TestStatus getStatus()
    {
        return testStatus.get();
    }
    
    public TestStatus getStatusBeforeError()
    {
        return statusBeforeError.get();
    }
    
    public void setStatus(final TestStatus status)
    {
        testStatus.set(status);
        statusChangeTime.set(System.nanoTime());
        if (status == TestStatus.INIT_UP)
        {
            // DOWN is finished
            downBitPerSec.set(Math.round(getTotalSpeed()));
            resetSpeed();
        }
    }
        
    public void startTrafficService(final int threadId, final TestStatus status) {
    	if (trafficService != null) {
    		//a concurrent map is needed in case multiple threads want to start the traffic service
    		//only the first thread should be able to start the service
    		TestMeasurement tm = new TestMeasurement(status.toString(), trafficService);
        	TestMeasurement previousTm = measurementMap.putIfAbsent(status, tm);
        	if (previousTm == null) {
        		tm.start(threadId);
        	}
    	}
    }
    
    public void stopTrafficMeasurement(final int threadId, final TestStatus status) {
    	final TestMeasurement testMeasurement = measurementMap.get(status);
    	if (testMeasurement != null)
    	    testMeasurement.stop(threadId);
    }
    
    public String getErrorMsg()
    {
        return errorMsg;
    }
    
    public ResultData sendResult(final JSONObject additionalValues) {
        ResultData resultData = new ResultData();

        if (controlConnection != null) {
    		final String errorMsg = controlConnection.sendTestResult(result, additionalValues);
            if (errorMsg != null) {
                setErrorStatus();
                log("Error sending Result...");
                log(errorMsg);
                resultData.setErrMsg(errorMsg);
            } else {
                resultData.setStatus(ResultData.ResultStatus.OK);
            }

            return resultData;
        }
    	resultData.setErrMsg("Control connection is null");
    	return resultData;
    }
        
    public ResultData sendQoSResult(final QoSResultCollector qosResult) {
        ResultData resultData = new ResultData();

        if (controlConnection != null) {
    		final String errorMsg = controlConnection.sendQoSResult(result, qosResult.toJson());
            if (errorMsg != null)
            {
                setErrorStatus();
                log("Error sending QoS Result...");
                log(errorMsg);
                resultData.setErrMsg(errorMsg);

            }else{
                resultData.setStatus(ResultData.ResultStatus.OK);
            }

            return resultData;
    	}

        resultData.setErrMsg("Control connection is null");
        return  resultData;
    }

    private void setErrorStatus()
    {
        final TestStatus lastStatus = testStatus.getAndSet(TestStatus.ERROR);
        if (lastStatus != TestStatus.ERROR)
            statusBeforeError.set(lastStatus);
    }
    
    void log(final CharSequence text)
    {
        if (outputToStdout)
            System.out.println(text);
        if (outputCallback != null)
            outputCallback.log(text);
    }
    
    void log(final Exception e)
    {
        if (outputToStdout)
            e.printStackTrace(System.out);
        if (outputCallback != null)
            outputCallback.log(String.format(Locale.US, "Error: %s", e.getMessage()));
    }
    
    void setInitTime(final long initDuration) {
    	initNano.set(initDuration);
    }
    
    long getInitTime() {
    	return initNano.get();
    }
    
    void setPing(final long shortestPing)
    {
        pingNano.set(shortestPing);
    }
    
    void updatePingStatus(final long tsStart, int pingsDone, long tsLastPing)
    {
        pingTsStart.set(tsStart);
        pingNumDome.set(pingsDone);
        pingTsLastPing.set(tsLastPing);
    }
    
    private float getPingProgress()
    {
        final long start = pingTsStart.get();
        
        if (start == -1) // not yet started
            return 0;
        
        final int numDone = pingNumDome.get();
        final long lastPing = pingTsLastPing.get();
        final long now = System.nanoTime();
        
        final int numPings = params.getNumPings();
        
        if (numPings <= 0) // nothing to do
            return 1;
        
        final float factorPerPing = (float)1 / (float)numPings;
        final float base = factorPerPing * numDone;
        
        final long approxTimePerPing;
        if (numDone == 0 || lastPing == -1) // during first ping, assume 100ms
            approxTimePerPing = 100000000;
        else
            approxTimePerPing = (lastPing - start) / numDone;
        
        float factorLastPing = (float)(now - lastPing) / (float)approxTimePerPing;
        if (factorLastPing < 0)
            factorLastPing = 0;
        if (factorLastPing > 1)
            factorLastPing = 1;
        //TODO MathUtils.clamp(factorLastPing, 0, 1);
        
        final float result = base + factorLastPing * factorPerPing;
        if (result < 0)
            return 0;
        if (result > 1)
            return 1;
        //TODO MathUtils.clamp(result, 0, 1);
        
//        System.out.println("atpp: " + approxTimePerPing + "; flp:" + factorLastPing+ "; res:" +result);
        return result;
    }
    
    public String getPublicIP()
    {
        if (controlConnection == null)
            return null;
        return controlConnection.getRemoteIp();
    }
    
    public String getServerName()
    {
        if (controlConnection == null)
            return null;
        return controlConnection.getServerName();
    }
    
    public String getProvider()
    {
        if (controlConnection == null)
            return null;
        return controlConnection.getProvider();
    }
    
    public String getTestUuid()
    {
        if (controlConnection == null)
            return null;
        return controlConnection.getTestUuid();
    }
    
    public ControlServerConnection getControlConnection()
    {
        return controlConnection;
    }

    public File getCacheDir() {
        return cacheDir;
    }

    /**
     * 
     * @return
     */
	public List<TaskDesc> getTaskDescList() {
		return taskDescList;
	}

   public boolean isAborted(){
	    return aborted.get();
   }
}
