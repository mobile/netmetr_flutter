/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.netmetrflutter;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import cz.nic.netmetrflutter.test.RMBTLoopService;
import cz.nic.netmetrflutter.util.InformationCollector;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.EventChannel;
import io.flutter.view.FlutterView;

import static cz.nic.netmetrflutter.util.InformationCollector.ON_NONE;

public class Loop implements RMBTLoopService.RMBTLoopServiceListener {
    private static final String LOOP_EVENT = "nic.cz.netmetrflutter/loopEvent";
    private static final String START_STAT = "loop start";
    private static final String STOP_STAT = "loop stop";

    private MainActivity activity;

    private RMBTLoopService loopService;
    private EventChannel.EventSink eventSink;
    private Runnable exit;
    private InformationCollector informationCollector;

    private final ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            RMBTLoopService.RMBTLoopBinder binder = (RMBTLoopService.RMBTLoopBinder) service;
            loopService = binder.getService();
            loopService.setRmbtLoopServiceListener(Loop.this);

            activity.unbindService(this);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            loopService.setRmbtLoopServiceListener(null);
            loopService = null;
        }
    };

    Loop(MainActivity activity, InformationCollector informationCollector, FlutterEngine flutterEngine) {
        this.activity = activity;
        this.informationCollector = informationCollector;
        initReceiver(flutterEngine);
    }

    private void initReceiver(FlutterEngine flutterEngine){
        new EventChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), LOOP_EVENT).setStreamHandler(new EventChannel.StreamHandler() {
            @Override
            public void onListen(Object o, EventChannel.EventSink eventSink) {
                Loop.this.eventSink = eventSink;
            }

            @Override
            public void onCancel(Object o) {
                Loop.this.eventSink = null;
            }
        });
    }

    public void startLoop(String uuid, String controlServer, int port, boolean useSSL, boolean qosSSL, long minDelay, long maxDelay, float maxMovement, int maxTests, boolean onlySignal, boolean runNDT, boolean runJitter, boolean runQoS, Runnable exit){
        this.exit = exit;
        Intent service = new Intent(activity, RMBTLoopService.class);

        service.putExtra("uuid", uuid);
        service.putExtra("controlServer", controlServer);
        service.putExtra("port", port);
        service.putExtra("useSSL", useSSL);
        service.putExtra("qosSSL", qosSSL);
        service.putExtra("minDelay", minDelay);
        service.putExtra("maxDelay", maxDelay);
        service.putExtra("maxMovement", maxMovement);
        service.putExtra("maxTests", maxTests);
        service.putExtra("onlySignal", onlySignal);
        service.putExtra("jitter", runJitter);
        service.putExtra("qos", runQoS);
        service.putExtra("runNDT", runNDT);
        service.putExtra("override", informationCollector != null ? informationCollector.getLastOverrideNetwork() : ON_NONE);

        activity.bindService(service, serviceConnection, Context.BIND_AUTO_CREATE);
        activity.startService(service);
    }

///////////////// SERVICE LOOP LISTENER

    @Override
    public void onServiceStarted() {
        if(eventSink != null){
            //event
            eventSink.success(START_STAT);
        }
    }

    @Override
    public void onServiceStopped() {
        if(exit != null)
            exit.run();

        if(eventSink != null){
            //event
            eventSink.success(STOP_STAT);
        }
    }

}


