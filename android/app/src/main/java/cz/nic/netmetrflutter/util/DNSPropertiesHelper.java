/*******************************************************************************
 * Copyright 2015 SPECURE GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package cz.nic.netmetrflutter.util;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.RouteInfo;
import android.os.Build;
import android.util.Log;

import java.lang.reflect.Method;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;
import static android.content.Context.CONNECTIVITY_SERVICE;

public class DNSPropertiesHelper {

    public static String findDNS(Context context){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            try {

                ArrayList<String> priorityServersArrayList  = new ArrayList<>();
                ArrayList<String> serversArrayList          = new ArrayList<>();

                ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
                if (connectivityManager != null) {
                    for (Network network : connectivityManager.getAllNetworks()) {

                        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(network);
                        if (networkInfo.isConnected()) {
                            LinkProperties linkProperties    = connectivityManager.getLinkProperties(network);
                            List<InetAddress> dnsServersList = linkProperties.getDnsServers();
                            if (linkPropertiesHasDefaultRoute(linkProperties)) {
                                for (InetAddress element: dnsServersList) {
                                    String dnsHost = element.getHostAddress();
                                    priorityServersArrayList.add(dnsHost);
                                }
                            } else {
                                for (InetAddress element: dnsServersList) {
                                    String dnsHost = element.getHostAddress();
                                    serversArrayList.add(dnsHost);
                                }
                            }
                        }
                    }
                }

                // Append secondary arrays only if priority is empty
                if (priorityServersArrayList.isEmpty()) {
                    priorityServersArrayList.addAll(serversArrayList);
                }

                // Stop here if we have at least one DNS server
                if (priorityServersArrayList.size() > 0) {
                    String findDns = priorityServersArrayList.get(0);
                    System.out.println("FIND DNS: " + findDns);
                    return findDns;
                }

            } catch (Exception ex) {
                Log.d(TAG, "Exception detecting DNS servers using ConnectivityManager method", ex);
                return null;
            }

        }else {

            ArrayList lserver = new ArrayList();
            try {
                @SuppressLint("PrivateApi") Class SystemProperties = Class.forName("android.os.SystemProperties");
                Method method = SystemProperties.getMethod("get", String.class);
                String[] netdns = new String[]{"net.dns1", "net.dns2", "net.dns3", "net.dns4"};

                for (int i = 0; i < netdns.length; ++i) {
                    Object[] args = new Object[]{netdns[i]};
                    String v = (String) method.invoke((Object) null, args);
                    if (v != null && (v.matches("^\\d+(\\.\\d+){3}$") || v.matches("^[0-9a-f]+(:[0-9a-f]*)+:[0-9a-f]+$")) && !lserver.contains(v)) {
                        lserver.add(v);
                    }
                }
            } catch (Exception var11) {
                return  null;
            }

            if (lserver.size() > 0) {
                String findDNS = (String) lserver.toArray(new String[0])[0];
                System.out.print("FIND DNS (SDK <26):" + findDNS);
                return findDNS;
            }
        }

        return null;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static boolean linkPropertiesHasDefaultRoute(LinkProperties linkProperties) {

        for (RouteInfo route : linkProperties.getRoutes()) {
            if (route.isDefaultRoute()) {
                return true;
            }
        }
        return false;

    }
}
