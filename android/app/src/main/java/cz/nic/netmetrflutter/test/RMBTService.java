/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.netmetrflutter.test;

import android.annotation.SuppressLint;
import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Icon;
import android.location.Location;
import android.net.wifi.WifiManager;
import android.os.*;
import android.util.Log;

import androidx.annotation.RequiresApi;
import at.alladin.rmbt.client.helper.NdtStatus;
import cz.nic.netmetrflutter.MainActivity;
import cz.nic.netmetrflutter.R;
import cz.nic.netmetrflutter.util.NotificationIDs;

import java.util.List;
import java.util.Map;

import at.alladin.rmbt.client.QualityOfServiceTest;
import at.alladin.rmbt.client.TotalTestResult;
import at.alladin.rmbt.client.helper.IntermediateResult;
import at.alladin.rmbt.client.v2.task.result.QoSResultCollector;
import at.alladin.rmbt.client.v2.task.result.QoSTestResultEnum;
import cz.nic.netmetrflutter.util.InformationCollector;
import cz.nic.netmetrflutter.util.support.NetworkData;

public class RMBTService extends Service implements EndTaskListener {
    private static final String INTENT_EXTRA_KEY_LOOP_MODE = "loopMode";
    private static final String CHANNEL_ID = "netmetr_notification";


    public enum ServiceStatus{
        NOT_CREATE, BIND, START, FINISH, DESTROY
    }


    public static String ACTION_START_TEST = "at.alladin.rmbt.android.startTest";
    public static String ACTION_ABORT_TEST = "at.alladin.rmbt.android.abortTest";
    static String ACTION_LOOP_TEST = "at.alladin.rmbt.android.loopTest";

    private static final String SERVICE_TAG = "RMBT_SERVICE";

    private final IBinder localRMBTBinder = new RMBTBinder();

    private ServiceStatus status = ServiceStatus.NOT_CREATE;
    private RMBTTask rmbtTask;
    private TotalTestResult totalTestResult;
    private Handler handler;

    private boolean bound = false;


    //LOOP
    static String BROADCAST_TEST_FINISHED = "at.alladin.rmbt.android.test.RMBTService.testFinished";

    private boolean loopMode;
    private boolean ndtTest;

    private static WifiManager wifiManager;
    private static WifiManager.WifiLock wifiLock;
    private static PowerManager.WakeLock wakeLock;
    private static long DEADMAN_TIME = 120 * 1000;

    private ControlServerListener serverListener;

    private final Runnable deadman = new Runnable()
    {
        @Override
        public void run()
        {
            stopTest();
        }
    };


    @Override
    public void sendResult(TotalTestResult testResult) {
        this.totalTestResult = testResult;
        status = ServiceStatus.FINISH;
    }

    @Override
    public void taskEnded() {
        unlock();
        removeNotification();
        handler.removeCallbacks(deadman);
        Intent finishIntent = new Intent(BROADCAST_TEST_FINISHED);
        finishIntent.putExtra(INTENT_EXTRA_KEY_LOOP_MODE, loopMode);
        sendBroadcast(finishIntent);
        stopSelf();

        Log.i("RMBTService", "stopped!");
    }

    public boolean isTestRunning()
    {
        return rmbtTask != null && rmbtTask.isRunning();
    }


    public class RMBTBinder extends Binder{
        public RMBTService getService(){
            return RMBTService.this;
        }
    }

    @SuppressLint("InvalidWakeLockTag")
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(SERVICE_TAG,"create");

        handler = new Handler();

        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiLock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, "RMBTWifiLock");
        wakeLock = ((PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE)).newWakeLock(
                PowerManager.PARTIAL_WAKE_LOCK, "RMBTWakeLock");

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(SERVICE_TAG, "start command");


        String action = null;
        if (intent != null)
            action = intent.getAction();

        Log.i(SERVICE_TAG, "onStartCommand; action="+action);

        if(action != null) {

            if (ACTION_ABORT_TEST.equals(action))
            {
                Log.i(SERVICE_TAG, "ACTION_ABORT_TEST received");
                stopTest();
                return START_NOT_STICKY;
            }

            if (action.equals(ACTION_START_TEST) || action.equals(ACTION_LOOP_TEST)) {
                loopMode =  intent.getBooleanExtra("loopMode", false);
                ndtTest = intent.getBooleanExtra("runNDT", false);

                if (rmbtTask != null && rmbtTask.isRunning())
                {
                    if (ACTION_LOOP_TEST.equals(action)) // do not cancel test if running in loop mode
                        return START_STICKY;
                    rmbtTask.cancel(); // otherwise cancel
                }

                lock();
                rmbtTask = new RMBTTask(getApplicationContext(),
                        loopMode,
                        new NetMeterSettings(
                                intent.getStringExtra("uuid"),
                                intent.getStringExtra("controlServer"),
                                intent.getIntExtra("port", 443),
                                intent.getBooleanExtra("useSSL", false),
                                intent.getBooleanExtra("qosSSL", false),
                                intent.getBooleanExtra("onlySignal", false),
                                intent.getBooleanExtra("jitter", true),
                                intent.getBooleanExtra("qos", true),
                                ndtTest),
                        intent.getIntExtra("override", 0),
                        this
                        );

                rmbtTask.setServerListener(serverListener);
                rmbtTask.setEndTaskListener(this);
                rmbtTask.execute(handler);
                Log.d(SERVICE_TAG, "RMBTTest started");

                handler.postDelayed(addNotificationRunnable, 200);
                handler.postDelayed(deadman, DEADMAN_TIME);

                status = ServiceStatus.START;
                return START_STICKY;


            } else if (action.equals(ACTION_ABORT_TEST)) {
                return START_NOT_STICKY;
            }
        }

        return START_NOT_STICKY;
    }

    public void continueTest(){
        if(rmbtTask != null){
            rmbtTask.setAborted(false);
            rmbtTask.continueNextPart();
        }
    }

    public boolean isTestAbort(){
        if(rmbtTask != null){
            return rmbtTask.isAbort();
        }

        return false;
    }

    public void stopTest()
    {

        if (rmbtTask != null)
        {
            Log.d(SERVICE_TAG, "RMBTTest stopped");
            rmbtTask.cancel();
            taskEnded();
        }
    }

    private void addNotificationIfTestRunning()
    {
        if (isTestRunning() && ! bound)
        {
            final Resources res = getResources();

            int contentFlag = 0;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
                contentFlag = PendingIntent.FLAG_MUTABLE;
            }

            final PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(
                    getApplicationContext(), MainActivity.class), contentFlag);

            final Notification.Builder builder;


            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                final Intent stopIntentReceive = new Intent(this, NotificationReceiver.class);
                stopIntentReceive.setAction(NotificationReceiver.ABORT_ACTION);
                stopIntentReceive.putExtra("loop", false);

                int stopIntentFlag = PendingIntent.FLAG_UPDATE_CURRENT;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
                    stopIntentFlag = PendingIntent.FLAG_MUTABLE;
                }

                final PendingIntent stopIntent = PendingIntent.getBroadcast(getApplicationContext(), NotificationReceiver.REQ_CODE, stopIntentReceive, stopIntentFlag);

                createNotificationChannel();

                builder = new Notification.Builder(this, CHANNEL_ID)
                        .setSmallIcon(R.drawable.stat_icon_test)
                        .setContentTitle(res.getText(R.string.test_notification_title))
                        .setContentText(res.getText(R.string.test_notification_text))
                        .setTicker(res.getText(R.string.test_notification_ticker))
                        .setChannelId(CHANNEL_ID)
                        .addAction(new Notification.Action.Builder(Icon.createWithResource(getApplicationContext(), R.drawable.stat_icon_test), res.getString(R.string.action_abort), stopIntent).build())
                        .setContentIntent(contentIntent)
                        .setAutoCancel(true);

            }else {
                builder = new Notification.Builder(this)
                        .setSmallIcon(R.drawable.stat_icon_test)
                        .setContentTitle(res.getText(R.string.test_notification_title))
                        .setContentText(res.getText(R.string.test_notification_text))
                        .setTicker(res.getText(R.string.test_notification_ticker))
                        .setContentIntent(contentIntent);
            }

            startForeground(NotificationIDs.TEST_RUNNING, builder.build());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createNotificationChannel(){
        NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "Netmetr background service", NotificationManager.IMPORTANCE_HIGH);
        channel.setLightColor(Color.WHITE);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager !=  null;
        manager.createNotificationChannel(channel);
    }

    private void removeNotification()
    {
        handler.removeCallbacks(addNotificationRunnable);
        stopForeground(true);
    }

    private final Runnable addNotificationRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            addNotificationIfTestRunning();
        }
    };

    public IntermediateResult getIntermediateResult(final IntermediateResult result)
    {
        return rmbtTask != null ? rmbtTask.getIntermediateResult(result) : null;
    }

    public TotalTestResult getTotalResult(){
        return rmbtTask != null ? rmbtTask.getTotalResult() : null;
    }

    public NetMeterSettings getSettings(){
        return  rmbtTask != null ? rmbtTask.getSettings() : null;
    }

    public QoSResultCollector getQosResult(){
        return rmbtTask != null ? rmbtTask.getQosResult() : null;
    }

    public QualityOfServiceTest getQosTest(){
        return rmbtTask != null ? rmbtTask.getQoSTest() : null;
    }

    public Map<QoSTestResultEnum, QualityOfServiceTest.Counter> getQoSGroupCounterMap() {
        if (rmbtTask != null) {
            return rmbtTask.getQoSGroupCounterMap();
        }
        else {
            return null;
        }
    }

    public void lock()
    {
        try
        {
            if (!wakeLock.isHeld())
                wakeLock.acquire();
            if (!wifiLock.isHeld())
                wifiLock.acquire();

            Log.d(SERVICE_TAG, "Lock");
        }
        catch (final Exception e)
        {
            Log.e(SERVICE_TAG, "Error getting Lock: " + e.getMessage());
        }
    }

    public static void unlock()
    {
        if (wakeLock != null && wakeLock.isHeld())
            wakeLock.release();
        if (wifiLock != null && wifiLock.isHeld())
            wifiLock.release();

        Log.d(SERVICE_TAG, "Unlock");
    }


    @Override
    public void onDestroy() {
        Log.d(SERVICE_TAG,"destroy");
        super.onDestroy();

        status = ServiceStatus.DESTROY;

        if (rmbtTask != null)
        {
            Log.d(SERVICE_TAG, "RMBTTest stopped by onDestroy");
            rmbtTask.cancel();
        }

        removeNotification();
        unlock();
        if(rmbtTask != null){
            rmbtTask.cancel();
            rmbtTask = null;
        }

        handler.removeCallbacks(addNotificationRunnable);
        handler.removeCallbacks(deadman);

    }

    @Override
    public IBinder onBind(Intent intent) {
        status = ServiceStatus.BIND;
        bound = true;
        return localRMBTBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        bound = false;
        handler.postDelayed(addNotificationRunnable, 200);
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        bound = true;
        removeNotification();
    }

    public String getTestUUID(){
        return rmbtTask.getTestUuid();
    }

    public ClientError getClientError(){
        return rmbtTask.getClientError();
    }


    public ServiceStatus getStatus() {
        return status;
    }

    public String getOperatorName()
    {
        if (rmbtTask != null)
            return rmbtTask.getOperatorName();
        else
            return null;
    }

    public int getNetworkType()
    {
        if (rmbtTask != null)
            return rmbtTask.getNetworkType();
        else
            return 0;
    }

    public NetworkData getLastNetworkData() {
        return rmbtTask != null ? rmbtTask.getLastNetworkData() : null;
    }

    public Location getLocation()
    {
        if (rmbtTask != null)
            return rmbtTask.getLocation();
        else
            return null;
    }

    public String getServerName()
    {
        if (rmbtTask != null)
            return rmbtTask.getServerName();
        else
            return null;
    }



    public String getIP()
    {
        if (rmbtTask != null)
            return rmbtTask.getIP();
        else
            return null;
    }

    public Integer getSignal()
    {
        if (rmbtTask != null)
            return rmbtTask.getSignal();
        else
            return null;
    }

    public int getSignalType()
    {
        if (rmbtTask != null)
            return rmbtTask.getSignalType();
        else
            return InformationCollector.SINGAL_TYPE_NO_SIGNAL;
    }

    public List<InformationCollector.SignalItem> getSignals(){
        return rmbtTask != null ? rmbtTask.getSignals() : rmbtTask.getSignals();
     }

    public boolean isNdtTest() {
        return ndtTest;
    }

    public NdtStatus getNdtStatus(){
        return rmbtTask.getNdtStatus();
    }

    public void setServerListener(ControlServerListener serverListener) {
        this.serverListener = serverListener;
    }
}
