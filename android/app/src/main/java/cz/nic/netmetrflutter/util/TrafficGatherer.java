/*******************************************************************************
 * Copyright 2015 SPECURE GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package cz.nic.netmetrflutter.util;

import android.net.TrafficStats;

public class TrafficGatherer {

    private long prevTxBytes;
    private long prevRxBytes;
    private long txBytes;
    private long rxBytes;
    private long rxTraffic;
    private long txTraffic;
    private long nsElapsed;
    private long nsTimestamp;

    public TrafficGatherer() {
        prevTxBytes = TrafficStats.getTotalTxBytes();
        prevRxBytes = TrafficStats.getTotalRxBytes();
        txBytes = prevTxBytes;
        rxBytes = prevRxBytes;
        nsTimestamp = System.nanoTime();
    }

    public void run() {
        this.prevRxBytes = this.rxBytes;
        this.prevTxBytes = this.txBytes;
        this.rxBytes = TrafficStats.getTotalRxBytes();
        this.txBytes = TrafficStats.getTotalTxBytes();
        final long timestamp = System.nanoTime();
        this.nsElapsed = timestamp - this.nsTimestamp;
        this.nsTimestamp = timestamp;
        //double perSecMultiplier = 1000d / msInterval;
        this.txTraffic = (nsElapsed > 0 ? (long)((double)(txBytes - prevTxBytes) / (double)(nsElapsed / 1000000000D)) : 0);
        this.rxTraffic = (nsElapsed > 0 ? (long)((double)(rxBytes - prevRxBytes) / (double)(nsElapsed / 1000000000D)) : 0);
    }

    public long getTxRate() {
        return txTraffic;
    }

    public long getRxRate() {
        return rxTraffic;
    }

}
