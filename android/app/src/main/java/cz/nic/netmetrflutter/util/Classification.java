/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.netmetrflutter.util;

public class Classification {
    public static final int[] THRESHOLD_UP = {1000, 500};
    public static final int[] THRESHOLD_DOWN = {2000, 1000};
    public static final int[] THRESHOLD_PING = {25000000, 75000000};

    public static final int[] THRESHOLD_JITTER = {30_000_000, 60_000_000};
    public static final int[] THRESHOLD_PACKET_LOSS = {5000, 15000};


    public static final int[] THRESHOLD_MOBILE = {-85, -101};
    public static final int[] THRESHOLD_LTE = {-95, -111};
    public static final int[] THRESHOLD_WIFI = {-61, -76};

    public static int classify(int[] threshold, double value){
        boolean inverse = threshold[0] < threshold[1];
        if(!inverse){
            return value >= threshold[0] ? 3 : value >= threshold[1] ? 2 : 1;
        }else{
            return value <= threshold[0] ? 3: value <= threshold[1] ? 2 : 1;
        }
    }

    public static int classifySignal(ClassificationSignalType signalType, int value){
        return classify(thresholdBySignal(signalType), value);
    }

    private static int[] thresholdBySignal(ClassificationSignalType signalType){
        switch (signalType){
            case MOBILE: return THRESHOLD_MOBILE;
            case LTE: return THRESHOLD_LTE;
            default: return  THRESHOLD_WIFI;
        }
    }

    public enum ClassificationSignalType{
        MOBILE, LTE, WIFI
    }

}
