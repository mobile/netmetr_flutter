/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.netmetrflutter;

import android.Manifest;
import android.app.Dialog;
import android.content.*;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.location.Location;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import cz.nic.netmetrflutter.impl.CpuStatAndroidImpl;
import cz.nic.netmetrflutter.impl.MemInfoAndroidImpl;
import cz.nic.netmetrflutter.test.RMBTService;
import cz.nic.netmetrflutter.util.Config;
import cz.nic.netmetrflutter.util.InfoCollector;
import cz.nic.netmetrflutter.util.InformationCollector;
import cz.nic.netmetrflutter.util.LanguageAlphabetMapperUtil;

import cz.nic.netmetrflutter.util.net.NetworkListener;
import cz.nic.netmetrflutter.util.net.NetworkManager;
import cz.nic.netmetrflutter.util.net.NetworkManagerV21;
import io.flutter.embedding.engine.FlutterEngine;
import io.sentry.SentryEvent;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

import at.alladin.rmbt.client.helper.RevisionHelper;
import cz.nic.netmetrflutter.util.NetworkInfoCollector;
import cz.nic.netmetrflutter.util.NetworkUtil;
import cz.nic.netmetrflutter.util.TrafficGatherer;
import io.flutter.embedding.android.FlutterActivity;
import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.MethodChannel;

import at.alladin.rmbt.util.tools.MemInfo;
import io.sentry.Sentry;
import io.sentry.SentryClient;

import static android.Manifest.permission.READ_PHONE_STATE;
import static android.content.ContentValues.TAG;
import static cz.nic.netmetrflutter.util.InformationCollector.NOT_CONNECTED;
import static cz.nic.netmetrflutter.util.InformationCollector.ON_NONE;

public class MainActivity extends FlutterActivity implements ServiceConnection {
    private static final int LOCATION = 1;
    private static final int PHONE = 2;
    private static final int FIRST = 3;

    private static final String PLATFORM_NAME = "Android";
    private static final String STAT_CHANNEL = "nic.cz.netmetrflutter/statChannel";
    private static final String SERVICE_CHANNEL = "nic.cz.netmetrflutter/serviceChannel";
    private static final String CONFIG_CHANNEL = "nic.cz.netmetrflutter/config";
    private static final String SETTINGS_CHANNEL = "nic.cz.netmetrflutter/settings";
    private static final String NETSTAT_EVENT_CHANNEL = "nic.cz.netmetrflutter/netStat";
    private static final String LOOP_CHANNEL = "nic.cz.netmetrflutter/loopChannel";

    private static final String OLD_PREFERENCES = "oldSettingsImported";
    private static final String NEW_PREFERENCES = "FlutterSharedPreferences";
    private static final String FLUTTER_REFIX   = "flutter.";

    private InformationCollector informationCollector;

    public InformationCollector getInformationCollector() {
        return informationCollector;
    }

    private CpuStatAndroidImpl cpuStat;
    private MemInfo memInfo;
    private RMBTService rmbtService;

    private String versionName;
    private int versionCode = -1;

    private boolean rationaleDialogShowed = false;

    private Context context;

    private RunTask runTask;
    private Loop activeLoop;

    private TrafficGatherer trafficGatherer;

    private EventChannel.EventSink netEvent;
    private InfoCollector infoCollector = InfoCollector.getInstance();

    /*
        Network information
     */

    private String lastConnection;
    private NetworkListener networkListener;

    /*

     Network Listener

     */

    private final Runnable networkChanged = this::networkChanged;

    private final NetworkInfoCollector.OnNetworkInfoChangedListener onNetworkAddressChangedListener = new NetworkInfoCollector.OnNetworkInfoChangedListener() {

        @Override
        public void onChange(InfoFlagEnum infoFlag, Object newValue) {

        if (informationCollector != null) {
                    if (infoFlag == InfoFlagEnum.PRIVATE_IPV4_CHANGED || infoFlag == InfoFlagEnum.PRIVATE_IPV6_CHANGED) {
                        if (NetworkInfoCollector.getInstance().hasConnectionFromAndroidApi()) {
                            infoCollector.dispatchInfoChangedEvent(InfoCollector.InfoCollectorType.SIGNAL, 0, infoCollector.getSignal());
                            infoCollector.dispatchInfoChangedEvent(InfoCollector.InfoCollectorType.SIGNAL_RSRQ, 0, infoCollector.getSignalRsrq());
                        }
                        else {
                            infoCollector.setSignal(Integer.MIN_VALUE);
                            infoCollector.setSignalRsrq(null);
                        }

                        ipChange(NetworkInfoCollector.getInstance());
                    }
                }
        }

    };

    private final InformationCollector.NetworkChannelListener networkChannelListener = this::postNetworkChangeEvent;

    private void ipChange(NetworkInfoCollector  collector){

        if(collector != null && collector.getActiveNetworkInfo() !=  null && !collector.hasIpFromControlServer() && collector.isConnected()) {
            collector.checkIp(new NetworkInfoCollector.CheckCompleteListener() {
                @Override
                public void complete(NetworkInfoCollector.OnNetworkInfoChangedListener.InfoFlagEnum flag) {
                    if(flag == NetworkInfoCollector.OnNetworkInfoChangedListener.InfoFlagEnum.IP_CHECK_SUCCESS) {
                        postNetworkEvent("ipChange", null);
                    }
                }
            });
        }
    }

    /*

        Exception handler

     */


    Thread.UncaughtExceptionHandler exceptionHandler = (t, e) -> {
        Sentry.captureEvent(new SentryEvent(e));
        System.exit(10);
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        SharedPreferences oldPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences newPrefs = getSharedPreferences(NEW_PREFERENCES, MODE_PRIVATE);

        if(!newPrefs.getBoolean(OLD_PREFERENCES, false)) {
            Log.e(getClass().getSimpleName(), "Copiing preferences...");
            for (Map.Entry<String, ?> entry : oldPrefs.getAll().entrySet()) {
                Object value = entry.getValue();
                if (value instanceof String) {
                    if(entry.getKey().equals("uuid")) {
                        newPrefs.edit().putString(FLUTTER_REFIX + entry.getKey(), (String) entry.getValue()).apply();
                        break;
                    }
                }
            }
            newPrefs.edit().putBoolean(OLD_PREFERENCES, true).apply();
        }

        PackageInfo pInfo;
        try
        {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            versionName = pInfo.versionName;
            versionCode = pInfo.versionCode;
        }
        catch (final PackageManager.NameNotFoundException e)
        {
            // e1.printStackTrace();
            Log.e(TAG, "version of the application cannot be found", e);
        }



        NetworkInfoCollector.init(this);
        NetmetrConfig.init();

        Sentry.init(options -> {
            options.setDsn("https://"+NetmetrConfig.getInstance().getSentryDSN() + "@" + NetmetrConfig.getInstance().getSentryUrl() + (BuildConfig.DEBUG ? "/38" : "/37"));
            options.setRelease(BuildConfig.VERSION_NAME);
        });
        Sentry.setTag("build_type", BuildConfig.DEBUG ? "Debug" : "Release");
        Sentry.setTag("platform", PLATFORM_NAME);
        Sentry.setTag("model",  Build.MODEL);
        Sentry.setTag("systemOS", "Android");
        Sentry.setTag("systemName", "Android " + Build.VERSION.SDK_INT);

        Thread.currentThread().setUncaughtExceptionHandler(exceptionHandler);

        informationCollector = new InformationCollector(this,false, true, networkChannelListener);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            networkListener = new NetworkManagerV21((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE), networkChanged);
        }else {
            networkListener = new NetworkManager(context, networkChanged);
        }


        trafficGatherer = new TrafficGatherer();
        cpuStat = new CpuStatAndroidImpl();
        memInfo = new MemInfoAndroidImpl();

    }

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);

        initConfigChannel(flutterEngine);
        initStatChannel(flutterEngine);
        initServiceChannel(flutterEngine);
        initNetStatEventChannel(flutterEngine);
        initSettingsChannel(flutterEngine);
        initLoopChannel(flutterEngine);

        checkFirstRun();
    }

    private void checkFirstRun(){
        SharedPreferences preferences = this.getPreferences(Context.MODE_PRIVATE);
        boolean firstRun = preferences.getBoolean("firstRun", true);
        showRationaleDialog(preferences, firstRun);
    }

    private void showRationaleDialog(SharedPreferences preferences, boolean firstRun){
        if(rationaleDialogShowed)
            return;

       boolean shouldShow = false;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ) {
            shouldShow = shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) ||
                    shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION) ||
                    shouldShowRequestPermissionRationale(READ_PHONE_STATE)        ;
        }

        if (firstRun || shouldShow)
        {
            PackageManager packageManager = this.getApplicationContext().getPackageManager();


            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.rationale_dialog, null);
            Button ok = dialogView.findViewById(R.id.rationale_but);
            TextView location = dialogView.findViewById(R.id.location_per);
            TextView readPhone = dialogView.findViewById(R.id.read_phone_per);

            try {
                location.setText(packageManager.getPermissionInfo(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.GET_META_DATA).loadLabel(packageManager));
                readPhone.setText(packageManager.getPermissionInfo(READ_PHONE_STATE, PackageManager.GET_META_DATA).loadLabel(packageManager));

            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            showDialog(dialogView, ok, () -> {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, READ_PHONE_STATE}, FIRST);
                preferences.edit().putBoolean("firstRun", false).apply();
                rationaleDialogShowed = false;
            });
            rationaleDialogShowed = true;
        }else{
            preferences.edit().putBoolean("firstRun", false).apply();
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, READ_PHONE_STATE}, FIRST);
        }

    }

    public void showRationaleReadPhonePermission(){
        if(rationaleDialogShowed || ContextCompat.checkSelfPermission( this, READ_PHONE_STATE ) == PackageManager.PERMISSION_DENIED)
            return;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && shouldShowRequestPermissionRationale(READ_PHONE_STATE)) {
            PackageManager packageManager = this.getApplicationContext().getPackageManager();
            LayoutInflater inflater = this.getLayoutInflater();

            View dialogView = inflater.inflate(R.layout.rationale_phone_dialog, null);
            TextView title = dialogView.findViewById(R.id.read_phone_per_rat);
            Button okBut = dialogView.findViewById(R.id.rationale_but_rat);

            try {
                title.setText(packageManager.getPermissionInfo(READ_PHONE_STATE, PackageManager.GET_META_DATA).loadLabel(packageManager));
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            showDialog(dialogView, okBut, () -> {
                checkPhoneStatePermissions();
                rationaleDialogShowed = false;
            });
            rationaleDialogShowed = true;
        }else{
            checkPhoneStatePermissions();
        }
    }

    private void showDialog(View dialogView, Button ok, Runnable finishListener){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(dialogView);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        ok.setOnClickListener(v ->{
            finishListener.run();
            dialog.cancel();
        });
        dialog.show();
    }

    private void checkLocationPermissions() {
        if (ActivityCompat.checkSelfPermission( this, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION);
        }
    }

    public void checkPhoneStatePermissions(){
        if (ActivityCompat.checkSelfPermission( this, READ_PHONE_STATE ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{READ_PHONE_STATE}, PHONE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            switch (requestCode){
                case FIRST:
                    setLocationPermissions();
                    setPhonePermission();
                    break;
                case  LOCATION:
                    setLocationPermissions();
                    break;
                case PHONE:
                    setPhonePermission();
                    break;
            }
        }
    }
    
    @Override
    protected void onNewIntent(@NonNull Intent intent) {
        super.onNewIntent(intent);
        if(intent.getBooleanExtra("close_activity",false)){
            this.finish();
        }
    }

    private void setLocationPermissions(){
        NetworkInfoCollector infoCollector = NetworkInfoCollector.getInstance();
        infoCollector.onNetworkChange(getApplicationContext(), null);

        postNetworkChangeEvent();

        if(informationCollector != null){
            informationCollector.updateLocationPermission();
            informationCollector.startLocationManager();
        }

    }


    private void setPhonePermission(){
        if(informationCollector != null) {
            informationCollector.updatePhonePermission();
            informationCollector.reInit();
        }
        postNetworkChangeEvent();
    }

    @Override
    protected void onStart() {
        super.onStart();

        final Intent serviceIntent = new Intent(this, RMBTService.class);
        bindService(serviceIntent, this, Context.BIND_AUTO_CREATE);

        informationCollector.init();

        networkListener.registerListener();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(this);
        informationCollector.unload();

        networkListener.unregisterListener();
    }

    @Override
    protected void onResume() {
        super.onResume();

        NetworkInfoCollector.getInstance().addOnNetworkChangedListener(onNetworkAddressChangedListener);
        if (informationCollector != null) {
            informationCollector.init();
            informationCollector.updateLocationPermission();
            informationCollector.updatePhonePermission();
        }

        infoCollector.refresh();
    }

    @Override
    protected void onPause() {
        super.onPause();

        NetworkInfoCollector.getInstance().removeOnNetworkInfoChangedListener(onNetworkAddressChangedListener);
        if (informationCollector != null) {
            informationCollector.unload();
        }
    }

///////////////////////////// NETWORK EVENT

    private void networkChanged(){
        NetworkInfoCollector infoCollector = NetworkInfoCollector.getInstance();
        infoCollector.onNetworkChange(getApplicationContext(), null);

        if(informationCollector != null){
            if(infoCollector.isConnected()) informationCollector.init();
            else informationCollector.unload();
        }

        postNetworkChangeEvent();

        String extraInfo = infoCollector.getActiveNetworkInfo() != null ? infoCollector.getActiveNetworkInfo().getExtraInfo() : null;
        if(lastConnection != null && lastConnection.equals(extraInfo)) {
            ipChange(infoCollector);
        }

        lastConnection = extraInfo;
    }

    private void initNetStatEventChannel(FlutterEngine flutterEngine){
        new EventChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), NETSTAT_EVENT_CHANNEL).setStreamHandler(new EventChannel.StreamHandler() {
            @Override
            public void onListen(Object o, EventChannel.EventSink eventSink) {
                netEvent = eventSink;
                postNetworkChangeEvent();
            }

            @Override
            public void onCancel(Object o) {
                netEvent = null;
            }
        });
    }

    private void postNetworkChangeEvent(){
        if(netEvent == null)
            return;


        JSONObject object = new JSONObject();
        try {

            int networkType = NOT_CONNECTED; //NOT_CONNECTION

            NetworkInfoCollector networkInfoCollector = NetworkInfoCollector.getInstance();
            NetworkInfo networkInfo = networkInfoCollector.getActiveNetworkInfo();
            JSONObject networkData = new JSONObject();

            if(networkInfo != null && networkInfo.isConnected()) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    networkType = informationCollector.getNetworkTypeAPI23();

                    if(networkType == NOT_CONNECTED){
                        networkInfo.getType();
                    }

                }else {
                    networkType = networkInfo.getType();
                }


                double relativeSignal = getNetworkSignal();

                //add network information
                networkData.put("extra", networkInfoCollector.getSsid());
                networkData.put("relativeSignal", relativeSignal);
                networkData.put("signal", informationCollector.getSignal());
                networkData.put("signalType", informationCollector.getSignalType());
                networkData.put("networkID", informationCollector.getNetwork());
            }else {
                networkInfoCollector.setHasIpFromControlServer(false);
            }


            object.put("networkType", networkType);
            object.put("data", networkData.toString());

            postNetworkEvent("networkChange", object);

        }catch (Exception e){
            Log.e(TAG, "Create network event json fail" + e.getMessage());
        }
    }
    
    private void postNetworkEvent(String event, JSONObject eventData){
        if(event == null)
            return;

        JSONObject object = new JSONObject();
        try {

            object.put("status", event);
            object.put("data", eventData != null ? eventData.toString() : null);

            netEvent.success(object.toString());

        }catch (Exception e){
            Log.e(TAG, "Create network event json fail");
        }

    }

    private double getNetworkSignal(){
        double relativeSignal = -1d;

        NetworkUtil.MinMax<Integer> signalBounds = NetworkUtil.getSignalStrengthBounds(informationCollector.getSignalType());

        Integer informationSignal = informationCollector.getSignal();
        if(informationSignal == null)
            return  relativeSignal;

        if (!(signalBounds.min == Integer.MIN_VALUE || signalBounds.max == Integer.MAX_VALUE)) {
            relativeSignal = (double) (informationSignal - signalBounds.min) / (double) (signalBounds.max - signalBounds.min);
        }

        return  relativeSignal;
    }
//////////////////////////// SETTINGS CHANNEL

    private  void initSettingsChannel(FlutterEngine flutterEngine){
        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), SETTINGS_CHANNEL).setMethodCallHandler((methodCall, result) -> {
            if(methodCall.method.equals("showSettings")){
                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                result.success(null);
            }else {
                result.notImplemented();
            }
        });
    }

//////////////////////////// LOOP CHANNEL

    private  void initLoopChannel(FlutterEngine flutterEngine){
        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), LOOP_CHANNEL).setMethodCallHandler((methodCall, result) -> {
            switch (methodCall.method){
                case "startLoopService":
                    //start loop
                    if(activeLoop == null) {
                       try {
                           String uuid = methodCall.argument("uuid");
                           String controlServer = methodCall.argument("controlServer");
                           int port = methodCall.<Integer>argument("port");
                           boolean useSSL =  methodCall.<Boolean>argument("useSSL");
                           boolean qosSSL = methodCall.<Boolean>argument("qosSSL");
                           int minDelay =  methodCall.<Integer>argument("minDelay");
                           int maxDelay =  methodCall.<Integer>argument("maxDelay");
                           double maxMovement = methodCall.<Double>argument("maxMovement");
                           int maxTests = methodCall.<Integer>argument("maxTests");
                           boolean onlySignal= methodCall.<Boolean>argument("onlySignal");
                           boolean runNDT = methodCall.<Boolean>argument("runNDT");
                           boolean qos = (Boolean) methodCall.argument("qos");
                           boolean jitter = (Boolean) methodCall.argument("jitter");

                           activeLoop = new Loop(MainActivity.this, informationCollector, flutterEngine);
                           activeLoop.startLoop(uuid, controlServer, port, useSSL, qosSSL, (long) minDelay, (long)  maxDelay, (float) maxMovement, maxTests, onlySignal, runNDT, jitter, qos,
                               new Runnable() {
                                   @Override
                                   public void run() {
                                       activeLoop = null;
                                   }
                               }
                           );

                           result.success(null);
                       }catch (Exception e){
                           result.error("Error", e.getMessage(), null);
                       }
                    }else {
                        result.error("Error", "Loop already running", null);
                    }

                    break;
                default:
                    result.notImplemented();
            }
        });
    }

//////////////////////////// CONFIG CHANNEL

    private void initConfigChannel(FlutterEngine flutterEngine){
        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CONFIG_CHANNEL).setMethodCallHandler((methodCall, result) -> {
            switch (methodCall.method) {
                case "getVersionName":
                    result.success(versionName);
                    break;
                case "getVersionCode":
                    result.success(versionCode);
                    break;
                case "getBuildConfig": {
                    JSONObject object = new JSONObject();
                    NetmetrConfig config = NetmetrConfig.getInstance();
                    try {
                        object.put("sentry_url", config.getSentryUrl());
                        object.put("sentry_dsn", config.getSentryDSN());
                        object.put("client_secret", config.getClientSecret());
                        object.put("control_host_dev", config.getControlHostDev());
                        object.put("control_ipv4_host", config.getCheckIpv4Host());
                        object.put("control_ipv6_host", config.getCheckIpv6Host());
                        object.put("check_ipv4_host", config.getCheckIpv4Host());
                        object.put("check_ipv6_host", config.getCheckIpv6Host());
                        object.put("port", config.getServerPort());
                        object.put("control_ssl", config.isControlSSL());
                        object.put("qos_ssl", config.isQosSSL());
                        
                        result.success(object.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                        result.error("Error", e.getMessage(), null);
                    }

                    break;
                }
                case "getBasicInfo": {
                    JSONObject object = new JSONObject();

                    try {
                        object.put("platform", PLATFORM_NAME);
                        object.put("os_version", Build.VERSION.RELEASE + "(" + Build.VERSION.INCREMENTAL + ")");
                        object.put("api_level", String.valueOf(Build.VERSION.SDK_INT));
                        object.put("device", Build.DEVICE);
                        object.put("model", Build.MODEL);
                        object.put("product", Build.PRODUCT);
                        object.put("language", LanguageAlphabetMapperUtil.getLanguageString(Locale.getDefault()));
                        object.put("timezone", TimeZone.getDefault().getID());
                        object.put("softwareRevision", RevisionHelper.getVerboseRevision());
                        object.put("softwareVersionCode", versionCode);
                        object.put("softwareVersionName", versionName);
                        object.put("type", Config.RMBT_CLIENT_TYPE);

                        result.success(object.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                        result.error("Error", e.getMessage(), null);
                    }

                    break;
                }
                case "updateConfig":
                    NetmetrConfig config = NetmetrConfig.getInstance();

                    config.setControlIpv4Host((String) methodCall.argument("control_ipv4_only"));
                    config.setControlIpv6Host((String) methodCall.argument("control_ipv6_only"));
                    config.setCheckIpv4Host((String) methodCall.argument("url_ipv4_check"));
                    config.setCheckIpv6Host((String) methodCall.argument("url_ipv6_check"));
                    config.setUuid((String) methodCall.argument("uuid"));

                    result.success(null);

                    break;
                case "getCheckIpInfo":
                    final NetworkInfoCollector network = NetworkInfoCollector.getInstance();
                    if (network.isConnected()) {

                        if (network.hasIpFromControlServer()) {
                            sendCheckResult(network, result);
                        }

                        network.checkIp(flag -> {
                            if (flag == NetworkInfoCollector.OnNetworkInfoChangedListener.InfoFlagEnum.IP_CHECK_SUCCESS) {

                                sendCheckResult(network, result);

                            } else {
                                result.error("CHECK_IP", "ERROR", NetworkInfoCollector.OnNetworkInfoChangedListener.InfoFlagEnum.IP_CHECK_ERROR);
                            }
                        });
                    }

                    break;
                default:
                    result.notImplemented();
                    break;
            }
        });
    }

///////////////////////////// SERVICE CHANNEL

    private void initServiceChannel(FlutterEngine flutterEngine){
        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), SERVICE_CHANNEL).setMethodCallHandler(
                (methodCall, result) -> {
                    switch (methodCall.method) {
                        case "startService": {

                            final Intent service = new Intent(RMBTService.ACTION_START_TEST, null, MainActivity.this, RMBTService.class);

                            service.putExtra("uuid", (String) methodCall.argument("uuid"));
                            service.putExtra("controlServer", (String) methodCall.argument("controlServer"));
                            service.putExtra("port", (Integer) methodCall.argument("port"));
                            service.putExtra("useSSL", (Boolean) methodCall.argument("useSSL"));
                            service.putExtra("qosSSL", (Boolean) methodCall.argument("qosSSL"));
                            service.putExtra("loopMode", false);
                            service.putExtra("onlySignal", false);
                            service.putExtra("jitter", (Boolean) methodCall.argument("jitter"));
                            service.putExtra("qos", (Boolean) methodCall.argument("qos"));
                            service.putExtra("runNDT", (Boolean) methodCall.argument("runNDT"));
                            service.putExtra("override", informationCollector != null ? informationCollector.getLastOverrideNetwork().get() : ON_NONE);


                            startService(service);

                            runTask = new RunTask(flutterEngine, rmbtService);
                            if (informationCollector != null) {
                                informationCollector.unload();
                            }

                            result.success(null);

                            break;
                        }
                        case "continueService":{

                             if(runTask != null){
                                 runTask.continueTest();
                             }

                            result.success(null);

                             break;
                        }
                        case "stopService": {

                            final Intent service = new Intent(RMBTService.ACTION_ABORT_TEST, null, MainActivity.this, RMBTService.class);
                            startService(service);

                            if (informationCollector != null) {
                                informationCollector.init();
                            }

                            if (runTask != null) {
                                runTask.stop();
                            }

                            result.success(null);
                            break;
                        }
                        default:
                            result.notImplemented();
                            break;
                    }
                }
        );
    }

/////////////////////////// STAT CHANNEL

    private void initStatChannel(FlutterEngine flutterEngine) {
        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), STAT_CHANNEL).setMethodCallHandler(
                (MethodChannel.MethodCallHandler) (methodCall, result) -> {
                    switch (methodCall.method) {
                        case "getCpu":

                            final float[] cpus = cpuStat.getCpuStat();
                            if (cpus != null) {

                                double[] doubleCpus = new double[cpus.length];
                                for (int i = 0; i < cpus.length; i++) {
                                    doubleCpus[i] = (double) cpus[i];
                                }

                                result.success(doubleCpus);

                            } else {
                                result.success(null);
                            }
                            break;
                        case "getMemInfo":
                            memInfo.update();

                            Map<String, Long> memoryStat = new HashMap<>();
                            memoryStat.put("free_memory", memInfo.getFreeMem());
                            memoryStat.put("total_memory", memInfo.getTotalMem());

                            result.success(memoryStat);
                            break;
                        case "getNetworkSignal":
                            double relativeSignal = getNetworkSignal();

                            Map<String, Object> networkSignal = new HashMap<>();
                            networkSignal.put("signalType", informationCollector.getSignalType());
                            networkSignal.put("relativeSignal", relativeSignal);
                            networkSignal.put("signal", informationCollector.getSignal());
                            networkSignal.put("networkID", informationCollector.getNetwork());
                            result.success(networkSignal);
                            break;
                        case "getTraffic":
                            trafficGatherer.run();

                            Map<String, Long> trafficStat = new HashMap<>();
                            trafficStat.put("down", trafficGatherer.getRxRate());
                            trafficStat.put("up", trafficGatherer.getTxRate());

                            result.success(trafficStat);

                            break;
                        case "getLocation":
                            Location locale = informationCollector.getLastLocation();
                            double latitude = 0;
                            double longitude = 0;
                            String latitudeString = null;
                            String longitudeString = null;
                            boolean hasAccuracy = false;
                            boolean haveLocationData = false;
                            double accuracy = 0;
                            int satellites = 0;
                            long age = 0;
                            String provider = null;
                            double altitude = 0;
                            float speed = 0;

                            if(locale != null){
                                haveLocationData=true;
                                latitude = locale.getLatitude();
                                longitude = locale.getLongitude();
                                latitudeString =  Location.convert(latitude, Location.FORMAT_MINUTES);
                                longitudeString = Location.convert(longitude, Location.FORMAT_MINUTES);
                                hasAccuracy = locale.hasAccuracy();
                                if(hasAccuracy) accuracy = locale.getAccuracy();
                                if (locale.getExtras() != null) satellites = locale.getExtras().getInt("satellites");
                                age = System.currentTimeMillis() - locale.getTime();
                                provider = locale.getProvider();
                                altitude = locale.getAltitude();
                                speed = locale.getSpeed();
                            }

                            JSONObject jsonObject = new JSONObject();

                            try {

                                jsonObject.put("locationPermission", informationCollector.isLocationPermission());
                                jsonObject.put("latitude", latitude);
                                jsonObject.put("longitude", longitude);
                                jsonObject.put("latitudeString", latitudeString);
                                jsonObject.put("longitudeString", longitudeString);
                                jsonObject.put("hasAccuracy", hasAccuracy);
                                jsonObject.put("accuracy", accuracy);
                                jsonObject.put("satellites", satellites);
                                jsonObject.put("age", age);
                                jsonObject.put("provider", provider);
                                jsonObject.put("altitude", altitude);
                                jsonObject.put("speed", speed);
                                jsonObject.put("haveLocationData", haveLocationData);

                                result.success(jsonObject.toString());
                            } catch (JSONException e) {
                                Log.e(TAG, e.getMessage());
                                result.success(null);
                            }

                            break;
                        default:
                            result.notImplemented();
                            break;
                    }
                }
        );
    }

    private void sendCheckResult(NetworkInfoCollector collector, MethodChannel.Result result){
        JSONObject ipCheck = new JSONObject();
        try {

            ipCheck.put("status_ipv4", collector.getIpv4Status().ordinal());
            ipCheck.put("public_ipv4", collector.getPublicIpv4());
            ipCheck.put("private_ipv4", collector.getPrivateIpv4() != null ? collector.getPrivateIpv4().getHostAddress() : null);
            ipCheck.put("status_ipv6", collector.getIpv6Status().ordinal());
            ipCheck.put("public_ipv6", collector.getPublicIpv6());
            ipCheck.put("private_ipv6", collector.getPrivateIpv6() != null ? collector.getPrivateIpv6().getHostAddress() : null);


            result.success(ipCheck.toString());
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
            result.success(null);
        }
    }


///////////////////////////// SERVICES

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        Log.d(TAG, "service connected");
        final RMBTService.RMBTBinder binder = (RMBTService.RMBTBinder) service;
        rmbtService = binder.getService();
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        Log.d(TAG, "service disconnected");
        rmbtService = null;
    }

}
