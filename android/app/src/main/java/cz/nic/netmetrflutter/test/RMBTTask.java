/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.netmetrflutter.test;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import at.alladin.rmbt.client.*;
import at.alladin.rmbt.client.helper.*;
import at.alladin.rmbt.client.tools.impl.CpuStatCollector;
import at.alladin.rmbt.client.tools.impl.MemInfoCollector;
import cz.nic.netmetrflutter.impl.CpuStatAndroidImpl;
import cz.nic.netmetrflutter.impl.MemInfoAndroidImpl;
import cz.nic.netmetrflutter.impl.TracerouteAndroidImpl;
import cz.nic.netmetrflutter.impl.TrafficServiceImpl;
import cz.nic.netmetrflutter.impl.WebsiteTestServiceImpl;
import cz.nic.netmetrflutter.util.Config;
import cz.nic.netmetrflutter.util.DNSPropertiesHelper;
import cz.nic.netmetrflutter.util.InformationCollector;

import cz.nic.netmetrflutter.util.support.NetworkData;
import net.measurementlab.ndt.NdtTests;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import at.alladin.rmbt.client.ndt.NDTRunner;
import at.alladin.rmbt.client.v2.task.QoSTestEnum;
import at.alladin.rmbt.client.v2.task.result.QoSResultCollector;
import at.alladin.rmbt.client.v2.task.result.QoSTestResultEnum;
import at.alladin.rmbt.client.v2.task.service.TestSettings;
import at.alladin.rmbt.util.tools.InformationCollectorTool;

import static cz.nic.netmetrflutter.util.InformationCollector.ON_NONE;

public class RMBTTask {
    private enum TaskPart{
        NONE, SPEED, QOS, NDT
    }

    private interface NDTFinishListener{
        void finish(boolean error);
    }

    private static final String LOG_TAG = "RMBTTask";

    private final AtomicBoolean started = new AtomicBoolean();
    private final AtomicBoolean running = new AtomicBoolean();
    private final AtomicBoolean finished = new AtomicBoolean();
    private final AtomicBoolean cancelled = new AtomicBoolean();
    private final AtomicBoolean aborted = new AtomicBoolean(false);

    private final AtomicReference<QualityOfServiceTest> qosReference = new AtomicReference<QualityOfServiceTest>();
    private List<InformationCollector.SignalItem> signals;
    private NetworkData lastNetworkData;

    private Handler handler;
    private final Runnable postExecuteHandler = new Runnable()
    {
        @Override
        public void run()
        {
            if (fullInfo != null)
            {
                signals = fullInfo.getSignals();
                lastNetworkData = new NetworkData(fullInfo.getNetwork(), fullInfo.getOperatorName());
                fullInfo.unload();
                fullInfo = null;
            }
            if (endTaskListener != null) {
                endTaskListener.sendResult(totalTestResult);
                endTaskListener.taskEnded();
            }
        }
    };

    final private Context context;
    final private boolean loopMode;

    private final ExecutorService executor = Executors.newCachedThreadPool();

    private final AtomicBoolean connectionError = new AtomicBoolean();
    private RMBTClient client;

    private InformationCollector fullInfo;
    private QoSResultCollector qosResult = null;

    private ControlServerInfo serverInfo = new ControlServerInfo();
    private ControlServerListener serverListener;

    private TotalTestResult totalTestResult;

    private EndTaskListener endTaskListener;
    private NetMeterSettings settings;

    private ClientError clientError = new ClientError();
    private TaskPart actualPart = TaskPart.NONE;
    private final AtomicInteger lastOverrideNetwork = new AtomicInteger(ON_NONE);


    public RMBTTask(final Context ctx, final boolean loopMode, final NetMeterSettings settings, int lastOverrideNetwork, EndTaskListener endTaskListener)
    {
        this.context = ctx;
        this.loopMode = loopMode;
        this.settings = settings;
        this.lastOverrideNetwork.set(lastOverrideNetwork);
        this.endTaskListener = endTaskListener;

        Log.d(LOG_TAG, settings.toString());

    }

    public void execute(final Handler _handler)
    {
        fullInfo = new InformationCollector(context,true, true, settings.isOnlySignal(), lastOverrideNetwork.get());
        cancelled.set(false);
        started.set(true);
        running.set(true);
        finished.set(false);

        handler = _handler;

        executeStartTest();
    }

    public void continueNextPart() {
        switch (actualPart){
            case SPEED:
                if (runQOS()) executeQosTest();
                else executeFinal();
                break;
            case QOS:
                if(settings.isRunNDT()) executeNDTTest();
                else executeFinal();
                break;
            case NDT: executeFinal(); break;


        }
    }

    public void executeStartTest(){
        if(executor.isShutdown())
            return;

        executor.execute(() -> {
            boolean error = false;
            connectionError.set(false);
            TestResult testResult = null;
            try
            {

                final String controlServer = settings.getControlServer();
                final int controlPort = settings.getPort();
                final boolean controlSSL = settings.isUseSSL();
                final String uuid = settings.getUuid();
                final boolean ndt = settings.isRunNDT();

                File cacheDir = context.getCacheDir();

                final ArrayList<String> geoInfo = fullInfo.getCurLocation();

                JSONObject requestData = RMBTClient.createNewConnectionRequestData(geoInfo, uuid, Config.RMBT_CLIENT_TYPE, getClientName(), fullInfo.getInfo("CLIENT_SOFTWARE_VERSION"), fullInfo.getInitialInfo(ndt), true);
                client = RMBTClient.getInstance(controlServer, null, controlPort, controlSSL, geoInfo,  uuid,
                        Config.RMBT_CLIENT_TYPE, getClientName(),
                        fullInfo.getInfo("CLIENT_SOFTWARE_VERSION"), null, requestData, fullInfo.getInitialInfo(ndt), cacheDir, settings.isRunJitter(), settings.isRunQoS());

                /*
                 * Example on how to implement the information collector tool:
                 *
                 *
                 */

                //set dns
                client.setDnsHostname(DNSPropertiesHelper.findDNS(context));

                //set qos settings for jitter and packet loss
                TestSettings qosTestSettings = new TestSettings();
                qosTestSettings.setCacheFolder(context.getCacheDir());
                qosTestSettings.setTrafficService(new TrafficServiceImpl());
                qosTestSettings.setTracerouteServiceClazz(TracerouteAndroidImpl.class);
                qosTestSettings.setStartTimeNs(getRmbtClient().getControlConnection().getStartTimeNs());
                qosTestSettings.setUseSsl(settings.isQosSSL());
                client.setQosSettings(qosTestSettings);


                final InformationCollectorTool infoTool = new InformationCollectorTool(TimeUnit.NANOSECONDS, TimeUnit.NANOSECONDS.convert(120, TimeUnit.SECONDS));
                infoTool.addCollector(new CpuStatCollector(new CpuStatAndroidImpl(), TimeUnit.NANOSECONDS.convert(1, TimeUnit.SECONDS)));
                infoTool.addCollector(new MemInfoCollector(new MemInfoAndroidImpl(), TimeUnit.NANOSECONDS.convert(1, TimeUnit.SECONDS)));
                client.setInformationCollectorTool(infoTool);
                client.startInformationCollectorTool();

                /////////////////////////////////////////////////////////////////

                client.setTrafficService(new TrafficServiceImpl());
                final ControlServerConnection controlConnection = client.getControlConnection();
                if (controlConnection != null)
                {
                    fullInfo.setUUID(controlConnection.getClientUUID());
                    fullInfo.setTestServerName(controlConnection.getServerName());
                }
            }
            catch (final RMBTClientException e){
                clientError.setError(true);
                clientError.setErrorMessage(e.getMessage());
                error = true;

            }
            catch (final Exception e)
            {
                e.printStackTrace();
                error = true;
            }

            if (error || client == null) {

                if (isCellularWithoutSignal(context)) {
                    final String uuid = fullInfo.getUUID();
                    final ArrayList<String> geoInfo = fullInfo.getCurLocation();
                    JSONObject resultValues = null;
                    try {
                        resultValues = fullInfo.setMockSignal().getResultValues(System.currentTimeMillis());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    JSONObject requestData = RMBTClient.createNewConnectionRequestData(geoInfo, uuid, Config.RMBT_CLIENT_TYPE, Config.RMBT_CLIENT_NAME, fullInfo.getInfo("CLIENT_SOFTWARE_VERSION"), resultValues, false);

                }

                connectionError.set(true);
            }else{
                executeSpeedTest();
            }
        });

    }

    public void executeSpeedTest(){
        if(executor.isShutdown())
            return;

        actualPart = TaskPart.SPEED;

        executor.execute(()-> {
            TestResult testResult = null;
            boolean error = false;

            try
            {

                if (Thread.interrupted() || cancelled.get())
                    throw new InterruptedException();

                Log.d(LOG_TAG, "runTest RMBTTask="+this);
                if (!settings.isOnlySignal()) {
                    testResult = client.runTest();
                }
                final ControlServerConnection controlConnection = client.getControlConnection();

                final InformationCollectorTool infoCollectorTool = client.getInformationCollectorTool();

                if (!fullInfo.getIllegalNetworkTypeChangeDetcted()) {
                    final JSONObject infoObject = fullInfo.getResultValues(controlConnection.getStartTimeNs());
                    if (infoCollectorTool != null) {
                        infoCollectorTool.stop();
                        infoObject.put("extended_test_stat", infoCollectorTool.getJsonObject(true, client.getControlConnection().getStartTimeNs()));
                    }

                    if(fullInfo.isAppNotConnected(context)){
                        connectivityError();
                        error = true;
                    }else {

                        serverInfo.setSpeedDataStatus(ControlServerInfo.TaskStatus.RUNNING);
                        sendServerInfo();

                        ResultData resultData = client.sendResult(infoObject);
                        if(resultData.getStatus() == ResultData.ResultStatus.FAILED){
                            error = true;
                            aborted.set(true);
                        }

                        serverInfo.addResultData(ControlServerInfo.TestTask.SPEED, resultData);
                        serverInfo.setSpeedDataStatus(ControlServerInfo.TaskStatus.FINISHED);
                        sendServerInfo();
                    }
                }
                else {
                    error = true;
                }
            }
            catch (final Exception e)
            {
                e.printStackTrace();
            }
            finally
            {
                totalTestResult = (TotalTestResult) testResult;
                client.shutdown();

                if(!error){
                    if(runQOS()  && !cancelled.get() )
                        executeQosTest();
                    else if(settings.isRunNDT())
                        executeNDTTest();
                    else
                        executeFinal();
                }


            }

        });
    }

    public boolean runQOS(){
        return settings.isRunQoS() && !settings.isOnlySignal() && client.getTaskDescList() != null && client.getTaskDescList().size() > 0;
    }

    public void executeQosTest(){
        if(executor.isShutdown())
            return;

        actualPart = TaskPart.QOS;

        executor.execute(() -> {
           QualityOfServiceTest qosTest = null;

           boolean error = false;


           try {

               TestSettings qosTestSettings = new TestSettings();
               qosTestSettings.setCacheFolder(context.getCacheDir());
               qosTestSettings.setWebsiteTestService(new WebsiteTestServiceImpl(context));
               qosTestSettings.setTrafficService(new TrafficServiceImpl());
               qosTestSettings.setTracerouteServiceClazz(TracerouteAndroidImpl.class);
               qosTestSettings.setStartTimeNs(getRmbtClient().getControlConnection().getStartTimeNs());
               qosTestSettings.setUseSsl(settings.isQosSSL());

               qosTest = new QualityOfServiceTest(client, qosTestSettings, false);
               qosReference.set(qosTest);
               client.setStatus(TestStatus.QOS_TEST_RUNNING);
               qosResult = qosTest.call();
               System.out.print(qosResult.toString());

               InformationCollector.qoSResult = qosResult;

               if (!cancelled.get()) {
                   if (qosResult != null && !qosTest.getStatus().equals(QoSTestEnum.ERROR)) {

                       if(fullInfo.isAppNotConnected(context)){
                           connectivityError();
                           error = true;
                       }else {
                           serverInfo.setQosDataStatus(ControlServerInfo.TaskStatus.RUNNING);
                           sendServerInfo();

                           ResultData qosResultData = client.sendQoSResult(qosResult);

                           if(qosResultData.getStatus() == ResultData.ResultStatus.FAILED){
                               error = true;
                               aborted.set(true);
                           }


                           serverInfo.addResultData(ControlServerInfo.TestTask.QOS, qosResultData);
                           serverInfo.setQosDataStatus(ControlServerInfo.TaskStatus.FINISHED);
                           sendServerInfo();
                       }
                   }
               }

           }catch (Exception e){
               System.err.println(e.getMessage());
               error = true;
           }


           if (!error && !cancelled.get() && qosTest.getStatus().equals(QoSTestEnum.QOS_FINISHED)) {
               if(settings.isRunNDT()){
                   qosTest.setStatus(QoSTestEnum.NDT_RUNNING);
                   executeNDTTest();
               }else{
                   executeFinal();
               }
               qosTest.setStatus(QoSTestEnum.STOP);
           }
       });
    }

    public void executeNDTTest(){
        if(executor.isShutdown())
            return;

        actualPart = TaskPart.NDT;

        executor.execute(()->{
            client.setStatus(TestStatus.NDT_RUNNING);
            runNDT(error -> {
                client.setStatus(TestStatus.NDT_END);

                if(!error)
                    executeFinal();
            });

        });
    }

    public void executeFinal(){
        if(executor.isShutdown())
            return;

        executor.execute(() -> {
            running.set(false);
            finished.set(true);
            if (handler != null)
                handler.post(postExecuteHandler);
            Log.d(LOG_TAG, "executor task finished");
        });
    }

    public void cancel()
    {

        setPreviousTestStatus();
        cancelled.set(true);
        executor.shutdownNow();

        Log.d(LOG_TAG, "shutdownNow called RMBTTask=" + this);

//        try
//        {
//            executor.awaitTermination(10, TimeUnit.SECONDS);
//        }
//        catch (InterruptedException e)
//        {
//            Thread.currentThread().interrupt();
//        }
    }

    private void sendServerInfo(){
        if(serverListener != null){
            serverListener.sendServerInfo(serverInfo);
        }
    }

    public void setServerListener(ControlServerListener serverListener) {
        this.serverListener = serverListener;
    }

    public boolean isFinished()
    {
        return finished.get();
    }

    public boolean isRunning()
    {
        return running.get() && ! cancelled.get();
    }

    public boolean isAbort(){
        if(aborted.get())
            return true;

        if(client == null)
            return false;

        return client.isAborted();
    }

    public void setAborted(boolean abort){
        aborted.set(abort);
        if(client != null){
            client.setAborted(abort);
        }
    }

    private void setPreviousTestStatus()
    {
        final TestStatus status;
        if (client == null)
            status = null;
        else
            status = client.getStatus();

        final String statusString;
        if (status == TestStatus.ERROR)
        {
            final TestStatus statusBeforeError = client.getStatusBeforeError();
            if (statusBeforeError != null)
                statusString = "ERROR_" + statusBeforeError.toString();
            else
                statusString = "ERROR";
        }
        else if (status != null)
            statusString = status.toString();
        else
            statusString = null;

        System.out.println("test status at end: " + statusString);
        //set status config help
    }

    private static boolean isPhone(Context context) {
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if(manager.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE){
            return false;
        }else{
            return true;
        }
    }

    public static boolean isMobileDataEnabled(Context context) {
        boolean mobileDataEnabled = false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            Class cmClass = Class.forName(cm.getClass().getName());
            Method method = cmClass.getDeclaredMethod("getMobileDataEnabled");
            method.setAccessible(true);
            mobileDataEnabled = (Boolean)method.invoke(cm);
        } catch (Exception e) {
            //nothing needed
        }
        return mobileDataEnabled;
    }

    private static boolean isCellularData(Context context) {
        final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null) {
            return networkInfo.isConnected();
        }
        return false;
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static boolean isAirplaneModeOn(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.System.getInt(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        }
    }

    public static boolean isCellularWithoutSignal(Context context) {
        return isPhone(context) && isMobileDataEnabled(context) && !isCellularData(context) && !isAirplaneModeOn(context);
    }

    private void connectivityError(){
        endTaskListener.taskEnded();
        client.setStatus(TestStatus.ERROR);
        clientError.setError(true);
        clientError.setConnectionError(true);
    }

    private final AtomicReference<NDTRunner> ndtRunnerHolder = new AtomicReference<NDTRunner>();

    public float getNDTProgress()
    {
        final NDTRunner ndtRunner = ndtRunnerHolder.get();
        if (ndtRunner == null)
            return 0;
        return ndtRunner.getNdtProgress();
    }

    public NdtStatus getNdtStatus()
    {
        final NDTRunner ndtRunner = ndtRunnerHolder.get();
        if (ndtRunner == null)
            return null;
        return ndtRunner.getNdtStatus();
    }

    public void stopNDT()
    {
        final NDTRunner ndtRunner = ndtRunnerHolder.get();
        if (ndtRunner != null)
            ndtRunner.setNdtCacelled(true);
    }

    public void runNDT(NDTFinishListener finishListener)
    {
        final NDTRunner ndtRunner = new NDTRunner();
        ndtRunnerHolder.set(ndtRunner);

        Log.d(LOG_TAG, "ndt status RUNNING");

        final String ndtNetworkType;
        final int networkType = getNetworkType();
        switch (networkType)
        {
            case InformationCollector.NETWORK_WIFI:
                ndtNetworkType = NdtTests.NETWORK_WIFI;
                break;

            case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                ndtNetworkType = NdtTests.NETWORK_UNKNOWN;
                break;

            default:
                ndtNetworkType = NdtTests.NETWORK_MOBILE;
                break;
        }

        ndtRunner.runNDT(ndtNetworkType, ndtRunner.new UiServices()
        {

            @Override
            public void sendResults()
            {

                boolean error = false;

                if(fullInfo.isAppNotConnected(context)){
                   connectivityError();
                   error = true;
                }else {
                    serverInfo.setNdtDataStatus(ControlServerInfo.TaskStatus.RUNNING);
                    sendServerInfo();

                    ResultData ndtResultData = client.getControlConnection().ndtResult(this, null);
                    error = ndtResultData.getStatus() == ResultData.ResultStatus.FAILED;
                    serverInfo.addResultData(ControlServerInfo.TestTask.NDT, ndtResultData);
                    serverInfo.setNdtDataStatus(ControlServerInfo.TaskStatus.FINISHED);
                    sendServerInfo();
                }

                if(finishListener != null){
                    finishListener.finish(error);
                }
            }

            public boolean wantToStop()
            {
                if (super.wantToStop())
                    return true;

                if (cancelled.get())
                {
                    cancel();
                    return true;
                }
                return false;
            }
        });
    }

    /**
     *
     * @return
     */
    public float getQoSTestProgress()
    {
        final QualityOfServiceTest nnTest = qosReference.get();
        if (nnTest == null)
            return 0;
        return nnTest.getProgress();
    }

    /**
     *
     * @return
     */
    public int getQoSTestSize() {
        final QualityOfServiceTest nnTest = qosReference.get();
        if (nnTest == null)
            return 0;
        return nnTest.getTestSize();
    }

    /**
     *
     * @return
     */
    public QualityOfServiceTest getQoSTest() {
        return qosReference.get();
    }

    /**
     *
     * @return
     */
    public QoSTestEnum getQoSTestStatus()
    {
        final QualityOfServiceTest nnTest = qosReference.get();
        if (nnTest == null)
            return null;
        return nnTest.getStatus();
    }

    /**
     *
     * @return
     */
    public Map<QoSTestResultEnum, QualityOfServiceTest.Counter> getQoSGroupCounterMap() {
        final QualityOfServiceTest nnTest = qosReference.get();
        if (nnTest == null)
            return null;
        return nnTest.getTestGroupCounterMap();
    }



    public void setEndTaskListener(final EndTaskListener endTaskListener)
    {
        this.endTaskListener = endTaskListener;
    }

    public Integer getSignal()
    {
        if (fullInfo != null)
            return fullInfo.getSignal();
        else
            return null;
    }

    public int getSignalType()
    {
        if (fullInfo != null)
            return fullInfo.getSignalType();
        else
            return InformationCollector.SINGAL_TYPE_NO_SIGNAL;
    }

    public IntermediateResult getIntermediateResult(final IntermediateResult result)
    {
        if (client == null)
            return null;
        return client.getIntermediateResult(result);
    }

    public TotalTestResult getTotalResult(){
        return client != null ? client.getResult() : null;
    }

    public QoSResultCollector getQosResult() {
        return qosResult;
    }

    public boolean isConnectionError()
    {
        return connectionError.get();
    }

    public String getOperatorName()
    {
        if (fullInfo != null)
            return fullInfo.getOperatorName();
        else
            return null;
    }

    public Location getLocation()
    {
        if (fullInfo != null)
            return fullInfo.getLastLocation();
        else
            return null;
    }

    public List<InformationCollector.SignalItem> getSignals() {
        return signals;
    }

    public String getServerName()
    {
        if (fullInfo != null)
            return fullInfo.getTestServerName();
        else
            return null;
    }

    public String getIP()
    {
        if (client != null)
            return client.getPublicIP();
        else
            return null;
    }

    public String getUuid() {
        return settings.getUuid();
    }

    public String getTestUuid()
    {
        if (client != null)
            return client.getTestUuid();
        else
            return null;
    }

    public ClientError getClientError() {
        return clientError;
    }

    public int getNetworkType()
    {
        if (fullInfo != null)
        {
            final int networkType = fullInfo.getNetwork();
            if (fullInfo.getIllegalNetworkTypeChangeDetcted())
            {
                Log.e(LOG_TAG, "illegal network change detected; cancelling test");
                cancel();
            }
            return networkType;
        }
        else
            return 0;
    }

    public RMBTClient getRmbtClient() {
        return client;
    }

    private String getClientName() {
        return loopMode ? Config.RMBT_CLIENT_NAME_FUP : Config.RMBT_CLIENT_NAME;
    }

    public NetMeterSettings getSettings() {
        return settings;
    }

    public NetworkData getLastNetworkData() {
        return lastNetworkData;
    }
}
