/*******************************************************************************
 * Copyright 2015 SPECURE GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package cz.nic.netmetrflutter.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class ConfigHelper {

    public static SharedPreferences getSharedPreferences(final Context context)
    {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    //TODO měnit v nastavení ?
    public static boolean useNetworkInterfaceIpMethod(final Context context)
    {
        return getSharedPreferences(context).getBoolean("dev_debug_ip_method", false);
    }

    public static boolean isIpPolling(final Context context)
    {
        return getSharedPreferences(context).getBoolean("dev_debug_ip_poll", true);
    }

}
