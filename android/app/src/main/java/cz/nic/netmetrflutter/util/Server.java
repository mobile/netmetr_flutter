/*******************************************************************************
 * Copyright 2015 SPECURE GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package cz.nic.netmetrflutter.util;


public class Server {
    private static final String ENCODING_FORMAT_SPLIT = ";";
    private static final String ENCODING_FORMAT = "%s" + ENCODING_FORMAT_SPLIT +
            "%s" + ENCODING_FORMAT_SPLIT +
            "%s" + ENCODING_FORMAT_SPLIT +
            "%s" + ENCODING_FORMAT_SPLIT;
    private static final int ENCODING_DATA_LENGTH = 4;

    private static final String LOCATION_FORMAT = "%s, %s";

    protected final String uuid;
    protected final String name;
    protected final String city;
    protected final String country;

    public Server(String uuid, String name, String city, String country) {
        this.uuid = uuid;
        this.name = name;
        this.city = city;
        this.country = country;
    }

    public String encodeToString() {
        return String.format(ENCODING_FORMAT, uuid, name, city, country);
    }

    public static Server decodeFromString(String data) {
        final String[] splitResult = data.split(ENCODING_FORMAT_SPLIT);

        if (splitResult.length < ENCODING_DATA_LENGTH) return null;

        return new Server(splitResult[0], splitResult[1], splitResult[2], splitResult[3]);
    }

    public String getName() {
        return name;
    }

    public String getUuid() {
        return uuid;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getFormattedLocation() {
        return String.format(LOCATION_FORMAT, getCity(), getCountry());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        Server other = (Server) obj;
        if (name == null) {
            if (other.name != null) return false;
        } else if (!name.equals(other.name)) {
            return false;
        }

        if (uuid == null) {
            if (other.uuid != null) return false;
        } else if (!uuid.equals(other.uuid)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return name + " " + getFormattedLocation();
    }
}
