/*******************************************************************************
 * Copyright 2015 SPECURE GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package cz.nic.netmetrflutter.impl;

import android.content.Context;
import android.os.Build;
import android.os.CpuUsageInfo;
import android.os.HardwarePropertiesManager;
import android.util.Log;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import at.alladin.rmbt.util.tools.CpuStat;
import at.alladin.rmbt.util.tools.CpuStat.CpuUsage.CoreUsage;
import at.alladin.rmbt.util.tools.ToolUtils;

public class CpuStatAndroidImpl extends CpuStat {
	private final static String PROC_PATH = "/proc/";

	private final static Pattern CPU_PATTERN = Pattern.compile("cpu[^0-9]([\\s0-9]*)");
	
	private final static Pattern CPU_CORE_PATTERN = Pattern.compile("cpu([0-9]+)([\\s0-9]*)");

	private static boolean canReadProcFiles = true;

	public float[] getCpuStat(){
		float[] cpus = null;
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.O){

			cpus = update(false);

		}

		return cpus;
	}

	public CpuUsage getCurrentCpuUsage(boolean getByCore) {
		CpuUsage cpuUsage = new CpuUsage();

		if (!canReadProcFiles) {
			cpuUsage.getCoreUsageList().add(new CoreUsage(0, 0, 0, 0, 0, 0, 0, 0));
			return cpuUsage;
		}

		String stat = null;
		try {
			stat = ToolUtils.readFromProc(PROC_PATH + "stat");
		} catch (IOException e) {
			//Welcome to Android Oreo...
			e.printStackTrace();

			Log.i(getClass().getSimpleName(), "Can't read /proc/* files. We are probably running Android O.");
			canReadProcFiles = false;
			cpuUsage.getCoreUsageList().add(new CoreUsage(0, 0, 0, 0, 0 ,0, 0, 0));
			return cpuUsage;
		}

		if (getByCore) {
			Matcher m = CPU_CORE_PATTERN.matcher(stat);
			while(m.find()) {
				int core = Integer.parseInt(m.group(1));
				String[] cpu = m.group(2).trim().split(" ");
				cpuUsage.getCoreUsageList().add(new 
						CoreUsage(core, Integer.parseInt(cpu[0]), 
								Integer.parseInt(cpu[1]), Integer.parseInt(cpu[2]), 
								Integer.parseInt(cpu[3]), Integer.parseInt(cpu[4]), 
								Integer.parseInt(cpu[5]), Integer.parseInt(cpu[6])));
			}
		}
		else {
			Matcher m = CPU_PATTERN.matcher(stat);
			while(m.find()) {
				String[] cpu = m.group(1).trim().split(" ");
				cpuUsage.getCoreUsageList().add(new 
						CoreUsage(0, Integer.parseInt(cpu[0]), 
								Integer.parseInt(cpu[1]), Integer.parseInt(cpu[2]), 
								Integer.parseInt(cpu[3]), Integer.parseInt(cpu[4]), 
								Integer.parseInt(cpu[5]), Integer.parseInt(cpu[6])));
			}
		}
		return cpuUsage;
	}	
}
