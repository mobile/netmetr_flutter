/*
 *  Copyright (C) 2021 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.netmetrflutter.test;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import java.util.List;

import cz.nic.netmetrflutter.MainActivity;

public class NotificationReceiver extends BroadcastReceiver {
    public static String ABORT_ACTION = "cz.nic.netmetrflutter.AbortAction";
    public static int REQ_CODE = 1001;
    public static int REQ_LOOP_CODE = 1002;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if(action.equals(ABORT_ACTION)){
            boolean loop = intent.getBooleanExtra("loop", false);
            Intent i = new Intent(context, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            
            if(loop){
                context.startService(new Intent(RMBTLoopService.ACTION_STOP, null, context, RMBTLoopService.class));
                i.putExtra("close_activity", !isAppForeground(context));
            }
            else {
                context.startService(new Intent(RMBTService.ACTION_ABORT_TEST, null, context, RMBTService.class));
                i.putExtra("close_activity", true);
            }
            
            context.startActivity(i);
        }
    }

    public boolean isAppForeground(Context mContext) {

        ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            return topActivity.getPackageName().equals(mContext.getPackageName());
        }

        return true;
    }

}
