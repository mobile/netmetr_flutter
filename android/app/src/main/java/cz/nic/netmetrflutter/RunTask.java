/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.netmetrflutter;

import android.os.Handler;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.util.Log;

import at.alladin.rmbt.client.JitterResult;
import at.alladin.rmbt.client.TotalTestResult;
import at.alladin.rmbt.client.helper.NdtStatus;
import at.alladin.rmbt.client.helper.ResultData;
import at.alladin.rmbt.client.v2.task.service.TestJitterStatus;
import cz.nic.netmetrflutter.test.*;

import cz.nic.netmetrflutter.util.Classification;
import cz.nic.netmetrflutter.util.InformationCollector;
import cz.nic.netmetrflutter.util.support.NetworkData;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodChannel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import at.alladin.rmbt.client.QualityOfServiceTest;
import at.alladin.rmbt.client.helper.IntermediateResult;
import at.alladin.rmbt.client.helper.TestStatus;
import at.alladin.rmbt.client.v2.task.AbstractQoSTask;
import at.alladin.rmbt.client.v2.task.result.QoSTestResultEnum;
import io.flutter.plugin.common.EventChannel;

public class RunTask {
    /**
     * max test time in ms
     */
    public final static int TEST_MAX_TIME = 3000;

    /**
     * highest value for an unfinished test (0-1)
     */
    public final static float MAX_VALUE_UNFINISHED_TEST = 0.9f;

    private static final String LOG_TAG = "RUN_TASK";
    private static final String TEST_RECEIVER_CHANNEL = "nic.cz.netmetrflutter/runTestReceiver";
    private static final String INFO_RECEIVER_CHANNEL = "nic.cz.netmetrflutter/infoChannel";
    private static final String OFF_RESULTS_CHANNEL = "nic.cz.netmetrflutter/off_result";

    private static final String SERVICE_RUNNING = "running";
    private static final String SERVICE_FINISH = "finish";
    private static final String TEST_FAILED = "failed";

    private static final long UPDATE_DELAY = 100;

    private final FlutterEngine flutterEngine;
    private final RMBTService service;

    private final Handler uiThreadHandler = new Handler(Looper.getMainLooper());

    private Handler handler;
    private EventChannel.EventSink testEventSink;
    private EventChannel.EventSink infoEventSink;

    private IntermediateResult intermediateResult;
    private boolean networkInfoSend = false;

    private Runnable updateTask = new Runnable() {
        @Override
        public void run() {
            if(service.isTestAbort()) {
                return;
            }

            RMBTService.ServiceStatus status = service.getStatus();
            final JSONObject object = new JSONObject();

            try {
                if(status == RMBTService.ServiceStatus.START) {
                    intermediateResult = service.getIntermediateResult(intermediateResult);

                    if (intermediateResult == null || intermediateResult.status != TestStatus.END) {
                        handler.postDelayed(this, UPDATE_DELAY);
                    }

                    object.put("serviceStatus", SERVICE_RUNNING);

                    JSONObject networkData = new JSONObject();

                    networkData.put("operatorName", service.getOperatorName());
                    networkData.put("networkType", service.getNetworkType());
                    networkData.put("serverName", service.getServerName());
                    networkData.put("ip", service.getIP());

                    object.put("networkData", networkData);


                    if (intermediateResult != null) {
                        JSONObject testData = new JSONObject();

                        TestJitterStatus jitterStatus = intermediateResult.testJitterStatus;

                        testData.put("status", intermediateResult.status.ordinal());
                        testData.put("initNano", intermediateResult.initNano);
                        testData.put("pingNano", intermediateResult.pingNano);
                        testData.put("downBitPerSec", intermediateResult.downBitPerSec);
                        testData.put("upBitPerSec", intermediateResult.upBitPerSec);
                        testData.put("progress", intermediateResult.progress);
                        testData.put("downBitPerSecLog", intermediateResult.downBitPerSecLog);
                        testData.put("upBitPerSecLog", intermediateResult.upBitPerSecLog);
                        testData.put("remainingWait", intermediateResult.remainingWait);
                        testData.put("jitterStatus", jitterStatus.ordinal());
                        testData.put("signal", service.getSignal());
                        testData.put("signalType", service.getSignalType());


                        if(jitterStatus == TestJitterStatus.FINISH) {
                            JSONObject down = new JSONObject();
                            down.put("meanJitter", intermediateResult.jitterMeanDown);
                            down.put("total", intermediateResult.totalPacketDown);
                            down.put("numPacket", intermediateResult.numPacketDown);


                            JSONObject up = new JSONObject();
                            up.put("meanJitter", intermediateResult.jitterMeanUp);
                            up.put("total", intermediateResult.totalPacketUp);
                            up.put("numPacket", intermediateResult.numPacketUp);

                            testData.put("down", down);
                            testData.put("up", up);
                        }


                        object.put("data", testData);

                    }

                    QualityOfServiceTest quality = service.getQosTest();
                    Map<QoSTestResultEnum, QualityOfServiceTest.Counter> counterMap = service.getQoSGroupCounterMap();

                    if(quality != null && counterMap != null){
                        JSONObject testData = new JSONObject();

                        for (QoSTestResultEnum key : quality.getTestMap().keySet()) {
                            if(key == QoSTestResultEnum.VOIP)
                                continue;

                            JSONObject qosData = new JSONObject();

                            QualityOfServiceTest.Counter counter = counterMap.get(key);
                            final int value = counter.value;

                            float testGroupProgress = 0f;
                            long currentTs = System.nanoTime();

                            List<AbstractQoSTask> taskList = quality.getTestMap().get(key);
                            if (taskList != null && value < counter.target) {
                                for (AbstractQoSTask task : taskList) {
                                    if (task.hasStarted()) {
                                        int runningMs = (int) (task.getRelativeDurationNs(currentTs) / 1000000);
                                        //System.out.println("TASKMAP - RUNNING MS: " +runningMs+ "/" + TEST_MAX_TIME +  "  counter:" + counter.target);
                                        if (runningMs >= ((float) TEST_MAX_TIME * MAX_VALUE_UNFINISHED_TEST) && !task.hasFinished()) {
                                            testGroupProgress += ((1f / (float) counter.target) * MAX_VALUE_UNFINISHED_TEST);
                                        } else if (!task.hasFinished()) {
                                            testGroupProgress += ((1f / (float) counter.target) * (runningMs / (float) TEST_MAX_TIME));
                                        }
                                    }
                                }

                                testGroupProgress += ((float) value / (float) counter.target);
                                testGroupProgress *= 100f;
                            } else if (value == counter.target) {
                                testGroupProgress = 100f;
                            } else {
                                System.out.println("NO TASKMAP FOUND: " + key);
                            }

                            qosData.put("qosType", key.ordinal());
                            qosData.put("progress", testGroupProgress);
                            qosData.put("value", value);
                            qosData.put("target", counter.target);
                            qosData.put("firstTest", counter.firstTest);
                            qosData.put("lastTest", counter.lastTest);
                            testData.put("qos" + key.ordinal(), qosData);
                        }

                        object.put("qosTest", testData);
                    }

                    if(service.isNdtTest()){
                        NdtStatus ndtStatus = service.getNdtStatus();
                        object.put("ndtData", ndtStatus == null ? NdtStatus.NOT_STARTED.ordinal() : ndtStatus.ordinal());
                    }

                    testEventSink.success(object.toString());

                }else if(status == RMBTService.ServiceStatus.FINISH){


                    if(!service.getClientError().isError()) {
                        object.put("serviceStatus", SERVICE_FINISH);
                        object.put("test_uuid", service.getTestUUID());

                        if (service.isNdtTest()) {
                            NdtStatus ndtStatus = service.getNdtStatus();
                            object.put("ndtData", ndtStatus == null ? NdtStatus.NOT_STARTED.ordinal() : ndtStatus.ordinal());
                        }
                    }else{
                        object.put("serviceStatus", TEST_FAILED);

                        ClientError clientError = service.getClientError();

                        JSONObject clientErrorJson = new JSONObject();
                        clientErrorJson.put("error_message", clientError.getErrorMessage());
                        clientErrorJson.put("connection_error", clientError.isConnectionError());

                        object.put("client_error", clientErrorJson);
                    }

                    testEventSink.success(object.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private final Runnable continueTask = new Runnable() {
        @Override
        public void run() {
            service.continueTest();
            handler.postDelayed(updateTask, UPDATE_DELAY);
        }
    };

    private final ControlServerListener serverListener = new ControlServerListener() {
        @Override
        public void sendServerInfo(ControlServerInfo serverInfo) {
            if(infoEventSink != null){

                uiThreadHandler.post(() -> {
                    try {

                        JSONObject jsonServerInfo = new JSONObject();

                        jsonServerInfo.put("sending_data", serverInfo.isSendingData());
                        jsonServerInfo.put("speed_status", serverInfo.getSpeedDataStatus().ordinal());
                        jsonServerInfo.put("qos_status", serverInfo.getQosDataStatus().ordinal());
                        jsonServerInfo.put("ndt_status", serverInfo.getNdtDataStatus().ordinal());

                        if(!serverInfo.dataEmpty()){
                            Map<ControlServerInfo.TestTask, ResultData> data = serverInfo.getData();
                            JSONArray dataJsonArray = new JSONArray();
                            for(ControlServerInfo.TestTask testTask: data.keySet()){
                                JSONObject testData = new JSONObject();
                                testData.put("test_task", testTask.ordinal());

                                ResultData resultData = data.get(testTask);
                                if(resultData != null){
                                    JSONObject jsonResultData = new JSONObject();

                                    jsonResultData.put("status",resultData.getStatus().ordinal());
                                    jsonResultData.put("errMsg", resultData.getErrMsg());
                                    testData.put("result_data", jsonResultData);

                                }

                                dataJsonArray.put(testData);
                            }

                            jsonServerInfo.put("results", dataJsonArray);
                        }

                        infoEventSink.success(jsonServerInfo.toString());

                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                });

            }
        }
    };

    public RunTask(final FlutterEngine flutterEngine, final RMBTService service){
        this.flutterEngine = flutterEngine;
        this.service = service;
        this.service.setServerListener(serverListener);

        initReceivers();
        initOffMethodChannel();

        handler = new Handler();
    }

    private void initOffMethodChannel(){
        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), OFF_RESULTS_CHANNEL).setMethodCallHandler((call, result) -> {
            if(call.method.equals("getResults")){
                TotalTestResult totalTestResult = service.getTotalResult();
                NetMeterSettings settings = service.getSettings();
                if(intermediateResult == null || settings == null){
                    result.error("NO_RESULTS", "Data is empty", null);
                    return;
                }

                try {
                    JSONObject resultData = new JSONObject();
                    JSONObject speedData = new JSONObject();

                    JSONArray measurement = new JSONArray();
                    JSONArray net = new JSONArray();

                    NetworkData networkData = service.getLastNetworkData();
                    int networkType = networkData.getNetworkType();
                    net.put(new JSONObject()
                        .put("title", "Network type")
                        .put("value", networkType == InformationCollector.NETWORK_WIFI ? "WLAN" : networkType == TelephonyManager.NETWORK_TYPE_UNKNOWN ? "Unknown" : "Mobile")
                    );

                    net.put(new JSONObject()
                        .put("title", networkType == InformationCollector.NETWORK_WIFI ? "SSID" : networkType == TelephonyManager.NETWORK_TYPE_UNKNOWN ? "-" : "Operator")
                        .put("value", networkData.getOperatorName())
                    );


                    measurement.put(new JSONObject()
                        .put("title", "Download")
                        .put("classification", Classification.classify(Classification.THRESHOLD_DOWN, totalTestResult.speed_download))
                        .put("value", mbs(totalTestResult.getDownloadSpeedBitPerSec()))
                    );

                    measurement.put(new JSONObject()
                        .put("title", "Upload")
                        .put("classification",  Classification.classify(Classification.THRESHOLD_UP, totalTestResult.speed_upload))
                        .put("value", mbs(totalTestResult.getUploadSpeedBitPerSec()))
                    );

                    measurement.put(new JSONObject()
                            .put("title", "Ping")
                            .put("classification", Classification.classify(Classification.THRESHOLD_PING, (double) totalTestResult.ping_shortest))
                            .put("value", signalMS(totalTestResult.ping_shortest))
                    );

                    if(settings.isRunJitter() && totalTestResult.jitterStatus == JitterResult.OK){
                        double jitter =  Math.round((totalTestResult.downJitterMedian.doubleValue() + totalTestResult.upJitterMedian.doubleValue()) / 2);
                        measurement.put(new JSONObject()
                            .put("title", "Jitter")
                            .put("classification", Classification.classify(Classification.THRESHOLD_JITTER, jitter))
                            .put("value",  String.format(Locale.ENGLISH,"%.2f ms", (jitter / 1000000)))
                        );

                        double packetLossPercent = packetLost(intermediateResult.numPacketDown, intermediateResult.totalPacketDown, intermediateResult.numPacketUp, intermediateResult.totalPacketUp);
                        measurement.put(new JSONObject()
                            .put("title", "Packet Loss")
                            .put("classification", Classification.classify(Classification.THRESHOLD_PACKET_LOSS, packetLossPercent * 1000))
                            .put("value", String.format(Locale.ENGLISH,"%.2f %%", packetLossPercent))
                        );
                    }

                    List<InformationCollector.SignalItem> signals = service.getSignals();
                    if(signals != null && !signals.isEmpty()) {
                        int signalSum = 0;
                        Classification.ClassificationSignalType signalType = null;

                        for(InformationCollector.SignalItem signalItem: signals){
                            int signal;
                            if(InformationCollector.isMobileNetwork(signalItem.networkId)){
                                signal = signalItem.rsrp != Integer.MIN_VALUE ? signalItem.rsrp : signalItem.signalStrength;
                                signalType =signalItem.rsrp != Integer.MIN_VALUE ? Classification.ClassificationSignalType.LTE : Classification.ClassificationSignalType.MOBILE;
                            }else {
                                signal = signalItem.wifiRssi;
                                signalType = Classification.ClassificationSignalType.WIFI;
                            }


                            if(signal != Integer.MIN_VALUE)
                                signalSum += signal;
                        }

                        if(signalSum != 0 || signalType == null) {
                            int finalSignal = signalSum / signals.size();

                            measurement.put(new JSONObject()
                                    .put("title", "Signal")
                                    .put("classification", Classification.classifySignal(signalType, finalSignal))
                                    .put("value", finalSignal + " dBm")
                            );
                        }
                    }

                    if(settings.isRunQoS()){
                        resultData.put("qos", service.getQosResult().toJson());
                    }

                    speedData.put("measurement", measurement);
                    speedData.put("net", net);
                    resultData.put("speed", speedData);

                    result.success(resultData.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                    result.error("JSON_ERR", "Json error", null);
                }
            }else{
                result.notImplemented();
            }
        });
    }

    private double packetLost(float down, float downTotal, float up, float upTotal){
        return (100f - ((down + up) / (downTotal + upTotal)) * 100f);
    }

    private String mbs(double perSec){
        return String.format(Locale.ENGLISH,"%.2f Mbit/s", (perSec / 1000000.0));
    }

    private String signalMS(long nano){
        return Math.round((nano / 1000000.0)) + " ms";
    }

    private void initReceivers(){

        new EventChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), TEST_RECEIVER_CHANNEL).setStreamHandler(
                new EventChannel.StreamHandler() {
                    @Override
                    public void onListen(Object o, final EventChannel.EventSink eventSink) {
                        RunTask.this.testEventSink = eventSink;
                        handler.post(updateTask);
                    }

                    @Override
                    public void onCancel(Object o) {
                        Log.d(LOG_TAG, "cancel test listener");
                    }
                }
        );

        new EventChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), INFO_RECEIVER_CHANNEL).setStreamHandler(
                new EventChannel.StreamHandler() {
                    @Override
                    public void onListen(Object arguments, EventChannel.EventSink events) {
                        RunTask.this.infoEventSink = events;
                    }

                    @Override
                    public void onCancel(Object arguments) {
                        Log.d(LOG_TAG, "cancel info listener");
                    }
                }
        );

    }

    public void continueTest(){
        continueTask.run();
    }

    public void stop(){
        service.onDestroy();
        if(handler != null) {
            handler.removeCallbacks(updateTask);
            handler = null;
        }
    }

}
