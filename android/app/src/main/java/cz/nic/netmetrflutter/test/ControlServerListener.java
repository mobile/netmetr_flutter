package cz.nic.netmetrflutter.test;

public interface ControlServerListener {
    public void sendServerInfo(ControlServerInfo serverInfo);
}
