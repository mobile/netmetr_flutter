package cz.nic.netmetrflutter.test;

import at.alladin.rmbt.client.helper.ResultData;

import java.util.HashMap;
import java.util.Map;

public class ControlServerInfo {
    public enum TaskStatus{
        WAIT, RUNNING, FINISHED
    }

    public enum TestTask{
        SPEED, QOS, NDT
    }

    private TaskStatus speedDataStatus = TaskStatus.WAIT;
    private TaskStatus qosDataStatus = TaskStatus.WAIT;
    private TaskStatus ndtDataStatus = TaskStatus.WAIT;

    private Map<TestTask, ResultData> data = new HashMap<>();

    public void addResultData(TestTask testTask, ResultData resultData){
        data.put(testTask, resultData);
    }

    public TaskStatus getSpeedDataStatus() {
        return speedDataStatus;
    }

    public void setSpeedDataStatus(TaskStatus speedDataStatus) {
        this.speedDataStatus = speedDataStatus;
    }

    public TaskStatus getQosDataStatus() {
        return qosDataStatus;
    }

    public void setQosDataStatus(TaskStatus qosDataStatus) {
        this.qosDataStatus = qosDataStatus;
    }

    public TaskStatus getNdtDataStatus() {
        return ndtDataStatus;
    }

    public void setNdtDataStatus(TaskStatus ndtDataStatus) {
        this.ndtDataStatus = ndtDataStatus;
    }

    public boolean isSendingData(){
        return speedDataStatus == TaskStatus.RUNNING || qosDataStatus == TaskStatus.RUNNING || ndtDataStatus == TaskStatus.RUNNING;
    }

    public Map<TestTask, ResultData> getData() {
        return data;
    }

    public boolean dataEmpty(){
        return data.isEmpty();
    }
}
