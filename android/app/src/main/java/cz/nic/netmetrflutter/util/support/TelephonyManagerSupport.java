/*******************************************************************************
 * Copyright 2015 SPECURE GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package cz.nic.netmetrflutter.util.support;

import java.util.List;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.telephony.TelephonyManager;
import androidx.core.app.ActivityCompat;

/**
 *
 * @author lb
 *
 */
public abstract class TelephonyManagerSupport {

    protected final TelephonyManager telephonyManager;
    protected final Context context;

    public TelephonyManagerSupport(final TelephonyManager telephonyManager, final Context context) {
        this.telephonyManager = telephonyManager;
        this.context = context;
    }

    public abstract List<CellInfoSupport> getAllCellInfo();

    public boolean isCoarseLocationPermitted(Context context) {

        boolean accessToLocationGranted = false;
        if (context != null) {
            accessToLocationGranted = (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
        }

        return accessToLocationGranted;
    }
}
