/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.netmetrflutter.util.net;

import android.annotation.TargetApi;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import androidx.annotation.RequiresApi;

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
public class NetworkManagerV21 extends NetworkListener {

    private Handler mainHandler;
    private ConnectivityManager connectivityManager;
    private ConnectivityManager.NetworkCallback callback = new ConnectivityManager.NetworkCallback(){

        @Override
        public void onAvailable(Network network) {
            super.onAvailable(network);
            postNetworkEvent();
            Log.i(getClass().getName(), "NETWORK AVAILABLE");
        }

        @Override
        public void onLost(Network network) {
            super.onLost(network);
            postNetworkEvent();
            Log.i(getClass().getName(), "NETWORK LOST");

        }


    };

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public NetworkManagerV21(ConnectivityManager connectivityManager, Runnable onChange){
        super(onChange);
        this.connectivityManager = connectivityManager;
        mainHandler = new Handler(Looper.getMainLooper());
    }


    @Override
    public void registerListener() {
        NetworkRequest.Builder builder = new NetworkRequest.Builder();
        builder.addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET);
        builder.addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR);
        builder.addTransportType(NetworkCapabilities.TRANSPORT_WIFI);
        builder.addTransportType(NetworkCapabilities.TRANSPORT_VPN);

        connectivityManager.registerNetworkCallback(builder.build(), callback);
    }

    @Override
    public void unregisterListener() {
        connectivityManager.unregisterNetworkCallback(callback);
    }

    private void postNetworkEvent(){
        if(onChanged != null) {
            mainHandler.post(onChanged);
        }
    }
}
