/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.netmetrflutter.test;

import android.annotation.SuppressLint;
import android.app.*;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.drawable.Icon;
import android.location.Location;
import android.os.*;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import cz.nic.netmetrflutter.MainActivity;
import cz.nic.netmetrflutter.R;
import cz.nic.netmetrflutter.util.GeoLocation;
import cz.nic.netmetrflutter.util.NotificationIDs;

import java.text.MessageFormat;

public class RMBTLoopService extends Service {

    private static final String TAG = "RMBTLoopService";
    private static final String CHANNEL_ID = "netmetr_notification";

    private PowerManager.WakeLock partialWakeLock;
    private PowerManager.WakeLock dimWakeLock;

    private RMBTLoopServiceListener rmbtLoopServiceListener;

    public interface RMBTLoopServiceListener {
        void onServiceStarted();
        void onServiceStopped();
    }

    public class RMBTLoopBinder extends Binder
    {
        public RMBTLoopService getService()
        {
            return RMBTLoopService.this;
        }
    }

    private class LocalGeoLocation extends GeoLocation
    {
        public LocalGeoLocation(Context ctx)
        {
            super(ctx, /*ConfigHelper.isLoopModeGPS(ctx)*/false, true, 10000, maxMovement); // TODO: smaller than maxMovement for minDistance??
        }

        @Override
        public void onLocationChanged(Location curLocation)
        {
            if (lastTestLocation != null)
            {
                final float distance = curLocation.distanceTo(lastTestLocation);
                Log.d(TAG, "location distance: " + distance);
                if (distance >= maxMovement)
                    onAlarmOrLocation();
            }
            lastLocation = curLocation;
        }
    }

    private class Receiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if (maxTests == 0 || numberOfTests < maxTests)
                setAlarm(maxDelay);
            else
                stopSelf();
        }
    }

    private static final String ACTION_ALARM = "at.alladin.rmbt.android.Alarm";
    private static final String ACTION_WAKEUP_ALARM = "at.alladin.rmbt.android.WakeupAlarm";
    public static final String ACTION_STOP = "at.alladin.rmbt.android.Stop";

    private static final long ACCEPT_INACCURACY = 1000; // accept 1 sec inaccuracy
    private static int DEFAULT_MAX_TESTS = 100;
    private static long DEFAULT_MIN_DELAY = 30;
    private static long DEFAULT_MAX_DELAY = 900;
    private static float DEFAULT_MOVEMENTS_METERS = 250;


    private final RMBTLoopBinder localBinder = new RMBTLoopBinder();

    private AlarmManager alarmManager;
    private PendingIntent alarm;
    private PendingIntent wakeupAlarm;
    private NotificationManager notificationManager;
    private Notification.Builder notificationBuilder;

    private LocalGeoLocation geoLocation;
    private Receiver receiver = new Receiver();

    private int numberOfTests;

    private Location lastLocation;
    private Location lastTestLocation;
    private long lastTestTime; // SystemClock.elapsedRealtime()

    private String uuid;
    private String controlServer;
    private int port;
    private boolean useSSL;
    private boolean qosSSL;
    private boolean onlySignal;

    private long minDelay;
    private long maxDelay;
    private float maxMovement;
    private int maxTests;

    private boolean runJitter = true;
    private boolean runQoS = true;

    @Override
    public IBinder onBind(Intent intent)
    {
        return localBinder;
    }

    private void triggerTest()
    {
        numberOfTests++;
        lastTestLocation = lastLocation;
        lastTestTime = SystemClock.elapsedRealtime();
        final Intent service = new Intent(RMBTService.ACTION_LOOP_TEST, null, this, RMBTService.class);

        service.putExtra("uuid", uuid);
        service.putExtra("controlServer", controlServer);
        service.putExtra("port", port);
        service.putExtra("useSSL", useSSL);
        service.putExtra("qosSSL", qosSSL);
        service.putExtra("loopMode", true);
        service.putExtra("onlySignal", onlySignal);
        service.putExtra("qos", runQoS);
        service.putExtra("jitter", runJitter);

        startService(service);

        updateNotification();
    }


    public void setRmbtLoopServiceListener(RMBTLoopServiceListener rmbtLoopServiceListener) {
        this.rmbtLoopServiceListener = rmbtLoopServiceListener;
    }

    @SuppressLint("InvalidWakeLockTag")
    @Override
    public void onCreate()
    {
        Log.d(TAG, "created");
        super.onCreate();

        partialWakeLock = ((PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE)).newWakeLock(
                PowerManager.PARTIAL_WAKE_LOCK, "RMBTLoopWakeLock");
        partialWakeLock.acquire();

        dimWakeLock = ((PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE)).newWakeLock(
                805306374, "RMBTLoopDimWakeLock");
        dimWakeLock.acquire();

        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel( new NotificationChannel(CHANNEL_ID, "Netmetr title", NotificationManager.IMPORTANCE_DEFAULT));
        }


        geoLocation = new LocalGeoLocation(this);
        geoLocation.start();

        notificationBuilder = createNotificationBuilder();

        startForeground(NotificationIDs.LOOP_ACTIVE, notificationBuilder.build());
        registerReceiver(receiver, new IntentFilter(RMBTService.BROADCAST_TEST_FINISHED));

        int flag = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
            flag = PendingIntent.FLAG_MUTABLE;
        }

        final Intent alarmIntent = new Intent(ACTION_ALARM, null, this, getClass());
        alarm = PendingIntent.getService(this, 0, alarmIntent, flag);

        final Intent wakeupAlarmIntent = new Intent(ACTION_WAKEUP_ALARM, null, this, getClass());
        wakeupAlarm = PendingIntent.getService(this, 0, wakeupAlarmIntent, flag);

        final long now = SystemClock.elapsedRealtime();
        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, now + 10000, 10000, wakeupAlarm);
    }

    private void setAlarm(long millis)
    {
        Log.d(TAG, "setAlarm: " + millis);

        final long now = SystemClock.elapsedRealtime();
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, now + millis, alarm);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.d(TAG, "onStartCommand: " + intent);

        //readConfig();
        if (intent != null)
        {
            if(rmbtLoopServiceListener != null) rmbtLoopServiceListener.onServiceStarted();
            final String action = intent.getAction();
            if (action != null && action.equals(ACTION_STOP)) {
                final Intent service = new Intent(RMBTService.ACTION_ABORT_TEST, null, this, RMBTService.class);
                startService(service);
                stopSelf();
            }
            else if (action != null && action.equals(ACTION_ALARM))
                onAlarmOrLocation();
            else if (action != null && action.equals(ACTION_WAKEUP_ALARM))
                onWakeup();
            else
            {
                if (lastTestTime == 0)
                {
                    uuid =  intent.getStringExtra("uuid");
                    controlServer = intent.getStringExtra("controlServer");
                    port = intent.getIntExtra("port", 443);
                    useSSL = intent.getBooleanExtra("useSSL", false);
                    qosSSL = intent.getBooleanExtra("qosSSL", false);

                    maxDelay = intent.getLongExtra("maxDelay", DEFAULT_MAX_DELAY) * 1000;
                    minDelay = intent.getLongExtra("minDelay", DEFAULT_MIN_DELAY) * 1000;
                    maxMovement = intent.getFloatExtra("maxMovement", DEFAULT_MOVEMENTS_METERS);
                    maxTests = intent.getIntExtra("maxTests", DEFAULT_MAX_TESTS);
                    onlySignal = intent.getBooleanExtra("onlySignal", false);
                    runJitter = intent.getBooleanExtra("jitter", true);
                    runQoS = intent.getBooleanExtra("qos", true);

                    Log.i(TAG, "START LOOP..... \n"
                            + "uuid: " + uuid + "\n"
                            + "controlServer: " + controlServer + "\n"
                            + "port: " + port + "\n"
                            + "useSSL: " + useSSL + "\n"
                            + "qosSSL: " + qosSSL + "\n"
                            + "minDelay: " + minDelay + "\n"
                            + "maxDelay: " + maxDelay + "\n"
                            + "maxMovement: " + maxMovement + "\n"
                            + "maxTests: " + maxTests + "\n"
                            + "onlySignal: " + onlySignal + "\n"
                            + "runJitter: " + runJitter + "\n"
                            + "runQos: " + runQoS + "\n");
                    Toast.makeText(this, R.string.loop_started, Toast.LENGTH_LONG).show();

                    onAlarmOrLocation();
                }
                else{
                    Toast.makeText(this, R.string.loop_already_active, Toast.LENGTH_LONG).show();

                }
            }
        }
        return START_NOT_STICKY;
    }

    @SuppressLint("Wakelock")
    private void onWakeup()
    {
        if (dimWakeLock != null)
        {
            if (dimWakeLock.isHeld())
                dimWakeLock.release();
            dimWakeLock.acquire();
        }
    }

    private void onAlarmOrLocation()
    {
        if (lastTestTime == 0)
        {
            triggerTest();
            return;
        }
        final long now = SystemClock.elapsedRealtime();
        final long lastTestDelta = now - lastTestTime;
        if (lastTestDelta + ACCEPT_INACCURACY >= minDelay)
        {
            triggerTest();
            return;
        }
        setAlarm(minDelay - lastTestDelta);
    }

    @Override
    public void onDestroy()
    {
        if(rmbtLoopServiceListener != null) rmbtLoopServiceListener.onServiceStopped();
        if (partialWakeLock != null && partialWakeLock.isHeld())
            partialWakeLock.release();
        if (dimWakeLock != null && dimWakeLock.isHeld())
            dimWakeLock.release();
        Log.d(TAG, "destroyed");
        super.onDestroy();
        unregisterReceiver(receiver);
        stopForeground(true);
        if (geoLocation != null)
            geoLocation.stop();
        if (alarmManager != null)
        {
            alarmManager.cancel(alarm);
            alarmManager.cancel(wakeupAlarm);
        }
    }

    private Notification.Builder createNotificationBuilder()
    {
        final Resources res = getResources();

        final CharSequence textTemplate = res.getText(R.string.loop_notification_text);
        final CharSequence text = MessageFormat.format(textTemplate.toString(), numberOfTests);

        final Notification.Builder builder;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int contentFlag = 0;
            int stopIntentFlag = PendingIntent.FLAG_CANCEL_CURRENT;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
                contentFlag = PendingIntent.FLAG_MUTABLE;
                stopIntentFlag = PendingIntent.FLAG_MUTABLE;
            }

            final PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(
                    getApplicationContext(), MainActivity.class), contentFlag);

            final Intent stopIntentReceive = new Intent(this, NotificationReceiver.class);
            stopIntentReceive.setAction(NotificationReceiver.ABORT_ACTION);
            stopIntentReceive.putExtra("loop", true);
            final PendingIntent stopIntent = PendingIntent.getBroadcast(getApplicationContext(), NotificationReceiver.REQ_LOOP_CODE, stopIntentReceive, stopIntentFlag);

            builder = new Notification.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.stat_icon_loop)
                    .setContentTitle(res.getText(R.string.loop_notification_title))
                    .setTicker(res.getText(R.string.loop_notification_ticker))
                    .addAction(new Notification.Action.Builder(Icon.createWithResource(getApplicationContext(), R.drawable.stat_icon_test), text, stopIntent).build())
                    .setChannelId(CHANNEL_ID)
                    .setContentIntent(contentIntent)
                    .setAutoCancel(true);
        }else{
            final Intent stopIntent = new Intent(ACTION_STOP, null, getApplicationContext(), getClass());
            final PendingIntent stopPIntent = PendingIntent.getService(getApplicationContext(), 0, stopIntent, 0);

            builder = new Notification.Builder(this)
                    .setSmallIcon(R.drawable.stat_icon_loop)
                    .setContentTitle(res.getText(R.string.loop_notification_title))
                    .setContentText(text)
                    .setTicker(res.getText(R.string.loop_notification_ticker))
                    .setContentIntent(stopPIntent);
        }

        return builder;
    }


    private void updateNotification()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            final Notification notification = notificationBuilder
                    .setChannelId(CHANNEL_ID)
                    .setNumber(numberOfTests)
                    .build();
            notificationManager.notify(NotificationIDs.LOOP_ACTIVE, notification);

        }else {
            final Notification notification = notificationBuilder.setNumber(numberOfTests).build();
            notificationManager.notify(NotificationIDs.LOOP_ACTIVE, notification);

        }
    }
}
