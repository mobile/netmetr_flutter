/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.netmetrflutter;

import android.util.Log;

public class NetmetrConfig {
    private static String TAG = "CONFIG";

    private String sentryUrl;
    private String sentryDSN;
    private String uuid;
    private String clientSecret;
    private String controlHostDev;
    private String controlIpv4Host;
    private String controlIpv6Host;
    private String checkIpv4Host;
    private String checkIpv6Host;
    private int serverPort;

    private String googleAPIKey;

    private boolean controlSSL;
    private boolean qosSSL;

    private static NetmetrConfig instance;

    public static NetmetrConfig getInstance() {
        if (instance == null) {
            String exceptionMessage = "NetmetrConfig is not initialized!";
            Log.e(TAG, exceptionMessage);
        }
        return instance;
    }

    public static void init(){
        if(instance != null){
            String exceptionMessage = "NetmetrConfig already initialized!";
            Log.e(TAG, exceptionMessage);
            return;
        }

        instance = new NetmetrConfig();
        instance.sentryDSN = BuildConfig.RMBT_SENTRY_DSN;
        instance.clientSecret = BuildConfig.RMBT_CLIENT_SECRET;
        instance.controlHostDev = BuildConfig.RMBT_CONTROL_HOST;
        instance.controlIpv4Host = BuildConfig.RMBT_CONTROL_IPV4_HOST;
        instance.controlIpv6Host = BuildConfig.RMBT_CONTROL_IPV6_HOST;
        instance.checkIpv4Host =  BuildConfig.RMBT_CONTROL_IPV4_CHECK_HOST;
        instance.checkIpv6Host = BuildConfig.RMBT_CONTROL_IPV6_CHECK_HOST;
        instance.serverPort = BuildConfig.RMBT_CONTROL_PORT;
        instance.controlSSL =  BuildConfig.RMBT_CONTROL_SSL;
        instance.qosSSL = BuildConfig.RMBT_QOS_SSL;
        instance.sentryUrl = BuildConfig.SENTRY_URL;
    }
    
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getControlHostDev() {
        return controlHostDev;
    }

    public void setControlHostDev(String controlHostDev) {
        this.controlHostDev = controlHostDev;
    }

    public String getControlIpv4Host() {
        return controlIpv4Host;
    }

    public void setControlIpv4Host(String controlIpv4Host) {
        this.controlIpv4Host = controlIpv4Host;
    }

    public String getControlIpv6Host() {
        return controlIpv6Host;
    }

    public void setControlIpv6Host(String controlIpv6Host) {
        this.controlIpv6Host = controlIpv6Host;
    }

    public String getCheckIpv4Host() {
        return checkIpv4Host;
    }

    public void setCheckIpv4Host(String checkIpv4Host) {
        this.checkIpv4Host = checkIpv4Host;
    }

    public String getCheckIpv6Host() {
        return checkIpv6Host;
    }

    public void setCheckIpv6Host(String checkIpv6Host) {
        this.checkIpv6Host = checkIpv6Host;
    }

    public int getServerPort() {
        return serverPort;
    }


    public boolean isControlSSL() {
        return controlSSL;
    }

    public boolean isQosSSL() {
        return qosSSL;
    }

    public String getSentryDSN() {
        return sentryDSN;
    }

    public String getSentryUrl() {
        return sentryUrl;
    }

    public void setSentryUrl(String sentryUrl) {
        this.sentryUrl = sentryUrl;
    }
}
