package cz.nic.netmetrflutter.test;

import android.util.Log;

import androidx.annotation.NonNull;

public class NetMeterSettings {

    private final String uuid;
    private final String controlServer;
    private final int port;
    private final boolean useSSL;
    private final boolean qosSSL;
    private final boolean onlySignal;
    private final boolean runJitter;
    private final boolean runQoS;
    private final boolean runNDT;

    NetMeterSettings(String uuid, String controlServer, int port, boolean useSSL, boolean qosSSL, boolean onlySignal, boolean runJitter, boolean runQoS, boolean runNDT){
        this.uuid = uuid;
        this.controlServer = controlServer;
        this.port = port;
        this.useSSL = useSSL;
        this.qosSSL = qosSSL;
        this.onlySignal = onlySignal;
        this.runJitter = runJitter;
        this.runQoS = runQoS;
        this.runNDT = runNDT;
    }

    @NonNull
    @Override
    public String toString() {
        return "UUID:"  + uuid + "\n" +
                "ControlServer: " + controlServer + "\n" +
                "Port: " + port + "\n" +
                "UseSSL: " + useSSL + "\n" +
                "QosSSL: " + qosSSL + "\n" +
                "Only Signal: " + onlySignal + "\n" +
                "Jitter: " + runJitter + "\n" +
                "QoS: " + runQoS + "\n" +
                "NDT: " + runNDT + "\n";
    }

    public String getControlServer() {
        return controlServer;
    }

    public int getPort() {
        return port;
    }

    public boolean isUseSSL() {
        return useSSL;
    }

    public String getUuid() {
        return uuid;
    }

    public boolean isQosSSL() {
        return qosSSL;
    }

    public boolean isOnlySignal() {
        return onlySignal;
    }

    public boolean isRunJitter() {
        return runJitter;
    }

    public boolean isRunQoS() {
        return runQoS;
    }

    public boolean isRunNDT() {
        return runNDT;
    }
}
