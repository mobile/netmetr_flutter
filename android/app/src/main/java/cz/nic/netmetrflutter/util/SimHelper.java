/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.netmetrflutter.util;

import android.content.Context;
import android.content.pm.PackageManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import androidx.core.content.ContextCompat;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import static android.Manifest.permission.READ_PHONE_STATE;

class SimHelper {

    public static String getActiveSimForMobileData(Context context){
        String currentSsid = null;

        TelephonyManager telManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        String sim1 = SimHelper.getOutput(context, "SimOperatorName", 0);
        String sim2 = SimHelper.getOutput(context, "SimOperatorName", 1);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP_MR1 ) {

            if (ContextCompat.checkSelfPermission( context, READ_PHONE_STATE ) == PackageManager.PERMISSION_GRANTED) {
                SubscriptionManager subscriptionManager = (SubscriptionManager) context.getApplicationContext().getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
                int simDataId = SimHelper.getDefaultDataSubscriptionId(subscriptionManager);

                if (ContextCompat.checkSelfPermission( context, READ_PHONE_STATE ) == PackageManager.PERMISSION_GRANTED) {

                    List<SubscriptionInfo> subscriptionInfoList = subscriptionManager.getActiveSubscriptionInfoList();


                    if(subscriptionInfoList != null) {
                        for (SubscriptionInfo subscriptionInfo : subscriptionInfoList) {
                            if (subscriptionInfo.getSubscriptionId() == simDataId) {
                                currentSsid = subscriptionInfo.getCarrierName().toString();
                                break;
                            }
                        }
                    }

                    if(currentSsid == null)
                        currentSsid = telManager.getNetworkOperatorName();

                }else {
                    switch (simDataId) {
                        case 0:
                            if (sim1 != null) currentSsid = sim1;
                            else telManager.getNetworkOperatorName();
                            break;
                        case 1:
                            if (sim2 != null) currentSsid = sim2;
                            else telManager.getNetworkOperatorName();
                            break;
                        default:
                            currentSsid = telManager.getNetworkOperatorName();
                    }
                }

            }else {
                currentSsid = telManager.getNetworkOperatorName();

            }
        }else {
            currentSsid = telManager.getNetworkOperatorName();
        }

        return currentSsid;
    }

    public static int getDefaultDataSubscriptionId(final SubscriptionManager subscriptionManager)  {
        if (android.os.Build.VERSION.SDK_INT >= 24)  {
            int nDataSubscriptionId = SubscriptionManager.getDefaultDataSubscriptionId();

            if (nDataSubscriptionId != SubscriptionManager.INVALID_SUBSCRIPTION_ID)  {
                return (nDataSubscriptionId);
            }
        }

        try  {
            Class<?> subscriptionClass = Class.forName(subscriptionManager.getClass().getName());
            try {
                Method getDefaultDataSubscriptionId = subscriptionClass.getMethod("getDefaultDataSubId");

                try {
                    return ((int) getDefaultDataSubscriptionId.invoke(subscriptionManager));
                }
                catch (IllegalAccessException e1) {
                    e1.printStackTrace();
                } catch (InvocationTargetException e1) {
                    e1.printStackTrace();
                }
            } catch (NoSuchMethodException e1) {
                e1.printStackTrace();
            }
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        }

        return (SubscriptionManager.INVALID_SUBSCRIPTION_ID);
    }

    public static String getOutput(Context context, String methodName, int slotId) {
        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        Class<?> telephonyClass;
        String reflectionMethod = null;
        String output = null;
        try {
            telephonyClass = Class.forName(telephony.getClass().getName());
            for (Method method : telephonyClass.getMethods()) {
                String name = method.getName();
                if (name.contains(methodName)) {
                    Class<?>[] params = method.getParameterTypes();
                    if (params.length == 1 && params[0].getName().equals("int")) {
                        reflectionMethod = name;
                    }
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (reflectionMethod != null) {
            try {
                output = getOpByReflection(telephony, reflectionMethod, slotId, false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return output;
    }

    private static String getOpByReflection(TelephonyManager telephony, String predictedMethodName, int slotID, boolean isPrivate) {

        //Log.i("Reflection", "Method: " + predictedMethodName+" "+slotID);
        String result = null;

        try {

            Class<?> telephonyClass = Class.forName(telephony.getClass().getName());

            Class<?>[] parameter = new Class[1];
            parameter[0] = int.class;
            Method getSimID;
            if (slotID != -1) {
                if (isPrivate) {
                    getSimID = telephonyClass.getDeclaredMethod(predictedMethodName, parameter);
                } else {
                    getSimID = telephonyClass.getMethod(predictedMethodName, parameter);
                }
            } else {
                if (isPrivate) {
                    getSimID = telephonyClass.getDeclaredMethod(predictedMethodName);
                } else {
                    getSimID = telephonyClass.getMethod(predictedMethodName);
                }
            }

            Object ob_phone;
            Object[] obParameter = new Object[1];
            obParameter[0] = slotID;
            if (getSimID != null) {
                if (slotID != -1) {
                    ob_phone = getSimID.invoke(telephony, obParameter);
                } else {
                    ob_phone = getSimID.invoke(telephony);
                }

                if (ob_phone != null) {
                    result = ob_phone.toString();

                }
            }
        } catch (Exception e) {
            //e.printStackTrace();
            return null;
        }
        //Log.i("Reflection", "Result: " + result);
        return result;
    }

}
