/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  RMBTHistoryResult.swift
//  RMBT
//
//  Created by Benjamin Pucher on 31.03.15.
//  Copyright © 2015 SPECURE GmbH. All rights reserved.
//

import Foundation
import CoreLocation


///
class RMBTHistoryResultItem {
    
    ///
    let title: String
    
    ///
    let value: String
    
    ///
    let classification: Int
    
    //
    
    ///
    init(response: [String: AnyObject]) {
        self.title = response["title"] as! String
        self.value = response["value"]!.description
        
        if let responseClassification = response["classification"] as? NSNumber {
            self.classification = responseClassification.intValue
        } else {
            self.classification = -1
        }
    }
}

///
enum RMBTHistoryResultDataState {
    case Index
    case Basic
    case Full
}

///
class RMBTHistoryResult {
    
    ///
    var dataState: RMBTHistoryResultDataState = .Index
    
    var uuid: String!
    var timestamp: NSDate!
    var timeString: String = ""
    var downloadSpeedMbpsString: String!
    var uploadSpeedMbpsString: String!
    var shortestPingMillisString: String!
    var deviceModel: String!
    var coordinate: CLLocationCoordinate2D!
    var locationString: String!
    
    /// "WLAN", "2G/3G" etc.
    let networkTypeServerDescription: String!
    
    /// Available in basic details
    var networkType: RMBTNetworkType!
    var shareText: String!
    var shareURL: NSURL!
    var netItems = [RMBTHistoryResultItem]()
    var measurementItems = [RMBTHistoryResultItem]()
    
    /// Full details
    var fullDetailsItems = [RMBTHistoryResultItem]()
    
    //
    
    ///
    private var currentYearFormatter = DateFormatter()
    
    ///
    private var previousYearFormatter = DateFormatter()
    
    //
    
    ///
    init(response: [String:AnyObject]) { // this methods takes only ["test_uuid": ...] after a new test...
        downloadSpeedMbpsString = response["speed_download"] as? String
        uploadSpeedMbpsString = response["speed_upload"] as? String
        shortestPingMillisString = response["ping_shortest"] as? String
        
        // Note: here network_type is a string with full description (i.e. "WLAN") and in the basic details response it's a numeric code
        networkTypeServerDescription = response["network_type"] as? String
        uuid = response["test_uuid"] as? String
        
        if (response["model"] as? String) != nil {
            self.deviceModel = UIDevice.current.model
        }/* else {
         self.deviceModel = "Unknown" // TODO: translate?
         }*/
        
        if let time = response["time"] as? NSNumber {
            let t: TimeInterval = time.doubleValue / 1000.0
            self.timestamp = NSDate(timeIntervalSince1970: t)
        }
        
        coordinate = kCLLocationCoordinate2DInvalid
        
        //
        
        currentYearFormatter.dateFormat = "MMM dd HH:mm"
        previousYearFormatter.dateFormat = "MMM dd YYYY"
    }
    
    ///
    func formattedTimestamp() -> String {
        _ = NSCalendar.current.dateComponents([Calendar.Component.day, Calendar.Component.month, Calendar.Component.year], from: timestamp as Date)
        let historyDateComponents = NSCalendar.current.dateComponents([Calendar.Component.day, Calendar.Component.month, Calendar.Component.year], from: timestamp as Date)
        let currentDateComponents = NSCalendar.current.dateComponents([Calendar.Component.day, Calendar.Component.month, Calendar.Component.year], from: Date())
        
        var result = ""
        
        if (currentDateComponents.year == historyDateComponents.year) {
            result = currentYearFormatter.string(from: timestamp as Date)
        } else {
            result = previousYearFormatter.string(from: timestamp as Date)
        }
        
        // For some reason MMM on iOS7 returns "Aug." with a trailing dot, let's strip the dot manually
        return result.replacingOccurrences(of: ".", with: "")
    }
    

}
