/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation
import CoreLocation
import UserNotificationsUI

///
protocol LoopDelegate {
    func loopStopped()
}

///
protocol LoopTestDelegate {
    func testFailed(index: Int)
    func testFinished(index: Int)
}

class Loop: NSObject, CLLocationManagerDelegate, FlutterStreamHandler, LoopTestDelegate{
    ///
    private var NOTIFICATIONS_ID : String = "netmetr-flutter-notification"
    private var REQUEST_ID : String = "netmetr-flutter-content"
    private var LOOP_EVENT_CHANNEL : String = "nic.cz.netmetrflutter/loopEvent"
    private var DEFAULT_MAX_TESTS : Int32 = 100
    private var DEFAULT_MAX_DELAY : Int64 = 900
    private var DEFAULT_MIN_DELAY : Int64 = 30
    private var DEFAULT_MOVEMENTS_METRES : Double = 250
    
    private var START_STAT : String = "loop start"
    private var STOP_STAT : String = "loop stop"
    
    private var NEW_TEST : String = "NEW"
    private var LOOP_FAILED: String = "FAIL"
    private var ABORTED_TEST : String = "ABORTED";

    
    ///
    private let locationManager = CLLocationManager()
    
    ///
    private var controller : FlutterViewController?
    
    ///
    private var connectivyWorker: ConnectivityWorkerDelegate?
    
    ///
    private var lastLocation: CLLocation? = nil
    
    ///
    private var lastTime: Int64? = nil
    
    ///
    private var eventSink: FlutterEventSink!
    
    ///
    private var loopDelegate: LoopDelegate!

    ///
    private var timer: Timer!
    
    ///
    private var minDelay: Int64
    
    ///
    private var maxDelay: Int64
    
    ///
    private var maxTests: Int32
    
    ///
    private var maxMovement: Double
    
    ///
    private var onlySignal: Bool
    
    ///
    private var runTests: Int = 0
    
    var tests = [RunTest]()
    
    init(delegate: LoopDelegate, controller: FlutterViewController, connectivyWorker: ConnectivityWorkerDelegate, parametrs: [String: AnyObject]) {
        self.loopDelegate = delegate
        self.controller = controller
        self.connectivyWorker = connectivyWorker
        
        maxTests = parametrs.keys.contains("maxTests") ? parametrs["maxTests"] as! Int32 : DEFAULT_MAX_TESTS
        maxDelay = parametrs.keys.contains("maxDelay") ? parametrs["maxDelay"] as! Int64 : DEFAULT_MAX_DELAY
        minDelay = parametrs.keys.contains("minDelay") ? parametrs["minDelay"] as! Int64 : DEFAULT_MIN_DELAY
        maxMovement = parametrs.keys.contains("maxMovement") ? parametrs["maxMovement"] as! Double : DEFAULT_MOVEMENTS_METRES
        onlySignal = parametrs.keys.contains("onlySignal") ? parametrs["onlySignal"] as! Bool : false
        
        super.init()
        let event = FlutterEventChannel.init(name: LOOP_EVENT_CHANNEL, binaryMessenger: controller.binaryMessenger)
        event.setStreamHandler(self)
        
        self.locationManager.delegate = self
    }
    
    func startNewTest(){
        runTests+=1
        if(runTests < maxTests || maxTests == 0){
            lastTime = Int64(Date().timeIntervalSince1970)
            
            let dispatch : DispatchQueue =  DispatchQueue.init(label: "cz.nic.netmetrflutter.loopQueue\(runTests)", attributes: .concurrent)

            tests.append(RunTest.init(controller: controller!, worker: connectivyWorker!, loopMode: true, onlySignal: onlySignal, index: runTests, loopTestDelegate: self, loopWorker: dispatch))
            dispatch.async {
                self.tests[self.runTests].startEvent()
            }
            postData(status: NEW_TEST, data: nil)
        }
    }
    
    func postData(status: String, data: Any?){
        if(eventSink == nil){
            return
        }
        
        let postData = [
            "status" : status,
            "data" : data
            ] as [String : Any?]
        
        do{
            let json = try JSONSerialization.data(withJSONObject: postData)
            let jsonString = String(data: json, encoding: .utf8)!
            
            eventSink(jsonString)
            
        } catch {
            print("JSON serialization failed: ", error)
        }
    }
    
    func startService(){
        //start
        print("[--------- Start Loop -------]")
        print("MAX TESTS: \(String(describing: maxTests))")
        print("MAX DELAY: \(String(describing: maxDelay))")
        print("MIN DELAY: \(String(describing: minDelay))")
        print("MAX MOVEMENT: \(String(describing: maxMovement))")
        print("ONLY SIGNAL: \(String(describing: onlySignal))")

        let authorizationStatus = CLLocationManager.authorizationStatus()
        
        lastTime = Int64(Date().timeIntervalSince1970)

        
        //TODO potreba kdyz development 8.0 ???
        if #available(iOS 8.0, *) {
            if (authorizationStatus == .authorizedWhenInUse || authorizationStatus == .authorizedAlways) {
                locationManager.startUpdatingLocation()
            }
        } else {
            if (authorizationStatus == CLAuthorizationStatus.authorized) {
                locationManager.startUpdatingLocation()
            }
        }
        
        timer = Timer.scheduledTimer(timeInterval: Double(minDelay), target: self, selector: #selector(self.loop), userInfo: nil, repeats: true)
        
        let dispatch : DispatchQueue = DispatchQueue.init(label: "cz.nic.netmetrflutter.loopQueue0", attributes: .concurrent)
        tests.append(RunTest.init(controller: controller! , worker: connectivyWorker!, loopMode: true, onlySignal: onlySignal, index: 0, loopTestDelegate: self, loopWorker: dispatch))
        dispatch.async {
            self.tests[0].startEvent()
        }
    }
    
    @objc
    func loop() {
        if(lastTime != nil){
            let actualTime : Int64 = Int64(Date().timeIntervalSince1970)
            let diffTime : Int64 = actualTime - lastTime!
            
            if(diffTime >= maxDelay && runTests < maxTests){
                lastLocation = locationManager.location
                startNewTest()
            }
        }
    }
    
    func appDidEnterBackground(){
        runTests = 0
        postData(status: ABORTED_TEST, data: nil)
        stopService(reason: .AppBackgrounded)
        tests = [RunTest]()
    }
    
    func stopService(reason: RMBTTestRunnerCancelReason){
        //stop
        print("[--------- Stop Loop -------]")
        
        for test in tests{
            test.stopLoop(reason: reason)
        }
        
        loopDelegate?.loopStopped()
        eventSink = nil
        locationManager.stopMonitoringSignificantLocationChanges()
        locationManager.stopUpdatingLocation()
        
        timer.invalidate()
        
        
    }
 
// MARK: location delegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let newLocation = locations.last
        
        if(lastLocation != nil){
            let distance : Double? = newLocation?.distance(from: lastLocation!)
            let actualTime : Int64 = Int64(Date().timeIntervalSince1970)
            let diffTime : Int64 = actualTime - lastTime!
           
            if let c = connectivyWorker?.connectivity(){
                if(c.networkType != .None && distance ?? 0.0 >= maxMovement && (runTests < maxTests || maxTests == 0) && diffTime >= minDelay){
                    startNewTest()
                    lastLocation = newLocation
                }
            }

        }else{
            lastLocation = newLocation
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("DETAIL ERROR \(error)")
        print(" \(error.localizedDescription)")


        postData(status: LOOP_FAILED, data: error.localizedDescription as AnyObject)
        self.stopService(reason: .UserRequested)
    }
    
//MARK: loop test delegate
    
    func testFailed(index: Int) {
        if(tests.count < index){
            tests[index].stopEvent(reason: .UserRequested)
        }
    }
    
    func testFinished(index: Int) {
        if(tests.count < index){
            tests[index].stopEvent(reason: .UserRequested)
        }
    }
    
// MARK: flutter stream handler

    func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
        self.eventSink = events
        return nil;
    }
    
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        NotificationCenter.default.removeObserver(self)
        self.eventSink = nil;
        return nil;
    }
    
}
