/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  RMBTLocationTracker.swift
//  RMBT
//
//  Created by Benjamin Pucher on 19.09.14.
//  Copyright © 2014 SPECURE GmbH. All rights reserved.
//

import Foundation
import CoreLocation

///
let RMBTLocationTrackerNotification: String = "RMBTLocationTrackerNotification"

///
class RMBTLocationTracker: NSObject, CLLocationManagerDelegate {
    
    ///
    static let sharedTracker = RMBTLocationTracker()
    
    ///
    let locationManager: CLLocationManager
    
    ///
    var authorizationCallback: RMBTBlock?
    
    ///
    var location: CLLocation? {
        // TODO: if app is not allowed to get location this code fails! WORKS without ".copy() as? CLLocation", but are there any consequences?
        if let result: CLLocation = locationManager.location/*.copy() as? CLLocation*/ {
            if (CLLocationCoordinate2DIsValid(result.coordinate)) {
                return result
            }
        }
        
        return nil
    }
    
    ///
    override private init() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 3.0
        //locationManager.requestAlwaysAuthorization()
        
        super.init()
        
        locationManager.delegate = self
    }
    
    ///
    func stop() {
        locationManager.stopMonitoringSignificantLocationChanges()
        locationManager.stopUpdatingLocation()
    }
    
    ///
    func startIfAuthorized() -> Bool {
        let authorizationStatus = CLLocationManager.authorizationStatus()
        
        if #available(iOS 8.0, *) {
            if (authorizationStatus == .authorizedWhenInUse || authorizationStatus == .authorizedAlways) {
                locationManager.startUpdatingLocation()
                return true
            }
        } else {
            if (authorizationStatus == CLAuthorizationStatus.authorized) {
                locationManager.startUpdatingLocation()
                return true
            }
        }
        
        return false
    }
    
    ///
    func startAfterDeterminingAuthorizationStatus(callback: @escaping RMBTBlock) {
        if (startIfAuthorized()) {
            callback()
        } else if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.notDetermined) {
            // Not determined yet
            authorizationCallback = callback
            
            if #available(iOS 8.0, *) {
                locationManager.requestWhenInUseAuthorization()
            } else {
                locationManager.startUpdatingLocation()
            }
        } else {
            logger.warning("User hasn't enabled or authorized location services")
            callback()
        }
    }
    
    ///
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
       /* NotificationCenter.default.post(name: NSNotification.Name(rawValue: RMBTLocationTrackerNotification), object: self, userInfo: ["locations" : locations])*/
    }
    
    ///
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if (locationManager.responds(to: #selector(CLLocationManager.startUpdatingLocation))) {
            locationManager.startUpdatingLocation()
        }
        
        if let authorizationCallback = self.authorizationCallback {
            authorizationCallback()
        }
    }
    
    ///
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        logger.error("Failed to obtain location \(error)")
    }
    
    ///
    func forceUpdate() -> Bool{
        stop()
        return startIfAuthorized()
    }
}
