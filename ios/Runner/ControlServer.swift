/*****************************************************************************************************
* Copyright 2014-2016 SPECURE GmbH
*
* Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
*
*   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
*   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
*     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
*     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
*     the executable runs, unless that component itself accompanies the executable.
*   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
*     other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************************/

//
//  ControlServer.swift
//  RMBT
//
//  Created by Benjamin Pucher on 04.02.15.
//  Copyright © 2015 SPECURE GmbH. All rights reserved.
//

import Foundation
import AFNetworking

///
class ControlServer {

///
private let LAST_NEWS_UID_PREFERENCE_KEY = "last_news_uid"

//

///
var uuidKey: String!

///
var uuid: String?

   
///
private var uuidQueue: DispatchQueue = DispatchQueue.init(label: "at.rtr.rmbt.controlserver.uuid") //TODO SERIAL TU BYLO??

///
var historyFilters: [String:[String]]!

///
var openTestBaseURL: String?

///
var ipv4RequestUrl: String?

///
var ipv6RequestUrl: String?

//

///
private var manager: AFHTTPSessionManager!

///
var lastNewsUid: UInt? {
    didSet {
        UserDefaults.standard.set(lastNewsUid, forKey: LAST_NEWS_UID_PREFERENCE_KEY)
        UserDefaults.standard.synchronize()
    }
}

///
static let sharedControlServer = ControlServer()

///
private init() {
    //updateWithCurrentSettings()
}

///
func updateWithCurrentSettings() {
    let settings = RMBTSettings.sharedSettings()
    let controlHostUrl = settings.controlHostDev!
    
    var baseURL = NSURL(string: controlHostUrl)!
    let scheme: String = settings.controlSSL ?? false ? "https" : "http"
    
    baseURL = NSURL(scheme:scheme, host: controlHostUrl, path: RMBT_CONTROL_SERVER_PATH)!

    manager = AFHTTPSessionManager(baseURL: baseURL as URL?)
    
    manager.requestSerializer = AFJSONRequestSerializer()
    manager.responseSerializer = AFJSONResponseSerializer()

    uuid = settings.clientUuid
    self.ipv4RequestUrl = settings.checkHostIpv4
    self.ipv6RequestUrl = settings.checkHostIpv6
}


/// TODO: is this in use?
func getRoamingStatusWithParams(params: NSDictionary, success: @escaping SuccessCallback) {
    logger.debug("Checking roaming status (params = \(params))")
    self.requestPost(path: "status", params: params, success: { response in
        
        // TODO: check "error" in json
        
        //                if let homeCountry = response["home_country"] as? NSNumber {
        //                    if (!homeCountry.boolValue) {
        //                        success(response: true)
        //                    }
        //                }
        //
        //                success(response: false)
        
        let roaming = (response["home_country"] as? NSNumber)?.boolValue == false
        success(roaming as AnyObject)
        
    }, error: { error, info in
        
    })
}

///
func getTestParamsWithParams(params: NSDictionary, success: @escaping SuccessCallback, error errorCallback: @escaping ErrorCallback) {
    let requestParams: NSMutableDictionary = NSMutableDictionary(dictionary:[
        "ndt": false,
        "time": RMBTTimestampWithNSDate(date: NSDate())
        ])
    
    requestParams.addEntries(from: params as [NSObject : AnyObject])
    
    self.requestPost(path: "", params: requestParams, success: { response in
                
        let tp = RMBTTestParams(response: response as! [String : AnyObject])
        success(tp)
        
    }, error: { error, info in
        //RMBTLog("Fetching test parameters failed with err=%@, response=%@", error, info)
        errorCallback(error, info)
    })
        
}

///
func submitResult(result: NSDictionary, success: @escaping SuccessCallback, error failure: @escaping ErrorCallback) {
    let mergedParams = NSMutableDictionary()
    mergedParams.addEntries(from: result as [NSObject: AnyObject])
    
    let systemInfo = systemInfoParams()
    let clientParams = NSMutableDictionary()
    
    clientParams.setObject(systemInfo["client"]!,           forKey: "client_name" as NSCopying)
    clientParams.setObject(systemInfo["version"]!,          forKey: "client_version" as NSCopying)
    clientParams.setObject(systemInfo["language"]!,         forKey: "client_language" as NSCopying)
    clientParams.setObject(systemInfo["softwareVersion"]!,  forKey: "client_software_version" as NSCopying)
    
    mergedParams.addEntries(from: clientParams as [NSObject: AnyObject])
    
    //logger.debug("Submit \(mergedParams)")
    
    requestPost( path: "result", params: mergedParams, success: { (response: AnyObject) in
        
        if let res = response as? NSDictionary {
            if let resErr = res["error"] as? NSArray {
                
                if (resErr.count == 0) {
                    success(NSArray())
                    return
                }
            }
        }
        
        //RMBTLog("Error subitting rest result: %@", response["error"])
        let error = NSError(domain: "error", code: 12345, userInfo: nil)
        failure(error, nil)
        
    }) { error, info in
        //RMBTLog("Error submitting result err=%@, response=%@", error, info)
        failure(error, info)
    }
}


///////////////////////////
// QOS
///////////////////////////

///
func getQOSObjectives(success: @escaping SuccessCallback, error errorCallback: @escaping ErrorCallback) {
    self.requestPost(path: "qosTestRequest", params: ["a": "b"] /*TODO*/, success: { response in
        // TODO: check "error" in json
        
        success(response)
    }, error: { error, info in
        errorCallback(error, info)
    })
}

///
func submitQOSTestResult(result: [String:AnyObject], success: @escaping EmptyCallback, error errorCallback: @escaping ErrorCallback) {
    let mergedParams = NSMutableDictionary()
    mergedParams.addEntries(from: result)
    
    let systemInfo = systemInfoParams()
    
    let clientParams = NSMutableDictionary()
    clientParams.setObject(systemInfo["client"]!,           forKey: "client_name" as NSCopying)
    clientParams.setObject(systemInfo["version"]!,          forKey: "client_version" as NSCopying)
    clientParams.setObject(systemInfo["language"]!,         forKey: "client_language" as NSCopying)
    clientParams.setObject(systemInfo["softwareVersion"]!,  forKey: "client_software_version" as NSCopying)
    
    mergedParams.addEntries(from: clientParams as [NSObject : AnyObject])
    
   
        
    self.requestPost(path: "resultQoS", params: mergedParams, success: { response in
        
        // TODO: check "error" in json

        success()
        
    }, error: { error, info in
        errorCallback(error, info)
    })
}

// MARK: convenience methods


/// TODO GET, and another
    func requestPost(path: String, params: NSDictionary/*TODO*/, success: @escaping SuccessCallback, error failure: @escaping ErrorCallback) {
        let mergedParams = NSMutableDictionary()
        
        if let uuid = self.uuid {
            mergedParams.setObject(uuid, forKey: "uuid" as NSCopying)
        }
        
        mergedParams.addEntries(from: systemInfoParams())
        mergedParams.addEntries(from: params as [NSObject : AnyObject])
        
        //logger.debug("Requesting \(mergedParams)")
        
        manager.requestSerializer = AFJSONRequestSerializer()
        
        let urlString: String = (baseURL().absoluteString?.appending(path))!
        
        manager.post(urlString, parameters: mergedParams, progress: nil, success: {(task: URLSessionDataTask?, responseObject: Any) -> Void in
            let response = (task?.response as! HTTPURLResponse)
            
            if (response.statusCode < 400) {
                success(responseObject as! NSObject)
            } else {
                let error = NSError(domain: "error", code: 12345, userInfo: nil)
                failure(error, nil)
            }
        
        }, failure: {(task: URLSessionDataTask?, error: Error) -> Void in
            
            if ((error as NSError?)?.code == Int(CFNetworkErrors.cfurlErrorCancelled.rawValue)) {
                return  // Ignore cancelled requests
            }
            
            
            if(task?.response != nil && task?.response is HTTPURLResponse){
                let response : HTTPURLResponse? = task?.response as? HTTPURLResponse
                let msg : String? = "ERR CODE: \(String(describing: response?.statusCode)) ERR MSG: \(HTTPURLResponse.localizedString(forStatusCode: response!.statusCode))"
                print(msg ?? "")
                failure(error as NSError, NSDictionary.init(dictionary: ["error_message" : msg ?? ""]))
                return
            }
            
            
            failure(error as NSError, nil)
        })
    
}

// MARK: - System Info

///
func systemInfoParams() -> [String: AnyObject] {
    return[
        "plattform": "iOS" as AnyObject,
        "os_version": UIDevice.current.systemVersion as AnyObject,
        "model": self.systemInfoDeviceInternalModel() as AnyObject,
        "device": UIDevice.current.model as AnyObject,
        "language": "cs" as AnyObject,
        "timezone": TimeZone.current.identifier as AnyObject,
        "type": "MOBILE" as AnyObject,
        "name": "RMBT" as AnyObject,
        "client": "RMBT" as AnyObject,
        "version": "0.3" as AnyObject,
        "softwareVersion": Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String as AnyObject,
        "softwareVersionCode": Int(Bundle.main.infoDictionary?["CFBundleVersion"] as! String) as AnyObject,
        "softwareRevision": "1.0" as AnyObject,
    ]
}

///
func systemInfoDeviceInternalModel() -> String {
    return UIDeviceHardware.platform()
}

///
func baseURL() -> NSURL {
    return self.manager.baseURL! as NSURL
}

///
func baseURLString() -> String {
    return self.manager.baseURL!.absoluteString
}

///
func cancelAllRequests() {
    self.manager.operationQueue.cancelAllOperations()
}
}
