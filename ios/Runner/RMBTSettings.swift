/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  RMBTSettings.swift
//  RMBT
//
//  Created by Benjamin Pucher on 21.09.15.
//  Copyright © 2015 SPECURE GmbH. All rights reserved.
//

import Foundation

///
class RMBTSettings: NSObject {
    
    // MARK: User configurable properties
    
    ///
    var forceIPv4: Bool = false

    ///
    var publishPublicData: Bool = false
    
    // MARK: Debug properties
    
    ///
    var debugUnlocked: Bool = false
    
    ///
    var debugForceIPv6: Bool = false
    
    // loop mode
    
    ///
    var debugLoopMode: Bool = false
    var debugLoopModeMaxTests: UInt = 0
    var debugLoopModeMinDelay: UInt = 0
    var debugLoopModeSkipQOS: Bool = false
    
    // control server
    
    ///
    var debugControlServerCustomizationEnabled: Bool = false
    var debugControlServerHostname: String!
    var debugControlServerPort: UInt = 0
    var debugControlServerUseSSL: Bool = false

    
    //NETMETR-FLUTTER
    var clientUuid: String!
    var controlHostDev : String!
    var checkHostIpv4: String!
    var checkHostIpv6: String!
    var qosSSL: Bool!
    var controlSSL: Bool!
    var port: Int!
    
    public func updateDataConfig(){
        let infoDictionary = Bundle.main.infoDictionary
        let userDefaults = UserDefaults.standard
        
        self.clientUuid = userDefaults.object(forKey: "uuid") as? String
        self.controlHostDev = (userDefaults.object(forKey: "control_server") ?? infoDictionary![CONTROL_HOST_BUNDLE]) as? String
        self.checkHostIpv4 = (userDefaults.object(forKey: "url_ipv4_check") ?? infoDictionary![CHECK_IPV4_HOST_BUNDLE]) as? String
        self.checkHostIpv6 = (userDefaults.object(forKey: "url_ipv6_check") ?? infoDictionary![CHECK_IPV6_HOST_BUNDLE]) as? String
        self.qosSSL = (infoDictionary![QOS_SSL_BUNDLE] as! String).toBool()
        self.controlSSL = (infoDictionary![CONTROL_SSL_BUNDLE] as! String).toBool()
        self.port = (userDefaults.object(forKey: "port") ?? infoDictionary![PORT_BUNDLE]) as? Int
    }
    
    // logging
    
    ///
    var debugLoggingEnabled: Bool = false
    
    ///
    private static let _sharedSettings = RMBTSettings()
    
    /// TODO: remove later
    class func sharedSettings() -> RMBTSettings {
        return _sharedSettings
    }
    
    ///
    private override init() {
       // mapOptionsSelection = RMBTMapOptionsSelection()
        
        super.init()
        
    }
}
