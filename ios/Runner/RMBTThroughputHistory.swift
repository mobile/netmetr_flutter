/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  RMBTThroughputHistory.swift
//  RMBT
//
//  Created by Benjamin Pucher on 31.03.15.
//  Copyright © 2015 SPECURE GmbH. All rights reserved.
//

import Foundation

///
class RMBTThroughputHistory: CustomStringConvertible {
    
    /// Total bytes/time transferred so far. Equal to sum of all reported lengths / largest reported timestamp.
    var totalThroughput = RMBTThroughput(length: 0, startNanos: 0, endNanos: 0)
    
    /// Time axis is split into periods of this duration. Each period has a throughput object associated with it.
    /// Reported transfers are then proportionally divided accross the throughputs it spans over.
    var resolutionNanos: UInt64
    
    /// Array of throughput objects for each period
    var periods = [RMBTThroughput]()
    
    /// Returns the index of the last period which is complete, meaning that no reports can change its value.
    /// -1 if not even the first period is complete yet
    var lastFrozenPeriodIndex: Int = -1
    
    /// See freeze
    var isFrozen: Bool = false
    
    //
    
    ///
    init(resolutionNanos: UInt64) {
        self.resolutionNanos = resolutionNanos
    }
    
    ///
    func addLength(length: UInt64, atNanos timestampNanos: UInt64) {
        assert(!isFrozen, "Tried adding to frozen history")
        
        totalThroughput.length += length
        totalThroughput.endNanos = max(totalThroughput.endNanos, timestampNanos)
        
        if (periods.count == 0) {
            // Create first period
            periods.append(RMBTThroughput(length: 0, startNanos: 0, endNanos: 0))
        }
                
        var leftoverLength = length
        
        let startPeriodIndex = periods.count - 1
        var endPeriodIndex = timestampNanos / resolutionNanos
        
        if (timestampNanos - (resolutionNanos * endPeriodIndex) == 0) {
            endPeriodIndex -= 1 // Boundary condition
        }
        
        assert(startPeriodIndex > lastFrozenPeriodIndex, "Start period \(startPeriodIndex) < \(lastFrozenPeriodIndex)")
        assert(Int(endPeriodIndex) > lastFrozenPeriodIndex, "End period \(endPeriodIndex) < \(lastFrozenPeriodIndex)")
        
        //
        
        let startPeriod = periods[startPeriodIndex]
        
        let transferNanos = timestampNanos - startPeriod.endNanos
        assert(transferNanos > 0, "Transfer happened before last reported transfer?")
        
        let lengthPerPeriod = UInt64(Double(length) * (Double(resolutionNanos) / Double(transferNanos))) // TODO: improve
        
        if (startPeriodIndex == Int(endPeriodIndex)) {
            // Just add to the start period
            startPeriod.length += length
            startPeriod.endNanos = timestampNanos
        } else {
            // Attribute part to the start period, except if we started on the boundary
            if (startPeriod.endNanos < (UInt64(startPeriodIndex) + 1) * resolutionNanos) {
                
                // TODO: improve calculation
                let startLength = UInt64(Double(length) * (Double(resolutionNanos - (startPeriod.endNanos % resolutionNanos)) / Double(transferNanos)))
                
                startPeriod.length += startLength
                startPeriod.endNanos = (UInt64(startPeriodIndex) + 1) * resolutionNanos
                leftoverLength -= startLength
            }
            
            // Create periods in between
            let start = UInt64(startPeriodIndex) + 1;
            for i in start..<endPeriodIndex {
                leftoverLength -= lengthPerPeriod
                
                periods.append(RMBTThroughput(length: lengthPerPeriod, startNanos: i * resolutionNanos, endNanos: (i + 1) * resolutionNanos))
            }
            
            // Create new end period and add the rest of bytes to it
            periods.append(RMBTThroughput(length: leftoverLength, startNanos: endPeriodIndex * resolutionNanos, endNanos: timestampNanos))
        }
        
        lastFrozenPeriodIndex = Int(endPeriodIndex) - 1
    }
    
    /// Marks history as frozen, also marking all periods as passed, not allowing futher reports.
    func freeze() {
        isFrozen = true
        lastFrozenPeriodIndex = periods.count - 1
    }
    
    /// Concatenetes last count periods into one, or nop if there are less than two periods in the history.
    func squashLastPeriods(count: Int) {
        assert(count >= 1, "Count must be >= 1")
        assert(isFrozen, "History should be frozen before squashing")
        
        if (periods.count < count) {
            return
        }
        
        let finalTput = periods[periods.count - count - 1]
        
        for _ in 0..<count{
            let t = periods.last!
            
            finalTput.endNanos = max(t.endNanos, finalTput.endNanos)
            finalTput.length += t.length
            
            periods.removeLast()
        }
    }
    
    ///
    func log() {
        logger.debug("Throughputs:")
        
        for t in periods {
            logger.debug("- \(t.description)")
        }
        
        logger.debug("Total: \(totalThroughput.description)")
    }
    
    ///
    var description: String {
        return "total = \(totalThroughput), entries = \(periods.description)"
    }
}
