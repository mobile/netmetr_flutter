/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  RMBTTestParams.swift
//  RMBT
//
//  Created by Benjamin Pucher on 27.03.15.
//  Copyright © 2015 SPECURE GmbH. All rights reserved.
//

import Foundation

///
class RMBTTestParams {
    
    ///
    let clientRemoteIp: String
    
    ///
    let pingCount = UInt(RMBT_TEST_PING_COUNT)
    
    ///
    let pretestDuration: TimeInterval = RMBT_TEST_PRETEST_DURATION_S
    
    ///
    let pretestMinChunkCountForMultithreading = UInt(RMBT_TEST_PRETEST_MIN_CHUNKS_FOR_MULTITHREADED_TEST)
    
    ///
    let serverAddress: String
    
    ///
    let serverEncryption: Bool
    
    ///
    let serverName: NSString
    
    ///
    let serverPort: UInt
    
    ///
    let resultURLString: String
    
    ///
    let testDuration: TimeInterval
    
    ///
    let testToken: String
    
    ///
    let testUUID: String
    
    ///
    let threadCount: UInt
    
    ///
    let waitDuration: TimeInterval
    
    //
    
    ///
    init(response: [String: AnyObject]) { // TODO: remove NSObject after complete swift rewrite...
        // TODO: why are some values NSNumber (or long) and some String?
        
        logger.debug("test parameters response: \(response)")
        
        clientRemoteIp      = response["client_remote_ip"] as! String//.copy()
        serverAddress       = response["test_server_address"] as! String//.copy()
        serverEncryption    = (response["test_server_encryption"] as! NSNumber).boolValue
        serverName          = response["test_server_name"] as! String as NSString//.copy()
        
        // We use -integerValue as it's defined both on NSNumber and NSString, so we're more resilient in parsing:
        
        serverPort = UInt((response["test_server_port"] as! NSNumber).intValue)
        //assert(serverPort > 0 && serverPort < 65536, "Invalid port")
        
        resultURLString = response["result_url"] as! String//.copy()
        testDuration    = TimeInterval((response["test_duration"] as! NSString).integerValue)
        //assert(testDuration > 0 && testDuration <= 100, "Invalid test duration")
        
        testToken   = response["test_token"] as! String//.copy()
        testUUID    = response["test_uuid"] as! String//.copy()
        threadCount = UInt((response["test_numthreads"] as! NSString).integerValue)
        //assert(threadCount > 0 && threadCount <= 128, "Invalid thread count")
        
        waitDuration = TimeInterval((response["test_wait"] as! NSNumber).intValue)
        //assert(waitDuration >= 0 && waitDuration <= 128, "Invalid wait duration")
    }
}
