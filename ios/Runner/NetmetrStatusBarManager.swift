/*
* Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import Foundation

class NetmetrStatusBarManager{
    init(){}
    func getStatusBar() -> UIView?{return nil}
}


class OldStatusBarManager : NetmetrStatusBarManager{
    override func getStatusBar() -> UIView? {
        guard let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView else { return nil}
        return statusBar
    }
}

@available(iOS 13.0, *)
class IOS13StatusBarManager: NetmetrStatusBarManager{
    private let statusBarManager: UIStatusBarManager?
    private let localStatusBar: UIView?
    
    override init() {
        self.statusBarManager = UIApplication.shared.keyWindow?.windowScene?.statusBarManager
        self.localStatusBar = self.statusBarManager!.perform(Selector(("createLocalStatusBar")))?.takeUnretainedValue() as? UIView
    }
    
    override func getStatusBar() -> UIView? {
        var status: UIView?
        let exception = tryBlock {
            if let statusBarMorden = self.localStatusBar?.perform(Selector(("statusBar")))?.takeUnretainedValue() as? UIView,
                let stb = statusBarMorden.value(forKey: "_statusBar") as? UIView {
                    status = stb
                }
        }
        
        if let exception = exception {
            print("get signal exception: \(exception)")
        }
        return status
    }
}

