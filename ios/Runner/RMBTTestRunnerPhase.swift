/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  RMBTTestRunner.swift
//  RMBT
//
//  Created by Benjamin Pucher on 09.09.15.
//  Copyright © 2015 SPECURE GmbH. All rights reserved.
//

import Foundation
import CoreLocation

///
let RMBTTestStatusNone              = "NONE"
let RMBTTestStatusAborted           = "ABORTED"
let RMBTTestStatusError             = "ERROR"
let RMBTTestStatusErrorFetching     = "ERROR_FETCH"
let RMBTTestStatusErrorSubmitting   = "ERROR_SUBMIT"
let RMBTTestStatusErrorBackgrounded = "ABORTED_BACKGROUNDED"
let RMBTTestStatusEnded             = "END"

///
let RMBTTestRunnerProgressUpdateInterval: TimeInterval = 0.1 // seconds

/*
 // Used to assert that we are running on a correct queue via dispatch_queue_set_specific(),
 // as dispatch_get_current_queue() is deprecated.
 static void *const kWorkerQueueIdentityKey = (void *)&kWorkerQueueIdentityKey;
 #define ASSERT_ON_WORKER_QUEUE() NSAssert(dispatch_get_specific(kWorkerQueueIdentityKey) != NULL, @"Running on a wrong queue")
 */

///
enum RMBTTestRunnerPhase: Int {
    case None = 0
    case FetchingTestParams
    case Wait
    case Init
    case Latency
    case Jitter
    case Down
    case InitUp
    case Up
    case SubmittingTestResult
}

///
enum RMBTTestRunnerCancelReason: Int {
    case UserRequested
    case NoConnection
    case MixedConnectivity
    case ErrorFetchingTestingParams
    case ErrorSubmittingTestResult
    case AppBackgrounded
}

///
protocol RMBTTestRunnerDelegate {
    
    ///
    func testRunnerDidStartPhase(phase: RMBTTestRunnerPhase)
    
    ///
    func testRunnerDidFinishPhase(phase: RMBTTestRunnerPhase)
    
    ///
    func testRunnerDidUpdateProgress(progress: Float, inPhase phase: RMBTTestRunnerPhase)
    
    ///
    func testRunnerDidMeasureThroughputs(throughputs: NSArray, inPhase phase: RMBTTestRunnerPhase)
    
    /// These delegate methods will be called even before the test starts
    func testRunnerDidDetectConnectivity(connectivity: RMBTConnectivity)
    
    ///
    func testRunnerDidDetectNoConnectivity()
    
    ///
    func testRunnerDidDetectLocation(location: CLLocation)
    
    ///
    func testRunnerDidCompleteWithResult(result: RMBTHistoryResult?)
    
    ///
    func testRunnerDidCancelTestWithReason(cancelReason: RMBTTestRunnerCancelReason, errorMessage: String?)
    
    ///
    func testRunnerDidFinishInit(time: UInt64)
}

///
class RMBTTestRunner: NSObject, RMBTTestWorkerDelegate, RMBTConnectivityTrackerDelegate, SignalDetectDelegate, QualityOfServiceTestDelegate {
   
    ///
    private let workerQueue: DispatchQueue // We perform all work on this background queue. Workers also callback onto this queue.
    
    ///
    private var timer: DispatchSourceTimer!
    
    ///
    private var workers = [RMBTTestWorker]()
    
    ///
    private let delegate: RMBTTestRunnerDelegate
    
    ///
    private var phase: RMBTTestRunnerPhase = .None
    
    ///
    private var dead = false
    
    /// Flag indicating that downlink pretest in one of the workers was too slow and we need to
    /// continue with a single thread only
    private var singleThreaded = false
    
    ///
    var testParams: RMBTTestParams!
    
    ///
    var testResult = RMBTTestResult(resolutionNanos: UInt64(RMBT_TEST_SAMPLING_RESOLUTION_MS) * NSEC_PER_MSEC)
    
    ///
    private var connectivityTracker: RMBTConnectivityTracker!
    
    /// Snapshots of the network interface byte counts at a given phase
    private var startInterfaceInfo: RMBTConnectivityInterfaceInfo?
    private var uplinkStartInterfaceInfo: RMBTConnectivityInterfaceInfo?
    private var uplinkEndInterfaceInfo: RMBTConnectivityInterfaceInfo?
    private var downlinkStartInterfaceInfo: RMBTConnectivityInterfaceInfo?
    private var downlinkEndInterfaceInfo: RMBTConnectivityInterfaceInfo?
    
    ///
    private var finishedWorkers: UInt = 0
    private var activeWorkers: UInt = 0
    
    ///
    private var progressStartedAtNanos: UInt64 = 0
    private var progressDurationNanos: UInt64 = 0
    
    ///
    private var progressCompletionHandler: RMBTBlock!
    
    ///
    private var downlinkTestStartedAtNanos: UInt64 = 0
    private var uplinkTestStartedAtNanos: UInt64 = 0
    
    ///loop
    private var loopMode : Bool = false
    private var loopWorker : DispatchQueue?
    
    ///
    private var onlySignal : Bool = false
    
    ///jitter
    private var qualityOfServiceTestRunner : QualityOfServiceTest?
    
    private var controlServerInfoDelegate : ControlServerInfoDelegate?
    
    ///
    init(delegate: RMBTTestRunnerDelegate, loopMode: Bool, onlySignal : Bool, loopWorker: DispatchQueue?, index: Int, statusBarManager: NetmetrStatusBarManager?, controlServerInfoDelegate : ControlServerInfoDelegate?) {
        self.delegate = delegate
        //self.phase = .None
        self.loopMode = loopMode
        self.loopWorker = loopWorker
        self.onlySignal = onlySignal
        self.controlServerInfoDelegate = controlServerInfoDelegate
        self.workerQueue = DispatchQueue(label: "at.rtr.rmbt.testrunner\(index)")
        
        super.init()
        
        /*
         void *nonNullValue = kWorkerQueueIdentityKey;
         dispatch_queue_set_specific(_workerQueue, kWorkerQueueIdentityKey, nonNullValue, NULL);
         */
        //dispatch_queue_set_specific(workerQueue, kWorkerQueueIdentityKey, nil, nil) // TODO!
        
        connectivityTracker = RMBTConnectivityTracker(delegate: self, stopOnMixed: true, statusBarManager: statusBarManager)
        connectivityTracker.start()
    }
    
    private func resultQueue() -> DispatchQueue{
        return loopMode ? loopWorker! : DispatchQueue.main
    }
    
    /// Run on main queue (called from VC)
    func start() {
        assert(phase == .None, "Invalid state")
        assert(!dead, "Invalid state")

        var locationJSON: [String:AnyObject]? = nil
        
        if let l = RMBTLocationTracker.sharedTracker.location {
            locationJSON = [
                "long":     NSNumber(value: l.coordinate.longitude),
                "lat":      NSNumber(value: l.coordinate.latitude),
                "time":     RMBTTimestampWithNSDate(date: l.timestamp as NSDate),
                "accuracy": NSNumber(value: l.horizontalAccuracy),
                "altitude": NSNumber(value: l.altitude),
                "speed":    NSNumber(value: (l.speed > 0.0 ? l.speed : 0.0))
            ]
            
            testResult.addLocation(location: l)

        }
        
        let params = [
            "location": RMBTValueOrNull(value: locationJSON as AnyObject)
        ]
        
        phase = .FetchingTestParams
        
        ControlServer.sharedControlServer.getTestParamsWithParams(params: params as NSDictionary, success: { response in
            self.workerQueue.async {
                
                if(!self.onlySignal){
                    self.continueWithTestParams(testParams: response as! RMBTTestParams)
                }else{
                   
                    self.testParams = response as? RMBTTestParams
                    self.testResult.markTestStart()
                    self.submitResult()
                }
                
            }
        }) {error, info in
            self.workerQueue.async {
                var errorMessage : String? = nil
                
                if let message = info?["error_message"]{
                    errorMessage = message as? String
                }
                
                self.cancelWithReason(reason: .ErrorFetchingTestingParams, errorMessage: errorMessage)
            }
        }
    }
    
    ///
    private func continueWithTestParams(testParams: RMBTTestParams) {
        //ASSERT_ON_WORKER_QUEUE();
        assert(phase == .FetchingTestParams || phase == .None, "Invalid state")

        if (dead) {
            return
        }
        
        self.testParams = testParams
        
        //testResult = RMBTTestResult(resolutionNanos: UInt64(RMBT_TEST_SAMPLING_RESOLUTION_MS) * NSEC_PER_MSEC)
        testResult.markTestStart()
        for i in 0..<testParams.threadCount {
            let worker = RMBTTestWorker(delegate: self, delegateQueue: workerQueue, index: i, testParams: testParams)
            workers.append(worker)
        }
        
        // Start observing app going to background notifications
        NotificationCenter.default.addObserver(self, selector: #selector(RMBTTestRunner.applicationDidSwitchToBackground(n:)), name: UIApplication.didEnterBackgroundNotification, object: nil)
        
        // Register as observer for location tracker updates
        NotificationCenter.default.addObserver(self, selector: #selector(RMBTTestRunner.locationsDidChange(notification:)), name: NSNotification.Name(rawValue: "RMBTLocationTrackerNotification"), object: nil)
        
        // ..and force an update right away
        _ = RMBTLocationTracker.sharedTracker.forceUpdate()
        connectivityTracker.forceUpdate()
        
        let startInit = {
            self.startPhase(phase: .Init, withAllWorkers: true, performingSelector: #selector(RMBTTestWorker.startDownlinkPretest), expectedDuration: testParams.pretestDuration, completion: nil)
        }
        
        if (testParams.waitDuration > 0) {
            // Let progress timer run, then start init
            startPhase(phase: .Wait, withAllWorkers: false, performingSelector: nil, expectedDuration: testParams.waitDuration, completion: startInit)
        } else {
            startInit()
        }
    }
    
    
    // MARK: Test worker delegate method
    
    ///
    func testWorker(worker: RMBTTestWorker, didFinishDownlinkPretestWithChunkCount chunks: UInt, withTime duration: UInt64) {
        //ASSERT_ON_WORKER_QUEUE();
        assert(phase == .Init, "Invalid state")
        assert(!dead, "Invalid state")

        //should it be calculated init time recent time - start test time
        delegate.testRunnerDidFinishInit(time: duration)
        
        logger.debug("Thread \(worker.index): finished download pretest (chunks = \(chunks))")
        
        if (!singleThreaded && chunks <= testParams.pretestMinChunkCountForMultithreading) {
            singleThreaded = true
        }
        
        if (markWorkerAsFinished()) {
            if (singleThreaded) {
                logger.debug("Downloaded <= \(testParams.pretestMinChunkCountForMultithreading) chunks in the pretest, continuing with single thread.")
                
                activeWorkers = testParams.threadCount - 1
                finishedWorkers = 0
                
                for i in 1..<testParams.threadCount {
                    workers[Int(i)].stop()
                }
                
                testResult.startDownloadWithThreadCount(threadCount: 1)
                
            } else {
                testResult.startDownloadWithThreadCount(threadCount: Int(testParams.threadCount))
                startPhase(phase: .Latency, withAllWorkers: false, performingSelector:  #selector(RMBTTestWorker.startLatencyTest), expectedDuration: 0, completion: nil)
            }
        }
    }
    
    ///
    func testWorkerDidStop(worker: RMBTTestWorker) {
        //ASSERT_ON_WORKER_QUEUE();
        assert(phase == .Init, "Invalid state")
        assert(!dead, "Invalid state")
        
        logger.debug("Thread \(worker.index): stopped")
        workers.remove(at: workers.firstIndex(of: worker)!)

        if (markWorkerAsFinished()) {
            // We stopped all but one workers because of slow connection. Proceed to latency with single worker.
            startPhase(phase: .Latency, withAllWorkers: false, performingSelector: #selector(RMBTTestWorker.startLatencyTest), expectedDuration: 0, completion: nil)
        }
    }
    
    ///
    func testWorker(worker: RMBTTestWorker, didMeasureLatencyWithServerNanos serverNanos: UInt64, clientNanos: UInt64) {
        //ASSERT_ON_WORKER_QUEUE();
        assert(phase == .Latency, "Invalid state")
        assert(!dead, "Invalid state")
        
        logger.debug("Thread \(worker.index): pong (server = \(serverNanos), client = \(clientNanos))")
        
        testResult.addPingWithServerNanos(serverNanos: serverNanos, clientNanos: clientNanos)
        
        let p = Double(testResult.pings.count) / Double(testParams.pingCount)
        resultQueue().async {
            self.delegate.testRunnerDidUpdateProgress(progress: Float(p), inPhase: self.phase)
        }
    }
    
    ///
    func testWorkerDidFinishLatencyTest(worker: RMBTTestWorker) {
        //ASSERT_ON_WORKER_QUEUE();
        assert(phase == .Latency, "Invalid state")
        assert(!dead, "Invalid state")
        
        if (markWorkerAsFinished()) {
            let runJitter = UserDefaults.standard.bool(forKey: "jitter_run")
            if(runJitter){
                runVoipTest()
            }else{
                runSpeedTest()
            }
        }
    }
    
    ///
    func testWorker(worker: RMBTTestWorker, didStartDownlinkTestAtNanos nanos: UInt64) -> UInt64 {
        //ASSERT_ON_WORKER_QUEUE();
        assert(phase == .Down, "Invalid state")
        assert(!dead, "Invalid state")
        
        if (downlinkTestStartedAtNanos == 0) {
            downlinkStartInterfaceInfo = testResult.lastConnectivity().getInterfaceInfo()
            downlinkTestStartedAtNanos = nanos
        }
        
        logger.debug("Thread \(worker.index): started downlink test with delay \(nanos - downlinkTestStartedAtNanos)")
        
        return downlinkTestStartedAtNanos
    }
    
    ///
    func testWorker(worker: RMBTTestWorker, didDownloadLength length: UInt64, atNanos nanos: UInt64) {
        //ASSERT_ON_WORKER_QUEUE();
        assert(phase == .Down, "Invalid state")
        assert(!dead, "Invalid state")
        
        let measuredThroughputs = testResult.addLength(length: length, atNanos: nanos, forThreadIndex: Int(worker.index))
        
        if measuredThroughputs != nil {
            //if let measuredThroughputs = testResult.addLength(length, atNanos: nanos, forThreadIndex: Int(worker.index)) {
            resultQueue().async {
                self.delegate.testRunnerDidMeasureThroughputs(throughputs: measuredThroughputs! as NSArray, inPhase: .Down)
            }
        }
    }
    
    ///
    func testWorkerDidFinishDownlinkTest(worker: RMBTTestWorker) {
        //ASSERT_ON_WORKER_QUEUE();
        assert(phase == .Down, "Invalid state")
        assert(!dead, "Invalid state")
        
        if (markWorkerAsFinished()) {
            logger.debug("Downlink test finished")
            
            downlinkEndInterfaceInfo = testResult.lastConnectivity().getInterfaceInfo()
            
            let measuredThroughputs = testResult.flush()
            
            testResult.totalDownloadHistory.log()
            
            if let _ = measuredThroughputs {
                resultQueue().async {
                    self.delegate.testRunnerDidMeasureThroughputs(throughputs: measuredThroughputs! as NSArray, inPhase: .Down)
                }
            }
            
            startPhase(phase: .InitUp, withAllWorkers: true, performingSelector: #selector(RMBTTestWorker.startUplinkPretest), expectedDuration: testParams.pretestDuration, completion: nil)
        }
    }
    
    ///
    func testWorker(worker: RMBTTestWorker, didFinishUplinkPretestWithChunkCount chunks: UInt) {
        //ASSERT_ON_WORKER_QUEUE();
        assert(phase == .InitUp, "Invalid state")
        assert(!dead, "Invalid state")
        
        logger.debug("Thread \(worker.index): finished uplink pretest (chunks = \(chunks))")
        
        if (markWorkerAsFinished()) {
            logger.debug("Uplink pretest finished")
            testResult.startUpload()
            startPhase(phase: .Up, withAllWorkers: true, performingSelector: #selector(RMBTTestWorker.startUplinkTest), expectedDuration: testParams.testDuration, completion: nil)
        }
    }
    
    ///
    func testWorker(worker: RMBTTestWorker, didStartUplinkTestAtNanos nanos: UInt64) -> UInt64 {
        //ASSERT_ON_WORKER_QUEUE();
        assert(phase == .Up, "Invalid state")
        assert(!dead, "Invalid state")
        
        var delay: UInt64 = 0
        
        if (uplinkTestStartedAtNanos == 0) {
            uplinkTestStartedAtNanos = nanos
            delay = 0
            uplinkStartInterfaceInfo = testResult.lastConnectivity().getInterfaceInfo()
        } else {
            delay = nanos - uplinkTestStartedAtNanos
        }
        
        logger.debug("Thread \(worker.index): started uplink test with delay \(delay)")
        
        return delay
    }
    
    ///
    func testWorker(worker: RMBTTestWorker, didUploadLength length: UInt64, atNanos nanos: UInt64) {
        //ASSERT_ON_WORKER_QUEUE();
        assert(phase == .Up, "Invalid state")
        assert(!dead, "Invalid state")
        
        if let measuredThroughputs = testResult.addLength(length: length, atNanos: nanos, forThreadIndex: Int(worker.index)) {
            resultQueue().async {
                self.delegate.testRunnerDidMeasureThroughputs(throughputs: measuredThroughputs as NSArray, inPhase: .Up)
            }
        }
    }
    
    ///
    func testWorkerDidFinishUplinkTest(worker: RMBTTestWorker) {
        //ASSERT_ON_WORKER_QUEUE();
        assert(phase == .Up, "Invalid state")
        assert(!dead, "Invalid state")
        
        if (markWorkerAsFinished()) {
            // Stop observing now, test is finished
            
            finalize()
            
            uplinkEndInterfaceInfo = testResult.lastConnectivity().getInterfaceInfo()
            
            let measuredThroughputs = testResult.flush()
            logger.debug("Uplink test finished.")
            
            testResult.totalUploadHistory.log()
            
            if let _ = measuredThroughputs {
                resultQueue().async {
                    self.delegate.testRunnerDidMeasureThroughputs(throughputs: measuredThroughputs! as NSArray, inPhase: .Up)
                }
            }
            
            //self.phase = .SubmittingTestResult
            setPhase(phase: .SubmittingTestResult)
            
            submitResult()
        }
    }
    
    ///
    func testWorkerDidFail(worker: RMBTTestWorker) {
        //ASSERT_ON_WORKER_QUEUE();
        //assert(!dead, "Invalid state") // TODO: if worker fails, then this assertion lets app terminate
        
        self.cancelWithReason(reason: .NoConnection, errorMessage: nil)
    }
    
    ///////
    
    func testCompleted(historyResult : RMBTHistoryResult?){
        self.resultQueue().async {
           self.delegate.testRunnerDidCompleteWithResult(result: historyResult)
        }
    }
    
    ///
    private func submitResult() {
        workerQueue.async {
            if (self.dead) {
                return
            }
            
            let result = self.resultDictionary()
             
            //self.phase = .SubmittingTestResult
            self.setPhase(phase: .SubmittingTestResult)
            
            self.controlServerInfoDelegate?.changeSpeedDataStatus(taskStatus: .RUNNING)

            
            ControlServer.sharedControlServer.submitResult(result: result, success: { response in
                self.workerQueue.async {
                    let resultData : ResultData = ResultData.init()

                   self.setPhase(phase: .None)
                   self.dead = true
                                                         
                   let historyResult = RMBTHistoryResult(response: ["test_uuid": self.testParams.testUUID as AnyObject])
                                      
                   self.controlServerInfoDelegate?.changeSpeedDataStatus(taskStatus: .FINISHED)
                   resultData.resultStatus = .OK
                   self.controlServerInfoDelegate?.taskFinished(task: .SPEED, resultData: resultData)
                    
                    self.testCompleted(historyResult: historyResult)
                }
                
            }, error:  {error, info in
                self.workerQueue.async {
                    let resultData : ResultData = ResultData.init()

                    self.controlServerInfoDelegate?.changeSpeedDataStatus(taskStatus: .FINISHED)
                    
                    var errorMessage : String? = nil
                                   
                    if let message = info?["error_message"]{
                       errorMessage = message as? String
                    }
                    
                    resultData.errorMessage = errorMessage
                    
                    self.controlServerInfoDelegate?.taskFinished(task: .SPEED, resultData: resultData)

                }
            })
        }
    }
    
    ///
    private func resultDictionary() -> /*[NSObject:AnyObject]*/NSDictionary {
        let result = NSMutableDictionary(dictionary: testResult.resultDictionary(onlySignal: onlySignal))
        
        result["test_token"] = testParams.testToken
        
        if(!onlySignal){
            // Collect total transfers from all threads
            var sumBytesDownloaded: UInt64 = 0
            var sumBytesUploaded: UInt64 = 0
            
            for w in workers {
                sumBytesDownloaded += w.totalBytesDownloaded
                sumBytesUploaded += w.totalBytesUploaded
            }
            
            assert(sumBytesDownloaded > 0, "Total bytes downloaded <= 0")
            assert(sumBytesUploaded > 0, "Total bytes uploaded <= 0")
            
            if let firstWorker = workers.first {
                result.addEntries(from: [
                    "test_total_bytes_download": NSNumber(value: sumBytesDownloaded),
                    "test_total_bytes_upload":   NSNumber(value: sumBytesUploaded),
                    "test_encryption":           firstWorker.negotiatedEncryptionString ?? "error",
                    "test_ip_local":             RMBTValueOrNull(value: firstWorker.localIp as AnyObject),
                    "test_ip_server":            RMBTValueOrNull(value: firstWorker.serverIp as AnyObject),
                    ])
            }
            
            result.addEntries(from: interfaceBytesResultDictionaryWithStartInfo(startInfo: downlinkStartInterfaceInfo!, endInfo: downlinkEndInterfaceInfo!, prefix: "testdl"))
            result.addEntries(from: interfaceBytesResultDictionaryWithStartInfo(startInfo: uplinkStartInterfaceInfo!,   endInfo: uplinkEndInterfaceInfo!,   prefix: "testul"))
            result.addEntries(from: interfaceBytesResultDictionaryWithStartInfo(startInfo: startInterfaceInfo!,         endInfo: uplinkEndInterfaceInfo!,   prefix: "test"))
            
            // Add relative time_(dl/ul)_ns timestamps
            let startNanos = testResult.testStartNanos
            
            result.addEntries(from: [
                "time_dl_ns": NSNumber(value: downlinkTestStartedAtNanos - startNanos),
                "time_ul_ns": NSNumber(value: uplinkTestStartedAtNanos - startNanos)
                ])
            
            if(testResult.jitterResult != nil){
                var jitter = [String: AnyObject]()
                
                let status : String = testResult.jitterResult!.resultDictionary[RESULT_VOIP_STATUS] as! String
                jitter["status"] = status as AnyObject
                
                if(status != "ERROR"){
                    let downPrefix = RESULT_VOIP_PREFIX + RESULT_VOIP_PREFIX_INCOMING
                    let upPrefix = RESULT_VOIP_PREFIX + RESULT_VOIP_PREFIX_OUTGOING
                    
                    let totalNumPackets = testResult.jitterResult!.resultDictionary[RESULT_VOIP_NUM_TOTAL_PACKETS]
                    
                    var down = [String: AnyObject?]()
                    
                    down["num_packets"] = testResult.jitterResult?.resultDictionary[downPrefix + RESULT_VOIP_NUM_PACKETS] ?? 0 as AnyObject
                    down["num_packets_total"] = totalNumPackets
                    down["mean_jitter"] = testResult.jitterResult?.resultDictionary[downPrefix + RESULT_VOIP_MEAN_JITTER]
                    down["max_jitter"] = testResult.jitterResult?.resultDictionary[downPrefix + RESULT_VOIP_MAX_JITTER]
                    
                    jitter["down"] = down as AnyObject
                    
                    var up = [String: AnyObject?]()
                    
                    up["num_packets"] = testResult.jitterResult?.resultDictionary[upPrefix + RESULT_VOIP_NUM_PACKETS] ?? 0 as AnyObject
                    up["num_packets_total"] = totalNumPackets
                    up["mean_jitter"] = testResult.jitterResult?.resultDictionary[upPrefix + RESULT_VOIP_MEAN_JITTER]
                    up["max_jitter"] = testResult.jitterResult?.resultDictionary[upPrefix + RESULT_VOIP_MAX_JITTER] 
                    
                    jitter["up"] = up as AnyObject
                    
                }

                result["jitter_packet_loss"] = jitter

            }
            
        }
        return result
    }
    
    ///
    private func interfaceBytesResultDictionaryWithStartInfo(startInfo: RMBTConnectivityInterfaceInfo, endInfo: RMBTConnectivityInterfaceInfo, prefix: String) -> [NSObject:AnyObject] {
        if (startInfo.bytesReceived <= endInfo.bytesReceived && startInfo.bytesSent < endInfo.bytesSent) {
            return [
                "\(prefix)_if_bytes_download" as NSObject:  NSNumber(value: UInt64(endInfo.bytesReceived - startInfo.bytesReceived)),
                "\(prefix)_if_bytes_upload" as NSObject:    NSNumber(value: UInt64(endInfo.bytesSent - startInfo.bytesSent))
            ]
        } else {
            return [:]
        }
    }
    
    // MARK: Utility methods
    
    ///
    private func setPhase(phase: RMBTTestRunnerPhase) {
        if (self.phase != .None) {
            let oldPhase = self.phase
            
            resultQueue().async {
                self.delegate.testRunnerDidFinishPhase(phase: oldPhase)
            }
        }
        
        self.phase = phase
        
        if (self.phase != .None) {
            resultQueue().async {
                self.delegate.testRunnerDidStartPhase(phase: self.phase)
            }
        }
    }
    
    ///
    private func startPhase(phase: RMBTTestRunnerPhase, withAllWorkers allWorkers: Bool, performingSelector selector: Selector!, expectedDuration duration: TimeInterval, completion completionHandler: RMBTBlock!) {
        
        //ASSERT_ON_WORKER_QUEUE();
        
        //self.phase = phase
        setPhase(phase: phase)
        
        finishedWorkers = 0
        progressStartedAtNanos = RMBTCurrentNanos()
        progressDurationNanos = UInt64(duration * Double(NSEC_PER_SEC))
        
        if (timer != nil) {
            timer.cancel()
            timer = nil
        }
        
        assert((completionHandler == nil) || duration > 0)
        
        if (duration > 0) {
            progressCompletionHandler = completionHandler
            
            timer = DispatchSource.makeTimerSource(queue: DispatchQueue.global())
            timer.schedule(deadline: DispatchTime.now(),
                           repeating: DispatchTimeInterval.nanoseconds(Int(RMBTTestRunnerProgressUpdateInterval * Double(NSEC_PER_SEC))),
                           leeway: DispatchTimeInterval.nanoseconds(Int(50 * NSEC_PER_MSEC)))
          
            timer.setEventHandler {                
                let elapsedNanos = (RMBTCurrentNanos() - self.progressStartedAtNanos)
                if (elapsedNanos > self.progressDurationNanos) {
                    // We've reached end of interval...
                    // ..send 1.0 progress one last time..
                    self.resultQueue().async {
                        self.delegate.testRunnerDidUpdateProgress(progress: 1.0, inPhase: phase)
                    }
                    
                    // ..then kill the timer
                    if (self.timer != nil) {
                        self.timer.cancel()// TODO: after swift rewrite of AppDelegate one test got an exception here!
                        self.timer = nil
                    }
                    self.timer = nil
                    
                    // ..and perform completion handler, if any.
                    if (self.progressCompletionHandler != nil) {
                        self.workerQueue.async(execute: self.progressCompletionHandler)
                        self.progressCompletionHandler = nil
                    }
                } else {
                    let p = Double(elapsedNanos) / Double(self.progressDurationNanos)
                    assert(p <= 1.0, "Invalid percentage")
                    
                    self.resultQueue().async {
                        self.delegate.testRunnerDidUpdateProgress(progress: Float(p), inPhase: phase)
                    }
                }
                
            }
            timer.resume()
        }
        
        if (selector == nil) {
            return
        }
        
        if (allWorkers) {
            activeWorkers = UInt(workers.count)
            for w in workers {
                w.perform(selector)
            }
        } else {
            activeWorkers = 1
            workers.first?.perform(selector)
        }
    }
    
    ///
    private func markWorkerAsFinished() -> Bool {
        finishedWorkers+=1
        return finishedWorkers == activeWorkers
    }
    
    public func setStartConnectivity(connectivity: RMBTConnectivity){
       self.startInterfaceInfo = connectivity.getInterfaceInfo()
       self.testResult.addConnectivity(connectivity: connectivity)
    }
    
    // MARK: Connectivity tracking
    
    ///
    func connectivityTrackerDidDetectNoConnectivity(tracker: RMBTConnectivityTracker) {
        // Ignore for now, let connection time out
        self.delegate.testRunnerDidDetectNoConnectivity();
    }
    
    ///
    func connectivityTracker(tracker: RMBTConnectivityTracker, didDetectConnectivity connectivity: RMBTConnectivity) {
        workerQueue.async {
            if (self.testResult.lastConnectivity() == nil) { // TODO: error here?
                self.startInterfaceInfo = connectivity.getInterfaceInfo()
            }
            
            if (self.phase != .None && connectivity.lastRssi != nil) {
                self.testResult.addConnectivity(connectivity: connectivity)
            }
        }
        
        resultQueue().async {
            self.delegate.testRunnerDidDetectConnectivity(connectivity: connectivity)
        }
    }
    
    ///
    func connectivityTracker(tracker: RMBTConnectivityTracker, didStopAndDetectIncompatibleConnectivity connectivity: RMBTConnectivity) {
        resultQueue().async {
            self.delegate.testRunnerDidDetectConnectivity(connectivity: connectivity)
        }
        
        workerQueue.async {
            if (self.phase != .None) {
                self.cancelWithReason(reason: .MixedConnectivity, errorMessage: nil)
            }
        }
    }
    
    // MARK: Signal changed delegate
    
    ///
    
    func signalChanged(connectivity: RMBTConnectivity) {
        ///
        print("Signal changed \(String(describing: connectivity.lastRssi))")
        
        if(connectivity.lastRssi != nil){
            self.testResult.addConnectivity(connectivity: connectivity)
        }

    }
    
    // MARK: App state tracking
    
    ///
    @objc open func applicationDidSwitchToBackground(n: NSNotification) {
        logger.debug("App backgrounded, aborting \(n)")
        workerQueue.async {
            self.cancelWithReason(reason: .AppBackgrounded, errorMessage: nil)
        }
    }
    
    // MARK: Tracking location
    
    ///
    @objc open func locationsDidChange(notification: NSNotification) {
        var lastLocation: CLLocation?
        
        for l in (notification.userInfo as! [String:AnyObject])["locations"] as! [CLLocation] { // !
            if (CLLocationCoordinate2DIsValid(l.coordinate)) {
                lastLocation = l
                testResult.addLocation(location: l)
                
                //NSLog(@"Location updated to (%f,%f,+/- %fm, %@)", l.coordinate.longitude, l.coordinate.latitude, l.horizontalAccuracy, l.timestamp);
                logger.debug("Location updated to (\(l.coordinate.longitude), \(l.coordinate.latitude), \(l.horizontalAccuracy), \(l.timestamp))")
            }
        }
        
        if let _ = lastLocation {
            resultQueue().async {
                self.delegate.testRunnerDidDetectLocation(location: lastLocation!) // !
            }
        }
    }
    
    // MARK: Cancelling and cleanup
    
    ///
    override func finalize() {
        // Stop observing
        connectivityTracker.stop()
        NotificationCenter.default.removeObserver(self)
        
        // Cancel timer
        if (timer != nil) {
            timer.cancel()
            timer = nil
        }
    }

    ///
    deinit {
        finalize()
    }
    
    ///
    private func cancelWithReason(reason: RMBTTestRunnerCancelReason, errorMessage: String?) {
        //ASSERT_ON_WORKER_QUEUE();
        
        logger.debug("REASON: \(reason)")
        
        finalize()
        
        for w in workers {
            w.abort()
        }
        
        phase = .None
        dead = true
        
        resultQueue().async {
            self.delegate.testRunnerDidCancelTestWithReason(cancelReason: reason, errorMessage: errorMessage)
        }
        
        self.finalize()
    }
    
    ///
    func cancel(reason: RMBTTestRunnerCancelReason) {
        workerQueue.async {
            self.cancelWithReason(reason: reason, errorMessage: nil)
        }
    }
    
    func runSpeedTest(){
        startPhase(phase: .Down, withAllWorkers: true, performingSelector: #selector(RMBTTestWorker.startDownlinkTest), expectedDuration: testParams.testDuration, completion: nil)
    }
    
    /// JITTER ///
    
    func runVoipTest(){
        startPhase(phase: .Jitter, withAllWorkers: false, performingSelector: #selector(RMBTTestWorker.startJitterTest), expectedDuration: testParams.testDuration, completion: nil)
        
        let qosSSL = RMBTSettings.sharedSettings().qosSSL
        qualityOfServiceTestRunner = QualityOfServiceTest(testToken: testParams.testToken, speedtestStartTime: testResult.testStartNanos, qosSSL: qosSSL ?? false, runJitter: true, loopMode: self.loopMode, loopWorker: self.loopWorker, controlServerInfoDelegate: nil)
        
        qualityOfServiceTestRunner?.delegate = self
        
        qualityOfServiceTestRunner?.start()
        
    }
    
    // MARK: QOS TEST RUNNER DELEGATE
    
    func qualityOfServiceTestDidStart(test: QualityOfServiceTest) {
        print("----- jitter start -----")
    }
    
    func qualityOfServiceTestDidStop(test: QualityOfServiceTest) {
        print("----- jitter stop -----")
        self.runSpeedTest()
    }
    
    func qualityOfServiceTest(test: QualityOfServiceTest, didFinishWithResults results: [QOSTestResult]) {
        print("----- jitter results -----")
        if(!results.isEmpty){
            testResult.jitterResult = results[0]
        }
    }
    
    func qualityOfServiceTest(test: QualityOfServiceTest, didFailWithError: NSError!, errorInfo: NSDictionary?, errorType: QoSErrorType) {
        print("----- jitter error -----")
        if(phase == .Jitter){
            runSpeedTest()
        }
    }
    
    func qualityOfServiceTest(test: QualityOfServiceTest, didFetchTestTypes testTypes: [QOSTestType]) {
        print("----- jitter fetshTypes -----")

    }
    
    func qualityOfServiceTest(test: QualityOfServiceTest, didFinishTestType testType: QOSTestType) {
        print("----- jitter finish test type -----")
        print("\(testType)")
        if(testType == .VOIP && phase == .Jitter){
            runSpeedTest()
        }
    }
    
    func qualityOfServiceTest(test: QualityOfServiceTest, didProgressToValue progress: Float) {
        print("----- jitter progress -----")

    }
}
