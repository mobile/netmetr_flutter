/// default qos socket character encoding
let QOS_SOCKET_DEFAULT_CHARACTER_ENCODING: UInt = String.Encoding.utf8.rawValue

///
let QOS_CONTROL_CONNECTION_TIMEOUT_NS: UInt64 = 10_000_000_000
let QOS_CONTROL_CONNECTION_TIMEOUT_SEC = TimeInterval(QOS_CONTROL_CONNECTION_TIMEOUT_NS / NSEC_PER_SEC)

///
let QOS_DEFAULT_TIMEOUT_NS: UInt64 = 10_000_000_000 // default timeout value in nano seconds

///
let WALLED_GARDEN_URL: String = "http://www.netmetr.cz/generate_204" // TODO: use url from settings request

///
let WALLED_GARDEN_SOCKET_TIMEOUT_MS: Double = 10_000



///
#if DEBUG

let QOS_ENABLED_TESTS: [QOSTestType] = [
    .JITTER,
    .HTTP_PROXY,
    .NON_TRANSPARENT_PROXY,
    .WEBSITE,
    .DNS,
    .TCP,
    .UDP,
    .VOIP,
    .TRACEROUTE
]


#else

// BETA / PRODUCTION

let QOS_ENABLED_TESTS: [QOSTestType] = [
    .JITTER,
    .HTTP_PROXY,
    .NON_TRANSPARENT_PROXY,
    .WEBSITE,
    .DNS,
    .TCP,
    .UDP,
    .VOIP,
    .TRACEROUTE
]

/// determine the tests which should show log messages
let QOS_ENABLED_TESTS_LOG: [QOSTestType] = [
    .HTTP_PROXY,
    .NON_TRANSPARENT_PROXY,
    .WEBSITE,
    .DNS,
    .TCP,
    .UDP,
    .VOIP,
    .TRACEROUTE
]

#endif
