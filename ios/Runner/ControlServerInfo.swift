/*
* Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import Foundation

protocol ControlServerInfoDelegate{
    func changeSpeedDataStatus(taskStatus: TaskStatus)
    func changeQoSDataStatus(taskStatus: TaskStatus)
    
    func taskFinished(task: TestTask, resultData: ResultData?)
}

enum TaskStatus : Int {
    case WAIT, RUNNING, FINISHED
}

enum TestTask : Int{
    case SPEED, QOS
}

class ControlServerInfo : NSObject{
    
    var speedDataStatus: TaskStatus = .WAIT
    var qosDataStatus: TaskStatus = .WAIT
    
    var data: [TestTask : ResultData?]
    
    override init() {
        data = [.SPEED : nil, .QOS: nil]
    }
   
    func sending() -> Bool{
        return speedDataStatus == .RUNNING || qosDataStatus == .RUNNING
    }
    
}
