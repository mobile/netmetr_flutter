/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  LogConfig.swift
//  RMBT
//
//  Created by Benjamin Pucher on 03.02.15.
//  Copyright © 2015 SPECURE GmbH. All rights reserved.
//

import Foundation
import XCGLogger

///
let logger = XCGLogger.default;

///
class LogConfig {
    
    // TODO:
    // *) set log level in app
    
    ///
    static let fileDateFormatter = DateFormatter()
    static let startedAt = NSDate()
    
    /// setup logging system
    class func initLoggingFramework() {
        setupFileDateFormatter()
        
        let logFilePath = getCurrentLogFilePath()
        
        #if RELEASE
        // Release config
        // 1 logfile per day
        logger.setup(.Info, showLogLevel: true, showFileNames: false, showLineNumbers: true, writeToFile: logFilePath) /*.Error*/
        #elseif DEBUG
        // Debug config
        logger.setup(.Verbose, showLogLevel: true, showFileNames: false, showLineNumbers: true, writeToFile: nil) // don't need log to file
        #elseif BETA
        // Beta config
        logger.setup(.Debug, showLogLevel: true, showFileNames: false, showLineNumbers: true, writeToFile: logFilePath)
        
        uploadOldLogs()
        #endif
    }
    
    ///
    private class func setupFileDateFormatter() {
        let bundleIdentifier = Bundle.main.bundleIdentifier ?? "rmbt"
        let uuid = ControlServer.sharedControlServer.uuid ?? "uuid_missing"
        
        #if RELEASE
        fileDateFormatter.dateFormat = "'\(bundleIdentifier)_\(uuid)_'yyyy_MM_dd'.log'"
        #else
        fileDateFormatter.dateFormat = "'\(bundleIdentifier)_\(uuid)_'yyyy_MM_dd_HH_mm_ss'.log'"
        #endif
    }
    
    ///
    class func getCurrentLogFilePath() -> String {
        return getLogFolderPath() + "/" + getCurrentLogFileName()
    }
    
    ///
    class func getCurrentLogFileName() -> String {
        return fileDateFormatter.string(from: startedAt as Date)
    }
    
    ///
    class func getLogFolderPath() -> String {
        let cacheDirectory = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]
        let logDirectory = cacheDirectory + "/logs"
        
        // try to create logs directory if it doesn't exist yet
        if (!FileManager.default.fileExists(atPath: logDirectory)) {
            do {
                try FileManager.default.createDirectory(atPath: logDirectory, withIntermediateDirectories: false, attributes: nil)
            } catch {
                // TODO
            }
        }
        
        return logDirectory
    }
    
    ///
}
// TODO: move to other file...
extension NSDate : Comparable {}

///
/*public func ==(lhs: NSDate, rhs: NSDate) -> Bool {
 return lhs.timeIntervalSince1970 == rhs.timeIntervalSince1970
 }*/

///
public func <(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.timeIntervalSince1970 < rhs.timeIntervalSince1970
}
