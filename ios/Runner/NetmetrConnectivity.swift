//
//  NetmetrConnectivity.swift
//  Runner
//
//  Created by tablexia on 30/01/2020.
//  Copyright © 2020 The Chromium Authors. All rights reserved.
//

import Foundation
import Connectivity
import CoreTelephony

class NetmetrConnectivity {
    fileprivate let queue = DispatchQueue(label: "cz.nic.netmetrConnectivity")

    fileprivate var connectivity = Connectivity()
    
    fileprivate var isCheckingConnectivity: Bool = false
    fileprivate var actualStatus: Connectivity.Status = Connectivity.Status.notConnected
    
    fileprivate var lastConnectivity: RMBTConnectivity!

    fileprivate var relativeSignal: Double = 0.0
    
    init() {
        config()
        start()
    }

    
    deinit {
        connectivity.stopNotifier()
    }
    
    func stop(){
        connectivity.stopNotifier()
    }
    
    func start(){
        connectivity.startNotifier(queue: queue)
    }
    
    func config(){
        performSingleConnectivityCheck()
        configureConnectivityNotifier()
    }
    
    func configureConnectivityNotifier(){
        let connectivityChanged: (Connectivity)-> Void = { [weak self] connectivity in
            self?.newConnectivity(connectivity)
        }
        
        connectivity.whenConnected = connectivityChanged
        connectivity.whenDisconnected = connectivityChanged
    }
    
    func performSingleConnectivityCheck(){
        connectivity.checkConnectivity{ check in
            self.newConnectivity(check)
        }
    }
    
    func checkConnectivity(){
        connectivity.checkConnectivity { connectivity in
            print("NETWORK DESCR.: \(connectivity.description)")
            print("Percent:  \(connectivity.successThreshold)")
            
            let percentagle : ConnectivityPercentage = connectivity.successThreshold
            relativeSignal = percentagle.v
            if(connectivity.status != self.actualStatus){
                self.newConnectivity(connectivity)
            }
        }
    }
    
    func newConnectivity(_ connectivity: Connectivity){
        print("NETWORK DESCR.: \(connectivity.description)")
        print("Percent:  \(connectivity.successThreshold)")
        updateStatus(connectivity.status)
    }
    
    func updateStatus(_ status: Connectivity.Status){
        switch status {
        case .connectedViaWiFi:
            lastConnectivity = RMBTConnectivity(networkType: RMBTNetworkType.WiFi)
            actualStatus = status
            break
        case .connectedViaCellular:
            lastConnectivity = RMBTConnectivity(networkType: RMBTNetworkType.Cellular)
            actualStatus = status
            break
        default:
            lastConnectivity = nil
            actualStatus = .notConnected
        }
    }
    
}
