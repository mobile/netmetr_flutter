/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  DNSRecordClass.swift
//  DNSTest
//
//  Created by Benjamin Pucher on 10.03.15.
//  Copyright © 2015 Benjamin Pucher. All rights reserved.
//

import Foundation

let DNS_RCODE_TABLE: [UInt8:String] = [
    0: "NOERROR",
    1: "FORMERR",
    2: "SERVFAIL",
    3: "NXDOMAIN",
    4: "NOTIMP",
    //4: "NOTIMPL",
    5: "REFUSED",
    6: "YXDOMAIN",
    7: "YXRRSET",
    8: "NXRRSET",
    9: "NOTAUTH",
    10: "NOTZONE",
    16: "BADVERS",
    //16: "BADSIG",
    17: "BADKEY",
    18: "BADTIME",
    19: "BADMODE"
]

///
class DNSRecordClass: CustomStringConvertible {
    let name: String
    var qType: UInt16!
    var qClass: UInt16!
    var ttl: UInt32!
    
    let rcode: UInt8
    
    // TODO: improve...use struct...
    
    ///
    var ipAddress: String?
    
    ///
    var mxPreference: UInt16?
    var mxExchange: String?
    
    //
    
    ///
    var description: String {
        if let addr = ipAddress {
            return addr
        }
        
        return "\(String(describing: ipAddress))"
    }
    
    //
    
    ///
    init(name: String, qType: UInt16, qClass: UInt16, ttl: UInt32, rcode: UInt8) {
        self.name = name
        self.qType = qType
        self.qClass = qClass
        self.ttl = ttl
        self.rcode = rcode
    }
    
    ///
    init(name: String, rcode: UInt8) {
        self.name = name
        self.rcode = rcode
    }
    
    ///
    func rcodeString() -> String {
        return DNS_RCODE_TABLE[rcode] ?? "UNKNOWN" // !
    }
}
