/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  QOSDNSTestExecutor.swift
//  RMBT
//
//  Created by Benjamin Pucher on 29.01.15.
//  Copyright © 2014 SPECURE GmbH. All rights reserved.
//

import Foundation
import dnssd

///
typealias DNSTestExecutor = QOSDNSTestExecutor<QOSDNSTest>

///
class QOSDNSTestExecutor<T: QOSDNSTest>: QOSTestExecutorClass<T> {
    
    private let RESULT_DNS_STATUS           = "dns_result_status"
    private let RESULT_DNS_ENTRY            = "dns_result_entries"
    private let RESULT_DNS_TTL              = "dns_result_ttl"
    private let RESULT_DNS_ADDRESS          = "dns_result_address"
    private let RESULT_DNS_PRIORITY         = "dns_result_priority"
    private let RESULT_DNS_DURATION         = "dns_result_duration"
    private let RESULT_DNS_QUERY            = "dns_result_info"
    private let RESULT_DNS_RESOLVER         = "dns_objective_resolver"
    private let RESULT_DNS_HOST             = "dns_objective_host"
    private let RESULT_DNS_RECORD           = "dns_objective_dns_record"
    private let RESULT_DNS_ENTRIES_FOUND    = "dns_result_entries_found"
    private let RESULT_DNS_TIMEOUT          = "dns_objective_timeout"
    
    //
    
    ///
    override init(controlConnection: QOSControlConnection, delegateQueue: DispatchQueue, testObject: T, speedtestStartTime: UInt64) {
        super.init(controlConnection: controlConnection, delegateQueue: delegateQueue, testObject: testObject, speedtestStartTime: speedtestStartTime)
    }
    
    ///
    override func startTest() {
        super.startTest()
        
        testResult.set(key: RESULT_DNS_RESOLVER, value: (testObject.resolver ?? "Standard") as AnyObject )
        testResult.set(key: RESULT_DNS_RECORD,   value: testObject.record! as AnyObject)
        testResult.set(key: RESULT_DNS_HOST,     value: testObject.host! as AnyObject)
        testResult.set(key: RESULT_DNS_TIMEOUT,  number: testObject.timeout)
    }
    
    ///
    override func executeTest() {
        
        if let host = testObject.host {
            qosLog.debug(logMessage: "EXECUTING DNS TEST \(host)")
            
            let startTimeTicks = UInt64.getCurrentTimeTicks()
            
            // do dns query
            // TODO: improve
            
            // TODO: check if record is supported (in map)
            if let resolver = self.testObject.resolver {
                DNSClient.queryNameserver(resolver, serverPort: 53, forName: host, recordType: self.testObject.record!, success: { responseObj in
                    self.try_afterDNSResolution(startTimeTicks: startTimeTicks, responseObj: responseObj, error: nil)
                }, failure: { error in
                    self.try_afterDNSResolution(startTimeTicks: startTimeTicks, responseObj: nil, error: error)
                })
            } else {
                DNSClient.query(host, recordType: self.testObject.record!, success: { responseObj in
                    self.try_afterDNSResolution(startTimeTicks: startTimeTicks, responseObj: responseObj, error: nil)
                }, failure: { error in
                    self.try_afterDNSResolution(startTimeTicks: startTimeTicks, responseObj: nil, error: error)
                })
            }
           
        }
    }
    
    ///
    private func try_afterDNSResolution(startTimeTicks: UInt64, responseObj: DNSRecordClass?, error: NSError?) {
        do {
            try afterDNSResolution(startTimeTicks: startTimeTicks, responseObj: responseObj)
        } catch {
            self.testDidFail()
        }
    }
    
    ///
    private func afterDNSResolution(startTimeTicks: UInt64, responseObj: DNSRecordClass?) throws {
        
        self.testResult.set(key: self.RESULT_DNS_DURATION, number: UInt64.getTimeDifferenceInNanoSeconds(startTimeTicks))
        
        //
        
        //testResult.set(RESULT_DNS_STATUS, value: "NOERROR") // TODO: Rcode
        
        //
        
        var resourceRecordArray = [[String: AnyObject]]()
        
        if let response = responseObj {
            
            //for response.resultRecords {
            
            var resultRecord = [String: AnyObject]()
            
            testResult.set(key: RESULT_DNS_STATUS, value: response.rcodeString() as AnyObject)
            
            // TODO: improve this section
            
            if let qType = response.qType {
                
                switch (Int(qType)) {
                case kDNSServiceType_A:
                    resultRecord[RESULT_DNS_ADDRESS] = jsonValueOrNull(obj: response.ipAddress as AnyObject)
                case kDNSServiceType_CNAME:
                    resultRecord[RESULT_DNS_ADDRESS] = jsonValueOrNull(obj: response.ipAddress as AnyObject)
                case kDNSServiceType_MX:
                    resultRecord[RESULT_DNS_ADDRESS] = jsonValueOrNull(obj: response.ipAddress as AnyObject)
                    resultRecord[RESULT_DNS_PRIORITY] = "\(response.mxPreference!)" as AnyObject
                case kDNSServiceType_AAAA:
                    resultRecord[RESULT_DNS_ADDRESS] = jsonValueOrNull(obj: response.ipAddress as AnyObject)
                default:
                    qosLog.debug(logMessage: "unknown result record type \(String(describing: response.qType)), skipping")
                }
                
                resultRecord[RESULT_DNS_TTL] = "\(response.ttl!)" as AnyObject

                resourceRecordArray.append(resultRecord)
            }
            
            //}
        } else /*if let err = error*/ {
            // TODO: error?
            throw NSError(domain: "testtest", code: 111, userInfo: nil) // TODO
        }
        
        qosLog.debug(logMessage: "going to submit resource record array: \(resourceRecordArray)")
        
        testResult.set(key: RESULT_DNS_ENTRY, value: resourceRecordArray.count > 0 ? resourceRecordArray as NSArray : nil) // cast needed to prevent "HStore format unsupported"
        testResult.set(key: RESULT_DNS_ENTRIES_FOUND, value: resourceRecordArray.count as AnyObject)
        
        //callFinishCallback()
        testDidSucceed()
    }
    
    ///
    override func testDidSucceed() {
        testResult.set(key: RESULT_DNS_QUERY, value: "OK" as AnyObject)
        
        super.testDidSucceed()
    }
    
    ///
    override func testDidTimeout() {
        testResult.set(key: RESULT_DNS_QUERY, value: "TIMEOUT" as AnyObject)
        
        testResult.set(key: RESULT_DNS_ENTRY, value: /*[] as NSArray*/nil)
        testResult.set(key: RESULT_DNS_ENTRIES_FOUND, value: NSNumber(value: 0))
        
        super.testDidTimeout()
    }
    
    ///
    override func testDidFail() {
        testResult.set(key: RESULT_DNS_QUERY, value: "ERROR" as AnyObject)
        
        testResult.set(key: RESULT_DNS_ENTRY, value: nil)
        testResult.set(key: RESULT_DNS_ENTRIES_FOUND, value: NSNumber(value: 0))
        
        super.testDidFail()
    }
    
    ///
    override func needsControlConnection() -> Bool {
        return false
    }
}
