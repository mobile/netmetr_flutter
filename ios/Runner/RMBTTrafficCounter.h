#import <Foundation/Foundation.h>

#include <arpa/inet.h>
#include <net/if.h>

typedef struct {
    uint32_t bytesReceived;
    uint32_t bytesSent;
} RMBTConnectivityInterfaceInfo;

@interface RMBTTrafficCounter : NSObject

- (NSDictionary *)getTrafficCount;
- (NSDictionary *)getTrafficCount:(NSString *)interfaceName;

+ (RMBTConnectivityInterfaceInfo)getInterfaceInfo:(long)networkType;

@end
