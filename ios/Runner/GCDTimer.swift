/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  GCDTimer.swift
//  RMBT
//
//  Created by Benjamin Pucher on 09.02.15.
//  Copyright © 2015 SPECURE GmbH. All rights reserved.
//

import Foundation

///
class GCDTimer {
    
    typealias TimerCallback = () -> ()
    
    ///
    var timerCallback: TimerCallback?
    
    ///
    var interval: Double?
    
    ///
    private var timerSource: DispatchSourceTimer!
    
    ///
    private let timerQueue: DispatchQueue
    
    ///
    init() {
        timerQueue = DispatchQueue(label: "cz.nic.netmetr.timer", attributes: .concurrent)
    }
    
    ///
    deinit {
        stop()
    }
    
    ///
    func start() {
        if let interval = self.interval {
            stop() // stop any previous timer
            
            // start new timer
            timerSource = createTimer(interval, timerQueue: timerQueue) {
                logger.debug("timer fired")
                self.stop()
                
                self.timerCallback?()
            }
        }
    }
    
    ///
    func stop() {
        if timerSource != nil {
            timerSource.cancel()
        }
    }
    
    ///
    private func createTimer(_ interval: Double, timerQueue: DispatchQueue, block: @escaping ()->()) -> DispatchSourceTimer {
        let timer = DispatchSource.makeTimerSource(queue: timerQueue)
        let dt = DispatchTime.now() + Double(Int64(interval * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        
        timer.schedule(deadline: dt, leeway: DispatchTimeInterval.seconds(0))
        
        timer.setEventHandler {
            block()
        }
        
        timer.resume()
        
        return timer
    }
    
}
