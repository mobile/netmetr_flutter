/*
* Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import Foundation

func packetLoss(down: Int, up: Int, total: Int) -> Float{
    let tP : Float = Float(total * 2)
    let dif : Float  = Float((down + up)) / Float(tP)
    
   return 100.0 - dif * 100.0
}

func packetLossClassify(precent: Float) -> UInt8{
    switch precent {
       case ...5000:
           return 3
       case ...15000:
           return 2
       default:
           return 1
       }
}

func pingMS(nano: UInt64?) -> String?{
    if(nano == nil){
        return nil
    }
    
    return String(format: "%.0f ms", Double(nano! / 1000000))
}

func jitterClassify(jitter: Double) -> UInt8{
   switch jitter {
    case ...30000000:
        return 3
    case ...60000000:
        return 2
    default:
        return 1
    }
}

func pingClassify(nano: UInt64) -> UInt8{
    switch nano {
    case ...25000000:
        return 3
    case ...75000000:
        return 2
    default:
        return 1
    }
}

func signalClassify(signal : Int?, type: Int) -> UInt8{
    if(signal == nil || type == 0){
        return 0
    }
    
    let c3 : Int
    let c2 : Int
    
    switch type {
        //MOBILE
    case 1:
        c3 = -85
        c2 = -101
        break
        //LTE
    case 2:
        c3 = -95
        c2 = -111
        break
    default:
        c3 = -61
        c2 = -76
    }
    
    switch signal! {
    case c3...:
        return 3
    case c2...:
        return 2
    default:
        return 1
    }
}
