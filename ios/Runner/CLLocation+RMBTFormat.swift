//
//  CLLocation+RMBTFormat.swift
//  Runner
//
//  Created by tablexia on 13/12/2018.
//  Copyright © 2018 The Chromium Authors. All rights reserved.
//

import Foundation
import CoreLocation


extension CLLocation {
    
    
    func rmbtFormattedLatitudeString() -> String{
        var latSeconds: Int = Int(round(abs(self.coordinate.latitude * 3600)))
        let latDegrees = latSeconds / 3600
        latSeconds = latSeconds % 3600
        let latMinutes: CLLocationDegrees = Double(latSeconds) / 60.0
        
        return String(format: "%ld:%f", latDegrees as CLong, latMinutes)
    }
    
    func rmbtFormattedLongitudeString() -> String{
        var longSeconds: Int = Int(round(abs(self.coordinate.longitude * 3600)))
        let longDegrees: Int = longSeconds / 3600
        longSeconds = longSeconds % 3600
        let longMinutes: CLLocationDegrees = Double(longSeconds) / 60.0
        
        return String(format: "%ld:%f", longDegrees as CLong, longMinutes)
    }
    
}
