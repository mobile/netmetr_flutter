/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  RMBTConnectivity.swift
//  RMBT
//
//  Created by Benjamin Pucher on 21.09.15.
//  Copyright © 2015 SPECURE GmbH. All rights reserved.
//

import Foundation
import CoreTelephony
import SystemConfiguration.CaptiveNetwork

struct SignalBounds{
    var min : Double
    var max : Double
}

///
protocol SignalDetectDelegate {
    func signalChanged(connectivity: RMBTConnectivity)
}

///
class RMBTConnectivity {
    

    ///
    let networkType: RMBTNetworkType
    
    ///
    var timestamp: NSDate
    
    ///
    var networkTypeDescription: String {
        switch (networkType) {
        case .None:
            return NSLocalizedString("connectivity.not-connected", tableName: nil, bundle: Bundle.main, value: "Not connected", comment: "network type description not connected")
        case .WiFi:
            return NSLocalizedString("connectivity.wifi", tableName: nil, bundle: Bundle.main, value: "Wi-Fi", comment: "network type description wifi")
        case .Cellular:
            if (cellularCodeDescription != nil) {
                return cellularCodeDescription
            } else {
                return NSLocalizedString("connectivity.cellular", tableName: nil, bundle: Bundle.main, value: "Cellular", comment: "network type description cellular")
            }
        default:
            logger.warning("Invalid network type \(networkType)")
            return NSLocalizedString("intro.network.connection.name-unknown", tableName: nil, bundle: Bundle.main, value: "Unknown", comment: "network type description unknown")
        }
    }
    
    ///
    var networkName: String!
    
    ///
    private var bssid: String!
    
    ///
    private var cellularCode: NSNumber!
    
    ///
    var relativeSignal: Double!
    
    ///
    var lastRssi: Int!
    
    ///
    private var cellularCodeDescription: String!
    
    ///
    private var telephonyNetworkSimOperator: String!
    
    ///
    private var telephonyNetworkSimCountry: String!
    
    ///
    private let cellularCodeTable = [
        CTRadioAccessTechnologyGPRS:         1,
        CTRadioAccessTechnologyEdge:         2,
        CTRadioAccessTechnologyWCDMA:        3,
        CTRadioAccessTechnologyCDMA1x:       4,
        CTRadioAccessTechnologyCDMAEVDORev0: 5,
        CTRadioAccessTechnologyCDMAEVDORevA: 6,
        CTRadioAccessTechnologyHSDPA:        8,
        CTRadioAccessTechnologyHSUPA:        9,
        CTRadioAccessTechnologyCDMAEVDORevB: 12,
        CTRadioAccessTechnologyLTE:          13,
        CTRadioAccessTechnologyeHRPD:        14
    ]
    
    ///
    private let cellularCodeDescriptionTable = [
        CTRadioAccessTechnologyGPRS:            "GPRS (2G)",
        CTRadioAccessTechnologyEdge:            "EDGE (2G)",
        CTRadioAccessTechnologyWCDMA:           "UMTS (3G)",
        CTRadioAccessTechnologyCDMA1x:          "CDMA (2G)",
        CTRadioAccessTechnologyCDMAEVDORev0:    "EVDO0 (2G)",
        CTRadioAccessTechnologyCDMAEVDORevA:    "EVDOA (2G)",
        CTRadioAccessTechnologyHSDPA:           "HSDPA (3G)",
        CTRadioAccessTechnologyHSUPA:           "HSUPA (3G)",
        CTRadioAccessTechnologyCDMAEVDORevB:    "EVDOB (2G)",
        CTRadioAccessTechnologyLTE:             "LTE (4G)",
        CTRadioAccessTechnologyeHRPD:           "HRPD (2G)"
    ]
    
    private let unknownType = 0
       
    ///
    var signalDelegate : SignalDetectDelegate!
    
    ///
    var statusManager: NetmetrStatusBarManager?
        
    ///
    init(networkType: RMBTNetworkType, statusManager: NetmetrStatusBarManager?) {
        self.networkType = networkType
        self.timestamp = NSDate()
        self.relativeSignal = 0.0
        self.statusManager = statusManager
        
        do{
            try getNetworkDetails()
        } catch {
            //ignore
        }

    }
    
    ///
    func newRssi(rssi: Int?){
        if(rssi == nil){ return;}
        
        if(self.lastRssi != rssi){
            self.timestamp = NSDate()
            self.lastRssi = rssi
            
            if(signalDelegate != nil){
                self.signalDelegate.signalChanged(connectivity: self)
            }
        }
    }
    func getMobileNetworkTypeID() -> NSNumber?{
        return cellularCode
    }
    
    // MARK: Internal
    
    ///
    func getNetworkDetails() throws{
        networkName = nil
        bssid = nil
        cellularCode = nil
        cellularCodeDescription = nil
        
        switch (networkType) {
            case .Cellular:
                // Get carrier name
                let netinfo = CTTelephonyNetworkInfo()
                var serviceCode : String!;
                
                if #available(iOS 12.0, *) {
                    for(service, carrier) in netinfo.serviceSubscriberCellularProviders ?? [:]{
                        if(carrier.mobileNetworkCode != nil && !(carrier.mobileNetworkCode!.isEmpty) ){
                            networkName = carrier.carrierName
                            telephonyNetworkSimCountry = carrier.isoCountryCode
                            telephonyNetworkSimOperator = "\(carrier.mobileCountryCode ?? "/")-\(carrier.mobileNetworkCode ?? "/")"
                            serviceCode = service
                            break
                        }
                    }
                } else {
                    if let carrier = netinfo.subscriberCellularProvider {
                        networkName = carrier.carrierName
                        telephonyNetworkSimCountry = carrier.isoCountryCode
                        telephonyNetworkSimOperator = "\(carrier.mobileCountryCode ?? "/")-\(carrier.mobileNetworkCode ?? "/")"
                    }
                }
                
                if (netinfo.responds(to: #selector(getter: CTTelephonyNetworkInfo.currentRadioAccessTechnology))) {

                    if #available(iOS 12.0, *) {
                        for(service, technology) in netinfo.serviceCurrentRadioAccessTechnology ?? [:]{
                            if(service == serviceCode){
                                cellularCode = cellularCodeForCTValue(value: technology)
                                cellularCodeDescription = cellularCodeDescriptionForCTValue(value: technology)
                                break
                            }
                        }
                    } else {
                        cellularCode = cellularCodeForCTValue(value: netinfo.currentRadioAccessTechnology)
                        cellularCodeDescription = cellularCodeDescriptionForCTValue(value: netinfo.currentRadioAccessTechnology)
                        
                    }
                    
                    
                }
            
            case .WiFi:
                // If WLAN, then show SSID as network name. Fetching SSID does not work on the simulator.
                if let wifiParams = getWiFiParameters() {
                    networkName = wifiParams.ssid
                    bssid = wifiParams.bssid
                }
                
                break
            
            case .None:
                break
            default:
                assert(false, "Invalid network type \(networkType)")
        }
     
    }
    
    func networkID() -> Int{
        switch networkType {
        case .Cellular:
            if let code = getMobileNetworkTypeID(){
                return code.intValue
            }else{
                return -1
            }
        case .WiFi:
            return 99
        default:
            return -1
        }
    }
    
    func refreshSignalStrength() throws{
        var rssi : Int?
        
        do{
            if #available(iOS 11, *){
                if let bars = numberOfActiveBars(type: networkType){
                    rssi = getSignalStrengthFromActiveBars(bars: bars, type: networkType)
                    relativeSignal = getRelativeSignalFromActiveBars(bars: bars)
                }
            }else{
                switch networkType {
                    case .Cellular:
                        // Earlier version of iOS
                        rssi = try getSignalStrength()
                        
                        if(rssi != nil){
                            relativeSignal = getRelativeSignalFromRSSI(rssi: rssi)
                        }
                        break
                    case .WiFi:
                        rssi = try getWifiStrength()
                        if(rssi != nil){
                            relativeSignal = getRelativeSignalFromRSSI(rssi: rssi)
                        }
                        break
                    default:
                        relativeSignal = 0.0
                }
            }
            
             self.newRssi(rssi: rssi)
            
        } catch{
        
        }
    }
    
    func getSignalType() -> Int{
        switch networkType {
            case .Cellular:
                return  lteSignalBounds() ? 2 : 1
            case .WiFi:
                return 3
            default:
                return 0
        }
    }
    
    func getSignal() -> Int?{
        var rssi : Int?
        do{
            if #available(iOS 11, *) {
                if let bars = numberOfActiveBars(type: networkType){
                    rssi = getSignalStrengthFromActiveBars(bars: bars, type: networkType);
                }
            }else{
                switch networkType {
                    case .Cellular:
        
                        // Earlier version of iOS
                        rssi = try getSignalStrength()
        
                        break
                    case .WiFi:
                        // Earlier version of iOS
                        rssi = try getWifiStrength()
                        break
                    case .Unknown:
                        rssi = try getSignalStrength()
                        break
                    default:
                        rssi = nil
                }
            }
            self.newRssi(rssi: rssi)

        }catch{
            //ignore
        }
        return rssi
    }
    
    //TODO poresit jak zpracovat active bars pro iPhone X
    private func getSignalStrengthFromActiveBars(bars: Int, type: RMBTNetworkType) -> Int{
        if(type == .WiFi){
            return getWifiSignalStrengthFromActiveBars(bars: bars)
        }
        
        if(cellularCodeDescription == cellularCodeDescriptionTable[CTRadioAccessTechnologyLTE]){
            return getLTESignalStrengthFromActiveBars(bars: bars)
        }
        
        return getMobileSignalStrengthFromActiveBars(bars: bars)
    }
    
    private func getMobileSignalStrengthFromActiveBars(bars: Int) -> Int{
        switch bars{
        case 4:
            return -65
        case 3:
            return -77
        case 2:
            return -92
        case 1:
            return -105
        default:
            return -110
        }
    }
    
    private func getLTESignalStrengthFromActiveBars(bars: Int) -> Int{
        switch bars{
            case 4:
                return -75
            case 3:
                return -85
            case 2:
                return -95
            case 1:
                return -105
            default:
                return -130
        }
    }
    
    private func getWifiSignalStrengthFromActiveBars(bars: Int) -> Int{
        switch bars{
            case 4:
                return -45
            case 3:
                return -55
            case 2:
                return -65
            case 1:
                return -75
            default:
                return -100
        }
    }
    
    private func getRelativeSignalFromActiveBars(bars: Int) -> Double{
        switch bars{
            case 4:
                return 1
            case 3:
                return 0.7
            case 2:
                return 0.4
            case 1:
                return 0.2
            default:
                 return 0
        }
    }
    
    private func getRelativeSignalFromRSSI(rssi: Int?) -> Double{
        if(rssi == nil){
            return 0.0
        }
        
        let signalBounds = getSignalBounds(type: networkType)
        
        if(signalBounds == nil){
            return 0.0
        }
        
        let signal : Double? = Double.init(exactly: rssi!) ?? nil
        
        if(signal == nil){
            return 0.0
        }
        
        return (signal! - signalBounds!.min) / (signalBounds!.max - signalBounds!.min)
    }
    
    private func getSignalBounds(type: RMBTNetworkType) -> SignalBounds!{
        switch type {
        case .WiFi:
            return SignalBounds(min: -100, max: -40)
        case .Cellular:
            if lteSignalBounds(){
                return SignalBounds(min: -130, max: -70)
            }
            
            return SignalBounds(min: -110, max: -50)
        case .Unknown:
            return SignalBounds(min: -110, max: -50) //RSRP
        default:
            return nil
        }
    }
    
    private func lteSignalBounds() -> Bool{
        if #available(iOS 14.1, *){
            return cellularCodeDescription == cellularCodeDescriptionTable[CTRadioAccessTechnologyLTE] || cellularCodeDescription == cellularCodeDescriptionTableIOS14[CTRadioAccessTechnologyNR]
        }
        
        return cellularCodeDescription == cellularCodeDescriptionTable[CTRadioAccessTechnologyLTE]
    }
    
    //iOS11 - network active bars
    private func numberOfActiveBars(type: RMBTNetworkType) -> Int? {
        var numberOfActiveBars: Int?
        
        let exception = tryBlock {
            guard let containerBar = self.statusManager?.getStatusBar() else {return}
            
            if #available(iOS 13, *){
                
                guard let currentData = containerBar.value(forKey: "currentData") else { numberOfActiveBars = 0; return }
                let bars = (currentData as AnyObject).value(forKeyPath: type == RMBTNetworkType.WiFi ? "wifiEntry" : "cellularEntry")

                numberOfActiveBars = (bars as AnyObject).value(forKeyPath: "displayValue") as? Int
            }else {
            
                guard let statusBar = containerBar.value(forKey: "statusBar") as? UIView else { return }
                
                if self.isiPhoneX() || UIDevice.current.userInterfaceIdiom == .pad{
                    guard let foregroundView = statusBar.value(forKey: "foregroundView") as? UIView else { return }
                    
                    for view in foregroundView.subviews {
                        if(!view.subviews.isEmpty){
                            for v in view.subviews {
                                if let statusBarWifiSignalView = NSClassFromString(type == RMBTNetworkType.WiFi ? "_UIStatusBarWifiSignalView" : "_UIStatusBarCellularSignalView"), v .isKind(of: statusBarWifiSignalView) {
                                    if let val = v.value(forKey: "numberOfActiveBars") as? Int {
                                        numberOfActiveBars = val
                                        break
                                    }
                                }
                            }
                        }else{
                            if let statusBarWifiSignalView = NSClassFromString(type == RMBTNetworkType.WiFi ? "_UIStatusBarWifiSignalView" : "_UIStatusBarCellularSignalView"), view .isKind(of: statusBarWifiSignalView) {
                                if let val = view.value(forKey: "numberOfActiveBars") as? Int {
                                    numberOfActiveBars = val
                                    break
                                }
                            }
                        }
                        
                        
                        if let _ = numberOfActiveBars {
                            break
                        }
                    }
                } else{
                    
                    for subbiew in statusBar.subviews{
                        if subbiew.classForKeyedArchiver.debugDescription == "Optional(UIStatusBarForegroundView)" {
                            for subbiew2 in subbiew.subviews {
                                
                                if subbiew2.classForKeyedArchiver.debugDescription == "Optional(UIStatusBarSignalStrengthItemView)" {
                                    
                                    if let bars = subbiew2.value(forKey: "signalStrengthBars") as? Int{
                                        numberOfActiveBars = bars
                                        break
                                    }
                                }
                            }
                        }
                        
                        if let _ = numberOfActiveBars {
                           break
                       }
                    }
                    
                }
            }
        
        }
        if let exception = exception {
            print("getWiFiNumberOfActiveBars exception: \(exception)")
        }
        
        return numberOfActiveBars
    }
    
    private func isiPhoneX() -> Bool {
        if UIDevice.current.userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                //print("iPhone 5 or 5S or 5C")
                break
            case 1334:
                //print("iPhone 6/6S/7/8")
                break
            case 2208:
                //print("iPhone 6+/6S+/7+/8+")
                break
            case 2436:
                //print("iPhone X")
                return true
            default:
                // iPhone XR, X+ 11 etc
                if UIScreen.main.nativeBounds.height > 2436 {
                    return true
                }
                break
                //print("unknown")
            }
        }
        return false
    }
    
    //Another devices than iOS11
    private func getWifiStrength() throws -> Int? {
        var rssi: Int?
        
        let exception = tryBlock {
    
            guard let statusBar = self.statusManager?.getStatusBar() else {return}
            guard let foregroundView = statusBar.value(forKey: "foregroundView") as? UIView else { return }
            
            for view in foregroundView.subviews {
                if let statusBarDataNetworkItemView = NSClassFromString("UIStatusBarDataNetworkItemView"), view .isKind(of: statusBarDataNetworkItemView) {
                    if let val = view.value(forKey: "wifiStrengthRaw") as? Int {
                        //print("rssi: \(val)")
                        
                        rssi = val
                        break
                    }
                }
            }

        }
        if let exception = exception {
            print("get signal exception: \(exception)")
        }
        return rssi
    }
    
    func getSignalStrength() throws -> Int? {
        var bars: Int?

        
        let exception = tryBlock {
            guard let statusBar = self.statusManager?.getStatusBar() else {return}
            guard let foregroundView = statusBar.value(forKey: "foregroundView") as? UIView else{ return }
            
            var dataNetworkItemView:UIView!
            let foregroundViewSubviews = foregroundView.subviews
        
            for subview in foregroundViewSubviews {
                if subview.isKind(of: NSClassFromString("UIStatusBarSignalStrengthItemView")!) {
                    dataNetworkItemView = subview
                    break
                }
            }
            
            if(dataNetworkItemView == nil){
                return
            }
            
            if let val = dataNetworkItemView.value(forKey: "signalStrengthBars") as? Int {
                bars = val
            }
           
        }
        if let exception = exception {
            print("get signal exception: \(exception)")
        }
        return bars
    }
    
    ///
    private func getWiFiParameters() -> (ssid: String, bssid: String)? {
        if let interfaces = CNCopySupportedInterfaces() {
            for i in 0..<CFArrayGetCount(interfaces) {
                let interfaceName: UnsafeRawPointer = CFArrayGetValueAtIndex(interfaces, i)
                let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
                if let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)" as CFString) {
                    let interfaceData = unsafeInterfaceData as [NSObject:AnyObject]
                    
                    if let currentSSID = interfaceData[kCNNetworkInfoKeySSID] as? String,
                        let currentBSSID = interfaceData[kCNNetworkInfoKeyBSSID] as? String {
                        return (ssid: currentSSID, bssid: RMBTReformatHexIdentifier(identifier: currentBSSID))
                    }
                }
            }
        }
        
        return nil
    }
    
    ///
    private func cellularCodeForCTValue(value: String!) -> NSNumber? {
        if (value == nil) {
            return nil
        }
        
        var cellularTable: [String : Int] = cellularCodeTable
        if #available(iOS 14.1, *) {
            cellularTable = cellularCodeTableIOS14
        }

        if let code = cellularTable[value]{
            return code as NSNumber
        }
        
        return unknownType as NSNumber
    }
    
    ///
    private func cellularCodeDescriptionForCTValue(value: String!) -> String? {
        if (value == nil) {
            return nil
        }
        
        
        var cellularTable: [String : String] = cellularCodeDescriptionTable
        if #available(iOS 14.1, *) {
            cellularTable = cellularCodeDescriptionTableIOS14
        }
        
        if let desc = cellularTable[value]{
            return desc
        }
        
        return "Unknown"
    }
    
    
    ///
    func testResultDictionary() -> NSDictionary {
        var result = [String:AnyObject]()
        
        var code : Int
        
        switch networkType{
        case .WiFi: code = networkType.rawValue; break
        case .Cellular:
            if let value = getMobileNetworkTypeID(){
                code = value.intValue
            }else{
                code = 0
            }
            break
        default: code = 0
        }
        
        if (code > 0) {
    
            result["network_type"] = NSNumber(value: code)
        }
        
        if (networkType == .WiFi) {
            if (networkName != nil) {
                result["wifi_ssid"] = networkName as AnyObject
            }
            
            if (bssid != nil) {
                result["wifi_bssid"] = bssid as AnyObject
            }
            
            //result["wifi_link_speed"] = 72 as AnyObject //TODO KDE LINK SPEED
            result["wifi_rssi"] = lastRssi as AnyObject
            
        } else if (networkType == .Cellular) {
            if (cellularCode != nil) {
                result["network_type"] = cellularCode
            }
            result["lte_rsrp"] = lastRssi as AnyObject
            
            result["telephony_network_sim_operator_name"]   = RMBTValueOrNull(value: networkName as AnyObject)
            result["telephony_network_sim_country"]         = RMBTValueOrNull(value: telephonyNetworkSimCountry as AnyObject)
            result["telephony_network_sim_operator"]        = RMBTValueOrNull(value: telephonyNetworkSimOperator as AnyObject)
        }
        
        return result as NSDictionary
    }
    
    ///
    func isEqualToConnectivity(otherConn: RMBTConnectivity?) -> Bool {
        if let other = otherConn {
            if (other === self) {
                return true
            }
            
            // cannot compare two optional strings with ==, because one or both could be nil
            if let oNetworkName = other.networkName, let sNetworkName = self.networkName {
                return other.networkTypeDescription == self.networkTypeDescription && oNetworkName == sNetworkName
            }
        }
        
        return false
    }
    
    ///
    func getInterfaceInfo() -> RMBTConnectivityInterfaceInfo {
        return RMBTTrafficCounter.getInterfaceInfo(networkType.rawValue)
    }
}
