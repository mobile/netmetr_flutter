//
//  OCCatch.h
//  Runner
//
//  Created by tablexia on 12/12/2018.
//  Copyright © 2018 The Chromium Authors. All rights reserved.
//

#ifndef ObjecException_h
#define ObjecException_h

#import <Foundation/Foundation.h>

NS_INLINE NSException * _Nullable tryBlock(void(^_Nonnull tryBlock)(void)) {
    @try {
        tryBlock();
    }
    @catch (NSException *exception) {
        return exception;
    }
    return nil;
}


#endif /* OCCatch_h */
