/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  RMBTConfig.swift
//  RMBT
//
//  Created by Tomáš Baculák on 14/01/15.
//  Copyright © 2015 SPECURE GmbH. All rights reserved.
//

// MARK: Fixed test parameters
import CoreLocation
///
let RMBT_TEST_CIPHER = SSL_RSA_WITH_RC4_128_MD5

///
let RMBT_TEST_SOCKET_TIMEOUT_S = 30.0

/// Maximum number of tests to perform in loop mode
let RMBT_TEST_LOOPMODE_LIMIT = 100

///
let RMBT_TEST_LOOPMODE_WAIT_BETWEEN_RETRIES_S = 5

///
let RMBT_TEST_PRETEST_MIN_CHUNKS_FOR_MULTITHREADED_TEST = 4

///
let RMBT_TEST_PRETEST_DURATION_S = 2.0

///
let RMBT_TEST_PING_COUNT = 10

/// In case of slow upload, we finalize the test even if this many seconds still haven't been received:
let RMBT_TEST_UPLOAD_MAX_DISCARD_S = 1.0

/// Minimum number of seconds to wait after sending last chunk, before starting to discard.
let RMBT_TEST_UPLOAD_MIN_WAIT_S    = 0.25

/// Maximum number of seconds to wait for server reports after last chunk has been sent.
/// After this interval we will close the socket and finish the test on first report received.
let RMBT_TEST_UPLOAD_MAX_WAIT_S    = 3

/// Measure and submit speed during test in these intervals
let RMBT_TEST_SAMPLING_RESOLUTION_MS = 250

///
let RMBT_CONTROL_SERVER_PATH = "/RMBTControlServer"

let RMBT_CONTROL_SERVER_URL        = "...\(RMBT_CONTROL_SERVER_PATH)"


///
let RMBT_MAP_SERVER_PATH = "/RMBTMapServer"

/// BuildConfig keys
let CLIENT_SECRET_BUNDLE = "CLIENT_SECRET"
let CONTROL_HOST_BUNDLE = "CONTROL_HOST"
let CONTROL_IPV4_HOST_BUNDLE = "CONTROL_IPV4_HOST"
let CONTROL_IPV6_HOST_BUNDLE = "CONTROL_IPV6_HOST"
let CHECK_IPV4_HOST_BUNDLE = "CHECK_IPV4_HOST"
let CHECK_IPV6_HOST_BUNDLE = "CHECK_IPV6_HOST"
let QOS_SSL_BUNDLE = "QOS_SSL"
let CONTROL_SSL_BUNDLE = "CONTROL_SSL"
let PORT_BUNDLE = "PORT"
let SENTRY_DSN = "SENTRY_DSN"
let SENTRY_URL = "SENTRY_URL"
let NETMETR_GOOGLE_API_KEY = "NETMETR_GOOGLE_API_KEY"

///JITTER
let JITTER_STATUS                 = "jitter_status"
let JITTER_MEAN_UP                = "jitter_mean_up"
let JITTER_MEAN_DOWN              = "jitter_mean_down"

let NUM_PACKET_DOWN              = "num_packet_up"
let NUM_PACKET_UP                = "num_packet_down"
let PACKET_TOTAL                 = "packet_total"

let RESULT_VOIP_NUM_TOTAL_PACKETS = "jitter_total_num_packets"
let RESULT_VOIP_PREFIX          = "voip_result"
let RESULT_VOIP_PREFIX_INCOMING = "_in_"
let RESULT_VOIP_PREFIX_OUTGOING = "_out_"
let RESULT_VOIP_STATUS              = "voip_result_status"
let RESULT_VOIP_MAX_JITTER          = "max_jitter"
let RESULT_VOIP_MEAN_JITTER         = "mean_jitter"
let RESULT_VOIP_NUM_PACKETS         = "num_packets"
let RESULT_VOIP_IN_NUM_PACKETS        = "voip_result_in_num_packets"


//
let RMBT_ABOUT_URL       = "https://www.ctu.cz/"
let RMBT_PROJECT_EMAIL   = "netmetr@labs.nic.cz"

let RMBT_REPO_URL        = "https://gitlab.labs.nic.cz/labs/netmetr-ios"
let RMBT_DEVELOPER_URL   = "https://specure.com"
let RMBT_LOCAL_DEVELOPER_URL = "https://labs.nic.cz"


/// Initial map center coordinates and zoom level
let RMBT_MAP_INITIAL_LAT: CLLocationDegrees = 49.600170585239695
let RMBT_MAP_INITIAL_LNG: CLLocationDegrees = 15.509453192353249

let RMBT_MAP_INITIAL_ZOOM: Float = 5.773921

/// Zoom level to use when showing a test result location
let RMBT_MAP_POINT_ZOOM: Float = 12.0

/// In "auto" mode, when zoomed in past this level, map switches to points
let RMBT_MAP_AUTO_TRESHOLD_ZOOM: Float = 12.0

// Google Maps API Key

///#warning Please supply a valid Google Maps API Key. See https://developers.google.com/maps/documentation/ios/start#the_google_maps_api_key
let RMBT_GMAPS_API_KEY = ""

// MARK: Misc

/// Current TOS version. Bump to force displaying TOS to users again.
let RMBT_TOS_VERSION = 1

///////////////////

let TEST_SHOW_TRAFFIC_WARNING_ON_CELLULAR_NETWORK = false
let TEST_SHOW_TRAFFIC_WARNING_ON_WIFI_NETWORK = false

let TEST_USE_PERSONAL_DATA_FUZZING = false

// If set to false: Statistics is not visible, tap on map points doesn't show bubble, ...
let USE_OPENDATA = true
