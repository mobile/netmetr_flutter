/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  QOSNonTransparentProxyTestExecutor.swift
//  RMBT
//
//  Created by Benjamin Pucher on 09.12.14.
//  Copyright © 2014 SPECURE GmbH. All rights reserved.
//

import Foundation
import CocoaAsyncSocket

///
typealias NonTransparentProxyTestExecutor = QOSNonTransparentProxyTestExecutor<QOSNonTransparentProxyTest>

///
class QOSNonTransparentProxyTestExecutor<T: QOSNonTransparentProxyTest>: QOSTestExecutorClass<T>, GCDAsyncSocketDelegate {
    
    private let RESULT_NONTRANSPARENT_PROXY_RESPONSE    = "nontransproxy_result_response"
    private let RESULT_NONTRANSPARENT_PROXY_REQUEST     = "nontransproxy_objective_request"
    private let RESULT_NONTRANSPARENT_PROXY_PORT        = "nontransproxy_objective_port"
    private let RESULT_NONTRANSPARENT_PROXY_TIMEOUT     = "nontransproxy_objective_timeout"
    private let RESULT_NONTRANSPARENT_PROXY_STATUS      = "nontransproxy_result"
    
    //
    
    let TAG_TASK_NTPTEST = 2001
    
    //
    
    let TAG_NTPTEST_REQUEST = -1
    
    ///
    private let socketQueue = DispatchQueue(label: "com.specure.rmbt.qos.ntp.socketQueue", attributes: .concurrent)
    
    ///
    private var ntpTestSocket: GCDAsyncSocket!
    
    ///
    private var gotReply = false
    
    //
    
    ///
    override init(controlConnection: QOSControlConnection, delegateQueue: DispatchQueue, testObject: T, speedtestStartTime: UInt64) {
        super.init(controlConnection: controlConnection, delegateQueue: delegateQueue, testObject: testObject, speedtestStartTime: speedtestStartTime)
    }
    
    ///
    override func startTest() {
        super.startTest()
        
        testResult.set(key: RESULT_NONTRANSPARENT_PROXY_TIMEOUT, number: testObject.timeout)
        testResult.set(key: RESULT_NONTRANSPARENT_PROXY_REQUEST, value: testObject.request?.stringByRemovingLastNewline() as AnyObject)
    }
    
    ///
    override func executeTest() {
        if let port = testObject.port {
            testResult.set(key: RESULT_NONTRANSPARENT_PROXY_PORT, number: port)
            
            qosLog.debug(logMessage: "EXECUTING NON TRANSPARENT PROXY TEST")
            qosLog.debug(logMessage: "requesting NTPTEST on port \(port)")
            
            // request NTPTEST
            controlConnection.sendTaskCommand(command: "NTPTEST \(port)", withTimeout: timeoutInSec, forTaskId: testObject.qosTestId, tag: TAG_TASK_NTPTEST)
        }
    }
    
    ///
    override func testDidSucceed() {
        testResult.set(key: RESULT_NONTRANSPARENT_PROXY_STATUS, value: "OK" as AnyObject)
        
        super.testDidSucceed()
    }
    
    ///
    override func testDidTimeout() {
        testResult.set(key: RESULT_NONTRANSPARENT_PROXY_STATUS, value: "TIMEOUT" as AnyObject)
        
        super.testDidTimeout()
    }
    
    ///
    override func testDidFail() {
        qosLog.debug(logMessage: "NTP: TEST DID FAIL")
        
        testResult.set(key: RESULT_NONTRANSPARENT_PROXY_RESPONSE, value: "" as AnyObject)
        testResult.set(key: RESULT_NONTRANSPARENT_PROXY_STATUS, value: "ERROR" as AnyObject)
        
        super.testDidFail()
    }
    
    // MARK: QOSControlConnectionDelegate methods

    override func controlConnection(_ connection: QOSControlConnection, didReceiveTaskResponse response: String, withTaskId taskId: UInt, tag: Int) {
        qosLog.debug(logMessage: "CONTROL CONNECTION DELEGATE FOR TASK ID \(taskId), WITH TAG \(tag), WITH STRING \(response)")
        
        switch tag {
        case TAG_TASK_NTPTEST:
            qosLog.debug(logMessage: "NTPTEST response: \(response)")
            
            if (response.hasPrefix("OK")) {
                
                // create client socket
                ntpTestSocket = GCDAsyncSocket(delegate: self, delegateQueue: delegateQueue, socketQueue: socketQueue)
                
                // connect client socket
                do {
                    try ntpTestSocket.connect(toHost: testObject.serverAddress, onPort: testObject.port!, withTimeout: timeoutInSec)
                } catch {
                    // there was an error
                    qosLog.debug(logMessage: "connection error \(error)")
                    
                    return testDidFail()
                }
            } else {
                testDidFail()
            }
            
        default:
            // do nothing
            qosLog.debug(logMessage: "default case: do nothing")
        }
    }
    
    // MARK: GCDAsyncSocketDelegate methods
    
    func socket(_ sock: GCDAsyncSocket, didConnectToHost host: String, port: UInt16) {
        if (sock == ntpTestSocket) {
            
            qosLog.debug(logMessage: "will send \(String(describing: testObject.request)) to the server")
            
            // write request message and read response
            ntpTestSocket.writeLine(line: testObject.request!, withTimeout: timeoutInSec, tag: TAG_NTPTEST_REQUEST) // TODO: what if request is nil?
            ntpTestSocket.readLine(tag: TAG_NTPTEST_REQUEST, withTimeout: timeoutInSec)
        }
    }
    
    func socket(_ sock: GCDAsyncSocket, didRead data: Data, withTag tag: Int) {
        if (sock == ntpTestSocket) {
            switch tag {
            case TAG_NTPTEST_REQUEST:
                
                gotReply = true
                
                qosLog.debug(logMessage: "\(String(describing: String(data: data as Data, encoding: String.Encoding.ascii)))")
                
                if let response = SocketUtils.parseResponseToString(data) {
                    qosLog.debug(logMessage: "response: \(response)")
                    
                    testResult.set(key: RESULT_NONTRANSPARENT_PROXY_RESPONSE, value: response.stringByRemovingLastNewline() as AnyObject)
                    
                    testDidSucceed()
                } else {
                    qosLog.debug(logMessage: "NO RESP")
                    testDidFail()
                }
                
            default:
                // do nothing
                qosLog.debug(logMessage: "do nothing")
            }
        }
    }
    
    func socketDidDisconnect(_ sock: GCDAsyncSocket, withError err: Error?) {
        //if (err != nil && err.code == GCDAsyncSocketConnectTimeoutError) { //check for timeout
        //    return testDidTimeout()
        //}
        qosLog.debug(logMessage: "DID DISC gotreply (before?): \(gotReply), error: \(String(describing: err))")
        if (!gotReply) {
            testDidFail()
        }
    }
}
