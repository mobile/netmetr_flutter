/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  UDPStreamReceiver.swift
//  RMBT
//
//  Created by Benjamin Pucher on 25.02.15.
//  Copyright © 2015 SPECURE GmbH. All rights reserved.
//

import Foundation
import CocoaAsyncSocket

///
struct UDPStreamReceiverSettings {
    var port: UInt16 = 0
    var delegateQueue: DispatchQueue
    var sendResponse: Bool = false
    var maxPackets: UInt16 = 5
    var timeout: UInt64 = 10_000_000_000
}

///
class UDPStreamReceiver: NSObject {
    
    ///
    private let socketQueue = DispatchQueue(label: "com.specure.rmbt.udp.socketQueue")
    
    ///
    private var udpSocket: GCDAsyncUdpSocket!
    
    ///
    private let countDownLatch = CountDownLatch()
    
    ///
    //private var running: AtomicBoolean = AtomicBoolean()
    private var running: Bool = false
    
    //
    
    ///
    var delegate: UDPStreamReceiverDelegate?
    
    ///
    private let settings: UDPStreamReceiverSettings
    
    ///
    private var packetsReceived: UInt16 = 0
    
    //
    
    ///
    required init(settings: UDPStreamReceiverSettings) {
        self.settings = settings
    }
    
    ///
    func stop() {
        countDownLatch.countDown()
    }
    
    ///
    private func connect() {
        logger.debug("connecting udp socket")
        udpSocket = GCDAsyncUdpSocket(delegate: self, delegateQueue: settings.delegateQueue, socketQueue: socketQueue)
        
        do {
            try udpSocket.bind(toPort: settings.port)
            try udpSocket.beginReceiving()
        } catch {
            // TODO: check error (i.e. fail if error)
        }
    }
    
    ///
    private func close() {
        logger.debug("closing udp socket")
        udpSocket?.close()
    }
    
    ///
    private func receivePacket(dataReceived: Data, fromAddress address: Data) { // TODO: use dataReceived
        packetsReceived+=1
        
        // didReceive callback
        
        var shouldStop: Bool = false
        
        settings.delegateQueue.sync {
            shouldStop = self.delegate?.udpStreamReceiver(self, didReceivePacket: dataReceived) ?? false
        }
        
        if (shouldStop || packetsReceived >= settings.maxPackets) {
            stop()
        }
        
        // send response
        
        if (settings.sendResponse) {
            var data = NSMutableData()
            
            var shouldSendResponse: Bool = false
            
            settings.delegateQueue.sync {
                shouldSendResponse = self.delegate?.udpStreamReceiver(self, willSendPacketWithNumber: self.packetsReceived, data: &data) ?? false
            }
            
            if (shouldSendResponse && data.length > 0) {
                udpSocket.send(data as Data, toAddress: address as Data, withTimeout: nsToSec(ns: settings.timeout), tag: -1) // TODO: TAG
            }
        }
    }
    
    ///
    func receive() {
        connect()
        
        running = true
        
        // TODO: move timeout handling to other class! this class should be more generic!
        _ = countDownLatch.await(settings.timeout) // TODO: timeout
        running = false
        
        close()
    }
}

// MARK: GCDAsyncUdpSocketDelegate methods

///
extension UDPStreamReceiver: GCDAsyncUdpSocketDelegate {
    
    ///
    func udpSocket(_ sock: GCDAsyncUdpSocket, didConnectToAddress address: Data) {
        logger.debug("didConnectToAddress: \(String(describing: address))")
    }
    
    func udpSocket(_ sock: GCDAsyncUdpSocket, didNotConnect error: Error?) {
        logger.debug("didNotConnect: \(String(describing: error))")
    }
    
    func udpSocket(_ sock: GCDAsyncUdpSocket, didSendDataWithTag tag: Int) {
        logger.debug("didSendDataWithTag: \(tag)")
    }
    
    func udpSocket(_ sock: GCDAsyncUdpSocket, didNotSendDataWithTag tag: Int, dueToError error: Error?) {
        logger.debug("didNotSendDataWithTag: \(String(describing: error))")
    }
    
    func udpSocket(_ sock: GCDAsyncUdpSocket, didReceive data: Data, fromAddress address: Data, withFilterContext filterContext: Any?) {
        logger.debug("didReceiveData: \(String(describing: data))")
        
        if (running) {
            receivePacket(dataReceived: data, fromAddress: address)
        }
    }
    
    func udpSocketDidClose(_ sock: GCDAsyncUdpSocket, withError error: Error?) {
        logger.debug("udpSocketDidClose: \(String(describing: error))")
    }
    
}

