/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  QOSHTTPProxyTestExecutor.swift
//  RMBT
//
//  Created by Benjamin Pucher on 19.01.15.
//  Copyright © 2014 SPECURE GmbH. All rights reserved.
//

import Foundation
import AFNetworking


///
typealias HTTPProxyTestExecutor = QOSHTTPProxyTestExecutor<QOSHTTPProxyTest>

///
class QOSHTTPProxyTestExecutor<T: QOSHTTPProxyTest>: QOSTestExecutorClass<T> {
    
    private let RESULT_HTTP_PROXY_STATUS    = "http_result_status"
    private let RESULT_HTTP_PROXY_DURATION  = "http_result_duration"
    private let RESULT_HTTP_PROXY_LENGTH    = "http_result_length"
    private let RESULT_HTTP_PROXY_HEADER    = "http_result_header"
    private let RESULT_HTTP_PROXY_RANGE     = "http_objective_range"
    private let RESULT_HTTP_PROXY_URL       = "http_objective_url"
    private let RESULT_HTTP_PROXY_HASH      = "http_result_hash"
    
    //
    
    ///
    private var requestStartTimeTicks: UInt64 = 0
    
    //
    
    ///
    override init(controlConnection: QOSControlConnection, delegateQueue: DispatchQueue, testObject: T, speedtestStartTime: UInt64) {
        super.init(controlConnection: controlConnection, delegateQueue: delegateQueue, testObject: testObject, speedtestStartTime: speedtestStartTime)
    }
    
    ///
    override func startTest() {
        super.startTest()
        
        testResult.set(key: RESULT_HTTP_PROXY_RANGE, value: testObject.range as AnyObject)
        testResult.set(key: RESULT_HTTP_PROXY_URL, value: testObject.url as AnyObject)
        testResult.set(key: RESULT_HTTP_PROXY_DURATION, value: -1 as AnyObject)
    }
    
    ///
    override func executeTest() {
        
        // TODO: check testObject.url
        if let url = testObject.url {
            
            qosLog.debug(logMessage: "EXECUTING HTTP PROXY TEST")
            
            let manager: AFHTTPSessionManager = AFHTTPSessionManager()
            
            // add range header if it exists
            if let range = testObject.range {
                manager.requestSerializer.setValue(range, forHTTPHeaderField: "Range")
            }
            
            // set timeout (timeoutInterval is in seconds)
            manager.requestSerializer.timeoutInterval = nsToSec(ns: testObject.downloadTimeout) // TODO: is this the correct timeout?
            
            // add image/jpeg to the accepted content types
            manager.responseSerializer = AFHTTPResponseSerializer.init()
            manager.responseSerializer.acceptableContentTypes?.insert("image/jpeg")
        
            
            manager.get(url, parameters: nil, progress: nil, success: {(task: URLSessionDataTask?, responseObject : Any) -> Void in
                
                self.qosLog.debug(logMessage: "GET SUCCESS")
                
                // compute duration
                let durationInNanoseconds = UInt64.getTimeDifferenceInNanoSeconds(self.requestStartTimeTicks)
                self.testResult.set(key: self.RESULT_HTTP_PROXY_DURATION, number: durationInNanoseconds)
                
                let response = (task?.response as! HTTPURLResponse)
                // set other result values
                self.testResult.set(key: self.RESULT_HTTP_PROXY_STATUS, value: response.statusCode as AnyObject)
                self.testResult.set(key: self.RESULT_HTTP_PROXY_LENGTH, number: response.expectedContentLength)
                
                // compute md5
                if let r = responseObject as? NSData {
                    self.qosLog.debug(logMessage: "ITS NSDATA!")
                    
                    self.testResult.set(key: self.RESULT_HTTP_PROXY_HASH, value: r.MD5().hexString() as AnyObject) // TODO: improve
                }
                // loop through headers
                var headerString: String = ""
                for (headerName, headerValue) in response.allHeaderFields{
                    headerString += "\(headerName): \(headerValue)\n"
                }
                
                self.testResult.set(key: self.RESULT_HTTP_PROXY_HEADER, value: headerString as AnyObject)
                
                ///
                self.testDidSucceed()
                ///

            }, failure: {(task:URLSessionDataTask?, error : Error) -> Void in
                self.qosLog.debug(logMessage: "GET FAILURE")
                self.qosLog.debug(logMessage: "\(error.localizedDescription)")
                
                if ((error as NSError).code == NSURLErrorTimedOut) {
                    // timeout
                    self.testDidTimeout()
                } else {
                    self.testDidFail()
                }

            })
            
            // set start time
            requestStartTimeTicks = UInt64.getCurrentTimeTicks()
         
        }
    }
    
    ///
    override func testDidTimeout() {
        testResult.set(key: RESULT_HTTP_PROXY_HASH, value: "TIMEOUT" as AnyObject)
        
        testResult.set(key: RESULT_HTTP_PROXY_STATUS, value: "" as AnyObject)
        testResult.set(key: RESULT_HTTP_PROXY_LENGTH, value: NSNumber(value: 0))
        testResult.set(key: RESULT_HTTP_PROXY_HEADER, value: "" as AnyObject)
        
        super.testDidTimeout()
    }
    
    ///
    override func testDidFail() {
        testResult.set(key: RESULT_HTTP_PROXY_HASH, value: "ERROR" as AnyObject)
        
        testResult.set(key: RESULT_HTTP_PROXY_STATUS, value: "" as AnyObject)
        testResult.set(key: RESULT_HTTP_PROXY_LENGTH, value: NSNumber(value: 0))
        testResult.set(key: RESULT_HTTP_PROXY_HEADER, value: "" as AnyObject)
        
        super.testDidFail()
    }
    
    ///
    override func needsControlConnection() -> Bool {
        return false
    }
}
