/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  RMBTSSLHelper.swift
//  RMBT
//
//  Created by Benjamin Pucher on 27.03.15.
//  Copyright © 2015 SPECURE GmbH. All rights reserved.
//

import Foundation

///
class RMBTSSLHelper {
    
    ///
    private init() {
        
    }
    
    ///
    class func encryptionStringForSSLContext(sslContext: SSLContext) -> String {
        return "\(encryptionProtocolStringForSSLContext(sslContext: sslContext)) (\(encryptionCipherStringForSSLContext(sslContext: sslContext)))"
    }
    
    ///
    class func encryptionProtocolStringForSSLContext(sslContext: SSLContext) -> String {
        var sslProtocol: SSLProtocol = .sslProtocolUnknown
        SSLGetNegotiatedProtocolVersion(sslContext, &sslProtocol)

        switch (sslProtocol) {
        case .sslProtocolUnknown: return "No Protocol"
        case .sslProtocol2:       return "SSLv2"
        case .sslProtocol3:       return "SSLv3"
        case .sslProtocol3Only:   return "SSLv3 Only"
        case .tlsProtocol1:       return "TLSv1"
        case .tlsProtocol11:      return "TLSv1.1"
        case .tlsProtocol12:      return "TLSv1.2"
        default:                  return "other protocol: \(sslProtocol)"
        }
    }
    
    ///
    class func encryptionCipherStringForSSLContext(sslContext: SSLContext) -> String {
        var cipher = SSLCipherSuite()
        SSLGetNegotiatedCipher(sslContext, &cipher)
        
        switch (cipher) {
        case SSL_RSA_WITH_RC4_128_MD5:    return "SSL_RSA_WITH_RC4_128_MD5"
        case SSL_NO_SUCH_CIPHERSUITE:     return "No Cipher"
        default:                          return String(format: "%X", cipher)
        }
    }
}
