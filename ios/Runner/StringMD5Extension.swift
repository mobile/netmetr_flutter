/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  StringMD5Extension.swift
//  RMBT
//
//  Created by Benjamin Pucher on 20.01.15.
//  Copyright © 2015 SPECURE GmbH. All rights reserved.
//

import Foundation


///
extension Int {
    func hexString() -> String {
        return String(format: "%02x", self)
    }
}

///
extension NSData {
    func hexString() -> String {
        var string = String()
        
        for i in UnsafeBufferPointer<UInt8>(start: bytes.assumingMemoryBound(to: UInt8.self), count: length) {
            string += Int(i).hexString()
        }
        return string
    }
    
    func MD5() -> NSData {
        let result = NSMutableData(length: Int(CC_MD5_DIGEST_LENGTH))!
        CC_MD5(bytes, CC_LONG(length), result.mutableBytes.assumingMemoryBound(to: UInt8.self))
        return NSData(data: result as Data)
    }
    
    func SHA1() -> NSData {
        let result = NSMutableData(length: Int(CC_SHA1_DIGEST_LENGTH))!
        CC_SHA1(bytes, CC_LONG(length), result.mutableBytes.assumingMemoryBound(to: UInt8.self))
        return NSData(data: result as Data)
    }
}
