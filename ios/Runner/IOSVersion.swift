//
//  IOSVersion.swift
//  Runner
//
//  Created by tablexia on 15/07/2019.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

import UIKit

public class IOSVersion {
    class func SYSTEM_VERSION_EQUAL_TO(version: NSString) -> Bool {
        return UIDevice.current.systemVersion.compare(version as String,
                                                      options: NSString.CompareOptions.numeric) == ComparisonResult.orderedSame
    }
    
    class func SYSTEM_VERSION_GREATER_THAN(version: NSString) -> Bool {
        return UIDevice.current.systemVersion.compare(version as String,
                                                      options: NSString.CompareOptions.numeric) == ComparisonResult.orderedDescending
    }
    
    class func SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(version: NSString) -> Bool {
        return UIDevice.current.systemVersion.compare(version as String,
                                                      options: NSString.CompareOptions.numeric) != ComparisonResult.orderedAscending
    }
    
    class func SYSTEM_VERSION_LESS_THAN(version: NSString) -> Bool {
        return UIDevice.current.systemVersion.compare(version as String,
                                                      options: NSString.CompareOptions.numeric) == ComparisonResult.orderedAscending
    }
    
    class func SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(version: NSString) -> Bool {
        return UIDevice.current.systemVersion.compare(version as String,
                                                      options: NSString.CompareOptions.numeric) != ComparisonResult.orderedDescending
    }
}
