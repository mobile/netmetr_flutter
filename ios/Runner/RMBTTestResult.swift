/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  RMBTTestResult.swift
//  RMBT
//
//  Created by Benjamin Pucher on 03.04.15.
//  Copyright © 2015 SPECURE GmbH. All rights reserved.
//

import Foundation
import CoreLocation

let RMBTTestResultSpeedNotAvailable = -1
let RMBTTestResultSpeedMeasurementFinished = -2

struct ConnectivityData
{
    var network: RMBTNetworkType
    var dictonary : NSDictionary
    var time : NSDate
    
}

///
class RMBTTestResult {
    
    ///
    var threadCount: Int!
    
    ///
    let resolutionNanos: UInt64
    
    ///
    var pings = [RMBTPing]()
    
    ///
    var bestPingNanos: UInt64 = 0
    
    ///
    var medianPingNanos: UInt64 = 0
    
    ///
    let totalDownloadHistory: RMBTThroughputHistory
    
    ///
    let totalUploadHistory: RMBTThroughputHistory
    
    ///
    var jitterResult: QOSTestResult!
    
    ///
    weak var totalCurrentHistory: RMBTThroughputHistory!
    
    ///
    var currentHistories: NSMutableArray!//[RMBTThroughputHistory]!
    
    ///
    var perThreadDownloadHistories: NSMutableArray!//[RMBTThroughputHistory]()
    
    ///
    var perThreadUploadHistories: NSMutableArray!//[RMBTThroughputHistory]()
    
    /// "extended_test_stat": {
    ///     "cpu_usage": {
    ///         "values": [
    ///             {
    ///                 "time_ns": 528015139,
    ///                 "value": 93.1053695678711
    ///             },
    ///             ...
    ///         ]
    ///     },
    ///     "mem_usage": {
    ///         "values": [
    ///             {
    ///                 "time_ns": 528015139,
    ///                 "value": 93.1053695678711
    ///             },
    ///             ...
    ///         ]
    ///     }
    /// }
    ///
    var extendedTestStat = [String:[String:[[String:AnyObject]]]]()
    
    ///
    var locations = [CLLocation]()
    
    ///
    var testStartNanos: UInt64 = 0
    
    ///
    var testStartDate: NSDate!
    
    ///
    private var maxFrozenPeriodIndex: Int!
    
    ///
    private var lastRMBTConnectivity: RMBTConnectivity!
    
    ///
    private var connectivitiesData = [ConnectivityData]()
    
    ///
    init(resolutionNanos nanos: UInt64) {
        self.resolutionNanos = nanos
        
        self.totalDownloadHistory = RMBTThroughputHistory(resolutionNanos: nanos)
        self.totalUploadHistory = RMBTThroughputHistory(resolutionNanos: nanos)
        
        extendedTestStat["cpu_usage"] = Dictionary()//[String:[[String:AnyObject]]]()
        extendedTestStat["cpu_usage"]!["values"] = Array()//[[String:AnyObject]]()
        
        extendedTestStat["mem_usage"] = Dictionary()//[String:[[String:AnyObject]]]()
        extendedTestStat["mem_usage"]!["values"] = Array()//[[String:AnyObject]]()
    }
    
    ///
    func markTestStart() {
        testStartNanos = RMBTCurrentNanos()
        testStartDate = NSDate()
    }
    
    ///
    func addPingWithServerNanos(serverNanos: UInt64, clientNanos: UInt64) {
        assert(testStartNanos > 0)
        
        let ping: RMBTPing = RMBTPing(serverNanos: serverNanos, clientNanos: clientNanos, relativeTimestampNanos: RMBTCurrentNanos() - testStartNanos)
        pings.append(ping)
        
        if (bestPingNanos == 0 || bestPingNanos > serverNanos) {
            bestPingNanos = serverNanos
        }
        
        if (bestPingNanos > clientNanos) {
            bestPingNanos = clientNanos
        }
        
        // Take median from server pings as "best" ping
        let sortedPings = pings.sorted(by: { (p1: RMBTPing, p2: RMBTPing) -> Bool in
            return p1.serverNanos < p2.serverNanos // TODO: is this correct?
        })
        
        let sortedPingsCount = sortedPings.count
        
        if (sortedPingsCount % 2 == 1) {
            // Uneven number of pings, median is right in the middle
            let i = (sortedPingsCount - 1) / 2
            medianPingNanos = sortedPings[i].serverNanos
        } else {
            // Even number of pings, median is defined as average of two middle elements
            let i = sortedPingsCount / 2
            medianPingNanos = (sortedPings[i].serverNanos + sortedPings[i - 1].serverNanos) / 2 // TODO: is division correct? should divisor be casted to double?
        }
    }
    
    ///
    func addLength(length: UInt64, atNanos ns: UInt64, forThreadIndex threadIndex: Int) -> [RMBTThroughput]! {
        assert(threadIndex >= 0 && threadIndex < threadCount, "Invalid thread index")
        
        let h = currentHistories.object(at: threadIndex) as! RMBTThroughputHistory//currentHistories[threadIndex]
        h.addLength(length: length, atNanos: ns)
        
        // TODO: optimize calling updateTotalHistory only when certain preconditions are met
        
        return updateTotalHistory()
    }
    
    ///
    func addCpuUsage(cpuUsage: Float, atNanos ns: UInt64) {
        let cpuUsageDict: [String:NSNumber] = [
            "time_ns":  NSNumber(value: ns),
            "value":    NSNumber(value: cpuUsage)
        ]
        
        extendedTestStat["cpu_usage"]!["values"]!.append(cpuUsageDict)
    }
    
    ///
    func addMemoryUsage(ramUsage: Float, atNanos ns: UInt64) {
        let ramUsageDict: [String:NSNumber] = [
            "time_ns":  NSNumber(value: ns),
            "value":    NSNumber(value: ramUsage)
        ]
        
        extendedTestStat["mem_usage"]!["values"]!.append(ramUsageDict)
    }
    
    /// Returns array of throughputs in intervals for which all threads have reported speed
    private func updateTotalHistory() -> [RMBTThroughput]! {
        var commonFrozenPeriodIndex = Int.max
        
        for h in currentHistories {
            commonFrozenPeriodIndex = min(commonFrozenPeriodIndex, (h as! RMBTThroughputHistory).lastFrozenPeriodIndex)
        }
        
        // TODO: assert ==
        if (commonFrozenPeriodIndex == Int.max || commonFrozenPeriodIndex <= maxFrozenPeriodIndex) {
            return nil
        }

        for i in maxFrozenPeriodIndex + 1...commonFrozenPeriodIndex {
            if (i == commonFrozenPeriodIndex && (currentHistories.object(at: 0) as! RMBTThroughputHistory).isFrozen) { //currentHistories[0].isFrozen) {
                // We're adding up the last throughput, clip totals according to spec
                // 1) find t*
                var minEndNanos: UInt64 = 0
                var minPeriodIndex: UInt64 = 0
                
                for threadIndex in 0..<threadCount {
                    let threadHistory = currentHistories.object(at: threadIndex) as! RMBTThroughputHistory //currentHistories[threadIndex]
                    assert(threadHistory.isFrozen)
                    
                    let threadLastFrozenPeriodIndex = threadHistory.lastFrozenPeriodIndex
                    
                    let threadLastTput = threadHistory.periods[threadLastFrozenPeriodIndex]
                    if (minEndNanos == 0 || threadLastTput.endNanos < minEndNanos) {
                        minEndNanos = threadLastTput.endNanos
                        minPeriodIndex = UInt64(threadLastFrozenPeriodIndex)
                    }
                }
                
                // 2) Add up bytes in proportion to t*
                var length: UInt64 = 0
                
                for threadIndex in 0..<threadCount {
                    let threadLastPut = (currentHistories[threadIndex] as! RMBTThroughputHistory).periods[Int(minPeriodIndex)]
                    // Factor = (t*-t(k,m-1)/t(k,m)-t(k,m-1))
                    let factor = Double(minEndNanos - threadLastPut.startNanos) / Double(threadLastPut.durationNanos)
                    
                    assert(factor >= 0.0 && factor <= 1.0, "Invalid factor")
                    
                    length += UInt64(factor) * threadLastPut.length
                }
                
                totalCurrentHistory.addLength(length: length, atNanos: minEndNanos)

            } else {
                var length: UInt64 = 0
                
                for threadIndex in 0..<threadCount {
                    let tt = (currentHistories[threadIndex] as! RMBTThroughputHistory).periods[i]
                    length += tt.length
                    
                    assert(totalCurrentHistory.totalThroughput.endNanos == tt.startNanos, "Period start time mismatch")
                }
                
                totalCurrentHistory.addLength(length: length, atNanos: UInt64(i + 1) * resolutionNanos)

            }
        }
        
        let range = NSMakeRange(maxFrozenPeriodIndex + 1, commonFrozenPeriodIndex - maxFrozenPeriodIndex);
        //let result = (totalCurrentHistory.periods as NSArray).subarray(with: NSMakeRange(maxFrozenPeriodIndex + 1, commonFrozenPeriodIndex - maxFrozenPeriodIndex)) as! [RMBTThroughput]
        //var result = Array(totalCurrentHistory.periods[Int(maxFrozenPeriodIndex + 1)...Int(commonFrozenPeriodIndex - maxFrozenPeriodIndex)])
        // TODO: why is this not optional? does this return an empty array? see return statement
        let currentPeriods = totalCurrentHistory.periods as NSArray
        
        let result = currentPeriods.subarray(with: range) as! [RMBTThroughput]
        
        maxFrozenPeriodIndex = commonFrozenPeriodIndex
        
        return result.count > 0 ? result : nil

    }
    
    ///
    func addLocation(location: CLLocation) {
        locations.append(location)
    }
    
    ///
    func addConnectivity(connectivity: RMBTConnectivity) { // -> computed property
        lastRMBTConnectivity = connectivity
        connectivitiesData.append(ConnectivityData(network: connectivity.networkType, dictonary: connectivity.testResultDictionary(), time: connectivity.timestamp))
    }
    
    ///
    func lastConnectivity() -> RMBTConnectivity! { // -> computed property
        return lastRMBTConnectivity
    }
    
    ///
    func startDownloadWithThreadCount(threadCount: Int) {
        self.threadCount = threadCount
        
        perThreadDownloadHistories = NSMutableArray(capacity: threadCount)
        perThreadUploadHistories = NSMutableArray(capacity: threadCount)
        
        //for i in [0..<threadCount] {
        for _ in 0..<threadCount {
            perThreadDownloadHistories.add(RMBTThroughputHistory(resolutionNanos: resolutionNanos))
            perThreadUploadHistories.add(RMBTThroughputHistory(resolutionNanos: resolutionNanos))
        }
        
        totalCurrentHistory = totalDownloadHistory // TODO: check pass by value on array
        currentHistories = perThreadDownloadHistories // TODO: check pass by value on array
        maxFrozenPeriodIndex = -1
    }
    
    /// Per spec has same thread count as download
    func startUpload() {
        totalCurrentHistory = totalUploadHistory // TODO: check pass by value on array
        currentHistories = perThreadUploadHistories // TODO: check pass by value on array
        maxFrozenPeriodIndex = -1
    }
    
    /// Called at the end of each phase. Flushes out values to total history.
    func flush() -> [AnyObject]! {
        var result: [AnyObject]!// = [AnyObject]()
        
        for h in currentHistories {
            (h as! RMBTThroughputHistory).freeze()
        }
        
        result = updateTotalHistory()
        
        totalCurrentHistory.freeze()
        
        let totalPeriodCount = totalCurrentHistory.periods.count
        
        totalCurrentHistory.squashLastPeriods(count: 1)
        
        // Squash last two periods in all histories
        for h in currentHistories {
            (h as! RMBTThroughputHistory).squashLastPeriods(count: 1 + ((h as! RMBTThroughputHistory).periods.count - totalPeriodCount))
        }
        
        // Remove last measurement from result, as we don't want to plot that one as it's usually too short
        if (result.count > 0) {
            result = Array(result[0..<(result.count - 1)])
        }
        
        return result
    }
    
    ///
    func resultDictionary(onlySignal: Bool) -> NSDictionary { // -> computed property
        let pingTestResultArray = pings.map { p in
            p.testResultDictionary()
        }
        
        var result : [String: AnyObject] = [:]
        
        if(!onlySignal){
            var speedDetails = [[String:AnyObject]]()
            
            speedDetails += subresultForThreadThroughputs(perThreadArray: perThreadDownloadHistories, withDirectionString: "download")
            speedDetails += subresultForThreadThroughputs(perThreadArray: perThreadUploadHistories, withDirectionString: "upload")
            
            result["test_ping_shortest"] = NSNumber(value: bestPingNanos)
            result["pings"] = pingTestResultArray as AnyObject
            result["speed_detail"] = speedDetails as AnyObject
            result["test_num_threads"] = NSNumber(value: UInt(threadCount))
            
            result += subresultForTotalThroughput(throughput: totalDownloadHistory.totalThroughput, withDirectionString: "download")
            result += subresultForTotalThroughput(throughput: totalUploadHistory.totalThroughput, withDirectionString: "upload")
        }
        
        result += locationsResultDictionary() as [String : AnyObject]
        result += connectivitiesResultDictionary()
        
        // add extended test statistics
        result += ["extended_test_stat": extendedTestStat as AnyObject]
        
        //logger.debug("TEST RESULT: \(result)")
        
        return result as NSDictionary
    }
    
    ///
    private func subresultForThreadThroughputs(perThreadArray: /*[RMBTThroughputHistory]*/NSArray, withDirectionString directionString: String) -> [[String:AnyObject]] {
        var result = [[String:AnyObject]]()
        
        for i in 0..<perThreadArray.count {
            let h = perThreadArray.object(at: i) as! RMBTThroughputHistory
            var totalLength: UInt64 = 0
            
            for t in h.periods {
                totalLength += t.length
                
                result.append([
                    "direction": directionString as AnyObject,
                    "thread":   NSNumber(value: UInt(i)),
                    "time":     NSNumber(value: t.endNanos),
                    "bytes":    NSNumber(value: totalLength)
                    ])
            }
        }
        
        return result
    }
    
    ///
    private func subresultForTotalThroughput(throughput: RMBTThroughput, withDirectionString directionString: String) -> [String:/*NSNumber*/AnyObject] {
        return [
            "test_speed_\(directionString)":    NSNumber(value: throughput.kilobitsPerSecond()), // TODO: change kbps to computed property?
            "test_nsec_\(directionString)":     NSNumber(value: throughput.endNanos),
            "test_bytes_\(directionString)":    NSNumber(value: throughput.length)
        ]
    }
    
    ///
    private func locationsResultDictionary() -> [String:[[String:NSNumber]]] {
        var result = [[String:NSNumber]]()
        
        for l in locations {
            
            let t = l.timestamp.timeIntervalSince(testStartDate as Date) // t can be negative, if gps timestamp is before test start timestamp
            
            let tsNanos = Int64(t) * Int64(NSEC_PER_SEC)
            
            result.append([
                "geo_long": NSNumber(value: l.coordinate.longitude),
                "geo_lat":  NSNumber(value: l.coordinate.latitude),
                "tstamp":   NSNumber(value: UInt64(l.timestamp.timeIntervalSince1970) * 1000),
                "time_ns":  NSNumber(value: tsNanos),
                "accuracy": NSNumber(value: l.horizontalAccuracy),
                "altitude": NSNumber(value: l.altitude),
                "speed":    NSNumber(value: (l.speed > 0.0) ? l.speed : 0.0)
                ])
        }
        
        return [
            "geoLocations": result
        ]
    }
    
    ///
    private func connectivitiesResultDictionary() -> [String:AnyObject] {
        var result = [String:AnyObject]()
        var signals = [[String:AnyObject]]()
        
        for c in connectivitiesData {
            let cResult = c.dictonary as! [String:AnyObject]
            signals.append([
                "time": RMBTTimestampWithNSDate(date: c.time),
                "network_type_id": cResult["network_type"]!
                ])
            
            if (c.network == .WiFi) {
                //signals[signals.count - 1]["wifi_link_speed"] = 72 as AnyObject //TODO KDE LINK SPEED?
                signals[signals.count - 1]["wifi_rssi"] = cResult["wifi_rssi"] as AnyObject
            } else if (c.network == .Cellular) {
                signals[signals.count - 1]["lte_rsrp"] = cResult["lte_rsrp"] as AnyObject
            }
            
            
            if (result.count == 0/* == nil*/) {
                //result = NSMutableDictionary(dictionary: cResult)
                //result = [String:AnyObject]()
                result += cResult
            } else {
                let previousNetworkType = result["network_type"]!.integerValue
                let currentNetworkType = cResult["network_type"]!.integerValue
                
                // Take maximum network type
                result["network_type"] = NSNumber(value: max(previousNetworkType!, currentNetworkType!)) // TODO
                //result.setValue(NSNumber(integer: max(previousNetworkType, currentNetworkType)), forKey: "network_type")
            }
        }
        
        result["signals"] = signals as AnyObject // TODO
        //result.setValue(signals, forKey: "signals")
        
        return result
    }
}
