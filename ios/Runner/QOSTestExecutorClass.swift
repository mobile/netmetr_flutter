/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  QOSTestExecutorClass.swift
//  RMBT
//
//  Created by Benjamin Pucher on 18.12.14.
//  Copyright © 2014 SPECURE GmbH. All rights reserved.
//

import Foundation
import XCGLogger

///
class QOSTestExecutorClass<T: QOSTest>: NSObject, QOSTestExecutorProtocol, QOSControlConnectionTaskDelegate {
    

    let RESULT_TEST_UID = "qos_test_uid"
    
    let RESULT_START_TIME = "start_time_ns"
    let RESULT_DURATION = "duration_ns"
    
    //
    
    //    let TIMEOUT_EXTENSION: UInt64 = /*1*/1000 * NSEC_PER_MSEC // add 100ms to timeout because of compution etc.
    
    //
    
    ///
    let controlConnection: QOSControlConnection
    
    ///
    let delegateQueue: DispatchQueue
    
    ///
    let testObject: T
    
    ///
    let testResult: QOSTestResult
    
    ///
    var finishCallback: ((QOSTestResult) -> ())!
    var progressCallback: (_ executor: NSObject, _ percent: Float) -> Void = { _, _ in }

    ///
    var hasStarted: Bool = false
    
    ///
    var hasFinished: Bool = false
    
    ///
    var testStartTimeTicks: UInt64!
    
    ///
    let timeoutInSec: Double
    
    ///
    private let timer = GCDTimer()
    
    ///
    var testToken: String!

    ///
    let qosLog: QOSLog!
    
    ///
    //    private let timeoutCountDownLatch: CountDownLatch = CountDownLatch()
    
    ///
    private var timeoutDuration: UInt64!
    
    ///
    private let speedtestStartTime: UInt64
    
    //
    
    ///
    init(controlConnection: QOSControlConnection, delegateQueue: DispatchQueue, testObject: T, speedtestStartTime: UInt64) {
        self.controlConnection = controlConnection
        
        //self.delegate = delegate
        self.delegateQueue = delegateQueue
        
        self.testObject = testObject
        
        self.speedtestStartTime = speedtestStartTime
        
        //////
        self.timeoutInSec = nsToSec(ns: testObject.timeout/* + TIMEOUT_EXTENSION*/)
        //////
        
        // initialize test result
        let testType = testObject.getType()
        testResult = QOSTestResult(type: testType!)
        
        // set initial values on test result
        testResult.set(key: RESULT_TEST_UID, value: self.testObject.qosTestId as AnyObject)
        
        //////
        qosLog = QOSLog(testType: testType!, testUid: testObject.qosTestId)
        //////
        
        super.init()
        
        // set control connection task delegate if needed
        if (needsControlConnection()) {
            controlConnection.registerTaskDelegate(self, forTaskId: testObject.qosTestId)
        }
        
        // create timeout timer
        timer.interval = timeoutInSec
        timer.timerCallback = {
            self.qosLog.error(logMessage: "TIMEOUT IN QOS TEST")
            
            if (!self.hasFinished) {
                self.delegateQueue.async {
                    //assert(self.finishCallback != nil)
                    self.testDidTimeout()
                }
            }
        }
    }
    
    ///
    func setTestToken(testToken: String) {
        self.testToken = testToken
    }
    
    ///
    func startTimer() {
        timer.start()
        
        timeoutDuration = UInt64.getCurrentTimeTicks()
    }
    
    ///
    func stopTimer() {
        timer.stop()
        
        if let _ = timeoutDuration {
            logger.info("stopped timeout timer after \((UInt64.getTimeDifferenceInNanoSeconds(timeoutDuration)) / NSEC_PER_MSEC)ms")
        }
    }
    
    ///
    func startTest() {
        // set start time in nanoseconds minus start time of complete test
        testStartTimeTicks = UInt64.getCurrentTimeTicks()
        
        testResult.set(key: RESULT_START_TIME, number: UInt64.ticksToNanoTime(testStartTimeTicks) - speedtestStartTime) // test start time is relative to speedtest start time
        
        // start timeout timer
        if (!needsCustomTimeoutHandling()) {
            startTimer()
        }
    }
    
    ///
    func endTest() {
        // put duration
        let duration: UInt64 = UInt64.getTimeDifferenceInNanoSeconds(testStartTimeTicks)
        testResult.set(key: RESULT_DURATION, number: duration)
    }
    
    func setFailData() {
       //ignore
    }
    
    func getResult() -> QOSTestResult{
        return self.testResult
    }
    
    func setProgressCallback(progressCallback: @escaping (_ executor: NSObject, _ percent: Float) -> Void) {
        self.progressCallback = progressCallback
    }
    
    func execute(finish finishCallback: @escaping (_ testResult: QOSTestResult) -> ()) {
        self.finishCallback = finishCallback
        
        // call startTest method
        self.startTest()
        
        // let subclasses execute the test
        self.executeTest()
    }
    
    ///
    func executeTest() {
        // do nothing, override this method in specialized classes
        assert(false, "func executeTest has to be overriden by sub classes!")
    }
    
    ///
    func callFinishCallback() {
        //        timeoutCountDownLatch.countDown()
        //        objc_sync_enter(self)
        // TODO: IMPROVE...let tests don't do anything if there are finished!
        // return if already finished
        if hasFinished {
            //            objc_sync_exit(self)
            return
        }
        hasFinished = true
        
        //        let serialQueue = DispatchQueue(label: "test-executor-queue")
        //        serialQueue.sync {
        
        
        self.stopTimer()
        
        // call endTest method
        self.endTest()
        
        // freeze test result
        testResult.freeze()
        
        // unregister controlConnection delegate if needed
        if needsControlConnection() {
            print("\(String(describing: self.controlConnection))")
            self.controlConnection.unregisterTaskDelegate(self, forTaskId: self.testObject.qosTestId)
        }
        
        // call finish callback saved in finishCallback variable
        qosLog.debug(logMessage: "calling finish callback")
        self.finishCallback(self.testResult) // TODO: run this in delegate queue?
        //        }
        //        objc_sync_exit(self)
    }
    
    ///
    func testDidSucceed() {
        // TODO: override in specific tests
        testResult.success = true
        if (!hasFinished) {
            callFinishCallback()
        }
    }
    
    ///
    func testDidTimeout() {
        // TODO: override in specific tests
        testResult.success = false

        if (!hasFinished) {
            callFinishCallback()
        }
    }
    
    ///
    func testDidFail() {
        // TODO: override in specific tests
        testResult.success = false

        if (!hasFinished) {
            callFinishCallback()
        }
    }
    
    ///
    func needsControlConnection() -> Bool {
        return true
    }
    
    ///
    func needsCustomTimeoutHandling() -> Bool {
        return false
    }
    
    ////////////
    
    // MARK: convenience methods
    
    ///
    func sendTaskCommand(command: String, withTimeout timeout: TimeInterval, tag: Int) {
        controlConnection.sendTaskCommand(command: command, withTimeout: timeout, forTaskId: testObject.qosTestId, tag: tag)
    }
    
    /// deprecated
    func failTestWithFatalError() {
        testResult.fatalError = true
        
        if (!hasFinished) {
            callFinishCallback()
        }
    }
    
    // MARK: QOSControlConnectionTaskDelegate methods
    
    func controlConnection(_ connection: QOSControlConnection, didReceiveTaskResponse response: String, withTaskId taskId: UInt, tag: Int) {
        qosLog.debug(logMessage: "CONTROL CONNECTION DELEGATE FOR TASK ID \(taskId), WITH TAG \(tag), WITH STRING \(response)")
    }
    
    ///
    func controlConnection(_ connection: QOSControlConnection, didReceiveTimeout elapsed: TimeInterval, withTaskId taskId: UInt, tag: Int) {
        qosLog.debug(logMessage: "CONTROL CONNECTION DELEGATE FOR TASK ID \(taskId), WITH TAG \(tag), TIMEOUT")
        
        // let test timeout
        testDidTimeout()
    }
}

///
class QOSLog {
    
    ///
    let testType: QOSTestType
    
    ///
    let testUid: UInt
    
    ///
    init(testType: QOSTestType, testUid: UInt) {
        self.testType = testType
        self.testUid = testUid
    }
    
    ///
    func verbose(logMessage: String, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) {
        self.logln(logMessage:logMessage, logLevel: .verbose, functionName: functionName, fileName: fileName, lineNumber: lineNumber)
    }
    
    ///
    func debug(logMessage: String, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) {
        self.logln(logMessage:logMessage, logLevel: .debug, functionName: functionName, fileName: fileName, lineNumber: lineNumber)
    }
    
    ///
    func info(logMessage: String, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) {
        self.logln(logMessage:logMessage, logLevel: .info, functionName: functionName, fileName: fileName, lineNumber: lineNumber)
    }
    
    ///
    func warning(logMessage: String, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) {
        self.logln(logMessage: logMessage, logLevel: .warning, functionName: functionName, fileName: fileName, lineNumber: lineNumber)
    }
    
    ///
    func error(logMessage: String, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) {
        self.logln(logMessage:logMessage, logLevel: .error, functionName: functionName, fileName: fileName, lineNumber: lineNumber)
    }
    
    ///
    func severe(logMessage: String, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) {
        self.logln(logMessage: logMessage, logLevel: .severe, functionName: functionName, fileName: fileName, lineNumber: lineNumber)
    }
    
    ///
    func logln(logMessage: String, logLevel: XCGLogger.Level = .debug, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) {
        let staticFunctionName : StaticString = "functionName"
        let staticFileName : StaticString = "fileName";

        //TODO StaticString repair with file and function name
        //"\(testType.rawValue.uppercased())<\(testUid)>: \(functionName)"
        if (QOS_ENABLED_TESTS_LOG.contains(testType)) {
            logger.logln(logMessage, level: logLevel, functionName: staticFunctionName, fileName: staticFileName, lineNumber: lineNumber)
        }
    }
}
