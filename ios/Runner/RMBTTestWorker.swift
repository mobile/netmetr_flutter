/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  RMBTTestWorker.swift
//  RMBT
//
//  Created by Benjamin Pucher on 15.09.15.
//  Copyright © 2015 SPECURE GmbH. All rights reserved.
//

import Foundation
import CocoaAsyncSocket

///
enum RMBTTestWorkerState: Int {
    case Initialized
    
    case DownlinkPretestStarted
    case DownlinkPretestFinished
    
    case LatencyTestStarted
    case LatencyTestFinished
    
    case DownlinkTestStarted
    case DownlinkTestFinished
    
    case UplinkPretestStarted
    case UplinkPretestFinished
    
    case UplinkTestStarted
    case UplinkTestFinished
    
    case Stopping
    case Stopped
    
    case Aborted
    case Failed
}

// We use long to be compatible with GCDAsyncSocket tag datatype
enum RMBTTestTag: Int {
    case RxPretestPart = -2
    case RxDownlinkPart = -1
    
    case RxBanner = 1
    case RxBannerAccept
    case TxToken
    case RxTokenOK
    case RxChunksize
    case RxChunksizeAccept
    case TxGetChunks
    case RxChunk
    case TxChunkOK
    case RxStatistic
    case RxStatisticAccept
    case TxPing
    case RxPong
    case TxPongOK
    case RxPongStatistic
    case RxPongAccept
    case TxGetTime
    case RxGetTime
    case RxGetTimeLeftoverChunk
    case TxGetTimeOK
    case RxGetTimeStatistic
    case RxGetTimeAccept
    case TxQuit
    case TxPutNoResult
    case RxPutNoResultOK
    case TxPutNoResultChunk
    case RxPutNoResultStatistic
    case RxPutNoResultAccept
    case TxPut
    case RxPutOK
    case TxPutChunk
    case RxPutStatistic
    case RxPutStatisticLast
}

/// All delegate methods are dispatched on the supplied delegate queue
protocol RMBTTestWorkerDelegate {
    
    ///
    func testWorker(worker: RMBTTestWorker, didFinishDownlinkPretestWithChunkCount chunks: UInt, withTime duration: UInt64)
    
    ///
    func testWorker(worker: RMBTTestWorker, didMeasureLatencyWithServerNanos serverNanos: UInt64, clientNanos: UInt64)
    
    ///
    func testWorkerDidFinishLatencyTest(worker: RMBTTestWorker)
    
    ///
    func testWorker(worker: RMBTTestWorker, didStartDownlinkTestAtNanos nanos: UInt64) -> UInt64
    
    ///
    func testWorker(worker: RMBTTestWorker, didDownloadLength length: UInt64, atNanos nanos: UInt64)
    
    ///
    func testWorkerDidFinishDownlinkTest(worker: RMBTTestWorker)
    
    ///
    func testWorker(worker: RMBTTestWorker, didFinishUplinkPretestWithChunkCount chunks: UInt)
    
    ///
    func testWorker(worker: RMBTTestWorker, didStartUplinkTestAtNanos nanos: UInt64) -> UInt64
    
    ///
    func testWorker(worker: RMBTTestWorker, didUploadLength length: UInt64, atNanos nanos: UInt64)
    
    ///
    func testWorkerDidFinishUplinkTest(worker: RMBTTestWorker)
    
    ///
    func testWorkerDidStop(worker: RMBTTestWorker)
    
    ///
    func testWorkerDidFail(worker: RMBTTestWorker)
}

///
class RMBTTestWorker: NSObject, GCDAsyncSocketDelegate {
    
    // Test parameters
    private var params: RMBTTestParams
    
    /// Weak reference to the delegate
    private let delegate: RMBTTestWorkerDelegate
    
    /// Current state of the worker
    private var state: RMBTTestWorkerState = .Initialized
    
    ///
    private var socket: GCDAsyncSocket!
    
    /// CHUNKSIZE received from server
    private var chunksize: UInt = 0
    
    /// One chunk of data cached from the downlink phase, to be used as upload data
    private var chunkData: NSMutableData!
    
    /// In pretest, we first request or send 1 chunk at once, then 2, 4, 8 etc.
    /// Number of chunks to request/send in this iteration
    private var pretestChunksCount: UInt = 0
    
    /// Uplink pretest: number of chunks sent so far in this iteration
    private var pretestChunksSent: UInt = 0
    
    /// Download pretest: length received so far
    private var pretestLengthReceived: UInt64 = 0
    
    /// Nanoseconds at which we started pretest
    private var pretestStartNanos: UInt64 = 0
    
    /// Nanoseconds at which we sent the PING
    private var pingStartNanos: UInt64 = 0
    
    /// Nanoseconds at which we received PONG
    private var pingPongNanos: UInt64 = 0
    
    /// Current ping sequence number (0.._params.pingCount-1)
    private var pingSeq: UInt = 0
    
    /// Nanoseconds at which test started. Used for both up/down tests.
    private var testStartNanos: UInt64 = 0
    
    /// Download buffer for capturing bytes for _chunkData
    private var testDownloadedData: NSMutableData!
    
    /// How many nanoseconds is this thread behind the first thread that started upload test
    private var testUploadOffsetNanos: UInt64 = 0
    
    /// Local timestamps after which we'll start discarding server reports and finalize the upload test
    private var testUploadEnoughClientNanos: UInt64 = 0
    private var testUploadMaxWaitReachedClientNanos: UInt64 = 0
    
    // Server timestamp after which it is considered that we have enough upload
    private var testUploadEnoughServerNanos: UInt64 = 0
    
    /// Flag indicating that last uplink packet has been sent. After last chunk has been sent, we'll wait upto X sec to
    /// collect statistics, then terminate the test.
    private var testUploadLastChunkSent = false
    
    /// Server reports total number of bytes received. We need to track last amount reported so we can calculate relative amounts.
    private var testUploadLastUploadLength: UInt64 = 0
    
    ///
    var index: UInt
    
    ///
    var totalBytesUploaded: UInt64 = 0
    
    ///
    var totalBytesDownloaded: UInt64 = 0
    
    ///
    var negotiatedEncryptionString: String!
    
    ///
    var localIp: String!
    
    ///
    var serverIp: String!
    
    ///
    init(delegate: RMBTTestWorkerDelegate, delegateQueue: DispatchQueue, index: UInt, testParams: RMBTTestParams) {
        self.delegate = delegate
        self.index = index
        self.params = testParams
        //self.state = .Initialized
        
        super.init()
        
        socket = GCDAsyncSocket(delegate: self, delegateQueue: delegateQueue)
    }
    
    // MARK: State handling
    
    ///
    @objc func startDownlinkPretest() {
        assert(state == .Initialized, "Invalid state")
        
        state = .DownlinkPretestStarted
        
        connect()
    }
    
    ///
    func stop() {
        assert(state == .DownlinkPretestFinished, "Invalid state")
        
        state = .Stopping
        
        socket.disconnect()
    }
    
    ///
    @objc func startLatencyTest() {
        assert(state == .DownlinkPretestFinished, "Invalid state")
        
        state = .LatencyTestStarted
        
        pingSeq = 0
        writeLine(line: "PING", withTag: .TxPing)
        
        pingStartNanos = RMBTCurrentNanos()
    }
    
    ///
    @objc func startDownlinkTest() {
        assert(state == .LatencyTestFinished || state == .DownlinkPretestFinished, "Invalid state")
        
        state = .DownlinkTestStarted
        
        writeLine(line: "GETTIME \(Int(params.testDuration))", withTag: .TxGetTime)
    }
    
    ///
    @objc func startUplinkPretest() {
        assert(state == .DownlinkTestFinished, "Invalid state")
        
        state = .UplinkPretestStarted
        
        connect()
    }
    
    ///
    @objc func startUplinkTest() {
        assert(state == .UplinkPretestFinished, "Invalid state")
        
        state = .UplinkTestStarted
        
        writeLine(line: "PUT", withTag: .TxPut)
    }
    
    @objc func startJitterTest(){
        logger.debug("start jitter test")
    }
    
    // MARK: ...
    
    private func tryDNSLookup(serverName: String) -> String? {
        let host = CFHostCreateWithName(nil, serverName as CFString).takeRetainedValue()
        CFHostStartInfoResolution(host, .addresses, nil)
        var success: DarwinBoolean = false
        if let addresses = CFHostGetAddressing(host, &success)?.takeUnretainedValue() as NSArray?, let theAddress = addresses.firstObject as? Data {
            var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
            if getnameinfo((theAddress as NSData).bytes.bindMemory(to: sockaddr.self, capacity: theAddress.count), socklen_t(theAddress.count),
                           &hostname, socklen_t(hostname.count), nil, 0, NI_NUMERICHOST) == 0 {
                if let numAddress = String(validatingUTF8: hostname) {
                    return numAddress
                }
            }
        }
        
        return nil
    }
    
    ///
    func connect() {
        do {
            var sAddr = params.serverAddress
            
            if let ip = tryDNSLookup(serverName: params.serverAddress) {
                sAddr = ip
            }
            
            try socket.connect(toHost: sAddr, onPort: UInt16(params.serverPort), withTimeout: RMBT_TEST_SOCKET_TIMEOUT_S)
            
        } catch {
            fail()
        }
    }

    
    ///
    func abort() {
        if (state == .Aborted) {
            return
        }
        
        state = .Aborted
        
        if (socket.isConnected) {
            socket.disconnect()
        }
    }
    
    ///
    func fail() {
        if (state == .Failed) {
            return
        }
        
        state = .Failed

        delegate.testWorkerDidFail(worker: self)
        
        if (socket.isConnected) {
            socket.disconnect()
        }
    }
    
    // MARK: Socket delegate methods
    
    ///
    func socket(_ sock: GCDAsyncSocket, didConnectToHost host: String, port: UInt16) {
        if(state == .Aborted){
            sock.disconnect();
            return
        }
        
        assert(state == .DownlinkPretestStarted || state == .UplinkPretestStarted, "Invalid state")

        localIp = sock.localHost
        serverIp = sock.connectedHost
        
        if(params.serverEncryption){
            sock.startTLS([
                GCDAsyncSocketManuallyEvaluateTrust: true as NSObject
                ])
        }else {
            readLineWithTag(tag: .RxBanner)
        }
    }
    
    
    func socketDidDisconnect(_ sock: GCDAsyncSocket, withError err: Error?) {
        if(err != nil){
            logger.debug("Socket disconnected with error \(String(describing: err))")
            fail()
        } else {
            if (state == .DownlinkTestStarted){
                state = .DownlinkTestFinished
                delegate.testWorkerDidFinishDownlinkTest(worker: self)
            }else if (state == .Stopping){
                state = .Stopped
                delegate.testWorkerDidStop(worker: self)
            }else if (state == .Failed || state == .Aborted || state == .UplinkTestFinished){
                // We've finished/aborted/failed and socket has disconnected. Nothing to do!
            } else {
                assert(false, "Disconnection in an unexpected state")
            }
        }
    }
    
    func socket(_ sock: GCDAsyncSocket, didReceive trust: SecTrust, completionHandler: @escaping (Bool) -> Void) {
        completionHandler(true)
    }
    
    func socketDidSecure(_ sock: GCDAsyncSocket) {
        assert(state == .DownlinkPretestStarted || state == .UplinkPretestStarted, "Invalid state")

        socket.perform {
            if let sslContext = sock.sslContext() {
                self.negotiatedEncryptionString = RMBTSSLHelper.encryptionStringForSSLContext(sslContext: sslContext.takeUnretainedValue()) // TODO: or use takeRetainedValue()?
            }
        }
        
        readLineWithTag(tag: .RxBanner)
    }
    
    func socket(_ sock: GCDAsyncSocket, didWriteDataWithTag tag: Int) {
        if (state == .Aborted) {
            return
        }
        
        socketDidReadOrWriteData(data: nil, withTag: tag, read: false)
    }
    
    func socket(_ sock: GCDAsyncSocket, didRead data: Data, withTag tag: Int) {
        if (state == .Aborted) {
            return
        }
        
        totalBytesDownloaded += UInt64(data.count)
        
        socketDidReadOrWriteData(data: data as NSData, withTag: tag, read: true)
    }
    
    
    /// We unify read and write callbacks for better state documentation
    func socketDidReadOrWriteData(data: NSData!, withTag tagRaw: Int, read: Bool) {
        let tag = RMBTTestTag(rawValue: tagRaw)!
        
        // Pretest
        if (tag == .RxBanner) {
            // <- RMBTv0.3
            readLineWithTag(tag: .RxBannerAccept)
        } else if (tag == .RxBannerAccept) {
            // <- ACCEPT
            writeLine(line: "TOKEN \(params.testToken)", withTag: .TxToken)
        } else if (tag == .TxToken) {
            // -> TOKEN ...
            readLineWithTag(tag: .RxTokenOK)
        } else if (tag == .RxTokenOK) {
            // <- OK
            readLineWithTag(tag: .RxChunksize)
        } else if (tag == .RxChunksize) {
            // <- CHUNKSIZE
            
            let line = String(data: data as Data, encoding: String.Encoding.ascii)!
            let scanner = Scanner(string: line)
            
            if (!scanner.scanString("CHUNKSIZE", into: nil)) {
                assert(false, "Didn't get CHUNKSIZE")
            }
            
            var scannedChunkSize: Int32 = 0
            if (!scanner.scanInt32(&scannedChunkSize)) {
                assert(false, "Didn't get int value for chunksize")
            }
            
            assert(scannedChunkSize > 0, "Invalid chunksize")
            
            chunksize = UInt(scannedChunkSize)
            
            readLineWithTag(tag: .RxChunksizeAccept)
        } else if (tag == .RxChunksizeAccept) {
            // <- ACCEPT ...
            
            if (state == .DownlinkPretestStarted) {
                pretestChunksCount = 1
                writeLine(line: "GETCHUNKS 1", withTag: .TxGetChunks)
            } else if (state == .UplinkPretestStarted) {
                pretestChunksCount = 1
                writeLine(line: "PUTNORESULT", withTag: .TxPutNoResult)
            } else {
                assert(false, "Invalid state")
            }
        } else if (tag == .TxGetChunks) {
            // -> GETCHUNKS X
            
            if (pretestChunksCount == 1) {
                pretestStartNanos = RMBTCurrentNanos()
            }
            
            pretestLengthReceived = 0
            
            socket.readData(withTimeout: RMBT_TEST_SOCKET_TIMEOUT_S, tag: RMBTTestTag.RxPretestPart.rawValue)
        } else if (tag == .RxPretestPart) {
            pretestLengthReceived += UInt64(data.length)
            
            if (pretestLengthReceived >= UInt64(pretestChunksCount * chunksize)) {
                assert(pretestLengthReceived == UInt64(pretestChunksCount * chunksize), "Received more than expected")
                
                writeLine(line: "OK", withTag: .TxChunkOK)
            } else {
                // Read more
                socket.readData(withTimeout: RMBT_TEST_SOCKET_TIMEOUT_S, tag: RMBTTestTag.RxPretestPart.rawValue)
            }
        } else if (tag == .TxChunkOK) {
            // -> OK
            readLineWithTag(tag: .RxStatistic)
        } else if (tag == .RxStatistic) {
            // <- STATISTIC
            readLineWithTag(tag: .RxStatisticAccept)
        } else if (tag == .RxStatisticAccept) {
            // <- ACCEPT ...
            
            // Did we run out of time?
            if (RMBTCurrentNanos() - pretestStartNanos >= UInt64(params.pretestDuration * Double(NSEC_PER_SEC))) {
                state = .DownlinkPretestFinished
                delegate.testWorker(worker: self, didFinishDownlinkPretestWithChunkCount: pretestChunksCount, withTime: RMBTCurrentNanos() - pretestStartNanos)
            } else {
                // ..no, get more chunks
                pretestChunksCount *= 2
                
                // -> GETCHUNKS *2
                writeLine(line: "GETCHUNKS \(pretestChunksCount)", withTag: .TxGetChunks)
            }
        }
            
            // Latency test
        else if (tag == .TxPing) {
            // -> PING
            pingSeq+=1
            
            logger.debug("Ping packet sent (delta = \(RMBTCurrentNanos() - pingStartNanos))")
            
            readLineWithTag(tag: .RxPong)
        } else if (tag == .RxPong) {
            pingPongNanos = RMBTCurrentNanos()
            // <- PONG
            writeLine(line: "OK", withTag: .TxPongOK)
        } else if (tag == .TxPongOK) {
            // -> OK
            readLineWithTag(tag: .RxPongStatistic)
        } else if (tag == .RxPongStatistic) {
            // <- TIME
            var ns: Int64 = -1
            
            let line = String(data: data as Data, encoding: String.Encoding.ascii)!
            let scanner = Scanner(string: line)
            
            if (!scanner.scanString("TIME", into: nil)) {
                assert(false, "Didn't get TIME statistic -> \(line)")
            }
            
            if (!scanner.scanInt64(&ns)) {
                assert(false, "Didn't get long value for latency")
            }
            
            assert(ns > 0, "Invalid latency time")
            
            delegate.testWorker(worker: self, didMeasureLatencyWithServerNanos: UInt64(ns), clientNanos: pingPongNanos - pingStartNanos)
            
            readLineWithTag(tag: .RxPongAccept)
        } else if (tag == .RxPongAccept) {
            // <- ACCEPT
            assert(pingSeq <= params.pingCount, "Invalid ping count")
            
            if (pingSeq == params.pingCount) {
                state = .LatencyTestFinished
                delegate.testWorkerDidFinishLatencyTest(worker: self)
            } else {
                // Send PING again
                writeLine(line: "PING", withTag: .TxPing)
                pingStartNanos = RMBTCurrentNanos()
            }
        }
            
            // Downlink test
        else if (tag == .TxGetTime) {
            // -> GETTIME (duration)
            testDownloadedData = NSMutableData(capacity: Int(chunksize))!
            
            socket.readData(withTimeout: RMBT_TEST_SOCKET_TIMEOUT_S, tag: RMBTTestTag.RxDownlinkPart.rawValue)
            
            // We want to align starting times of all threads, so allow delegate to supply us a start timestamp
            // (usually from the first thread that reached this point)
            testStartNanos = delegate.testWorker(worker: self, didStartDownlinkTestAtNanos: RMBTCurrentNanos())
        } else if (tag == .RxDownlinkPart) {
            let elapsedNanos = RMBTCurrentNanos() - testStartNanos
            let finished = (elapsedNanos >= UInt64(params.testDuration * Double(NSEC_PER_SEC)))
            
            if (chunkData == nil) {
                // We still need to fill up one chunk for transmission in upload test
                testDownloadedData.append(data as Data)
                if (testDownloadedData.length >= Int(chunksize)) {
                    chunkData = NSMutableData(data: testDownloadedData.subdata(with: NSMakeRange(0, Int(chunksize))))
                }
            } // else discard the received data
            
            delegate.testWorker(worker: self, didDownloadLength: UInt64(data.length), atNanos: elapsedNanos)
            
            if (finished) {
                socket.disconnect()
            } else {
                // Request more
                socket.readData(withTimeout: RMBT_TEST_SOCKET_TIMEOUT_S, tag: RMBTTestTag.RxDownlinkPart.rawValue)
            }
        }
            
            // We always abruptly disconnect after test duration has passed, so following is not really used
            //    } else if (tag == RMBTTestTagTxGetTimeOK) {
            //        // -> OK
            //        [self readLineWithTag:RMBTTestTagRxGetTimeStatistic];
            //    } else if (tag == RMBTTestTagRxGetTimeStatistic) {
            //        // <- TIME ...
            //        [self readLineWithTag:RMBTTestTagRxGetTimeAccept];
            //    } else if (tag == RMBTTestTagRxGetTimeAccept) {
            //        // -> QUIT
            //        [self writeLine:@"QUIT" withTag:RMBTTestTagTxQuit];
            //        [_socket disconnectAfterWriting];
            //    }
            
            // Uplink pretest
        else if (tag == .TxPutNoResult) {
            readLineWithTag(tag: .RxPutNoResultOK)
        } else if (tag == .RxPutNoResultOK) {
            if (pretestChunksCount == 1) {
                pretestStartNanos = RMBTCurrentNanos()
            }
            pretestChunksSent = 0
            
            updateLastChunkFlagToValue(lastChunk: pretestChunksCount == 1)
            
            writeData(data: chunkData, withTag: .TxPutNoResultChunk)
        } else if (tag == .TxPutNoResultChunk) {
            pretestChunksSent+=1
            
            assert(pretestChunksSent <= pretestChunksCount)
            
            if (pretestChunksSent == pretestChunksCount) {
                readLineWithTag(tag: .RxPutNoResultStatistic)
            } else {
                updateLastChunkFlagToValue(lastChunk: pretestChunksSent == (pretestChunksCount - 1))
                writeData(data: chunkData, withTag: .TxPutNoResultChunk)
            }
        } else if (tag == .RxPutNoResultStatistic) {
            readLineWithTag(tag: .RxPutNoResultAccept)
        } else if (tag == .RxPutNoResultAccept) {
            if (RMBTCurrentNanos() - pretestStartNanos >= UInt64(params.pretestDuration * Double(NSEC_PER_SEC))) {
                state = .UplinkPretestFinished
                delegate.testWorker(worker: self, didFinishUplinkPretestWithChunkCount: pretestChunksCount)
            } else {
                pretestChunksCount *= 2
                writeLine(line: "PUTNORESULT", withTag: .TxPutNoResult)
            }
        }
            
            // Uplink test
        else if (tag == .TxPut) {
            // -> PUT
            readLineWithTag(tag: .RxPutOK)
        } else if (tag == .RxPutOK) {
            testUploadLastUploadLength = 0
            testUploadLastChunkSent = false
            testStartNanos = RMBTCurrentNanos()
            testUploadOffsetNanos = delegate.testWorker(worker: self, didStartUplinkTestAtNanos: testStartNanos)
            
            var enoughInterval = (params.testDuration - RMBT_TEST_UPLOAD_MAX_DISCARD_S)
            if (enoughInterval < 0) {
                enoughInterval = 0
            }
            
            testUploadEnoughServerNanos = UInt64(enoughInterval * Double(NSEC_PER_SEC))
            testUploadEnoughClientNanos = testStartNanos + UInt64((params.testDuration + RMBT_TEST_UPLOAD_MIN_WAIT_S) * Double(NSEC_PER_SEC))
            
            updateLastChunkFlagToValue(lastChunk: false)
            writeData(data: chunkData, withTag: .TxPutChunk)
            readLineWithTag(tag: .RxPutStatistic)
        } else if (tag == .TxPutChunk) {
            if (testUploadLastChunkSent) {
                // This was the last chunk
            } else {
                let nanos = RMBTCurrentNanos() + testUploadOffsetNanos
                
                if (nanos - testStartNanos >= UInt64(params.testDuration * Double(NSEC_PER_SEC))) {
                    logger.debug("Sending last chunk in thread \(index)")
                    
                    testUploadLastChunkSent = true
                    testUploadMaxWaitReachedClientNanos = RMBTCurrentNanos() + UInt64(RMBT_TEST_UPLOAD_MAX_WAIT_S) * NSEC_PER_SEC
                    
                    // We're done, send last chunk
                    updateLastChunkFlagToValue(lastChunk: true)
                }
                
                writeData(data: chunkData, withTag: .TxPutChunk)
            }
        } else if (tag == .RxPutStatistic) {
            // <- TIME
            
            let line = String(data: data as Data, encoding: String.Encoding.ascii)! // !
            
            if (line.hasPrefix("TIME")) {
                var ns: Int64 = -1
                var bytes: Int64 = -1
                
                let scanner = Scanner(string: line)
                
                // redundant, remove?
                if (!scanner.scanString("TIME", into: nil)) {
                    assert(false, "Didn't scan TIME")
                }
                
                if (!scanner.scanInt64(&ns)) {
                    assert(false, "Didn't get long value for TIME")
                }
                
                assert(ns > 0, "Invalid time")
                
                if (scanner.scanString("BYTES", into: nil)) {
                    if (!scanner.scanInt64(&bytes)) {
                        assert(false, "Didn't get long value for BYTES")
                    }
                    
                    assert(bytes > 0, "Invalid bytes")
                }
                
                ns += Int64(testUploadOffsetNanos)
                
                // Did upload
                if (bytes > 0) {
                    delegate.testWorker(worker: self, didUploadLength: UInt64(bytes) - testUploadLastUploadLength, atNanos: UInt64(ns))
                    testUploadLastUploadLength = UInt64(bytes)
                }
                
                let now = RMBTCurrentNanos()
                
                if (testUploadLastChunkSent && now >= testUploadMaxWaitReachedClientNanos) {
                    logger.debug("Max wait reached in thread \(index). Finalizing.")
                    finalize()
                    return
                }
                
                if (testUploadLastChunkSent && now >= testUploadEnoughClientNanos && UInt64(ns) >= testUploadEnoughServerNanos) {
                    // We can finalize
                    logger.debug("Thread \(index) has read enough upload reports at local=\(now - testStartNanos) server=\(ns). Finalizing...")
                    finalize()
                    return
                }
                
                readLineWithTag(tag: .RxPutStatistic)
            } else if (line.hasPrefix("ACCEPT")) {
                logger.debug("Thread \(index) has read ALL upload reports. Finalizing...")
                finalize()
            } else {
                // INVALID LINE
                assert(false, "Invalid response received")
                logger.debug("Protocol error")
                fail()
            }
        } else {
            assert(false, "RX/TX with unknown tag \(tag)")
            logger.debug("Protocol error")
            fail()
        }
    }
    
    /// Finishes the uplink test and closes the connection
    override func finalize() {
        state = .UplinkTestFinished
        
        socket.disconnect()
        delegate.testWorkerDidFinishUplinkTest(worker: self)
    }
    
    // MARK: Socket helpers
    
    ///
    private func readLineWithTag(tag: RMBTTestTag) {
        socket.readData(to: "\n".data(using: String.Encoding.ascii)!, withTimeout: RMBT_TEST_SOCKET_TIMEOUT_S, tag: tag.rawValue)
    }
    
    ///
    private func writeLine(line: String, withTag tag: RMBTTestTag) {
        writeData(data: line.appending("\n").data(using: String.Encoding.ascii)! as NSData, withTag: tag) // !
    }
    
    ///
    private func writeData(data: NSData, withTag tag: RMBTTestTag) {
        totalBytesUploaded += UInt64(data.length)
        socket.write(data as Data, withTimeout: RMBT_TEST_SOCKET_TIMEOUT_S, tag: tag.rawValue)
    }
    
    ///
    private func logData(data: NSData) {
        logger.debug("RX: \(String(describing: String(data: data as Data, encoding: String.Encoding.ascii)))")
    }
    
    ///
    private func isLastChunk(data: NSData) -> Bool {
        var bytes = [UInt8](repeating: 0, count: (data.length / MemoryLayout<UInt8>.size)) // TODO: better way?
        data.getBytes(&bytes, length: bytes.count)
        let lastByte: UInt8 = bytes[data.length - 1]
        
        return lastByte == 0xff
    }
    
    ///
    private func updateLastChunkFlagToValue(lastChunk: Bool) {
        var lastByte: UInt8 = lastChunk ? 0xff : 0x00
        
        chunkData.replaceBytes(in: NSMakeRange(chunkData.length - 1, 1), withBytes: &lastByte)
    }
}

