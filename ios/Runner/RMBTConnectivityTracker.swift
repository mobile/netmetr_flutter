/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  RMBTConnectivityTracker.swift
//  RMBT
//
//  Created by Benjamin Pucher on 17.09.15.
//  Copyright © 2015 SPECURE GmbH. All rights reserved.
//

import Foundation
import CoreTelephony
import GCNetworkReachability

///
protocol RMBTConnectivityTrackerDelegate {
    
    ///
    func connectivityTracker(tracker: RMBTConnectivityTracker, didDetectConnectivity connectivity: RMBTConnectivity)
    
    ///
    func connectivityTracker(tracker: RMBTConnectivityTracker, didStopAndDetectIncompatibleConnectivity connectivity: RMBTConnectivity)
    
    ///
    func connectivityTrackerDidDetectNoConnectivity(tracker: RMBTConnectivityTracker)
    
}

///
class RMBTConnectivityTracker: NSObject {
   
    /// GCNetworkReachability is not made to be multiply instantiated, so we create a global
    /// singleton first time a RMBTConnectivityTracker is instatiated
    private static let sharedReachability: GCNetworkReachability = GCNetworkReachability.forInternetConnection()
    
    /// According to http://www.objc.io/issue-5/iOS7-hidden-gems-and-workarounds.html one should
    /// keep a reference to CTTelephonyNetworkInfo live if we want to receive radio changed notifications (?)
    private static let sharedNetworkInfo: CTTelephonyNetworkInfo = CTTelephonyNetworkInfo()
    
    ///
    private let queue = DispatchQueue(label: "com.specure.nettest.connectivitytracker")
    
    ///
    private let delegate: RMBTConnectivityTrackerDelegate
    
    ///
    private var lastConnectivity: RMBTConnectivity!
    
    ///
    private var stopOnMixed = false
    
    ///
    private var started = false
    
    ///
    private var lastRadioAccessTechnology: String!
    
    ///
    var statusManager: NetmetrStatusBarManager?
    
    ///
    struct Static {
        //static var token: dispatch_once_t = 0
        static var token = "com.specure.nettest.token.once"
    }
    
    ///
    init(delegate: RMBTConnectivityTrackerDelegate, stopOnMixed: Bool, statusBarManager: NetmetrStatusBarManager?) {
        self.delegate = delegate
        self.stopOnMixed = stopOnMixed
        self.statusManager = statusBarManager
        
        DispatchQueue.once(token: Static.token) {
            RMBTConnectivityTracker.sharedReachability.startMonitoringNetworkReachabilityWithNotification()
        }
    }
    
    ///
    @objc func appWillEnterForeground(notification: NSNotification) {
        queue.async() {
            // Restart various observartions and force update (if already started)
            if (self.started) {
                self.start()
            }
        }
    }
    
    ///
    func start() {
        queue.async() {
            self.started = true
            self.lastRadioAccessTechnology = nil
            
            // Re-Register for notifications
            NotificationCenter.default.removeObserver(self)
            
            NotificationCenter.default.addObserver(self, selector: #selector(RMBTConnectivityTracker.appWillEnterForeground(notification:)), name: UIApplication.willEnterForegroundNotification, object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(RMBTConnectivityTracker.reachabilityDidChange(n:)), name: NSNotification.Name.gcNetworkReachabilityDidChange, object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(RMBTConnectivityTracker.radioDidChange(n:)), name: NSNotification.Name.CTRadioAccessTechnologyDidChange, object: nil)
            
            self.reachabilityDidChangeToStatus(status: RMBTConnectivityTracker.sharedReachability.currentReachabilityStatus())
        }
    }
    
    ///
    func stop() {
        queue.async() {
            NotificationCenter.default.removeObserver(self)
            
            self.started = false
        }
    }
    
    ///
    func forceUpdate() {
        queue.async() {
            assert(self.lastConnectivity != nil, "Connectivity should be known by now")
            self.delegate.connectivityTracker(tracker: self, didDetectConnectivity: self.lastConnectivity)
        }
    }
    
    ///
    @objc func reachabilityDidChange(n: NSNotification) {
        if let status = n.userInfo?[kGCNetworkReachabilityStatusKey] as? NSNumber {
            queue.async() {
                self.reachabilityDidChangeToStatus(status: GCNetworkReachabilityStatus.init(status.uint8Value))
            }
        }
    }
    
    ///
    @objc func radioDidChange(n: NSNotification) {
        queue.async() {
            // Note:Sometimes iOS delivers multiple notification w/o radio technology actually changing
            if ((n.object as? String) == self.lastRadioAccessTechnology) {
                return
            }
            
            self.lastRadioAccessTechnology = n.object as? String
            
            self.reachabilityDidChangeToStatus(status: RMBTConnectivityTracker.sharedReachability.currentReachabilityStatus())
        }
    }
    
    ///
    private func reachabilityDidChangeToStatus(status: GCNetworkReachabilityStatus) {
        let networkType: RMBTNetworkType
        
        if (status == GCNetworkReachabilityStatusNotReachable) {
            networkType = .None
        } else if (status == GCNetworkReachabilityStatusWiFi) {
            networkType = .WiFi
        } else if (status == GCNetworkReachabilityStatusWWAN) {
            networkType = .Cellular
        } else {
            logger.debug("Unknown reachability status \(status)")
            return
        }
        
        if (networkType == .None) {
            logger.debug("No connectivity detected.")
            
            lastConnectivity = nil
            
            delegate.connectivityTrackerDidDetectNoConnectivity(tracker: self)
            
            return
        }
        
        let connectivity = RMBTConnectivity(networkType: networkType, statusManager: self.statusManager)
        
        if (connectivity.isEqualToConnectivity(otherConn: lastConnectivity)) {
            return
        }
        
        logger.debug("New connectivity =  \(connectivity.testResultDictionary())")
        
        if (stopOnMixed) {
            // Detect compatilibity
            var compatible = true
            
            if (lastConnectivity != nil) {
                if (connectivity.networkType != lastConnectivity.networkType) {
                    logger.debug("Connectivity network mismatched \(lastConnectivity.networkTypeDescription) -> \(connectivity.networkTypeDescription)")
                    compatible = false
                } else if (connectivity.networkName != lastConnectivity.networkName) {
                    logger.debug("Connectivity network name mismatched \(String(describing: lastConnectivity.networkName)) -> \(String(describing: connectivity.networkName))")
                    compatible = false
                }
            }
            
            lastConnectivity = connectivity
            
            if (compatible) {
                delegate.connectivityTracker(tracker: self, didDetectConnectivity: connectivity)
            } else {
                // stop
                stop()
                
                delegate.connectivityTracker(tracker: self, didStopAndDetectIncompatibleConnectivity: connectivity)
            }
        } else {
            lastConnectivity = connectivity
            delegate.connectivityTracker(tracker: self, didDetectConnectivity: connectivity)
        }
    }
    
    ///
    deinit {
        NotificationCenter.default.removeObserver(self)
    }


}
