/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation
import CoreLocation

///
protocol ConnectivityWorkerDelegate {
    func connectivity() -> RMBTConnectivity?
    func statusBarManager() -> NetmetrStatusBarManager?
}

enum RunTestStatus : Int{
    case STOP = 0
    case RUNNING = 1
    case ABORTED = 2
    case FAIL = 3
    case FINISH = 4
}

enum TestPart {
    case NONE, SPEED, QOS
}

class RunTest : NSObject, RMBTTestRunnerDelegate, QualityOfServiceTestDelegate, FlutterStreamHandler, ControlServerInfoDelegate {
    private var RECEIVER_CHANNEL : String = "nic.cz.netmetrflutter/runTestReceiver"
    private var INFO_CHANNEL : String = "nic.cz.netmetrflutter/infoChannel"
    private var OFF_RESULTS_CHANNEL : String = "nic.cz.netmetrflutter/off_result"
    private var SERVICE_RUNNING : String = "running"
    private var UPDATE_PROGRESS : String = "update_progress"
    private var SERVICE_FINISH : String = "finish"
    private var SERVICE_ABORTED : String = "aborted"
    private var TEST_FAILED: String = "failed"
    
    private var recentResults : RMBTHistoryResult!
    private var connectivityWorker: ConnectivityWorkerDelegate!
    private var actualPhase : RMBTTestRunnerPhase
    
    private var previousPhase: RMBTTestRunnerPhase
    
    private var runner: RMBTTestRunner!
    
    private var finishedPercentage = 0

    private var status : RunTestStatus = .STOP
    
    private var runEventSink: FlutterEventSink!
    
    private var infoEventSink: FlutterEventSink!
    
    private var phaseProgress: Float;
    
    var q_tableTitles = [QOSTestType : Bool]()
    private var qosResult: [QOSTestResult]!

    ///
    private var running : Bool = false
    
    private var testPart: TestPart = .NONE
    
    private var controlServerInfo : ControlServerInfo
    
    private var qosManager: QualityOfServiceTest!

    /// loop
    
    private var loopMode: Bool = false
    private var onlySignal : Bool = false
    
    private var loopTestDelegate: LoopTestDelegate?
    public var loopWorker: DispatchQueue?
    
    private var index: Int = 0
    
    convenience init(controller: FlutterViewController, worker: ConnectivityWorkerDelegate){
        self.init(controller: controller, worker: worker, loopMode: false, onlySignal:false, index: 0, loopTestDelegate: nil, loopWorker: nil)
    }
    
    init(controller: FlutterViewController, worker: ConnectivityWorkerDelegate, loopMode: Bool, onlySignal: Bool,index: Int, loopTestDelegate: LoopTestDelegate?, loopWorker: DispatchQueue?) {
        self.actualPhase = RMBTTestRunnerPhase.None
        self.previousPhase = RMBTTestRunnerPhase.None
        self.connectivityWorker = worker
        self.phaseProgress = 0.0
        
        self.loopMode = loopMode
        self.onlySignal = onlySignal
        self.index = index;
        self.loopTestDelegate = loopTestDelegate;
        self.loopWorker = loopWorker
        
        self.controlServerInfo = ControlServerInfo.init()
        super.init()
        
        let runEvent = FlutterEventChannel.init(name: loopMode ? RECEIVER_CHANNEL + String(index) : RECEIVER_CHANNEL, binaryMessenger: controller.binaryMessenger)
        
        let infoEvent = FlutterEventChannel.init(name: INFO_CHANNEL, binaryMessenger: controller.binaryMessenger);
        
        self.iniOffChannel(controller: controller)
        
        runEvent.setStreamHandler(self)
        infoEvent.setStreamHandler(self)
        
        self.runner = RMBTTestRunner.init(delegate: self, loopMode: self.loopMode, onlySignal: onlySignal, loopWorker: self.loopWorker, index: index, statusBarManager: worker.statusBarManager(), controlServerInfoDelegate: self)

    }
    
    //MARK: OFF CHANNEL
    
    func iniOffChannel(controller: FlutterViewController){
        let offChannel = FlutterMethodChannel.init(name: OFF_RESULTS_CHANNEL, binaryMessenger: controller.binaryMessenger)
        offChannel.setMethodCallHandler({(call: FlutterMethodCall, result: FlutterResult) -> Void in
            if("getResults" == call.method){
                if(self.runner == nil){
                    return result(nil)
                }
                
                var measurement = [[String: Any]]()
                var net = [[String: Any]]()
                
                measurement.append([
                    "title" : "Download",
                    "classification" : self.runner?.testResult.totalDownloadHistory.totalThroughput.classification() ?? 0,
                    "value" : self.runner?.testResult.totalDownloadHistory.totalThroughput.mbs() ?? "no data"
                ])
                
                measurement.append([
                    "title": "Upload",
                    "classification": self.runner?.testResult.totalUploadHistory.totalThroughput.classification() ?? 0,
                    "value" : self.runner?.testResult.totalUploadHistory.totalThroughput.mbs() ?? "no data"
                ])
                
                measurement.append([
                    "title": "Ping",
                    "classification" : pingClassify(nano: self.runner?.testResult.medianPingNanos ?? 0),
                    "value" : pingMS(nano: self.runner?.testResult.medianPingNanos) ?? "no data"
                ])
                
                measurement.append([
                    "title": "Signal",
                    "classification" : signalClassify(signal: self.connectivityWorker.connectivity()?.getSignal(), type: self.connectivityWorker.connectivity()?.getSignalType() ?? 0),
                    "value" : String(describing: self.connectivityWorker.connectivity()?.getSignal() ?? 0) + " dBm"
                ])
                
                
                let userDefaults = UserDefaults.standard;
                if userDefaults.bool(forKey: "jitter_run"){
                    let resultDictionary = self.runner?.testResult.jitterResult?.resultDictionary

                    let jitterMeanDown : Int?  = resultDictionary?[JITTER_MEAN_DOWN] as? Int
                    let jitterMeanUp : Int? = resultDictionary?[JITTER_MEAN_UP] as? Int
                   
                    if(jitterMeanDown != nil && jitterMeanUp != nil){
                    
                        let total = jitterMeanDown! + jitterMeanUp!
                        let jitter : Double = total != 0 ? round(Double((jitterMeanDown! + jitterMeanUp!)) / 2.0) : 0
                    
                        measurement.append([
                            "title": "Jitter",
                            "classification": jitterClassify(jitter: jitter),
                            "value":  String(format: "%.2f ms", (jitter / 1000000))
                        ])
                    }
                    
                    let downPacket : Int? = resultDictionary?[NUM_PACKET_DOWN] as? Int
                    let upPacket : Int? = resultDictionary?[NUM_PACKET_UP] as? Int
                    let totalPacket : Int? = resultDictionary?[PACKET_TOTAL] as? Int
                    
                    if(downPacket != nil && upPacket != nil && totalPacket != nil && downPacket != 0 && upPacket != 0 && totalPacket != 0){
                        let pl = packetLoss(down: downPacket!, up: upPacket!, total: totalPacket!)
                        measurement.append([
                            "title" : "Packet Loss",
                            "classification" : packetLossClassify(precent: pl * 1000),
                            "value" : String(format: "%.2f %%", pl)
                        ])
                        
                    }
                    
                }
                
                    
                
                let signalType : Int = self.connectivityWorker.connectivity()?.getSignalType() ?? 0
                net.append([
                    "title" : "Network Type",
                    "value" : signalType == 3 ? "Wifi" : signalType != 0 ? "Mobile" : "Unknown"
                ])
                
                net.append([
                    "title" : signalType == 3 ? "SSID" : signalType != 0 ? "Operator" : "-",
                    "value" : self.connectivityWorker.connectivity()?.networkName ?? "-"
                ])
                
                
                var resultData = [
                    "speed" : [
                        "measurement" : measurement,
                        "net" : net
                    ]
                ] as [String : Any]
                
                
                if(self.qosResult != nil){
                    var qosData = [[String: Any]]()
                    
                    for qR in self.qosResult!{
                        qosData.append(qR.resultDictionary)
                    }

                    resultData["qos"] = qosData
                }
                
                do{
                    
                    let json = try JSONSerialization.data(withJSONObject: resultData)
                    let jsonString = String(data: json, encoding: .utf8)!
                    result(jsonString)
                    
                } catch {
                    print("JSON serialization failed: ", error)
                    result(nil)
                }
                
            }else{
                result(FlutterMethodNotImplemented)
            }
            
        })
    }
    
    
    //MARK: EVENT CHANNEL DELEGATE SINK
    
    func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
        
        if(arguments as? Int == 1){
            self.runEventSink = events
        }
        

        if(arguments as? Int == 2){
            self.infoEventSink = events
        }
        
       return nil;
   }
       
   func onCancel(withArguments arguments: Any?) -> FlutterError? {
       NotificationCenter.default.removeObserver(self)
       self.runEventSink = nil;
        self.infoEventSink = nil;
       return nil;
   }
    
    
    //MARK: EVENT
    
    func nextPart(){
        switch testPart {
        case .SPEED:
            self.runner?.testCompleted(historyResult: recentResults)
            break
        case .QOS:
            postFinalData()
            break
        default:
              //ignore
            break
          
        }
    }
    
    public func startEvent(){
        running = true
        
        if let c = connectivityWorker.connectivity(){
            runner.setStartConnectivity(connectivity: c)
        }
        
        status = .RUNNING
        testPart = .SPEED
        runner.start()
    }
    
    public func appDidEnterBackground(){
        stopEvent(reason: .AppBackgrounded);
        postTestAborted();
    }
    
    public func stopEvent(reason: RMBTTestRunnerCancelReason){
        actualPhase = RMBTTestRunnerPhase.None
        previousPhase = RMBTTestRunnerPhase.None
        phaseProgress = 0.0
        
        running = false
        status = .STOP
        
        runner.cancel(reason: reason)
        
        if(qosManager != nil){
            qosManager.stop()
            qosResult = qosManager.resultArray
            qosManager = nil
        }
    }
    
    public func stopLoop(reason: RMBTTestRunnerCancelReason){
        if(!running) {
            return
            
        }
        
        loopWorker?.async {
            self.stopEvent(reason: reason)
        }
    }
    
    //MARK: CONTROL SERVER INFO DELEFAGE
    
    func sendControlServerInfo(){
        if (self.infoEventSink == nil){
            return
        }
        
        var resultsArray: [[String: Any]] = [[String: Any]]()
        
        for testTask in self.controlServerInfo.data.keys{
            
            let resultData = self.controlServerInfo.data[testTask]
            var result = [
                "test_task" : testTask.rawValue,
                ] as [String: Any]
            
            if(resultData != nil && resultData??.resultStatus != nil){
                result["result_data"] = [
                    "status" : resultData??.resultStatus.rawValue,
                    "errMsg" : resultData??.errorMessage
                    ] as [String: Any?]
            }
            
            resultsArray.append(result)
        }
        
        let infoData = [
            "sending_data" : self.controlServerInfo.sending(),
            "speed_status" : self.controlServerInfo.speedDataStatus.rawValue,
            "qos_status" : self.controlServerInfo.qosDataStatus.rawValue,
            "results" : resultsArray
        ] as [String : Any]
        
        do{
            let json = try JSONSerialization.data(withJSONObject: infoData)
            let jsonString = String(data: json, encoding: .utf8)!
            self.infoEventSink(jsonString)
        } catch {
            print("JSON serialization failed: ", error)
        }
    }
    
    func changeSpeedDataStatus(taskStatus: TaskStatus) {
        print("CHANGE SPEED STAUTS \(taskStatus)")
        self.controlServerInfo.speedDataStatus = taskStatus
        self.sendControlServerInfo()
    }
      
    func changeQoSDataStatus(taskStatus: TaskStatus) {
        print("CHANGE QOS STAUTS \(taskStatus)")
        self.controlServerInfo.qosDataStatus = taskStatus
        self.sendControlServerInfo()
    }
      
    func taskFinished(task: TestTask, resultData: ResultData?) {
        print("\(task) FINISHED")
        
        switch task {
        case .SPEED:
            
            self.controlServerInfo.speedDataStatus = .FINISHED
            break
            
        case .QOS:
            
            self.controlServerInfo.qosDataStatus = .FINISHED
            break
        }
        
        self.controlServerInfo.data[task] = resultData
        self.sendControlServerInfo()
    }
    
    //MARK: FLUTTER EVENTS CHANNEL POST
    
    private func postTestAborted(){
        if(status == .ABORTED){
            return
        }
        
        status = .ABORTED
        let postData = [
            "serviceStatus" : SERVICE_ABORTED
        ]
        
        postEvent(postData: postData)
    }
    
    private func postTestFailed(errMsg: String?, connectionError: Bool){
        if(status == .FAIL){
           return
        }
        
        status = .FAIL
        
        let clientError = [
            "error_message" : errMsg ?? "-",
            "connection_error" : connectionError
            ] as [String : Any]
        
        let postData = [
            "serviceStatus" : TEST_FAILED,
            "client_error" : clientError
            ] as [String : Any]
        
        postEvent(postData: postData)
        
        if loopMode{
            loopTestDelegate?.testFailed(index: index)
        }
    }
    
    private func postFinalData(){
        status = .FINISH
        testPart = .NONE
        
        var postData : [String: Any] = [:]
        
        postData["serviceStatus"] = SERVICE_FINISH
        
        if(recentResults != nil || recentResults?.uuid != nil){
            postData["test_uuid"] = recentResults!.uuid!
        }
        postEvent(postData: postData)
        
        if loopMode{
            loopTestDelegate?.testFinished(index: index)
        }
    }
    
    private func originQosTest(qosType: QOSTestType) -> NSNumber{
        switch qosType {
        case .VOIP:
            return NSNumber(value: 1)
        case .HTTP_PROXY:
            return NSNumber(value: 2)
        case .WEBSITE:
            return NSNumber(value: 3)
        case .NON_TRANSPARENT_PROXY:
            return NSNumber(value: 4)
        case .DNS:
            return NSNumber(value: 5)
        case .TCP:
            return NSNumber(value: 6)
        case .UDP:
            return NSNumber(value: 7)
        default:
            return NSNumber(value: 0)
        }
    }
    
    private func postSpeedData(result: RMBTTestResult){
        var resultData = [
            "status" : indexPhase(phase: actualPhase),
            "initNano" : result.resolutionNanos,
            "pingNano" : result.bestPingNanos,
            "downBitPerSec" : result.totalDownloadHistory.totalThroughput.kilobitsPerMicroSecond(),
            "upBitPerSec" : result.totalUploadHistory.totalThroughput.kilobitsPerMicroSecond(),
            "progress" : self.phaseProgress,
            "downBitPerSecLog" : result.totalDownloadHistory.totalThroughput.kilobitsPerMicroSecond(),
            "upBitPerSecLog" : result.totalUploadHistory.totalThroughput.kilobitsPerMicroSecond(),
            "remainingWait" : NSNumber(value: 0),
            "signal" : connectivityWorker.connectivity()?.getSignal() ?? 0,
            "signalType" : connectivityWorker.connectivity()?.getSignalType() ?? 0,
            "relativeSignal" :  connectivityWorker.connectivity()?.relativeSignal ?? 0
            ] as [String : Any]
        
        let jitterStatus : JitterTestStatus = result.jitterResult?.resultDictionary[JITTER_STATUS] as? JitterTestStatus ?? JitterTestStatus.NOT_STARTED
        resultData["jitterStatus"] = jitterStatus.rawValue;

        if(jitterStatus == JitterTestStatus.FINISH){
            let down = [
                "meanJitter" : result.jitterResult?.resultDictionary[JITTER_MEAN_DOWN] ?? nil,
                "total" : result.jitterResult?.resultDictionary[PACKET_TOTAL] ?? -1,
                "numPacket" : result.jitterResult?.resultDictionary[NUM_PACKET_DOWN] ?? -1
            ] as [String : Any?]
            
            let up = [
                "meanJitter" : result.jitterResult?.resultDictionary[JITTER_MEAN_UP] ?? nil,
                "total" : result.jitterResult?.resultDictionary[PACKET_TOTAL] ?? -1,
                "numPacket" : result.jitterResult?.resultDictionary[NUM_PACKET_UP] ?? -1
            ] as [String : Any?]
            
            resultData["down"] = down
            resultData["up"] = up
        }
        
        var postData = [
            "serviceStatus" : SERVICE_RUNNING,
            "data" : resultData,
            ] as [String : Any]
        
        if let network = networkData(){
           postData.updateValue(network, forKey: "networkData")
        }
        
       postEvent(postData: postData)
        
    }
    
    private func networkData() -> [String: AnyObject]?{
        if let connectivity = connectivityWorker.connectivity(){
    
            var type : Int
            
            switch connectivity.networkType{
            case .WiFi: type = connectivity.networkType.rawValue; break
            case .Cellular:
                if let code = connectivity.getMobileNetworkTypeID(){
                    type = code.intValue
                }else{
                    type = -1
                }
                break
            default: type = 0
            }
            
            return [
                "operatorName" : connectivity.networkName as AnyObject,
                "networkType" : type as AnyObject,
                "serverName" : runner.testParams.serverName,
                "ip" : runner.testParams.clientRemoteIp as AnyObject
            ]
        }
        
        return nil
    }
    
    private func postQosData(success: UInt16, failed: UInt16, testsCount: UInt16){
        var qosData = [String : [String : AnyObject]]()
        
        for qos in q_tableTitles{
            let testType = qos.key;
            let status = qos.value;
            let origin = indexQos(qosType: testType)
            
            qosData["qos" + origin.stringValue] = [
                "qosType" : origin,
                "progress" : status ? NSNumber(value: 100.0) : NSNumber(value: 0.0),
                "value" : NSNumber(value: 0), //TODO value, target, firstTest, lastTest like Android?
                "target" : NSNumber(value: 0),
                "firstTest" : NSNumber(value: 0),
                "lastTest" : NSNumber(value: 0)
            ]
            
        }
        
        let postData = [
            "serviceStatus" : SERVICE_RUNNING,
            "qosTest" : qosData,
            "qosCounter" : [
                "success" : NSNumber(value: success),
                "failed" : NSNumber(value: failed),
                "testsCount": NSNumber(value: testsCount)
            ]
            ] as [String : Any]
        
        postEvent(postData: postData)
    }
    
    private func postEvent(postData : [String : Any]){
        if(self.runEventSink == nil) {
            return
        }
        
        do{
            let json = try JSONSerialization.data(withJSONObject: postData)
            let jsonString = String(data: json, encoding: .utf8)!
            
            runEventSink(jsonString)
            
        } catch {
            print("JSON serialization failed: ", error)
        }
    }
    
    private func indexQos(qosType: QOSTestType) -> NSNumber{
        switch qosType {
        case .VOIP:
            return NSNumber(value: 1)
        case .HTTP_PROXY:
            return NSNumber(value: 2)
        case .WEBSITE:
            return NSNumber(value: 3)
        case .NON_TRANSPARENT_PROXY:
            return NSNumber(value: 4)
        case .DNS:
            return NSNumber(value: 5)
        case .TCP:
            return NSNumber(value: 6)
        case .UDP:
            return NSNumber(value: 7)
        default:
            return NSNumber(value: 0)
        }
    }
    
    private func indexPhase(phase: RMBTTestRunnerPhase) -> NSNumber{
        switch phase {
        case .Init:
            return NSNumber(value: 1)
        case .Latency:
            return NSNumber(value: 2)
        case .Jitter:
            return NSNumber(value: 3)
        case .Down:
            return NSNumber(value: 4)
        case .Up:
            return NSNumber(value: 5)
        case .InitUp:
            return NSNumber(value: 6)
        case .SubmittingTestResult:
            return NSNumber(value: 7)
        default:
            return NSNumber(value: 0)
        }
    }
    
    
    func percentageForPhase(phase: RMBTTestRunnerPhase) -> Int {
        switch (phase) {
        case .Init:    return 7
        case .Latency: return 5
        case .Down:    return 17
        case .Up:      return 19
        default:       return 0
        }
    }
    

    // MARK: TEST RUNNER DELEGATE
    
    func testRunnerDidStartPhase(phase: RMBTTestRunnerPhase) {
        self.connectivityWorker.connectivity()?.signalDelegate = runner
        self.actualPhase = phase;
    }
    
    func testRunnerDidFinishPhase(phase: RMBTTestRunnerPhase) {
        self.postSpeedData(result: runner.testResult)

        self.previousPhase = phase;
    }
    
    func testRunnerDidUpdateProgress(progress: Float, inPhase phase: RMBTTestRunnerPhase) {
        if phase == .Jitter{
            return
        }
        
        if(phase == .Init || phase == .Latency || phase == .Down || phase == .Up){
            self.phaseProgress = progress
        }else if(phase == .InitUp){
            self.phaseProgress = 0.0
        }

        let resultData = [
            "status" : indexPhase(phase: actualPhase),
            "progress" : self.phaseProgress,
            
            ] as [String : Any]
        
        let postData = [
            "serviceStatus" : UPDATE_PROGRESS,
            "data" : resultData
            ] as [String : Any]

        postEvent(postData: postData)
        
    }
    
    
    func testRunnerDidMeasureThroughputs(throughputs: NSArray, inPhase phase: RMBTTestRunnerPhase) {
        self.postSpeedData(result: runner.testResult)
    }
    
    func testRunnerDidDetectConnectivity(connectivity: RMBTConnectivity) {
        if let connectivity = connectivityWorker.connectivity(){
            if(connectivity.signalDelegate == nil){
                connectivity.signalDelegate = runner
            }
        }
    }
    
    func testRunnerDidDetectNoConnectivity() {
        running = false
        stopEvent(reason: .NoConnection)
        self.postTestFailed(errMsg: nil, connectionError: true)
    }
    
    func testRunnerDidDetectLocation(location: CLLocation) {
        print("----- detect location -----")
    }
    
    func testRunnerDidCompleteWithResult(result: RMBTHistoryResult?) {
        print("test completed")
        
        recentResults = result
        self.connectivityWorker.connectivity()?.signalDelegate = nil

        let runQos = !self.onlySignal && UserDefaults.standard.bool(forKey: "qos_run")

        if(runQos){
            let qosSSL = RMBTSettings.sharedSettings().qosSSL
            qosManager = QualityOfServiceTest(testToken: runner.testParams.testToken, speedtestStartTime: runner.testResult.testStartNanos, qosSSL: qosSSL ?? false, runJitter: false ,loopMode: self.loopMode, loopWorker: self.loopWorker, controlServerInfoDelegate: self)
            qosManager.delegate = self
            
            testPart = .QOS
            qosManager.start()

        }else{
            running = false
            stopEvent(reason: .UserRequested)
            
            postFinalData()
        }
    }
    
    func testRunnerDidCancelTestWithReason(cancelReason: RMBTTestRunnerCancelReason, errorMessage: String?) {
        print("---- did cancel test ----")
        print("Reason: \(cancelReason)")
        if(cancelReason == .ErrorFetchingTestingParams || cancelReason == .MixedConnectivity || cancelReason == .NoConnection){
            self.postTestFailed(errMsg: errorMessage, connectionError: cancelReason == .NoConnection)
        }
    }
    
    func testRunnerDidFinishInit(time: UInt64) {
        print("---- init finished ----")
    }
    
    //MARK: QOS RUNNER DELEGATE
    
    func qualityOfServiceTestDidStart(test: QualityOfServiceTest) {
        print("---- qos start -----")
    }
    
    func qualityOfServiceTestDidStop(test: QualityOfServiceTest) {
        print("---- qos stop -----")
    }
    
    func qualityOfServiceTest(test: QualityOfServiceTest, didFinishWithResults results: [QOSTestResult]) {
        print("---- qos finish -----")
        print("QOS RESULT: \(results)")
        
        running = false

        stopEvent(reason: .UserRequested)
        
        postFinalData()
    
    }
    
    func qualityOfServiceTest(test: QualityOfServiceTest, didFailWithError: NSError!, errorInfo: NSDictionary?, errorType: QoSErrorType) {
        print("---- qos fail -----")
        running = false
        
        if(errorType == .ErrorSubmittingFinalResult){
            //postFinalData()
        }else{
            var errorMessage : String? = nil
                           
            if let message = errorInfo?["error_message"]{
                errorMessage = message as? String
            }
            
            postTestFailed(errMsg: errorMessage, connectionError: false)
        }
    }
    
    func qualityOfServiceTest(test: QualityOfServiceTest, didFetchTestTypes testTypes: [QOSTestType]) {
        print("---- qos fetch type  -----")
        print("\(test)")
        
        for iniStruct in testTypes{
            q_tableTitles[iniStruct] = false
        }
        
        postQosData(success: test.successTestCount, failed: test.failedTestCount, testsCount: test.testCount)
        
    }
    
    func qualityOfServiceTest(test: QualityOfServiceTest, didFinishTestType testType: QOSTestType) {
        print("---- qos finish test type \(testType) -----")
        q_tableTitles.updateValue(true, forKey: testType)
        postQosData(success: test.successTestCount, failed: test.failedTestCount, testsCount: test.testCount)
    }
    
    func qualityOfServiceTest(test: QualityOfServiceTest, didProgressToValue progress: Float) {
        print("---- qos progress value -----\((progress * 100))")
        let resultData = [
            "status" : 8, //QOS RUNNING
            "qosProgress" : NSNumber(value: progress * 100),
            
            ] as [String : Any]
        
        let postData = [
            "serviceStatus" : UPDATE_PROGRESS,
            "data" : resultData
            ] as [String : Any]
        
        postEvent(postData: postData)

    }
    
}
