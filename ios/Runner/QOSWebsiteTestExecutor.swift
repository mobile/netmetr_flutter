/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  QOSWebsiteTestExecutor.swift
//  RMBT
//
//  Created by Benjamin Pucher on 20.01.15.
//  Copyright © 2014 SPECURE GmbH. All rights reserved.
//

import Foundation

///
typealias WebsiteTestExecutor = QOSWebsiteTestExecutor<QOSWebsiteTest>

///
class QOSWebsiteTestExecutor<T: QOSWebsiteTest>: QOSTestExecutorClass<T> {
    
    private let RESULT_WEBSITE_URL      = "website_objective_url"
    private let RESULT_WEBSITE_TIMEOUT  = "website_objective_timeout"
    private let RESULT_WEBSITE_DURATION = "website_result_duration"
    private let RESULT_WEBSITE_STATUS   = "website_result_status"
    private let RESULT_WEBSITE_INFO     = "website_result_info"
    private let RESULT_WEBSITE_RX_BYTES = "website_result_rx_bytes"
    private let RESULT_WEBSITE_TX_BYTES = "website_result_tx_bytes"
    
    //
    
    ///
    //private var webView: UIWebView?
    
    private var requestStartTimeTicks: UInt64 = 0
    
    //
    
    ///
    override init(controlConnection: QOSControlConnection, delegateQueue: DispatchQueue, testObject: T, speedtestStartTime: UInt64) {
        super.init(controlConnection: controlConnection, delegateQueue: delegateQueue, testObject: testObject, speedtestStartTime: speedtestStartTime)
    }
    
    ///
    override func startTest() {
        super.startTest()
        
        testResult.set(key: RESULT_WEBSITE_URL, value: testObject.url as AnyObject?)
        testResult.set(key: RESULT_WEBSITE_TIMEOUT, number: testObject.timeout)
    }
    
    ///
    override func executeTest() {
        
        if let url = testObject.url {
            
            qosLog.debug(logMessage: "EXECUTING WEBSITE TEST")
            
            let request: URLRequest = URLRequest(url: NSURL(string: url)! as URL)
            let session = URLSession.shared
            
            requestStartTimeTicks = UInt64.getCurrentTimeTicks()
            
            session.dataTask(with: request, completionHandler: {(data, response, error) in
                //                print(data as Any)
                //                print(response as Any)
                //                print(error as Any)
                
                let durationInNanoseconds = UInt64.getTimeDifferenceInNanoSeconds(self.requestStartTimeTicks)
                self.testResult.resultDictionary[self.RESULT_WEBSITE_DURATION] = NSNumber(value: durationInNanoseconds)
                
                if let httpResponse = response as? HTTPURLResponse {
                    let status = httpResponse.statusCode
                    
                    if 200..<300 ~= status{
                        self.testResult.success = true
                    }
                    
                    self.testResult.resultDictionary[self.RESULT_WEBSITE_STATUS] = String(status) as AnyObject
                }
                
                self.callFinishCallback()
                
            }).resume()
        }
    }
    
    ///
    override func needsControlConnection() -> Bool {
        return false
    }
    
}
