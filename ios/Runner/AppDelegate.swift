/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import UIKit
import Flutter
import CoreLocation
import Sentry
import UserNotificationsUI
import GoogleMaps

#if DEBUG
let debugMode = true
#else
let debugMode = false
#endif

extension String {
    func toBool() -> Bool? {
        switch self {
        case "True", "true", "yes", "1":
            return true
        case "False", "false", "no", "0":
            return false
        default:
            return nil
        }
    }
}

extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
}



@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate, RMBTConnectivityTrackerDelegate, FlutterStreamHandler, CLLocationManagerDelegate, ConnectivityWorkerDelegate, LoopDelegate {
    
    func connectivity() -> RMBTConnectivity? {
        return lastConnectivity
    }
    
    func statusBarManager() -> NetmetrStatusBarManager?{
        return statusManager
    }
       
    private var runTest: RunTest!
    
    private var connectivityTracker: RMBTConnectivityTracker!
    
    private var lastConnectivity: RMBTConnectivity!
    
    private var networkEvent: FlutterEventSink!
        
    ///
    private let cpuMonitor = RMBTCPUMonitor()
    
    ///
    private let ramMonitor = RMBTRAMMonitor()
    
    ///
    var netStats = RMBTTrafficCounter()
    
    ///
    var connectivityService : ConnectivityService?
    
    ///
    var actualLoop : Loop?
    
    ///
    var lastIpv4 : IPInfo?
    
    ///
    var lastIpv6 :IPInfo?
    
    ///
    var lastDict = [String: Int]()
    
    ///
    let locationManager = CLLocationManager()
    
    ///
    var location = CLLocation()
    
    ///
    var statusManager: NetmetrStatusBarManager?

    
    override func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        self.controlOldVesion()
        
        return super.application(application, willFinishLaunchingWithOptions: launchOptions)
    }
    
    override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey(Bundle.main.infoDictionary?[NETMETR_GOOGLE_API_KEY] as! String)
        GeneratedPluginRegistrant.register(with: self)
        let controller : FlutterViewController = window?.rootViewController as! FlutterViewController;
        
        initSentry()
        
        
        self.initConfigChannel(controller: controller)
        self.initServiceChannel(controller: controller)
        self.initStatChannel(controller: controller)
        self.initNetworkEventChannel(controller: controller)
        self.initSettingsChannel(controller: controller)
        self.initLoopChannel(controller: controller)
        
        self.initConnectivity()
        
        
        locationManager.requestAlwaysAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        locationManager.distanceFilter = 3.0
        
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    override func applicationDidEnterBackground(_ application: UIApplication) {
        print("App backgrounded")
        
        self.actualLoop?.appDidEnterBackground()
        self.runTest?.appDidEnterBackground()
        
        
        self.actualLoop = nil
        self.runTest = nil
    }
    
    func initConnectivity(){
        //status bar manager
         let exception = tryBlock {
            if #available(iOS 13.0, *){
                DispatchQueue.main.async {
                    self.statusManager = IOS13StatusBarManager.init()
                    self.startConnectivityTracker()
                }
            }else{
                self.statusManager = OldStatusBarManager.init()
                self.startConnectivityTracker()
            }
        }
        
        if let exception = exception {
            print("init status bar manager failed: \(exception)")
        }
        
    }
    
    func startConnectivityTracker(){
        if (connectivityTracker == nil) {
            connectivityTracker = RMBTConnectivityTracker(delegate: self, stopOnMixed: false, statusBarManager: self.statusManager)
            connectivityTracker.start()
        }
    }
    
    func controlOldVesion(){
        let infoDictionary = Bundle.main.infoDictionary
        let userDefaults = UserDefaults.standard
        let checkOld : Bool = userDefaults.bool(forKey: "checkOldVersion")
        
        if(!checkOld){
            let oldKey : String = String(format: "uuid_%@", (userDefaults.object(forKey: "control_server") ?? infoDictionary![CONTROL_HOST_BUNDLE]) as! String)
            
            let oldUuid : String? = userDefaults.string(forKey: oldKey)
            
            if(oldUuid != nil){
                userDefaults.set(oldUuid, forKey: "flutter.uuid")
                userDefaults.synchronize()
            }
            
            userDefaults.set(true, forKey: "checkOldVersion")
        }
        
    }
    
//MARK: SENTRY
    
    private func initSentry(){
        let dsn : String = Bundle.main.infoDictionary?[SENTRY_DSN] as! String;
        let url : String = Bundle.main.infoDictionary?[SENTRY_URL] as! String;
        do {
            var sentryURL : String = "https://"
            sentryURL.append(dsn)
            sentryURL.append("@")
            sentryURL.append(url)
            sentryURL.append(debugMode ? "/38" : "/37")
            Client.shared = try Client(options:[
                "dsn": sentryURL,
                "release": Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String            ])
            try Client.shared?.startCrashHandler()
            
            Client.shared?.tags = [
                "platform" : "iOS",
                "build_type" : debugMode ? "Debug" : "Release"
            ]
        } catch let error {
            print("Sentry initialization failed \(error)")
        }
        
    }
    
//MARK: NETWORK LISTENER
    
    private func initNetworkEventChannel(controller: FlutterViewController){
        let networkChannel = FlutterEventChannel.init(name: "nic.cz.netmetrflutter/netStat", binaryMessenger: controller.binaryMessenger)
        networkChannel.setStreamHandler(self)
    }
    
    func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
        self.networkEvent = events
        postNetworkChangeEvent(connectivity: lastConnectivity)
        return nil;
    }
    
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        NotificationCenter.default.removeObserver(self)
        self.networkEvent = nil
        self.connectivityTracker = nil
        return nil;
    }
    
    
    func connectivityTracker(tracker: RMBTConnectivityTracker, didDetectConnectivity connectivity: RMBTConnectivity) {
        print(connectivity)
        postNetworkChangeEvent(connectivity: connectivity)
        lastConnectivity = connectivity
        ipCheck()
    }
    
    func connectivityTracker(tracker: RMBTConnectivityTracker, didStopAndDetectIncompatibleConnectivity connectivity: RMBTConnectivity) {
        print(connectivity)
        postNetworkChangeEvent(connectivity: connectivity)
        lastConnectivity = connectivity
        //ipCheck()
    }
    
    func connectivityTrackerDidDetectNoConnectivity(tracker: RMBTConnectivityTracker) {
        print("disconnect")
        postNetworkChangeEvent(connectivity: nil)
        lastConnectivity = nil
        //ipCheck()

    }
    
    func connectivitySignalChanged(tracker: RMBTConnectivityTracker, connectivity: RMBTConnectivity) {
        print("signal changed")
    }
    
    func postNetworkChangeEvent(connectivity: RMBTConnectivity!){
        lastConnectivity = connectivity
        
        var networkType = -1;
        var networkData = [String : Any?]()
        do{
            if(connectivity != nil){
                networkType = connectivity.networkType == RMBTNetworkType.WiFi ? 1 : 0
                networkData["extra"] = connectivity.networkName
                networkData["signal"] = connectivity.getSignal()
                networkData["signalType"] = connectivity.getSignalType()
                networkData["relativeSignal"] = connectivity.relativeSignal
                networkData["networkID"] = connectivity.networkID()
            }
            
            let json = try JSONSerialization.data(withJSONObject: networkData)
            let jsonString = String(data: json, encoding: .utf8)!
            
            let changeEventData = [
                "networkType" : networkType,
                "data" : jsonString
            ] as [String : Any]
            
            postNetworkEvent(event: "networkChange", eventData: changeEventData)
        } catch {
            print("JSON serialization failed: ", error)
        }
    }
    
    func ipCheck(){
        DispatchQueue.global().async {
            self.connectivityService?.checkConnectivity { connectivityInfo in
                logger.debug("CONNECTIVITY INFO: \(connectivityInfo)")
                
                self.lastIpv4 = connectivityInfo.ipv4
                self.lastIpv4?.status = self.ipStatus(ipInfo: connectivityInfo.ipv4)
                
                self.lastIpv6 = connectivityInfo.ipv6
                self.lastIpv6?.status = self.ipStatus(ipInfo: connectivityInfo.ipv6)
                
                logger.debug("IPv4: \(String(describing: self.lastIpv4)) Ipv6: \(String(describing: self.lastIpv6))")
                
                self.postNetworkEvent(event: "ipChange", eventData: nil)
            }
        }
    }
    
    func ipStatus(ipInfo: IPInfo) -> IpStatus{
        if(ipInfo.connectionAvailable){
            if(ipInfo.externalIp == nil && ipInfo.internalIp == nil){
                return .NO_ADDRESS
            }else if(ipInfo.internalIp != nil && ipInfo.externalIp == nil){
                return .ONLY_LOCAL
            }else if(ipInfo.nat){
                return .CONNECTED_NAT
            }
            
            return .CONNECTED_NO_NAT
        }
        
        return .STATUS_NOT_AVAILABLE
    }
    
    func postNetworkEvent(event: String, eventData: [String : Any]?){
        if(networkEvent == nil){
            return
        }
        do{
            var jsonEventString : String! = nil
            
            if(eventData != nil){
                let jsonEvent = try JSONSerialization.data(withJSONObject: eventData!)
                jsonEventString = String(data: jsonEvent, encoding: .utf8)!
            }
            
            let event = [
                "status" : event,
                "data" : jsonEventString
                ] as [String : Any?]
        
        
            let json = try JSONSerialization.data(withJSONObject: event)
            let jsonString = String(data: json, encoding: .utf8)!
            
            networkEvent(jsonString)
            
            
        } catch {
            print("JSON serialization failed: ", error)
        }
 
    }
    
// MARK: SERVICE
    
    private func initServiceChannel(controller: FlutterViewController){
    
        let serviceChannel = FlutterMethodChannel.init(name: "nic.cz.netmetrflutter/serviceChannel", binaryMessenger: controller.binaryMessenger)
        
        serviceChannel.setMethodCallHandler({(call: FlutterMethodCall, result: FlutterResult) -> Void in
            
            if("getServiceStatus" == call.method){
                result("status...")

            }else if("startService" == call.method){
                let arguments = call.arguments! as! [String: AnyObject?]
                let controlServer : String = arguments["controlServer"] as! String
                let port : Int = arguments["port"] as! Int
                let useSSL : Bool = arguments["useSSL"] as! Bool
                let qosSSL : Bool = arguments["qosSSL"] as! Bool
                let uuid : String = arguments["uuid"] as! String
                let jitter : Bool = arguments["jitter"] as? Bool ?? true
                let qos : Bool = arguments["qos"] as? Bool ?? true
                
                let userDefaults = UserDefaults.standard;
                userDefaults.set(uuid, forKey: "uuid")
                userDefaults.set(controlServer, forKey: "control_server")
                userDefaults.set(port, forKey: "port")
                userDefaults.set(useSSL, forKey: "use_ssl")
                userDefaults.set(qosSSL, forKey: "qos_ssl")
                userDefaults.set(jitter, forKey: "jitter_run")
                userDefaults.set(qos, forKey: "qos_run")
                userDefaults.synchronize()
                
                RMBTSettings.sharedSettings().updateDataConfig()
                ControlServer.sharedControlServer.updateWithCurrentSettings();
                
                self.runTest = RunTest.init(controller: controller, worker: self)
                self.runTest?.startEvent()
                result("start service")

            }else if("stopService" == call.method){
                result("stop service")
                self.runTest?.stopEvent(reason: .UserRequested)
                self.runTest = nil

            }else if("continueService" == call.method){
                self.runTest?.nextPart();
                result(nil)
            }else{
                result(FlutterMethodNotImplemented)
            }
            
        })
    
    }
    
// MARK: LOOP
    
    private func initLoopChannel(controller: FlutterViewController){
        let loopChannel = FlutterMethodChannel.init(name: "nic.cz.netmetrflutter/loopChannel", binaryMessenger: controller.binaryMessenger)
        
        loopChannel.setMethodCallHandler({(call: FlutterMethodCall, result: FlutterResult) -> Void in
            
            if("startLoopService" == call.method){
                
                if(self.actualLoop == nil){
                    let arguments = call.arguments! as! [String: AnyObject]
                    let uuid : String = arguments["uuid"] as! String
                    let controlServer : String = arguments["controlServer"] as! String
                    let port : Int = arguments["port"] as! Int
                    let useSSL : Bool = arguments["useSSL"] as! Bool
                    let qosSSL : Bool = arguments["qosSSL"] as! Bool
                    let jitter : Bool = arguments["jitter"] as? Bool ?? true
                    let qos : Bool = arguments["qos"] as? Bool ?? true
                    
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(uuid, forKey: "uuid")
                    userDefaults.set(controlServer, forKey: "control_server")
                    userDefaults.set(port, forKey: "port")
                    userDefaults.set(useSSL, forKey: "use_ssl")
                    userDefaults.set(qosSSL, forKey: "qos_ssl")
                    userDefaults.set(jitter, forKey: "jitter_run")
                    userDefaults.set(qos, forKey: "qos_run")
                    userDefaults.synchronize()
                    
                    RMBTSettings.sharedSettings().updateDataConfig()
                    ControlServer.sharedControlServer.updateWithCurrentSettings();
                    
                    self.actualLoop = Loop.init(delegate: self, controller: controller, connectivyWorker: self, parametrs: arguments)
                    self.actualLoop?.startService()
                    result(nil)
                }else{
                    result(FlutterError(code: "Error", message: "Loop already running", details: nil))
                }
                
            }else if("stopLoopService" == call.method){
                self.actualLoop?.stopService(reason: .UserRequested)
                self.actualLoop = nil
                result(nil)
            }else{
                result(FlutterMethodNotImplemented)
            }
            
        })
    }

    func loopStopped() {
        self.actualLoop = nil
    }
    
// MARK: STAT
    
    private func initStatChannel(controller: FlutterViewController){
        let statChannel = FlutterMethodChannel.init(name: "nic.cz.netmetrflutter/statChannel", binaryMessenger: controller.binaryMessenger)
        
        statChannel.setMethodCallHandler({(call: FlutterMethodCall, result: FlutterResult) -> Void in
            
            if("getCpu" == call.method){
                var cpuUsage: Float = -1
                if let array = self.cpuMonitor.getCPUUsage() as? [NSNumber] {
                    cpuUsage = array[0].floatValue
                }
    
                result(cpuUsage)
            }else if("getMemInfo" == call.method){
                var memFree : UInt64 = 0
                if let array = self.ramMonitor.getRAMUsage() as? [NSNumber]{
                    memFree = array[0].uint64Value
                }
                
                let physicalMemory = ProcessInfo.processInfo.physicalMemory

    
                var memArraykB: UInt64 = 0
                var physicalMemorykB: UInt64 = 0
                
                if memFree > 0 {
                    memArraykB = memFree / 1024
                }
                
                if physicalMemory > 0{
                    physicalMemorykB = physicalMemory / 1023
                }
                
                
                result([
                    "free_memory" : memArraykB,
                    "total_memory" : physicalMemorykB
                    ])
                
              
            }else if("getTraffic" == call.method){
                
    
                let newDict = self.netStats.getTrafficCount() as! [String:Int]
                var sent_difference = 0
                var received_difference = 0
                
                if(!self.lastDict.isEmpty){
                    // calc difference
                    let wifi_sent_difference: Int = newDict.keys.contains("wifi_sent") && self.lastDict.keys.contains("wifi_sent") ?  (newDict["wifi_sent"]! - self.lastDict["wifi_sent"]!) : 0
                    let wifi_received_difference: Int = newDict.keys.contains("wifi_received") && self.lastDict.keys.contains("wifi_received") ? (newDict["wifi_received"]! - self.lastDict["wifi_received"]!) : 0
                    
                    let wwan_sent_difference: Int = newDict.keys.contains("wwan_sent") && self.lastDict.keys.contains("wwan_sent") ?(newDict["wwan_sent"]! - self.lastDict["wwan_sent"]!) : 0
                    let wwan_received_difference: Int =  newDict.keys.contains("wwan_received") && self.lastDict.keys.contains("wwan_received") ? (newDict["wwan_received"]! - self.lastDict["wwan_received"]!) : 0
                    
                    sent_difference = wifi_sent_difference + wwan_sent_difference
                    received_difference = wifi_received_difference + wwan_received_difference
                }
                
                self.lastDict = newDict
                
                result([
                    "down" : received_difference,
                    "up" : sent_difference
                    ])
                
            }else if("getLocation" == call.method){
                let locationPermission = self.isLocationEnabled()
                var locationData = [String : Any?]()
                
                var latitude : Double = 0.0
                var longitude : Double = 0.0
                var latitudeString : String? = nil
                var longitudeString : String? = nil
                var hasAccuracy : Bool = false
                var accuracy : Double = 0.0
                let satellites  : Int = 0 //??
                var age : Int64 = 0
                var altitude : Double = 0
                var speed: Double = 0
                var haveLocationData : Bool = false
                
                if(locationPermission){
                    latitude = self.location.coordinate.latitude
                    longitude = self.location.coordinate.longitude
                    latitudeString = self.location.rmbtFormattedLatitudeString()
                    longitudeString = self.location.rmbtFormattedLongitudeString()
                    accuracy = self.location.horizontalAccuracy
                    hasAccuracy = (accuracy == 0.0) ? false : true
                    age = Date().millisecondsSince1970 - self.location.timestamp.millisecondsSince1970
                    altitude = self.location.altitude
                    speed = self.location.speed
                    haveLocationData = true
                }
                
                locationData["locationPermission"] = locationPermission
                locationData["latitude"] = latitude
                locationData["longitude"] = longitude
                locationData["latitudeString"] = latitudeString
                locationData["longitudeString"] = longitudeString
                locationData["hasAccuracy"] = hasAccuracy
                locationData["accuracy"] = accuracy
                locationData["satellites"] = satellites
                locationData["age"] = age
                locationData["provider"] = nil //cannot on ios
                locationData["altitude"] = altitude
                locationData["speed"] = speed
                locationData["haveLocationData"] = haveLocationData
                
                do {
                    let data = try JSONSerialization.data(withJSONObject: locationData)
                    let dataString = String(data: data, encoding: .utf8)!
                    result(dataString)
                    
                } catch {
                    
                    result(nil)
                }
               
            }else if("getNetworkSignal" == call.method){
                
                if(self.lastConnectivity != nil){
                    
                    do{
                        try self.lastConnectivity.refreshSignalStrength()
                        
                        result([
                            "relativeSignal" : self.lastConnectivity.relativeSignal ?? 0,
                            "signalType" : self.lastConnectivity.getSignalType(),
                            "signal" : self.lastConnectivity.getSignal() as Any,
                            "networkID" : self.lastConnectivity.networkID()
                            ])
                    }catch {
                        //ignore
                    }
                    
                }else{
                    result(nil)
                }
            }else{
                result(FlutterMethodNotImplemented)
            }
        })
    }
    
    func isLocationEnabled() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
               return false
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            @unknown default:
                return false
            }
        }
        
        return false
    }
    
    // MARK: - CLLocationManagerDelegate
    
    ///
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.location = locations[0]
        print(locations)
    }
 
// MARK: SETTINGS
    
    private func initSettingsChannel(controller: FlutterViewController){
        let settingsChannel = FlutterMethodChannel.init(name: "nic.cz.netmetrflutter/settings", binaryMessenger: controller.binaryMessenger)
        
        settingsChannel.setMethodCallHandler({(call: FlutterMethodCall, result: FlutterResult) -> Void in
            if("showSettings" == call.method){
                
                if let locationSettings = URL(string: UIApplication.openSettingsURLString){
                    UIApplication.shared.openURL(locationSettings)
                }
                result(nil)
            }else{
                result(FlutterMethodNotImplemented)
            }
        })
    }
    
// MARK: CONFIG
    
    private func initConfigChannel(controller: FlutterViewController){
        
        let configChannel = FlutterMethodChannel.init(name: "nic.cz.netmetrflutter/config", binaryMessenger: controller.binaryMessenger)
        
        configChannel.setMethodCallHandler({(call: FlutterMethodCall, result: FlutterResult) -> Void in
            if("getBuildConfig" == call.method){
                let setConfig = self.buildConfig()
                RMBTSettings.sharedSettings().updateDataConfig()
                do {
                    let data = try JSONSerialization.data(withJSONObject:setConfig)
                    let dataString = String(data: data, encoding: .utf8)!
                    result(dataString)
                    
                } catch {
                    print("JSON serialization failed: ", error)
                    result(nil)
                }
            }else if("getVersionName" == call.method){
                result("1.0-ios")
            }else if("getVersionCode" == call.method){
                result(1)
            }else if("getBasicInfo" == call.method){
                let basicInfo = self.systemInfoParams()
                
                do {
                    let data = try JSONSerialization.data(withJSONObject:basicInfo)
                    let dataString = String(data: data, encoding: .utf8)!
                    result(dataString)
                    
                } catch {
                    print("JSON serialization failed: ", error)
                    result(nil)
                }
            }else if("updateConfig" == call.method){
                let arguments = call.arguments! as! [String: AnyObject]
                let checkIpV4 : String = arguments["url_ipv4_check"] as! String
                let checkIpV6 : String = arguments["url_ipv6_check"] as! String
                let uuid : String = arguments["uuid"] as! String

                let userDefaults = UserDefaults.standard;
                userDefaults.set(uuid, forKey: "uuid")
                userDefaults.set(checkIpV4, forKey: "url_ipv4_check")
                userDefaults.set(checkIpV6, forKey: "url_ipv6_check")
                userDefaults.synchronize()
                
                //Update Control Config
                RMBTSettings.sharedSettings().updateDataConfig()
                ControlServer.sharedControlServer.updateWithCurrentSettings();
                self.connectivityService = ConnectivityService()
                self.ipCheck()
                
                result(nil)
            }else if("getCheckIpInfo" == call.method){
                
                if(self.lastIpv4 != nil && self.lastIpv6 != nil){
                    
                  
                    let ipData = [
                        "status_ipv4" : self.lastIpv4?.ipStatusOriginal,
                        "public_ipv4" : self.lastIpv4?.externalIp,
                        "private_ipv4" : self.lastIpv4?.internalIp,
                        "status_ipv6" : self.lastIpv6?.ipStatusOriginal,
                        "public_ipv6" : self.lastIpv6?.externalIp,
                        "private_ipv6" : self.lastIpv6?.internalIp
                    ] as [String : Any?]
                    
                    do {
                        let data = try JSONSerialization.data(withJSONObject:ipData)
                        let dataString = String(data: data, encoding: .utf8)!
                        result(dataString)
                        
                    } catch {
                        print("JSON serialization failed: ", error)
                        result(nil)
                    }
                }else {
                    result(nil)
                }
            }else {
                result(FlutterMethodNotImplemented)
            }
            
        })
        
    }
    
    private func buildConfig() -> NSMutableDictionary{
        
        let qos = Bundle.main.infoDictionary?[QOS_SSL_BUNDLE] as! String
        let ssl = Bundle.main.infoDictionary?[CONTROL_SSL_BUNDLE] as! String
        let port = Int(Bundle.main.infoDictionary?[PORT_BUNDLE] as! String)
        
        return [
            "client_secret" : Bundle.main.infoDictionary?[CLIENT_SECRET_BUNDLE] as! String,
            "control_host_dev" : Bundle.main.infoDictionary?[CONTROL_HOST_BUNDLE] as! String,
            "control_ipv4_host" : Bundle.main.infoDictionary?[CONTROL_IPV4_HOST_BUNDLE] as! String,
            "control_ipv6_host" : Bundle.main.infoDictionary?[CONTROL_IPV6_HOST_BUNDLE] as! String,
            "check_ipv4_host" : Bundle.main.infoDictionary?[CHECK_IPV4_HOST_BUNDLE] as! String,
            "check_ipv6_host" : Bundle.main.infoDictionary?[CHECK_IPV6_HOST_BUNDLE] as! String,
            "sentry_dsn" : Bundle.main.infoDictionary?[SENTRY_DSN] as! String,
            "sentry_url" : Bundle.main.infoDictionary?[SENTRY_URL] as! String,
            "port" : port as Any,
            "control_ssl" : ssl.toBool() as Any,
            "qos_ssl" : qos.toBool() as Any]
    }
    
    private func systemInfoParams() -> NSMutableDictionary{
        return[
            "platform": "iOS",
            "os_version": UIDevice.current.model,
            "model": self.systemInfoDeviceInternalModel(),
            "device": UIDevice.current.model,
            "language": Locale.current.languageCode ?? "en",
            "timezone": TimeZone.current.identifier,
            "type": "MOBILE",
            "name": "RMBT",
            "client": "RMBT",
            "version": "0.3",
            "softwareVersionName": Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String,
            "softwareVersionCode": Int(Bundle.main.infoDictionary?["CFBundleVersion"] as! String) as Any,
            "softwareRevision": "1.0",
        ]
    }
    
    private func systemInfoDeviceInternalModel() -> String{
        var size = 0
        sysctlbyname("hw.machine", nil, &size, nil, 0)
        var machine = [CChar](repeating: 0,  count: size)
        sysctlbyname("hw.machine", &machine, &size, nil, 0)
        return String(cString: machine)
    }
    

}
