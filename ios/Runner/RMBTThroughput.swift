/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  RMBTThroughput.swift
//  RMBT
//
//  Created by Benjamin Pucher on 29.03.15.
//  Copyright © 2015 SPECURE GmbH. All rights reserved.
//

import Foundation

///
class RMBTThroughput: CustomStringConvertible {
    
    ///
    var length: UInt64
    
    ///
    private var _startNanos: UInt64 // nasty hack to get around endless loop with didSet
    var startNanos: UInt64 {
        get {
            return _startNanos
        }
        set {
            _startNanos = newValue
            _durationNanos = _endNanos - _startNanos
            
            assert(_durationNanos >= 0, "Invalid duration")
        }
    }
    
    ///
    private var _endNanos: UInt64 // nasty hack to get around endless loop with didSet
    var endNanos: UInt64 {
        get {
            return _endNanos
        }
        set {
            _endNanos = newValue
            _durationNanos = _endNanos - _startNanos
            
            assert(_durationNanos >= 0, "Invalid duration")
        }
    }
    
    ///
    private var _durationNanos: UInt64 // nasty hack to get around endless loop with didSet
    var durationNanos: UInt64 {
        get {
            return _durationNanos
        }
        set {
            _durationNanos = newValue
            _endNanos = _startNanos + _durationNanos
            
            assert(_durationNanos >= 0, "Invalid duration")
        }
    }
    
    //
    
    convenience init() {
        self.init(length: 0, startNanos: 0, endNanos: 0)
    }
    
    ///
    init(length: UInt64, startNanos: UInt64, endNanos: UInt64) {
        self.length = length
        self._startNanos = startNanos
        self._endNanos = endNanos
        
        self._durationNanos = endNanos - startNanos
        
        assert(_durationNanos >= 0, "Invalid duration")
    }
    
    ///
    func containsNanos(nanos: UInt64) -> Bool {
        return (_startNanos <= nanos && _endNanos >= nanos)
    }
    
    ///
    func kilobitsPerSecond() -> UInt32 {
        if(length == 0 || _durationNanos == 0){
            return UInt32(0)
        }
        
       return UInt32(Double(length) * 8.0 / (Double(_durationNanos) * Double(1e-6)))
    }
    
    func kilobitsPerMicroSecond() -> UInt32 {
        if(length == 0 || _durationNanos == 0){
            return UInt32(0)
        }
        
        return UInt32(Double(length) * 8.0 / (Double(_durationNanos) * Double(1e-9)))
    }
    
    func classification() -> UInt8{
        let kb = kilobitsPerSecond()
        switch kb {
        case 2000...:
            return 3
        case 1000...:
            return 2
        default:
            return 1
        }
    }
    
    func mbs() -> String{
        let kb = kilobitsPerSecond()
        let mb : Double = Double(kb) / Double(1000)
        return String(format: "%.2f Mbit/s", mb)
    }
    
    func totalBytes() -> UInt32 {
        if(length == 0 || _durationNanos == 0){
            return UInt32(0)
        }
        
        return UInt32(Double(length) * 8.0 / (Double(_durationNanos)))
    }
    ///
    var description: String {
        return String(format: "(%@-%@, %lld bytes, %@)",
                      RMBTSecondsStringWithNanos(nanos: _startNanos),
                      RMBTSecondsStringWithNanos(nanos: _endNanos),
                      length,
                      RMBTSpeedMbpsString(kbps: kilobitsPerSecond()))
    }
}
