/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  QualityOfServiceTest.swift
//  RMBT
//
//  Created by Benjamin Pucher on 16.01.15.
//  Copyright © 2015 SPECURE GmbH. All rights reserved.
//

import Foundation

///
public class QualityOfServiceTest  {
    
    ///
    typealias ConcurrencyGroup = UInt
    
    ///
    typealias JsonObjectivesType = [String: [[String: AnyObject]]]
    
    //
    
    ///
    private let executorQueue = DispatchQueue(label: "com.specure.rmbt.executorQueue", attributes: .concurrent)
    //private let executorQueue = dispatch_queue_create("com.specure.rmbt.executorQueue", DISPATCH_QUEUE_CONCURRENT)
    
    ///
    private let qosQueue = DispatchQueue(label: "com.specure.rmbt.qosQueue", attributes: .concurrent)

   //private let qosQueue = dispatch_queue_create("com.specure.rmbt.qosQueue", DISPATCH_QUEUE_CONCURRENT)
    
    ///
     private let mutualExclusionQueue = DispatchQueue(label: "com.specure.rmbt.qos.mutualExclusionQueue")
    //private let mutualExclusionQueue = dispatch_queue_create("com.specure.rmbt.qos.mutualExclusionQueue", DISPATCH_QUEUE_SERIAL)
    
    ///
    var delegate: QualityOfServiceTestDelegate?
    
    ///
    private let testToken: String
    
    ///
    private let speedtestStartTime: UInt64
    
    ///
    public var testCount: UInt16 = 0
    
    ///
    public var currentTestCount: UInt16 = 0
    
    ///
    public var successTestCount: UInt16 = 0
    
    ///
    public var failedTestCount: UInt16 = 0
    
    ///
    private var activeTestsInConcurrencyGroup = 0
    
    ///
    private var controlConnectionMap = [String: QOSControlConnection]()
    
    ///
    private var qosTestConcurrencyGroupMap = [ConcurrencyGroup: [QOSTest]]()
    
    ///
    private var testTypeCountMap = [QOSTestType: UInt16]()
    
    ///
    private var sortedConcurrencyGroups = [ConcurrencyGroup]()
    
    ///
    var resultArray = [QOSTestResult]()
    
    ///
    private var stopped = false
    
    ///
    private var qosSSL : Bool = false
    
    ///loopMode
    private var loopMode: Bool = false
    private var loopWorker: DispatchQueue?
    
    ///
    private var onlyJitter : Bool
    
    private var controlServerInfoDelegate: ControlServerInfoDelegate?
    
    ///
    convenience init() {
        self.init(testToken: "f7e75c4c-f81f-41d6-b5c5-53b9452a459b_1424341414_dXX9tW1uNORSPPw0xaKvYQatfbU=", speedtestStartTime: UInt64.nanoTime() - 30 * UInt64(NSEC_PER_SEC), qosSSL: true, runJitter: false, loopMode: false, loopWorker: nil, controlServerInfoDelegate: nil) // for testing
    }
    
    ///
    init(testToken: String, speedtestStartTime: UInt64, qosSSL : Bool, runJitter: Bool, loopMode: Bool, loopWorker: DispatchQueue?, controlServerInfoDelegate: ControlServerInfoDelegate?) {
        self.testToken = testToken
        self.speedtestStartTime = speedtestStartTime
        self.qosSSL = qosSSL
        self.onlyJitter = runJitter
        self.loopMode = loopMode
        self.loopWorker = loopWorker
        self.controlServerInfoDelegate = controlServerInfoDelegate

        logger.debug("QualityOfServiceTest initialized with test token: \(testToken) at start time \(speedtestStartTime)")
    }
    
    private func resultQueue() -> DispatchQueue{
        return loopMode ? loopWorker! : DispatchQueue.main
    }
    
    ///
    public func start() {
        if (!stopped) {
            qosQueue.async {
                self.fetchQOSTestParameters()
            }
        }
    }
    
    ///
    public func stop() {
        logger.debug("ABORTING QOS TEST")
        
        mutualExclusionQueue.sync {
            self.stopped = true
            
            // close all control connections
            self.closeAllControlConnections()
            
            // inform delegate
            resultQueue().async {
                self.delegate?.qualityOfServiceTestDidStop(test: self)
                return
            }
        }
    }
    
    
    ///
    private func fetchQOSTestParameters() {
        if (stopped) {
            return
        }
        
        ControlServer.sharedControlServer.getQOSObjectives(success: { (response) -> () in
            self.qosQueue.async {
                self.continueWithQOSParameters(responseObject: response)
            }
        }, error: { (error, info) -> () in
            logger.debug("ERROR fetching qosTestRequest")
            
            self.fail(errorType: .ErrorGettingQoSObjectives, error: error, info: info) // TODO: error message...
        })
    }
    
    ///
    private func continueWithQOSParameters(responseObject: AnyObject) {
        if (stopped) {
            return
        }
        
        // call didStart delegate method // TODO: right place here?
       resultQueue().async {
            self.delegate?.qualityOfServiceTestDidStart(test: self)
            return
        }
        
        if onlyJitter{
            if let objectives = responseObject["objectives"] as? JsonObjectivesType {
                if(!objectives.keys.contains(QOSTestType.JITTER.description)){
                    self.delegate?.qualityOfServiceTestDidStop(test: self)
                    return
                }
            }
        }
        
        parseRequestResult(responseObject: responseObject)
        createTestTypeCountMap()
        //openAllControlConnections() // open all control connections before tests
        runQOSTests()
    }
    
    ///
    private func parseRequestResult(responseObject: AnyObject) {
        if (stopped) {
            return
        }
        
        if let resultDictionary = responseObject as? [String:AnyObject] {
            
            if let errorArray = resultDictionary["error"] as? [AnyObject] { // TODO: type
                if (!errorArray.isEmpty) { // error element is always present in json, therefore check if it is empty to find an error
                    logger.debug("ERROR ON QOS TEST REQUEST: \(errorArray)")
                    // TODO: call did fail delegate method
                    self.fail(errorType: .ErrorGettingQoSObjectives, error: nil, info: nil) // TODO: error message...
                    
                    return
                }
            }
            
            // loop through objectives
            if let objectives = resultDictionary["objectives"] as? JsonObjectivesType {
                
                // objective type is TCP, UDP, etc.
                // objective values are arrays of dictionaries for each test
                for (objectiveType, objectiveValues) in objectives {
                    
                    // loop each test
                    for (objectiveParams) in objectiveValues {
                        logger.verbose("-----")
                        logger.verbose("\(objectiveType): \(objectiveParams)")
                        logger.verbose("-------------------")
                        
                        // try to create qos test object from params
                        if let qosTest = QOSFactory.createQOSTest(typeString: objectiveType, params: objectiveParams) {
                            ///////////// ONT
                            if onlyJitter {
                                if let type = QOSTestType(rawValue: objectiveType) {
                                    if type == .JITTER {
                                        
                                        logger.debug("created VOIP test as the main test: \(qosTest)")
                                        
                                        var concurrencyGroupArray: [QOSTest]? = qosTestConcurrencyGroupMap[qosTest.concurrencyGroup]
                                        if concurrencyGroupArray == nil {
                                            concurrencyGroupArray = [QOSTest]()
                                        }
                                        
                                        concurrencyGroupArray!.append(qosTest)
                                        qosTestConcurrencyGroupMap[qosTest.concurrencyGroup] = concurrencyGroupArray // is this line needed? wasn't this passed by reference?
                                        
                                        // increase test count
                                        testCount += 1
                                    }
                                }
                                
                            } else {
                                if let type = QOSTestType(rawValue: objectiveType) {
                                    if type != .JITTER {
                                        logger.debug("created qos test: \(qosTest)")
                                        
                                        var concurrencyGroupArray: [QOSTest]? = qosTestConcurrencyGroupMap[qosTest.concurrencyGroup]
                                        if concurrencyGroupArray == nil {
                                            concurrencyGroupArray = [QOSTest]()
                                        }

                                        concurrencyGroupArray!.append(qosTest)
                                        qosTestConcurrencyGroupMap[qosTest.concurrencyGroup] = concurrencyGroupArray // is this line needed? wasn't this passed by reference?

                                        // increase test count
                                        testCount += 1
                                    }
                                }
                            }
                            
                        } else {
                            logger.debug("unimplemented/unknown qos type: \(objectiveType)")
                        }
                    }
                }
            }
            
            currentTestCount = testCount
        }
        
        // create sorted array of keys to let the concurrencyGroups increase
        let keys = Array(qosTestConcurrencyGroupMap.keys)
        sortedConcurrencyGroups = keys.sorted(by: <)
        
        logger.debug("sorted concurrency groups: \(sortedConcurrencyGroups)")
    }
    
    ///
    private func createTestTypeCountMap() {
        if (stopped) {
            return
        }
        
        var testTypeSortDictionary = [QOSTestType:ConcurrencyGroup]()
        
        // fill testTypeCount map (used for displaying the finished test types in ui)
        for (cg, testArray) in qosTestConcurrencyGroupMap { // loop each concurrency group
            for test in testArray { // loop the tests inside each concurrency group
                let testType = test.getType()
                
                var count: UInt16? = testTypeCountMap[testType!]
                if (count == nil) {
                    count = 0
                }
                
                count! += 1
                
                testTypeCountMap[testType!] = count!
                
                //////
                
                if (testTypeSortDictionary[testType!] == nil) {
                    testTypeSortDictionary[testType!] = cg
                }
            }
        }
        
        // get test types and sort them according to their first execution
        var testTypeArray = Array<QOSTestType>(self.testTypeCountMap.keys)
        testTypeArray.sort() { lhs, rhs in
            testTypeSortDictionary[lhs]! < testTypeSortDictionary[rhs]!
        }
        
        // call didFetchTestTypes delegate method
        resultQueue().async {
            self.delegate?.qualityOfServiceTest(test: self, didFetchTestTypes: testTypeArray)
            return
        }
        
        logger.debug("TEST TYPE COUNT MAP: \(testTypeCountMap)")
    }
    
    ///
    private func runQOSTests() {
        if (stopped) {
            return
        }
        
        // start with first concurrency group
        runTestsOfNextConcurrencyGroup()
    }
    
    ///
    private func runTestsOfNextConcurrencyGroup() {
        if (stopped) {
            return
        }
        
        if (sortedConcurrencyGroups.count > 0) {
            let concurrencyGroup = sortedConcurrencyGroups.remove(at: 0) // what happens if empty?
            
            if let testArray = qosTestConcurrencyGroupMap[concurrencyGroup] {
                
                // set count of tests
                activeTestsInConcurrencyGroup = testArray.count
                
                // calculate control connection timeout (TODO: improve)
                //var controlConnectionTimeout: UInt64 = 0
                //for qosTest in testArray {
                //    controlConnectionTimeout += qosTest.timeout
                //}
                /////
                var index: UInt16 = 0
                // loop test array
                for qosTest in testArray {
                    if (stopped) {
                        return
                    }
                    
                    // get previously opened control connection
                    let controlConnection = getControlConnection(qosTest: qosTest) // blocking if new connection has to be established
                    
                    
                    // get test executor
                    if let testExecutor = QOSFactory.createTestExecutor(testObject: qosTest, controlConnection: controlConnection, delegateQueue: executorQueue, speedtestStartTime: speedtestStartTime) {
                        // TODO: which queue?
                        
                        // set test token (TODO: IMPROVE)
                        testExecutor.setTestToken(testToken: self.testToken)
                        //testExecutor.setTestIndex(testIndex: index)
                        
                        if(onlyJitter){
                           controlConnection.delegate = JitterConnectionWorker.init(delegate: delegate, serviceTest: self, jitterExecutor: testExecutor)
                         }
                        
                        if (testExecutor.needsControlConnection()) {
                            // set control connection timeout (TODO: compute better! (not all tests may use same control connection))
                            logger.debug("setting control connection timeout to \(/*controlConnectionTimeout*/nsToMs(ns: qosTest.timeout)) ms")
                            controlConnection.setTimeout(/*controlConnectionTimeout*/timeout: qosTest.timeout)
                            
                            // TODO: DETERMINE IF TEST NEEDS CONTROL CONNECTION
                            // IF IT NEEDS IT, AND CONTROL CONNECTION CONNECT FAILED THEN SKIP THIS TEST AND DON'T SEND RESULT TO SERVER
                            if (!controlConnection.connected) {
                                // don't do this test
                                logger.info("skipping test because it needs control connection but we don't have this connection. \(qosTest)")
                                
                                self.mutualExclusionQueue.async {
                                    self.qosTestFinishedWithResult(testType: qosTest.getType(), withTestResult: nil) // no result because test didn't run
                                }
                                
                                continue
                            }
                        }
                        
                        logger.debug("starting execution of test: \(qosTest)")
                        index+=1
                        
                        // execute test
                        self.executorQueue.async {
                            testExecutor.execute() { (testResult: QOSTestResult) in
                                
                                self.mutualExclusionQueue.sync {
                                    self.qosTestFinishedWithResult(testType: testResult.testType, withTestResult: testResult)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    ///
    private func qosTestFinishedWithResult(testType: QOSTestType, withTestResult testResult: QOSTestResult?) {
        if (stopped) {
            return
        }
        logger.debug("qos: \(testType)")
        logger.debug("qos test finished with result: \(String(describing: testResult))")
        
        if let testResult = testResult {
            logger.debug("success: \(String(describing: testResult.success))")

            if (testResult.fatalError) {
                // TODO: quit whole test due to fatal error
                // !!
                
                // TODO: dispatch delegate method
                self.fail(errorType: .ErrorQosTest ,error: nil, info: nil)// TODO: error message...
            }else{
                
            }
            
            if testResult.success{
                self.successTestCount += 1
            }
            else{
                self.failedTestCount += 1
            }

            // add result to result map
            resultArray.append(testResult)
        }
        
        checkProgress()
        checkTypeCount(testType: testType)
        checkTestState()
    }
    
    ///
    private func checkProgress() {
        if (stopped) {
            return
        }
        
        // decrement test counts
        self.currentTestCount -= 1
        
        // check for progress
        let testsLeft = self.testCount - self.currentTestCount
        let percent: Float = Float(testsLeft) / Float(self.testCount)
        
        logger.debug("QOS: increasing progress to \(percent)")
        
        resultQueue().async {
            self.delegate?.qualityOfServiceTest(test: self, didProgressToValue: percent)
            return
        }
    }
    
    ///
    private func checkTypeCount(testType: QOSTestType) {
        if (stopped) {
            return
        }
        
        // check for finished test type
        self.testTypeCountMap[testType]! -= 1
        if (self.testTypeCountMap[testType]! == 0) {
            
            logger.debug("QOS: finished test type: \(testType)")
            
            resultQueue().async {
                self.delegate?.qualityOfServiceTest(test: self, didFinishTestType: testType)
                return
            }
        }
    }
    
    ///
    private func checkTestState() {
        if (stopped) {
            return
        }
        
        activeTestsInConcurrencyGroup -= 1
        
        if (activeTestsInConcurrencyGroup == 0) {
            // all tests in concurrency group finished
            // -> go on with next concurrency group
            
            if (sortedConcurrencyGroups.count > 0) {
                // there are more concurrency groups (and tests)
                
                self.qosQueue.async {
                    self.runTestsOfNextConcurrencyGroup()
                }
                
            } else {
                // all concurrency groups finished
                self.qosQueue.async {
                    self.finalizeQOSTests()
                }
            }
        }
    }
    
    ///
    private func finalizeQOSTests() {
        if (stopped) {
            return
        }
        
        logger.debug("ALL FINISHED")
        
        closeAllControlConnections()
        
        // submit results
        submitQOSTestResults()
    }
    
    ///
    private func getControlConnection(qosTest: QOSTest) -> QOSControlConnection {
        // determine control connection
        let controlConnectionKey: String = "\(qosTest.serverAddress)_\(qosTest.serverPort)"
        
        // TODO: make instantiation of control connection synchronous with locks!
        var conn: QOSControlConnection! = self.controlConnectionMap[controlConnectionKey]
        if (conn == nil) {
            logger.debug("\(controlConnectionKey): trying to open new control connection")
            //logger.debug("NO CONTROL CONNECTION PRESENT FOR \(controlConnectionKey), creating a new one")
            logger.debug("\(controlConnectionKey): BEFORE LOCK")
            
            // TODO: fail after timeout if qos server not available
            
            conn = QOSControlConnection(testToken: testToken, qosSSL: qosSSL)
            
            // connect
            _ = conn.connect(host: qosTest.serverAddress, onPort: qosTest.serverPort) // blocking
            
            //logger.debug("AFTER LOCK: have control connection?: \(isConnected)")
            // TODO: return nil? if not connected
            
            logger.debug("\(controlConnectionKey): AFTER LOCK -> CONTROL CONNECTION READY TO USE")
            
            controlConnectionMap[controlConnectionKey] = conn
        } else {
            logger.debug("\(controlConnectionKey): control connection already opened")
        }
        
        if (!conn.connected) {
            // reconnect
            _ = conn.connect(host: qosTest.serverAddress, onPort: qosTest.serverPort)
        }
        
        return conn
    }
    
    ///
    private func openAllControlConnections() {
        logger.info("opening all control connections")
        
        for concurrencyGroup in self.sortedConcurrencyGroups {
            if let testArray = qosTestConcurrencyGroupMap[concurrencyGroup] {
                for qosTest in testArray {
                    //dispatch_sync(mutualExclusionQueue) {
                    _ = self.getControlConnection(qosTest: qosTest)
                    //logger.debug("opened control connection for qosTest \(qosTest)")
                    //}
                }
            }
        }
    }
    
    ///
    private func closeAllControlConnections() {
        logger.info("closing all control connections")
        
        // TODO: if everything is done: close all control connections
        for (_, controlConnection) in self.controlConnectionMap {
            logger.debug("closing control connection \(controlConnection)")
            controlConnection.disconnect()
        }
    }
    
    ///////////////
    
    ///
    private func fail(errorType: QoSErrorType,error: NSError?, info: NSDictionary?) {
        if (stopped) {
            return
        }
        
        stop()
        
        resultQueue().async  {
            self.delegate?.qualityOfServiceTest(test: self, didFailWithError: error, errorInfo: info, errorType: errorType)
            return
        }
    }
    
    ///
    private func success() {
        if (stopped) {
            return
        }
        
        resultQueue().async {
            self.delegate?.qualityOfServiceTest(test: self, didFinishWithResults: self.resultArray)
            return
        }
    }
    
    ///
    func jitterFail(testResult : QOSTestResult?){
        if (stopped) {
           return
        }
        
        if(testResult != nil){
            self.resultArray.append(testResult!)
                   
           resultQueue().async {
               self.delegate?.qualityOfServiceTest(test: self, didFinishWithResults: self.resultArray)
           }
        }
        
        self.stop()
    }
    
    ////////////////////////////////////////////
    
    ///
    private func submitQOSTestResults() {
        if (stopped) {
            return
        }
        
        var _testResultArray = [QOSTestResults]()
        
        for testResult in resultArray { // TODO: resultArray == _testResultArray? just use resultArray?
            if (!testResult.isEmpty()) {
                _testResultArray.append(testResult.resultDictionary)
            }
        }
        
        // don't send results if all results are empty (e.g. only tcp tests and no control connection)
        if (_testResultArray.isEmpty || onlyJitter) {
            
            // inform delegate
            success()
            
            return
        }
        
        //let qosDuration = nanoTime() - qosStartTime
        
        var params: [String: AnyObject] = [
            "time": NSNumber(value: UInt64.currentTimeMillis()), // currently unused on server!
            "test_token": testToken as AnyObject,
            ]
        
        params["qos_result"] = _testResultArray as NSArray // because array is a struct, nsarray is array's objc counterpart and this is an object...
        
        logger.debug("\(params)")
        
        self.controlServerInfoDelegate?.changeQoSDataStatus(taskStatus: .RUNNING)
        let resultData: ResultData = ResultData.init()
        
        ControlServer.sharedControlServer.submitQOSTestResult(result: params, success: { () -> () in
            logger.debug("QOS TEST RESULT SUBMIT SUCCESS")
            
            // now the test has finished...succeeding methods should go here
            resultData.resultStatus = .OK
            self.controlServerInfoDelegate?.taskFinished(task: .QOS, resultData: resultData)
            self.success()
            
        }) { (error, info) -> () in
            logger.debug("QOS TEST RESULT SUBMIT ERROR: \(error)")
            
            var errorMessage : String? = nil
                           
            if let message = info?["error_message"]{
               errorMessage = message as? String
            }
            
            resultData.errorMessage = errorMessage
            resultData.resultStatus = .FAILED
            self.controlServerInfoDelegate?.taskFinished(task: .QOS, resultData: resultData)

            // here the test failed...
            self.fail(errorType: .ErrorSubmittingFinalResult, error: error, info: info)
        }
    }
}

class JitterConnectionWorker : QOSControlConnectionDelegate{
    var delegate: QualityOfServiceTestDelegate?
    var serviceTest: QualityOfServiceTest?
    var jitterExecutor: QOSTestExecutorProtocol?

    init(delegate: QualityOfServiceTestDelegate?, serviceTest: QualityOfServiceTest, jitterExecutor: QOSTestExecutorProtocol?) {
        self.delegate = delegate
        self.serviceTest = serviceTest
        self.jitterExecutor = jitterExecutor
    }
    
    func controlConnectionReadyToUse(_ connection: QOSControlConnection) {
        //ignore
    }
    
    func controlConnectionFailed(_ connection: QOSControlConnection) {
        jitterExecutor?.setFailData()
        serviceTest?.jitterFail(testResult: jitterExecutor?.getResult() ?? nil)
    }
    
    
}
