/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  QOSControlConnection.swift
//  RMBT
//
//  Created by Benjamin Pucher on 09.12.14.
//  Copyright © 2014 SPECURE GmbH. All rights reserved.
//

import Foundation
import CocoaAsyncSocket

class QOSControlConnectionTaskWeakObserver: NSObject {
    weak var delegate: QOSControlConnectionTaskDelegate?
    
    init(_ delegate: QOSControlConnectionTaskDelegate) {
        self.delegate = delegate
    }
}

class QOSControlConnection : NSObject {
    
    private let TAG_GREETING = -1
    private let TAG_FIRST_ACCEPT = -2
    private let TAG_TOKEN = -3
    //private let TAG_OK = -4
    private let TAG_SECOND_ACCEPT = -4
    
    private let TAG_SET_TIMEOUT = -10
    
    private let TAG_TASK_COMMAND = -100
    
    //
    
    ///
    var delegate: QOSControlConnectionDelegate?
    
    ///
    var connected = false
    
    ///
    private let testToken: String
    
    ///
    private let qosSSL: Bool
    
    ///
    private let connectCountDownLatch = CountDownLatch()
    
    ///
    private let socketQueue = DispatchQueue(label: "com.specure.rmbt.controlConnectionSocketQueue")
    
    ///
    private let mutableQueue = DispatchQueue(label: "com.specure.rmbt.mutableQueue")
    
    ///
    private var qosControlConnectionSocket: GCDAsyncSocket!
    
    ///
    internal var taskDelegateDictionary: [UInt: [QOSControlConnectionTaskWeakObserver]] = [:]
    
    ///
    private var pendingTimeout: Double = 0
    private var currentTimeout: Double = 0
    
    //
    
    ///
    init(testToken: String, qosSSL: Bool) {
        self.testToken = testToken
        self.qosSSL = qosSSL
        
        super.init()
        
        // create socket
        qosControlConnectionSocket = GCDAsyncSocket(delegate: self, delegateQueue: socketQueue) // TODO: specify other dispath queue
        
        logger.verbose("control connection created")
    }
    
    // MARK: connection handling
    
    ///
    func connect(host: String, onPort port: UInt16) -> Bool {
        return connect(host: host, onPort: port, withTimeout: QOS_CONTROL_CONNECTION_TIMEOUT_NS)
    }
    
    ///
    func connect(host: String, onPort port: UInt16, withTimeout timeout: UInt64) -> Bool {
        let connectTimeout = nsToSec(ns: timeout)
        
        do {
            try qosControlConnectionSocket.connect(toHost: host, onPort: port, withTimeout: connectTimeout)
        } catch {
            // there was an error
            logger.verbose("connection error \(error)")
        }
        
        _ = connectCountDownLatch.await(timeout)
        
        return connected
    }
    
    ///
    func disconnect() {
        // send quit
        logger.debug("QUIT QUIT QUIT QUIT QUIT")
        
        writeLine(line: "QUIT", withTimeout: QOS_CONTROL_CONNECTION_TIMEOUT_SEC, tag: -1) // don't bother with the tag, don't need read after this operation
        qosControlConnectionSocket.disconnectAfterWriting()
        //qosControlConnectionSocket.disconnectAfterReadingAndWriting()
    }
    
    // MARK: commands
    
    ///
    func setTimeout(timeout: UInt64) {
        //logger.debug("SET TIMEOUT: \(timeout)")
        
        // timeout is in nanoseconds -> convert to ms
        var msTimeout = nsToMs(ns: timeout)
        
        // if msTimeout is lower than 15 seconds, increase it
        if (msTimeout < 15_000) {
            msTimeout = 15_000
        }
        
        if (currentTimeout == msTimeout) {
            logger.debug("skipping change of control connection timeout because old value = new value")
            return // skip if old == new timeout
        }
        
        pendingTimeout = msTimeout
        
        //logger.debug("REQUEST CONN TIMEOUT \(msTimeout)")
        writeLine(line: "REQUEST CONN TIMEOUT \(msTimeout)", withTimeout: QOS_CONTROL_CONNECTION_TIMEOUT_SEC, tag: TAG_SET_TIMEOUT)
        readLine(tag: TAG_SET_TIMEOUT, withTimeout: QOS_CONTROL_CONNECTION_TIMEOUT_SEC)
    }
    
    // MARK: control connection delegate methods
    
    func clearObservers() {
        for (_, arrayOfObservers) in taskDelegateDictionary.enumerated() {
            var observers = arrayOfObservers.value
            for observer in arrayOfObservers.value {
                if observer.delegate == nil {
                    if let index = observers.firstIndex(of: observer) {
                        observers.remove(at: index)
                    }
                }
            }
            if observers.count == 0 {
                taskDelegateDictionary.removeValue(forKey: arrayOfObservers.key)
            }
        }
    }
    
    ///
    func registerTaskDelegate(_ delegate: QOSControlConnectionTaskDelegate, forTaskId taskId: UInt) {
        self.mutableQueue.sync {
            var observers: [QOSControlConnectionTaskWeakObserver]? = taskDelegateDictionary[taskId]
            if observers == nil {
                observers = []
            }
            
            observers?.append(QOSControlConnectionTaskWeakObserver(delegate))
            taskDelegateDictionary[taskId] = observers
            self.clearObservers()
            print("registerTaskDelegate: \(taskId), delegate: \(delegate)")
        }
    }
    
    ///
    func unregisterTaskDelegate(_ delegate: QOSControlConnectionTaskDelegate, forTaskId taskId: UInt) {
        self.mutableQueue.sync {
            if let tempObservers = taskDelegateDictionary[taskId] {
                var observers = tempObservers
                if let index = observers.firstIndex(where: { (observer) -> Bool in
                    return observer.delegate! === delegate
                }) {
                    observers.remove(at: index)
                    print("unregisterTaskDelegate: \(taskId), delegate: \(delegate)")
                }
                if observers.count == 0 {
                    taskDelegateDictionary[taskId] = nil
                }
                else {
                    taskDelegateDictionary[taskId] = observers
                }
            }
            else {
                print("TaskDelegate: \(taskId) Not found")
            }
            self.clearObservers()
        }
    }
    
    // MARK: task command methods
    
    // TODO: use closure instead of delegate methods
    /// command should not contain \n, will be added inside this method
    func sendTaskCommand(command: String, withTimeout timeout: TimeInterval, forTaskId taskId: UInt, tag: Int) {
        /*if (!qosControlConnectionSocket.isConnected) {
         logger.error("control connection is closed, sendTaskCommand won't work!")
         }*/
        
        let _command = command + " +ID\(taskId)"
        
        let t = createTaskCommandTag(forTaskId: taskId, tag: tag)
        logger.verbose("SENDTASKCOMMAND: (taskId: \(taskId), tag: \(tag)) -> \(t) (\(String(t, radix: 2)))")
        
        // write command
        writeLine(line: _command, withTimeout: timeout, tag: t)
        
        // and then read? // TODO: or use thread with looped readLine?
        readLine(tag: t, withTimeout: timeout)
    }
    
    /// command should not contain \n, will be added inside this method
    func sendTaskCommand(command: String, withTimeout timeout: TimeInterval, forTaskId taskId: UInt) {
        sendTaskCommand(command: command, withTimeout: timeout, forTaskId: taskId, tag: TAG_TASK_COMMAND)
    }
    
    // MARK: convenience methods
    
    ///
    private func writeLine(line: String, withTimeout timeout: TimeInterval, tag: Int) {
        qosControlConnectionSocket.writeLine(line: line, withTimeout: timeout, tag: tag)
    }
    
    ///
    private func readLine(tag: Int, withTimeout timeout: TimeInterval) {
        qosControlConnectionSocket.readLine(tag: tag, withTimeout: timeout)
    }
    
    // MARK: other methods
    
    ///
    private func createTaskCommandTag(forTaskId taskId: UInt, tag: Int) -> /*UInt32*/Int {
        // bitfield: 0111|aaaa_aaaa_aaaa|bbbb_bbbb_bbbb_bbbb
        
        var bitfield: UInt32 = 0x7
        
        bitfield = bitfield << 12
        
        bitfield = bitfield + (UInt32(abs(tag)) & 0x0000_0FFF)
        
        bitfield = bitfield << 16
        
        bitfield = bitfield + (UInt32(taskId) & 0x0000_FFFF)
        
        //logger.verbose("created BITFIELD for taskId: \(taskId), tag: \(tag) -> \(String(bitfield, radix: 2))")
        //logger.verbose("created BITFIELD for taskId: \(taskId), tag: \(tag) -> \(String(Int(bitfield), radix: 2))")
        
        return Int(bitfield)
    }
    
    ///
    private func parseTaskCommandTag(taskCommandTag commandTag: Int) -> (taskId: UInt, tag: Int)? {
        let _commandTag = UInt(commandTag)
        
        if (!isTaskCommandTag(taskCommandTag: commandTag)) {
            return nil // not a valid task command tag
        }
        
        let taskId: UInt = (_commandTag & 0x0000_FFFF)
        let tag = Int((_commandTag & 0x0FFF_0000) >> 16)
        
        logger.verbose("BITFIELD2: \(commandTag) -> (taskId: \(taskId), tag: \(tag))")
        
        return (taskId, tag)
    }
    
    ///
    private func isTaskCommandTag(taskCommandTag commandTag: Int) -> Bool {
        if (commandTag < 0) {
            return false
        }
        
        return UInt(commandTag) & 0x7000_0000 == 0x7000_0000
    }
    
    ///
    private func matchAndGetTestIdFromResponse(response: String) -> UInt? {
        do {
            let regex = try NSRegularExpression(pattern: "\\+ID(\\d*)", options: [])
            
            if let match = regex.firstMatch(in: response, options: [], range: NSMakeRange(0, response.count)) {
                //println(match)
                
                if (match.numberOfRanges > 0) {
                    let idStr = (response as NSString).substring(with: match.range(at: 1))
                    
                    //return UInt(idStr.toInt()) // does not work because of Int?
                    return UInt(idStr)
                }
            }
        } catch {
            // TODO?
        }
        
        return nil
    }
}

// MARK: GCDAsyncSocketDelegate methods

///
extension QOSControlConnection : GCDAsyncSocketDelegate {
    
    func socket(_ sock: GCDAsyncSocket, didConnectToHost host: String, port: UInt16) {
        logger.verbose("connected to host \(String(describing: host)) on port \(port)")
        
        if (self.qosSSL) {
            let tlsSettings: [String: NSObject] = [
                GCDAsyncSocketManuallyEvaluateTrust: true as NSObject
            ]
            
            // control connection to qos server uses tls
            sock.startTLS(tlsSettings)
        } else {
            readLine(tag: TAG_GREETING, withTimeout: QOS_CONTROL_CONNECTION_TIMEOUT_SEC)
        }
    }
  
    func socket(_ sock: GCDAsyncSocket, didReceive trust: SecTrust, completionHandler: @escaping (Bool) -> Void) {
        logger.verbose("DID RECEIVE TRUST")
        completionHandler(true)
    }
    
    func socketDidSecure(_ sock: GCDAsyncSocket) {
        logger.verbose("socketDidSecure")
        
        // tls connection has been established, start with QTP handshake
        readLine(tag: TAG_GREETING, withTimeout: QOS_CONTROL_CONNECTION_TIMEOUT_SEC)
    }
    
    func socket(_ sock: GCDAsyncSocket, didRead data: Data, withTag tag: Int) {
        logger.verbose("didReadData \(String(describing: data)) with tag \(tag)")
        
        let str: String = SocketUtils.parseResponseToString(data)!
        
        logger.verbose("didReadData \(str)")
        
        switch tag {
        case TAG_GREETING:
            // got greeting
            logger.verbose("got greeting")
            
            // read accept
            readLine(tag: TAG_FIRST_ACCEPT, withTimeout: QOS_CONTROL_CONNECTION_TIMEOUT_SEC)
            
        case TAG_FIRST_ACCEPT:
            // got accept
            logger.verbose("got accept")
            
            // send token
            let tokenCommand = "TOKEN \(testToken)\n"
            writeLine(line: tokenCommand, withTimeout: QOS_CONTROL_CONNECTION_TIMEOUT_SEC, tag: TAG_TOKEN)
            
            // read token response
            readLine(tag: TAG_TOKEN, withTimeout: QOS_CONTROL_CONNECTION_TIMEOUT_SEC)
            
        case TAG_TOKEN:
            // response from token command
            logger.verbose("got ok")
            
            // read second accept
            readLine(tag: TAG_SECOND_ACCEPT, withTimeout: QOS_CONTROL_CONNECTION_TIMEOUT_SEC)
            
        case TAG_SECOND_ACCEPT:
            // got second accept
            logger.verbose("got second accept")
            
            // now connection is ready
            logger.verbose("CONNECTION READY")
            
            connected = true // set connected to true to unlock
            connectCountDownLatch.countDown()
            
            // call delegate method // TODO: on which queue?
            self.delegate?.controlConnectionReadyToUse(self)
            
        //
        case TAG_SET_TIMEOUT:
            // return from REQUEST CONN TIMEOUT
            if (str == "OK\n") {
                logger.debug("set timeout ok")
                
                currentTimeout = pendingTimeout
                
                // OK
            } else {
                logger.debug("set timeout fail \(str)")
                // FAIL
            }
            
        default:
            //case TAG_TASK_COMMAND:
            // got reply from task command
            
            logger.verbose("TAGTAGTAGTAG: \(tag)")
            
            //dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            if (self.isTaskCommandTag(taskCommandTag: tag)) {
                if let (_taskId, _tag) = self.parseTaskCommandTag(taskCommandTag: tag) {
                    
                    logger.verbose("\(tag): got reply from task command")
                    logger.verbose("\(tag): taskId: \(_taskId), _tag: \(_tag)")
                    
                    if let taskId = self.matchAndGetTestIdFromResponse(response: str) {
                        logger.verbose("\(tag): TASK ID: \(taskId)")
                        
                        //logger.verbose("\(taskDelegateDictionary.count)")
                        //logger.verbose("\(taskDelegateDictionary.indexForKey(1))")
                        
                        if let observers = self.taskDelegateDictionary[taskId] {
                            for observer in observers {
                                logger.verbose("\(tag): TASK DELEGATE: \(String(describing: observer.delegate))")
                                
                                logger.debug("CALLING DELEGATE METHOD of \(String(describing: observer.delegate)), withResponse: \(str), taskId: \(taskId), tag: \(tag), _tag: \(_tag)")
                                
                                // call delegate method // TODO: dispatch delegate methods with dispatch queue of delegate
                                observer.delegate?.controlConnection(self, didReceiveTaskResponse: str, withTaskId: taskId, tag: _tag)
                            }
                        }
                    }
                }
            }
            //}
        }
    }
    
    func socket(_ sock: GCDAsyncSocket, didReadPartialDataOfLength partialLength: UInt, tag: Int) {
        logger.verbose("didReadPartialDataOfLength \(partialLength), tag: \(tag)")
    }
    
    func socket(_ sock: GCDAsyncSocket, didWriteDataWithTag tag: Int) {
        logger.verbose("didWriteDataWithTag \(tag)")
    }
    
    func socket(_ sock: GCDAsyncSocket, didWritePartialDataOfLength partialLength: UInt, tag: Int) {
        logger.verbose("didWritePartialDataOfLength \(partialLength), tag: \(tag)")
    }
   
    func socket(_ sock: GCDAsyncSocket, shouldTimeoutWriteWithTag tag: Int, elapsed: TimeInterval, bytesDone length: UInt) -> TimeInterval {
        logger.verbose("shouldTimeoutReadWithTag \(tag), elapsed: \(elapsed), bytesDone: \(length)")
        
        //if (tag < TAG_TASK_COMMAND) {
        if (isTaskCommandTag(taskCommandTag: tag)) {
            //let taskId = UInt(-tag + TAG_TASK_COMMAND)
            if let (taskId, _tag) = parseTaskCommandTag(taskCommandTag: tag) {
                
                logger.verbose("TASK ID: \(taskId)")
                
                if let observers = taskDelegateDictionary[taskId] {
                    for observer in observers {
                        logger.verbose("TASK DELEGATE: \(String(describing: observer.delegate))")
                        
                        // call delegate method // TODO: dispatch delegate methods with dispatch queue of delegate
                        observer.delegate?.controlConnection(self, didReceiveTimeout: elapsed, withTaskId: taskId, tag: _tag)
                    }
                }
            }
        }
        
        //return -1 // always let this timeout
        return 10000
    }
    
    func socket(_ sock: GCDAsyncSocket, shouldTimeoutReadWithTag tag: Int, elapsed: TimeInterval, bytesDone length: UInt) -> TimeInterval {
        logger.verbose("shouldTimeoutReadWithTag \(tag), elapsed: \(elapsed), bytesDone: \(length)")
        
        //if (tag < TAG_TASK_COMMAND) {
        if (isTaskCommandTag(taskCommandTag: tag)) {
            //let taskId = UInt(-tag + TAG_TASK_COMMAND)
            if let (taskId, _tag) = parseTaskCommandTag(taskCommandTag: tag) {
                
                logger.verbose("TASK ID: \(taskId)")
                
                if let observers = taskDelegateDictionary[taskId] {
                    for observer in observers {
                        logger.verbose("TASK DELEGATE: \(String(describing: observer.delegate))")
                        
                        // call delegate method // TODO: dispatch delegate methods with dispatch queue of delegate
                        observer.delegate?.controlConnection(self, didReceiveTimeout: elapsed, withTaskId: taskId, tag: _tag)
                        logger.debug("!!! AFTER DID_RECEIVE_TIMEOUT !!!!")
                    }
                }
            }
        }
        
        //return -1 // always let this timeout
        return 10000
    }
    
    func socketDidDisconnect(_ sock: GCDAsyncSocket, withError err: Error?) {
        connected = false
        self.delegate?.controlConnectionFailed(self)
        
        if (err == nil) {
            logger.debug("QOS CC: socket closed by server after sending QUIT")
            return // if the server closed the connection error is nil (this happens after sending QUIT to the server)
        }
        
        logger.debug("QOS CC: disconnected with error \(String(describing: err))")
    }
    
}
