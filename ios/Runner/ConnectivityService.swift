/*****************************************************************************************************
 * Copyright 2014-2016 SPECURE GmbH
 *
 * Redistribution and use of the RMBT code or any derivative works are permitted provided that the following conditions are met:
 *
 *   - Redistributions may not be sold, nor may they be used in a commercial product or activity.
 *   - Redistributions that are modified from the original source must include the complete source code, including the source code for all components
 *     used by a binary built from the modified sources. However, as a special exception, the source code distributed need not include anything that is
 *     normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which
 *     the executable runs, unless that component itself accompanies the executable.
 *   - Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************************************/

//
//  ConnectivityService.swift
//  RMBT
//
//  Created by Benjamin Pucher on 02.02.15.
//  Copyright © 2015 SPECURE GmbH. All rights reserved.
//

import Foundation
import CocoaAsyncSocket
import AFNetworking

enum IpStatus{
    case STATUS_NOT_AVAILABLE, NO_ADDRESS, ONLY_LOCAL, CONNECTED_NAT, CONNECTED_NO_NAT
}

///
struct IPInfo: CustomStringConvertible {
    var status: IpStatus = .STATUS_NOT_AVAILABLE
    
    ///
    var connectionAvailable = false
    
    ///
    var nat: Bool {
        return internalIp != externalIp
    }
    
    ///
    var internalIp: String? = nil
    
    ///
    var externalIp: String? = nil
    
    ///
    var description: String {
        return "IPInfo: connectionAvailable: \(connectionAvailable), nat: \(nat), internalIp: \(String(describing: internalIp)), externalIp: \(String(describing: externalIp))"
    }
    
    var ipStatusOriginal: Int{
        switch status {
        case .NO_ADDRESS:
            return 1
        case .ONLY_LOCAL:
            return 2
        case .CONNECTED_NAT:
            return 3
        case .CONNECTED_NO_NAT:
            return 4
        default:
            return 0
        }
    }
}

///
struct ConnectivityInfo: CustomStringConvertible {
    
    ///
    var ipv4 = IPInfo()
    
    ///
    var ipv6 = IPInfo()
    
    ///
    var description: String {
        return "ConnectivityInfo: ipv4: \(ipv4), ipv6: \(ipv6)"
    }
}

///
class ConnectivityService: NSObject {
    
    typealias ConnectivityInfoCallback = (_ connectivityInfo: ConnectivityInfo) -> ()
    
    //
    
    ///
    let manager: AFHTTPSessionManager
    
    ///
    var callback: ConnectivityInfoCallback?
    
    ///
    var connectivityInfo: ConnectivityInfo!
    
    ///
    var ipv4Finished = false
    
    ///
    var ipv6Finished = false
    

    ///
    override init() {
        manager = AFHTTPSessionManager(baseURL: NSURL(string: ControlServer.sharedControlServer.baseURLString())! as URL)
        
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.responseSerializer = AFJSONResponseSerializer()
        
        manager.requestSerializer.timeoutInterval = 5 // 5 sec
        manager.requestSerializer.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
    }
    
    ///
    func checkConnectivity(callback: @escaping ConnectivityInfoCallback) {
        if (self.callback != nil) { // don't allow multiple concurrent executions
            return
        }
        
        self.callback = callback
        self.connectivityInfo = ConnectivityInfo()
        
        getLocalIpAddresses()
        getLocalIpAddressesFromSocket()
        
        ipv4Finished = false
        ipv6Finished = false
        
        checkIPV4()
        checkIPV6()
    }
    
    ///
    private func checkIPV4() {
        let userDefaults = UserDefaults.standard;

        let ipv4Url = userDefaults.string(forKey: "url_ipv4_check")
        if(ipv4Url == nil){
            return
        }
    
        var infoParams = ControlServer.sharedControlServer.systemInfoParams()
        infoParams["uuid"] = userDefaults.string(forKey: "uuid") as AnyObject
        manager.post(ipv4Url!, parameters: infoParams , progress: nil, success:  {(task: URLSessionDataTask?, responseData : Any) -> Void in
            
            logger.debug("\(responseData)")
            let response = (task?.response as! HTTPURLResponse)
            let responseObject = responseData as AnyObject?
            
            if (response.statusCode == 200) {
                
                let ip = responseObject!["ip"]
                let v = responseObject!["v"]
                
                logger.debug("IP: \(String(describing: ip)), version: \(String(describing: v))")
                
                self.connectivityInfo.ipv4.connectionAvailable = true
                self.connectivityInfo.ipv4.externalIp = (ip as? String)
            } else {
                // TODO: ?
            }
            
            self.ipv4Finished = true
            self.callCallback()
            
        }, failure: {(task:URLSessionDataTask?, error : Error) -> Void in
            logger.debug("ipv4 request ERROR")
            
            self.connectivityInfo.ipv4.connectionAvailable = false
            
            self.ipv4Finished = true
            self.callCallback()
        })
        
    }
    
    ///
    private func checkIPV6() {
        let userDefaults = UserDefaults.standard;
        
        let ipv6Url = userDefaults.string(forKey: "url_ipv6_check")
        if(ipv6Url == nil){
            return
        }
        
        var infoParams = ControlServer.sharedControlServer.systemInfoParams()
        infoParams["uuid"] = userDefaults.string(forKey: "uuid") as AnyObject
        
        manager.post(ipv6Url!, parameters: infoParams, progress: nil, success: {(task: URLSessionDataTask?, responseData : Any) -> Void in
            
            let response = (task?.response as! HTTPURLResponse)
            let responseObject = responseData as AnyObject?
            
            if (response.statusCode == 200) {
                let ip = responseObject!["ip"] as! String
                let v = responseObject!["v"] as! String
                
                if (ip.isValidIPv6()) {
                    logger.debug("IPv6: \(ip), version: \(v)")
                    
                    self.connectivityInfo.ipv6.connectionAvailable = true
                    self.connectivityInfo.ipv6.externalIp = ip
                } else {
                    logger.debug("IPv6: \(ip), version: \(v), NOT VALID")
                }
                
                
            } else {
                // TODO: ?
            }
            
            self.ipv6Finished = true
            self.callCallback()
            
        }, failure: {(task:URLSessionDataTask?, error : Error) -> Void in
        
            logger.debug("ipv6 request ERROR")
            
            self.connectivityInfo.ipv6.connectionAvailable = false
            
            self.ipv6Finished = true
            self.callCallback()
            
        })
    }
    
    ///
    private func callCallback() {
        objc_sync_enter(self)
        
        if (!(ipv4Finished && ipv6Finished)) {
            objc_sync_exit(self)
            return
        }
        
        objc_sync_exit(self)
        
        let _callback = callback
        callback = nil
        
        _callback?(connectivityInfo)
    }
    
}

// MARK: IP addresses

///
extension ConnectivityService {
    
    ///
    private func getLocalIpAddresses() { // see: http://stackoverflow.com/questions/25626117/how-to-get-ip-address-in-swift
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            
            // For each interface ...
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }
                
                let flags = Int32(bitPattern: (ptr?.pointee.ifa_flags)!)
                var addr = ptr?.pointee.ifa_addr.pointee
                
                // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
                if ((flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING)) {
                    if (addr?.sa_family == UInt8(AF_INET) || addr?.sa_family == UInt8(AF_INET6)) {
                        
                        // Convert interface address to a human readable string:
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        if (getnameinfo(&addr!, socklen_t(MemoryLayout.size(ofValue: addr?.sa_len) ), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                            hostname.withUnsafeBufferPointer{ pointer -> Void in
                                let address = String(cString: pointer.baseAddress!)
                                if (addr?.sa_family == UInt8(AF_INET)) {
                                    if (self.connectivityInfo.ipv4.internalIp == nil) {
                                        self.connectivityInfo.ipv4.internalIp = address
                                        logger.debug("local ipv4 address from getifaddrs: \(address)")
                                    }
                                } else if (addr?.sa_family == UInt8(AF_INET6)) {
                                    if (self.connectivityInfo.ipv6.internalIp == nil) {
                                        self.connectivityInfo.ipv6.internalIp = address
                                        logger.debug("local ipv6 address from getifaddrs: \(address)")
                                    }
                                }
                            }
                        }
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
    }
    
    ///
    private func getLocalIpAddressesFromSocket() {
        let udpSocket = GCDAsyncUdpSocket(delegate: self, delegateQueue: DispatchQueue.global())
        let url = RMBTSettings.sharedSettings().controlHostDev!
        let host = NSURL(string: url)?.host ?? "specure.com"
        
        // connect to any host
        do {
            try udpSocket.connect(toHost: host, onPort: 11111) // TODO: which host, which port? // try!
        } catch {
            getLocalIpAddresses() // fallback
        }
    }
}

// MARK: GCDAsyncUdpSocketDelegate

///
extension ConnectivityService: GCDAsyncUdpSocketDelegate {
    
    func udpSocket(_ sock: GCDAsyncUdpSocket, didConnectToAddress address: Data) {
        connectivityInfo.ipv4.internalIp = sock.localHost_IPv4()
        connectivityInfo.ipv6.internalIp = sock.localHost_IPv6()
        
        logger.debug("local ipv4 address from socket: \(String(describing: connectivityInfo.ipv4.internalIp))")
        logger.debug("local ipv6 address from socket: \(String(describing: connectivityInfo.ipv6.internalIp))")
        
        sock.close()
    }
    
    func udpSocket(_ sock: GCDAsyncUdpSocket, didNotConnect error: Error?) {
        logger.debug("didNotConnect: \(String(describing: error))")
        
        getLocalIpAddresses() // fallback
    }
    
    func udpSocketDidClose(_ sock: GCDAsyncUdpSocket, withError error: Error?) {
        logger.debug("udpSocketDidClose: \(String(describing: error))")
        
        getLocalIpAddresses() // fallback
    }
    
}
