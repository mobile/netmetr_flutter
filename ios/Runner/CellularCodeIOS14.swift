//
//  CellularCodeIOS14.swift
//  Runner
//
//  Created by tablexia on 02/12/2020.
//  Copyright © 2020 The Chromium Authors. All rights reserved.
//

import Foundation
import CoreTelephony


@available(iOS 14.1, *)
public let cellularCodeTableIOS14 = [
        CTRadioAccessTechnologyGPRS:         1,
        CTRadioAccessTechnologyEdge:         2,
        CTRadioAccessTechnologyWCDMA:        3,
        CTRadioAccessTechnologyCDMA1x:       4,
        CTRadioAccessTechnologyCDMAEVDORev0: 5,
        CTRadioAccessTechnologyCDMAEVDORevA: 6,
        CTRadioAccessTechnologyHSDPA:        8,
        CTRadioAccessTechnologyHSUPA:        9,
        CTRadioAccessTechnologyCDMAEVDORevB: 12,
        CTRadioAccessTechnologyLTE:          13,
        CTRadioAccessTechnologyeHRPD:        14,
        CTRadioAccessTechnologyNR:           20,
        CTRadioAccessTechnologyNRNSA:        21
    ]

@available(iOS 14.1, *)
public let cellularCodeDescriptionTableIOS14 = [
    CTRadioAccessTechnologyGPRS:            "GPRS (2G)",
    CTRadioAccessTechnologyEdge:            "EDGE (2G)",
    CTRadioAccessTechnologyWCDMA:           "UMTS (3G)",
    CTRadioAccessTechnologyCDMA1x:          "CDMA (2G)",
    CTRadioAccessTechnologyCDMAEVDORev0:    "EVDO0 (2G)",
    CTRadioAccessTechnologyCDMAEVDORevA:    "EVDOA (2G)",
    CTRadioAccessTechnologyHSDPA:           "HSDPA (3G)",
    CTRadioAccessTechnologyHSUPA:           "HSUPA (3G)",
    CTRadioAccessTechnologyCDMAEVDORevB:    "EVDOB (2G)",
    CTRadioAccessTechnologyLTE:             "LTE (4G)",
    CTRadioAccessTechnologyeHRPD:           "HRPD (2G)",
    CTRadioAccessTechnologyNR:              "NR (5G)",
    CTRadioAccessTechnologyNRNSA:           "NRNSA (5G)"
]
