//
//  JitterTestStatus.swift
//  Runner
//
//  Created by tablexia on 05/11/2019.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

import Foundation

enum JitterTestStatus : Int{
    case NOT_STARTED = 0
    case RUNNING = 1
    case FINISH = 2
    case FAIL = 3
}
