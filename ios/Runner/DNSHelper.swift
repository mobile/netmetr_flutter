//
//  DNSFounder.swift
//  Runner
//
//  Created by tablexia on 22/05/2019.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

import Foundation

open class DNSHelper {
    
    fileprivate var state = __res_9_state()
    
    public init() {
        res_9_ninit(&state)
    }
    
    deinit {
        res_9_ndestroy(&state)
    }
    
    public final func getDNSDisctonary() -> [String : Any]? {
        var address : String?;
        var port : UInt16?;
        
        for s in getservers(){
            let host = getnameinfo(s)
            if(host == "0.0.0.0"){
                continue
            }
            
            address = host
            port = CFSwapInt16(s.sin.sin_port)
            print("found dns server host: \(String(describing: address)) port: \(String(describing: port))")
            break
        }
        
        if(address != nil){
            return [
                "host" : address as Any,
                "port" : port as Any
            ]
        }else{
            return nil;
        }
    }
    
    private final func getservers() -> [res_9_sockaddr_union] {
        
        let maxServers = 10
        var servers = [res_9_sockaddr_union](repeating: res_9_sockaddr_union(), count: maxServers)
        let found = Int(res_9_getservers(&state, &servers, Int32(maxServers)))
        
        // filter is to remove the erroneous empty entry when there's no real servers
        return Array(servers[0 ..< found]).filter() { $0.sin.sin_len > 0 }
    }
    
    private func getnameinfo(_ s: res_9_sockaddr_union) -> String {
        var s = s
        var hostBuffer = [CChar](repeating: 0, count: Int(NI_MAXHOST))
        
        let sinlen = socklen_t(s.sin.sin_len)
        let _ = withUnsafePointer(to: &s) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                Darwin.getnameinfo($0, sinlen, &hostBuffer, socklen_t(hostBuffer.count), nil, 0, NI_NUMERICHOST)
            }
        }
        
        return String(cString: hostBuffer)
    }
}
