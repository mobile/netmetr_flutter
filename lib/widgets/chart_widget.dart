/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'dart:ui' as sky;
import 'dart:math' as math;
import 'package:netmetr_flutter/util/math_helper.dart' as mathHelper;


class ChartWidget extends StatefulWidget {
  final Map<int, List<dynamic>> chartsData;
  final Map<int, List<dynamic>> rowValues;
  final Map<int, List<PartInterval>> partInterval;
  final Map<int, Color> chartsColor;
  final List<double> ratioData;

  final Map<int, PartInterval>? backgroundParts;
  final Map<int, Color>? backgroundPartsColor;

  final bool useBackgroundFillParts;
  final bool showBackground;

  final double width;
  final double height;

  final double minValue;
  final double maxValue;

  final int columns;
  final int rows;

  final Color backgroundColor;

  ChartWidget({
    required this.chartsData,
    required this.rowValues,
    required this.chartsColor,
    required this.ratioData,
    required this.partInterval,
    this.width = 400,
    this.height = 200,
    this.minValue = 0,
    this.maxValue = 100,
    this.columns = 8,
    this.rows = 4,
    this.backgroundColor = Colors.blueGrey,
    this.useBackgroundFillParts = false,
    this.showBackground = true,
    this.backgroundParts,
    this.backgroundPartsColor
  });

  @override
  _ChartWidgetState createState() => _ChartWidgetState();
}

class _ChartWidgetState extends State<ChartWidget> {
  late double fontSize;


  @override
  void initState() {
    super.initState();
    fontSize = widget.width * 0.025;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width,
      height: widget.height,
      child: new CustomPaint(
         painter: new ChartPainter(
           chartsData: widget.chartsData,
           partInterval: widget.partInterval,
           ratioData: widget.ratioData,
           minValue: widget.minValue,
           maxValue: widget.maxValue,
           rowValues: widget.rowValues,
           colorData: widget.chartsColor,
           rows: widget.rows,
           columns: widget.columns,
           background: widget.backgroundColor,
           fontSize: fontSize,
           backgroundParts: widget.backgroundParts,
           useBackgroundFillParts: widget.useBackgroundFillParts,
           backgroundPartsColor: widget.backgroundPartsColor,
           showBackground: widget.showBackground
         ),
       )
    );
  }

}


class ChartPainter extends CustomPainter{
  late Paint _backgroundPaint;
  late List<Paint> _linePaints;
  late List<Paint> _chartBackPaints;
  late List<Paint> _backgroundPartsPaints;

  double minValue;
  double maxValue;
  Map<int, List<dynamic>> chartsData;
  Map<int, List<dynamic>> rowValues;
  Map<int, List<PartInterval>> partInterval;
  List<double> ratioData;
  Map<int, Color> colorData;
  int rows;
  int columns;
  double fontSize;

  Map<int, PartInterval>? backgroundParts;
  Map<int, Color>? backgroundPartsColor;

  bool useBackgroundFillParts;
  bool showBackground;

  Color background;

  ChartPainter({
    required this.chartsData,
    required this.partInterval,
    required this.ratioData,
    required this.minValue,
    required this.maxValue,
    required this.rowValues,
    required this.colorData,
    required this.rows,
    required this.columns,
    required this.background,
    this.fontSize = 10.0,
    required this.backgroundParts,
    required this.backgroundPartsColor,
    this.useBackgroundFillParts = false,
    this.showBackground = true
  }){
    _backgroundPaint = new Paint()
        ..color = background;

    _linePaints = [];
    _chartBackPaints = [];
    _backgroundPartsPaints = [];

    if(useBackgroundFillParts){
      for(Color color in backgroundPartsColor!.values){
        _backgroundPartsPaints.add(new Paint()
          ..color = Color.fromARGB(125, color.red, color.green, color.blue)
        );
      }
    }

    for(Color color in colorData.values){
      _linePaints.add(new Paint()
        ..strokeWidth = 2.5
        ..color = color
      );

      _chartBackPaints.add(new Paint()
        ..color = Color.fromARGB(125, color.red, color.green, color.blue)
      );
    }
  }

  @override
  void paint(Canvas canvas, Size size) {
    if(size.isEmpty) return;

    double width = size.width;
    double height = size.height;

    double leftLinePadding = (width / columns) / 2;
    double leftPadding = leftLinePadding + fontSize ;

    //Back
    if(showBackground) {
      _drawBackground(canvas, height, width, leftLinePadding, leftPadding);
    }

    if(useBackgroundFillParts){
      backgroundParts!.forEach((int index, PartInterval part){
        _drawBackgroundPart(canvas, part, _backgroundPartsPaints[index], height, width);
      });
    }

    chartsData.forEach((int index, List<dynamic> data){
      dynamic maxValue;

      for(dynamic value in data){
         if(value == null)
          continue;

        if(maxValue == null || maxValue < value)
          maxValue = value;
      }

      _drawData(canvas, data, partInterval[index], ratioData, _linePaints[index], _chartBackPaints[index], height, width, leftLinePadding, leftPadding, maxValue);
    });
  }

  _drawBackgroundPart(Canvas canvas, PartInterval part, Paint paint, double height, double width){

    double startX = width * part.min;
    double finishX = width * (part.max < 1.0 ? part.max : 1.0);

    Path path = new Path();
    path.moveTo(startX, 0);
    path.lineTo(startX, height);
    path.lineTo(finishX, height);
    path.lineTo(finishX, 0);

    canvas.drawPath(path, paint);

  }

  _drawData(Canvas canvas, List<dynamic> chartData, List<PartInterval>? partInterval, List<double> ratioData, Paint linePaint, Paint backPaint, double height, double width, double leftLinePadding, double leftPadding, dynamic maxValue){
    if(partInterval == null || chartData.isEmpty || partInterval.isEmpty )
      return;

    List<Offset> linePoints = [];
    Path back = new Path();

    double partHeight = height / partInterval.length;

    double startX = leftLinePadding + leftPadding;
    double x = startX;
    double y = 0.0;

    back.moveTo(startX, height);

    for(int i = 0; i < chartData.length; i++){
      var data = chartData[i];
      y = height - _calculateY(data, partHeight, height, partInterval, maxValue);

      linePoints.add(new Offset(x, y));
      back.lineTo(x, y);

      if(i < chartData.length - 1) {
        x = startX + ((width - leftLinePadding - leftPadding) * (ratioData[i + 1]));
      }else{
        back.lineTo(x, height);
      }
    }

    canvas.drawPath(back, backPaint);
    canvas.drawPoints(sky.PointMode.polygon, linePoints, linePaint);
    canvas.drawCircle(sky.Offset(x, y), 3.0, linePaint);
  }

  double _calculateY(dynamic data, double partHeight, double chartHeight, List<PartInterval> partIntervals, dynamic maxValue){
    if(data == null || data == 0.0) return 0.0;

    PartInterval? partInterval;
    int index = 0;
    for(int i = 0; i < partIntervals.length; i++){
      var interval = partIntervals[i];

      if(data >= interval.min && data <= interval.max){
        partInterval = interval;
        index = i;
        break;
      }
    }

    if(partInterval == null || partInterval.min == null || partInterval.max == null) {
      int digits = mathHelper.roundToInt(maxValue + 0.0, 0).toString().length;
      num tmp = math.pow(10, digits -1);
      double rounded = ((maxValue + tmp) / tmp );
      dynamic totalMax = rounded.floor() * tmp;

      dynamic lastIntervalMax = partIntervals[partIntervals.length - 1].max;


      double percent = (data - lastIntervalMax) / (totalMax - lastIntervalMax);

      return partHeight * partIntervals.length + (partHeight / 2) * percent;
    }

    double percentInPart = (data - partInterval.min) / (partInterval.max - partInterval.min);

    return (partHeight * (index)) + partHeight * percentInPart;
  }


  _drawBackground(Canvas canvas, double height, double width, double leftLinePadding, double leftPadding){

    double columnSpace = (width - leftLinePadding - leftPadding) / columns;
    double rowSpace = height / rows;
    //columns
    double dx = leftLinePadding + leftPadding;
    for(int i = 0; i <= columns; i++){
      canvas.drawLine(new Offset(dx, - rowSpace /2), new Offset(dx, height), _backgroundPaint);
      dx += columnSpace;
    }

    //rows
    double dy = 0;
    for(int i = 0; i <= rows ; i++){
      canvas.drawLine(new Offset((i == 0 || i == rows) ? leftPadding : leftLinePadding + leftPadding, dy), new Offset(width, dy), _backgroundPaint);
      dy += rowSpace;
    }

    //data
    rowValues.forEach((int index, List<dynamic> data){

      Color color = colorData[index]!;

      double dy = height - (index + 1) * fontSize / 2;
      double dx = 0.0 + index * (fontSize*3);
      for(dynamic number in data){

        if(number != null) {
          _drawNumber(canvas, number, color, fontSize, new Offset(dx, dy));
        }

        dy -= rowSpace;
      }

    });
  }

  _drawNumber(Canvas canvas, dynamic number, Color textColor, double fontSize, Offset offset){
    TextSpan textSpan = new TextSpan(
      style: new TextStyle(color: textColor, fontSize: fontSize),
      text: number.toString()
    );

    TextPainter tp = new TextPainter(text: textSpan, textAlign: TextAlign.right, textDirection: TextDirection.ltr);
    tp.layout();
    tp.paint(canvas, offset);
  }

  int _dataLength(){
    int length = 0;

    chartsData.forEach((_, List<dynamic> data){
      length += data.length;
    });

    return length;
  }

  @override
  bool shouldRepaint(ChartPainter oldDelegate) {
    return oldDelegate._dataLength() != _dataLength();
  }

}

class PartInterval{
  dynamic max;
  dynamic min;

  PartInterval(this.min, this.max);
}