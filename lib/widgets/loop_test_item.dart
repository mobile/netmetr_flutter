/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:netmetr_flutter/net/response/test_result_qos_reponse.dart';
import 'package:netmetr_flutter/pages/run_test_page.dart';
import 'package:netmetr_flutter/test/qos_response.dart';
import 'package:netmetr_flutter/test/test_response.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:netmetr_flutter/util/network/network_data.dart';
import 'package:netmetr_flutter/util/network/network_connection_type.dart';
import 'package:netmetr_flutter/util/network/network_type.dart';
import 'package:netmetr_flutter/util/widget_helper.dart' as widgetHelper;
import 'package:netmetr_flutter/util/math_helper.dart' as mathHelper;

const SERVICE_RUNNING = "running";
const SERVICE_FINISH = "finish";
const UPDATE_PROGRESS = "update_progress";
const TEST_FAILED = "failed";

class LoopTestItem extends StatefulWidget {
  final int? index;
  final bool onlySignal;
  final NetworkConnectionType networkType;
  final Function(int?, bool) finished;

  final bool runJitter;
  final bool runQoS;

  LoopTestItem({
    Key? key,
    required this.index,
    required this.onlySignal,
    required this.networkType,
    required this.finished,
    this.runJitter = true,
    this.runQoS = true
  }) : super(key: key);

  @override
  _LoopTestItemState createState() => _LoopTestItemState();
}

class _LoopTestItemState extends State<LoopTestItem> {

  TestNetworkData? testNetworkData;
  NetworkType? networkType;
  TaskStatus taskStatus = TaskStatus.WAIT;

  double? pingNano;
  double? downBitPerSec;
  double? upBitPerSec;
  double downProgress = 0.0;
  double upProgress = 0.0;
  int? signal;
  double relativeSignal = -1;
  SignalType signalType = SignalType.NO_SIGNAL;

  //jitter
  JitterStatus jitterStatus = JitterStatus.NOT_STARTED;
  int? jitter;
  double? packetLoss;

  //test final stat
  bool finished = false;
  bool failed = false;


  //qos
  Map<QosResultType, QosResponse> qosResponses = {};
  double qosProgress = 0.0;
  int qosSuccess = 0;
  int qosFailed = 0;
  int qosTotal = 0;
  int qosFinished = 0;

  @override
  void initState() {
    super.initState();


    _initReceiveChannel();
  }

  _initReceiveChannel(){
    new EventChannel('nic.cz.netmetrflutter/runTestReceiver${widget.index}').receiveBroadcastStream(1).listen((value){
      print(value.toString());

      if(value != null){
        Map<String, dynamic> jsonData = json.decode(value);
        String status = jsonData['serviceStatus'];

        switch(status){
          case SERVICE_RUNNING:

            if(mounted){
              setState(() {

                if(testNetworkData == null && jsonData.containsKey('networkData')){
                  testNetworkData = TestNetworkData.fromJson(jsonData['networkData']);
                  networkType = NetworkType.getType(testNetworkData!.networkType);
                }

                if(jsonData.containsKey('data')) {
                  TestResponse testResponse = TestResponse.fromJson(jsonData['data']);

                  relativeSignal = testResponse.relativeSignal;
                  signal = testResponse.signal;
                  signalType = testResponse.signalType;
                  pingNano = 0.0 + testResponse.pingNano!;
                  downBitPerSec = 0.0 + testResponse.downBitPerSec!;
                  upBitPerSec = 0.0 + testResponse.upBitPerSec!;

                  jitterStatus = testResponse.jitterStatus;

                  if(jitterStatus == JitterStatus.FINISH) {
                    JitterData? up = testResponse.up;
                    JitterData? down = testResponse.down;

                    jitter = _jitter(down, up);
                    packetLoss = _packetLoss(down, up);
                  }

                }

                if(jsonData.containsKey('qosTest')) {
                  Map<String, dynamic> qosData = jsonData['qosTest'];

                  downProgress = 1.0;
                  upProgress = 1.0;

                  for (var data in qosData.values) {
                    QosResponse response = QosResponse.fromJson(data);
                    qosResponses.update(response.type!, (value) {
                      value.progress = response.progress;
                      value.target = response.target;
                      value.value = response.value;
                      value.firstTest = response.firstTest;
                      value.lastTest = response.lastTest;

                      return value;
                    }, ifAbsent: () => response);
                  }

                  if(jsonData.containsKey("qosCounter")){
                    Map<String, dynamic> qosCounter = jsonData["qosCounter"];
                    qosSuccess = qosCounter["success"];
                    qosFailed = qosCounter["failed"];
                    qosTotal = qosCounter["testsCount"];
                    qosFinished = qosSuccess + qosFailed;
                  }

                }

              });
            }

            break;
          case UPDATE_PROGRESS:
            Map<String, dynamic> data = jsonData['data'];

            if(data.containsKey("progress")){
              double speedProgress = 0.0 + data['progress'];
              if(taskStatus == TaskStatus.DOWN && downProgress != 1.0) downProgress = speedProgress != 0.0 ? speedProgress : downProgress;
              else if(taskStatus == TaskStatus.UP && upProgress != 1.0) upProgress = speedProgress != 0.0 ? speedProgress : upProgress;
            }

            if(data.containsKey('qosProgress') && qosProgress != 1.0){
              qosProgress = 0.0 + data['qosProgress'];
            }

            if(mounted){
              setState(() {
                taskStatus = TaskStatus.values[data["status"]];
              });
            }

            break;
          case SERVICE_FINISH:

            if(!jsonData.containsKey('test_uuid')){
             _testFailed();
            }else{
              _testFinished();
            }

            break;
          case TEST_FAILED:

            break;
        }
      }
    });
  }

  double _packetLoss(JitterData? down, JitterData? up){
    return 100.0 - ((down!.numPacket + up!.numPacket) / (down.totalPacket + up.totalPacket)) * 100.0;
  }

  int? _jitter(JitterData? down, JitterData? up){
    if(down!.meanJitter == null && up!.meanJitter == null) return null;
    else if(down.meanJitter == null) return up!.meanJitter;
    else if(up!.meanJitter == null) return down.meanJitter;

    return ((down.meanJitter! + up.meanJitter!) / 2).round();

  }

  _testFinished(){
    if(mounted) {
      setState(() {
        finished = true;
        widget.finished(widget.index!, true);
      });
    }
  }

  _testFailed(){
    signal = null;
    pingNano = null;
    downBitPerSec = null;
    upBitPerSec = null;

    if(mounted) {
      setState(() {
        finished = true;
        failed = true;
        widget.finished(widget.index, false);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations localizations = AppLocalizations.of(context)!;
    MediaQueryData queryData = MediaQuery.of(context);
    ThemeData themeData = Theme.of(context);
    Color accentColor = themeData.colorScheme.secondary;

    TextStyle captionStyle = themeData.textTheme.overline!.copyWith(color: Colors.black);
    TextStyle captionBoldStyle = themeData.textTheme.overline!.copyWith(color: Colors.black, fontWeight: FontWeight.bold);
    TextStyle subtitleStyle = themeData.textTheme.subtitle2!.copyWith(color: Colors.black, fontWeight: FontWeight.bold);
    TextStyle subtitleAccentStyle = themeData.textTheme.subtitle2!.copyWith(color: accentColor);
    TextStyle titleStyle = themeData.textTheme.headline6!.copyWith(color: Colors.black);
    TextStyle titleStyleWhite = themeData.textTheme.headline6!.copyWith(color: Colors.white);

    double speedProgress = (downProgress + upProgress) * 100 / 2;
    int totalProgress =  widget.runQoS ? ((speedProgress + qosProgress) / 2).round() : speedProgress.round();

    return new Card(
      child: !widget.onlySignal ? new Container(
        width: queryData.size.width,
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children:  <Widget>[
            new Container(
              padding: const EdgeInsets.all(5.0),
              width: queryData.size.width,
              color: !finished ? Colors.black : failed ? Colors.red : Colors.green,
              child: new Row(
                children: <Widget>[
                  new Expanded(
                    child: new Container(
                      alignment: Alignment.centerLeft,
                      margin: const EdgeInsets.only(left: 10.0),
                      child: new Text("${widget.index! + 1}.", style: titleStyleWhite,),
                    )
                  ),
                  new Expanded(
                    child: !finished ? new Container(
                      alignment: Alignment.centerRight,
                      margin: const EdgeInsets.only(right: 10.0),
                      child: taskStatus.index > TaskStatus.PING.index ? new Text("$totalProgress %", style: titleStyleWhite,) : widgetHelper.getSmallProgress(color: Colors.white),
                    ) : new Container()
                  ),

                ],
              ),
            ),
            widget.runJitter ? new Row(
              children: <Widget>[
                new Flexible(
                    child: new Container(
                      alignment: Alignment.center,
                      margin: const EdgeInsets.all(5.0),
                      child: _singalData(captionStyle: captionStyle, captionBoldStyle: captionBoldStyle, localizations: localizations),
                    )
                ),
                new Flexible(
                  child: new Container(
                    alignment: Alignment.center,
                    margin: const EdgeInsets.all(5.0),
                    child: _jitterData(captionStyle: captionStyle, captionBoldStyle: captionBoldStyle, localizations: localizations)
                  )
                )
              ],
            ) : new Container(
              padding: const EdgeInsets.symmetric(vertical: 5.0),
              alignment: Alignment.center,
              child: _singalData(captionStyle: captionStyle, captionBoldStyle: captionBoldStyle, localizations: localizations),
            ),
            widgetHelper.divider(),
            new Row(
              children: <Widget>[
                new Expanded(
                  child: new Container(
                    padding: const EdgeInsets.symmetric(vertical: 5.0),
                    child: new Column(
                      children: <Widget>[
                        new Text("Download", style: subtitleStyle,),
                        new Padding(padding: const EdgeInsets.only(top: 2.0),
                          child: new Text(downBitPerSec != null ? "${mathHelper.roundDouble(downBitPerSec! / 1000000.0, 2).round()} Mbps" : "-", style: titleStyle,),
                        ),
                      ],
                    ),
                  )
                ),
                new Expanded(
                  child: new Container(
                    padding: const EdgeInsets.symmetric(vertical: 5.0),
                    child: new Column(
                      children: <Widget>[
                        new Text("Upload", style: subtitleStyle,),
                        new Padding(padding: const EdgeInsets.only(top: 2.0),
                          child: new Text(upBitPerSec != null ? "${mathHelper.roundDouble(upBitPerSec! / 1000000.0, 2).round()} Mbps" : "-", style: titleStyle,),
                        ),
                      ],
                    ),
                  )
                )
              ],
            ),
            widgetHelper.divider(),
            qosResponses.isEmpty ? new Container(
              padding: const EdgeInsets.only(top: 20.0) ,
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Container(
                      padding: const EdgeInsets.only(top: 10.0),
                      alignment: Alignment.center,
                      child: new Text(testNetworkData != null ? "${testNetworkData!.operatorName} ${networkType != null ? networkType!.name : "-"}" : "Informace o testu", style: captionStyle,)
                  ),
                  widgetHelper.divider(),
                  new Container(
                    margin: const EdgeInsets.all(5.0),
                    width: double.maxFinite,
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        new Row(
                            children: [
                              new Expanded(
                                flex: 2,
                                child: new Text("IP: ", textAlign: TextAlign.left, style: captionStyle,),
                              ),
                              new Expanded(
                                  flex: 8,
                                  child: new Text((testNetworkData != null && testNetworkData!.ip != null && testNetworkData!.ip!.isNotEmpty ? testNetworkData!.ip : " - ")!, textAlign: TextAlign.left, style: captionStyle,)
                              )
                            ]
                        ),
                        new Row(
                            children: [
                              new Expanded(
                                flex: 2,
                                child:  new Text("Server: ", textAlign: TextAlign.left, style: captionStyle,),
                              ),
                              new Expanded(
                                  flex: 8,
                                  child: new Text((testNetworkData != null && testNetworkData!.serverName != null && testNetworkData!.serverName!.isNotEmpty ? testNetworkData!.serverName : " - ")!, textAlign: TextAlign.left, style: captionStyle,)
                              )
                            ]
                        )
                      ],
                    ),
                  )
                ] ,
              ),

            ) : new Container(
              width: queryData.size.width,
              child: !(finished && failed) ?new Row(
                children: <Widget>[
                  new Expanded(
                    child: new Container(
                      alignment: Alignment.center,
                      child:  new Text("${qosProgress.round()} %", style: subtitleAccentStyle,),
                    )
                  ),
                  new Expanded(
                    child: new Container(
                      alignment: Alignment.center,
                      child: new Row(
                        children: <Widget>[
                          new Text("$qosSuccess", style: subtitleStyle,),
                          new Icon(Icons.check, color: Colors.green,)
                        ],
                      ),
                    )
                  ),
                  new Expanded(
                      child: new Container(
                        alignment: Alignment.center,
                        child: new Row(
                          children: <Widget>[
                            new Text("$qosFailed", style: subtitleStyle,),
                            new Icon(Icons.clear, color: Colors.red,)
                          ],
                        ),
                      )
                  ),
                  new Expanded(
                    child: new Container(
                      alignment: Alignment.center,
                      child: new Text("$qosFinished / $qosTotal", style: subtitleStyle,),
                    )
                  )
                ],
              ) : new Container(
                alignment: Alignment.center,
                child: new Text("-", style: captionBoldStyle,),
              ),
            )
          ],
        ),
      ) : new Container(
        padding: const EdgeInsets.all(5.0),
        width: queryData.size.width,
        child:  new Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
           new Flexible(
            flex: 1,
            child: new Container(
               margin: const EdgeInsets.only(top: 20.0, bottom: 20.0),
               alignment: Alignment.center,
               child: relativeSignal == -1 ? new Icon(Icons.signal_wifi_off, color: Colors.blueGrey, size: 40.0,) : widgetHelper.getConnectivitySignalImage(widget.networkType, relativeSignal, 7.0),
             )
           ),
           new Flexible(
             flex: 2,
             child: signalType != SignalType.NO_SIGNAL && signal != null ? new Column(
               crossAxisAlignment: CrossAxisAlignment.stretch,
               mainAxisAlignment: MainAxisAlignment.spaceBetween,
               mainAxisSize: MainAxisSize.max,
               children: <Widget>[
                 new Row(
                   children: [
                    new Text("${localizations.getLocalizedValue("connection")}:", textAlign: TextAlign.left, style: captionBoldStyle,),
                    new Container(
                      margin: const EdgeInsets.only(left: 5.0),
                      alignment: Alignment.centerLeft,
                      child: new Text(signalType != SignalType.NO_SIGNAL ? signalType == SignalType.WLAN ? "WLAN" : "MOBILE" : "-" , style: captionStyle, textAlign: TextAlign.left),
                    )
                   ]
                 ),
                 new Padding(padding: const EdgeInsets.only(top: 10.0)),
                 new Row(
                     children: [
                       new Text("${signalType == SignalType.WLAN ? "WLAN SSID" : localizations.getLocalizedValue("operator")}:", textAlign: TextAlign.left, style: captionBoldStyle,),
                       new Container(
                         margin: const EdgeInsets.only(left: 5.0),
                         alignment: Alignment.centerLeft,
                         child: new Text(testNetworkData != null ? "${testNetworkData!.operatorName} ${networkType != null ? networkType!.name : "-"}" : "-" , style: captionStyle, textAlign: TextAlign.left),
                       )
                     ]
                 ),
                 new Padding(padding: const EdgeInsets.only(top: 10.0)),
                 new Row(
                   children: <Widget>[
                     new Text("${localizations.getLocalizedValue('signal')}:", style: captionBoldStyle, ),
                     new Container(
                       margin: const EdgeInsets.only(left: 5.0),
                       alignment: Alignment.centerLeft,
                       child:  new Text("$signal dBm", style: captionStyle,),
                     )
                   ],
                 )
               ],
             ) : new Container(
               alignment: Alignment.center,
               child: new Text("-", style: captionBoldStyle,),
             )
           )
          ],
        ) ,
      ),
    );
  }

  Widget _singalData({required TextStyle captionStyle,required TextStyle captionBoldStyle, required AppLocalizations localizations}){
    return new Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        new Row(
          children: <Widget>[
            new Expanded(
                child: new Text("${localizations.getLocalizedValue('signal')!.toUpperCase()}:", style: captionBoldStyle,)
            ),
            new Expanded(
                child: new Text(signal != null ? "$signal dBm" : "-", style: captionStyle,)
            )
          ],
        ),
        new Padding(padding: const EdgeInsets.only(top: 5.0),
          child: new Row(
            children: <Widget>[
              new Expanded(
                  child: new Text("PING:", style: captionBoldStyle)
              ),
              new Expanded(
                  child: new Text(taskStatus.index > TaskStatus.PING.index && pingNano != null ? "${(pingNano! / 1000000.0).round()} ms" : " - ", style: captionStyle,)
              )
            ],
          ),
        )
      ],
    );
  }

  Widget _jitterData({required TextStyle captionStyle,required TextStyle captionBoldStyle, required AppLocalizations localizations}){
    return new Column(
      children: <Widget>[
        new Padding(padding: const EdgeInsets.only(top: 5.0),
          child: new Row(
            children: <Widget>[
              new Expanded(
                  child: new Text("JITTER:", style: captionBoldStyle)
              ),
              new Expanded(
                  child: new Text((jitter == null || jitter == -1) ? "-" : "${mathHelper.roundDouble(jitter! / 1000000, 2)} ms", style: captionStyle,)
              )
            ],
          ),
        ),
        new Padding(padding: const EdgeInsets.only(top: 5.0),
          child: new Row(
            children: <Widget>[
              new Expanded(
                  child: new Text("${localizations.getLocalizedValue('packet_loss')!.toUpperCase()}:", style: captionBoldStyle)
              ),
              new Expanded(
                  child: new Text(packetLoss == null || packetLoss == -1 ? " - " : "${mathHelper.roundDouble(packetLoss!, 1)} %", style: captionStyle,)
              )
            ],
          ),
        ),
      ],
    );
  }
}
