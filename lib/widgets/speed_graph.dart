/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/test/test_response.dart';
import 'dart:ui' as ui;
import 'package:netmetr_flutter/util/math_helper.dart' as mathHelper;
import 'package:netmetr_flutter/util/widget_helper.dart' as widgetHelper;
import 'package:netmetr_flutter/util/network/network_data.dart';


const GRAPH_RATIO_MOBILE = 0.79;
const GRAPH_RATIO_TABLET = 0.7;

const DEFAULT_LAYOUT_MOBILE = 320;
const DEFAULT_SCALE_MOBILE = 2.0;

const DEFAULT_LAYOUT_TABLET = 800;
const DEFAULT_SCALE_TABLET = 0.9;

class SpeedGraph extends StatefulWidget {
  final double currentProgress;
  final double qosProgress;
  final TaskStatus taskStatus;
  final double downBitPerSec;
  final double upBitPerSec;
  final int? signal;
  final SignalType signalType;
  final SpeedGraphType speedGraphType;
  final bool qosEnable;


  SpeedGraph({Key? key,
    required this.currentProgress,
    required this.qosProgress,
    required this.taskStatus,
    required this.downBitPerSec,
    required this.upBitPerSec,
    required this.signal,
    required this.signalType,
    this.speedGraphType = SpeedGraphType.TYPE_1,

    this.qosEnable = true
  }) : super(key: key);

  @override
  _SpeedGraphState createState() => _SpeedGraphState();
}

class _SpeedGraphState extends State<SpeedGraph> {

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    MediaQueryData queryData = MediaQuery.of(context);
    TextStyle progressStyle = themeData.textTheme.headline6!.copyWith(color: themeData.colorScheme.secondary);
    TextStyle speedStyle = themeData.textTheme.headline6!.copyWith(color: Colors.black);
    TextStyle signalStyle = themeData.textTheme.bodyText2!.copyWith(color: Colors.amber);

    bool runQos = !(widget.taskStatus.index < TaskStatus.QOS_TEST_RUNNING.index) && widget.qosEnable;
    NetworkGraphType networkGraphType = NetworkGraphType.getType(widget.signalType);

    //progress
    ProgressSegment progressSegment = ProgressSegment.getSegment(widget.taskStatus);
    double speedProgress = progressSegment.startPercent + ((progressSegment.finalPercent - progressSegment.startPercent) * widget.currentProgress);
    double testProgress = widget.qosEnable ? (speedProgress / 2) : speedProgress;

   //signal
    double signalPercent = 0.0;

    if(widget.signal != null && widget.signal != 0.0) {
      int tmpSignal = widget.signal! - networkGraphType.min;
      int tmpSignalMax = networkGraphType.max - networkGraphType.min;
      signalPercent = (tmpSignal / tmpSignalMax) * 100;
    }

    //speed

    double speedValue = 0.0;
    double speedPercent = 0.0;
    if(!runQos) {
      var speedGraphMax = widget.speedGraphType.values[SpeedSegment.S5.startIndexValue ];
      var actualTestValue = widget.taskStatus == TaskStatus.DOWN ? widget.downBitPerSec : widget.upBitPerSec;

      if(actualTestValue > 0.0) {
        speedValue = actualTestValue / 1000000.0;
      }

      if(speedValue <= speedGraphMax) {

        SpeedSegment speedSegment = SpeedSegment.getSegment(speedValue, widget.speedGraphType.values);
        double incValue = widget.speedGraphType.incValue;
        if(widget.speedGraphType == SpeedGraphType.TYPE_1) {
          if(speedValue >= 10.0) incValue = 10;
          else if(speedValue < 1 && speedValue >= 0.1) incValue = 0.1;
          else if(speedValue < 0.1) incValue = 0.01;
        }

        double s1MinValue = widget.speedGraphType.values[SpeedSegment.S1.startIndexValue];

        double minSegmentValue = widget.speedGraphType.values[speedSegment.startIndexValue];
        double maxSegmentValue = speedSegment != SpeedSegment.S5 ? widget.speedGraphType.values[speedSegment.startIndexValue + 1] :s1MinValue;

        int part =  ((speedValue - minSegmentValue) / incValue).floor();

        PartOfSegment partOfSegment = PartOfSegment.getPartOfSegment(part);

        var minPartValue = speedSegment != SpeedSegment.S5 ? minSegmentValue + incValue * part : maxSegmentValue;
        var maxPartValue = speedSegment != SpeedSegment.S5 ? minSegmentValue + incValue * (part + 1) : maxSegmentValue + 10;

        var tmpValue = speedValue - minPartValue;
        var tmpMax = maxPartValue - minPartValue;

        var percentInPart = (partOfSegment.finalPercent - partOfSegment.startPercent) * (tmpValue / tmpMax);
        var percentInSegment = ((partOfSegment.startPercent +  percentInPart) * (speedSegment.finalPercent - speedSegment.startPercent)) / 100;

        speedPercent = speedSegment.startPercent + percentInSegment;

      }else{
        speedPercent = 100.0;
      }

    }else{
      testProgress += widget.qosProgress / 2;
    }

    bool useMobileLayout = widgetHelper.useMobileLayout(queryData);
   
    var graphSize = queryData.size.width * (useMobileLayout ? GRAPH_RATIO_MOBILE : GRAPH_RATIO_TABLET);
    var graphScale = (useMobileLayout ? (DEFAULT_LAYOUT_MOBILE * DEFAULT_SCALE_MOBILE) : (DEFAULT_LAYOUT_TABLET * DEFAULT_SCALE_TABLET)) / queryData.size.width;

    return new Container(
      width: graphSize,
      height: graphSize,
      child: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Progress(
              alignment: Alignment.topRight,
              value:speedProgress,
              graphType: GraphType.PROGRESS,
              completeColor: themeData.colorScheme.secondary,
              centerLabel: new Text("${testProgress.floor()} %", style: progressStyle,),
              scale: graphScale
          ),
          new Progress(
              alignment: Alignment.bottomLeft,
              value: runQos ? widget.qosProgress : speedPercent,
              graphType: runQos ? GraphType.QOS : GraphType.SPEED,
              completeColor: Colors.black,
              speedGraphType: widget.speedGraphType,
              centerLabel: runQos ? new Text("QOS", style: speedStyle,) : new Text("${speedValue >= 1.0 ? speedValue.round() : mathHelper.roundDouble(speedValue, 2)} Mbit/s", style: speedStyle,),
              scale: graphScale
          ),
          widget.signal != null && widget.signal != 0.0 ? new Progress(
              alignment: Alignment.bottomRight,
              value: signalPercent,
              graphType: GraphType.SIGNAL,
              completeColor: Colors.amber,
              centerLabel: new Text("${widget.signal} dBm", style: signalStyle,),
              scale: graphScale,
              networkGraphType: networkGraphType,
          ) : new Container()
        ],
      ),
    );
  }
}

class Progress extends StatefulWidget {
  final double value;
  final GraphType graphType;
  final NetworkGraphType? networkGraphType;
  final SpeedGraphType? speedGraphType;

  final Color completeColor;
  final Widget centerLabel;
  final double scale;
  final Alignment alignment;

  Progress({
    Key? key,
    required this.value,
    required this.graphType,
    required this.completeColor,
    required this.centerLabel,
    required this.scale,
    required this.alignment,
    this.speedGraphType,
    this.networkGraphType
  }) : super(key : key);

  @override
  _ProgressState createState() => _ProgressState();
}

class _ProgressState extends State<Progress> {


  String _ringPath(){
    switch (widget.graphType){
      case GraphType.SIGNAL: return widget.networkGraphType == null ? widget.graphType.ring : widget.graphType.ring + widget.networkGraphType!.ring;
      case GraphType.SPEED:  return widget.speedGraphType == null ? widget.graphType.ring : widget.graphType.ring + widget.speedGraphType!.ring;
      default: return widget.graphType.ring;
    }
  }

  @override
  Widget build(BuildContext context) {

    var ring = _ringPath();

    return new Container(
      alignment: widget.alignment,
      child: new Stack(
        alignment: Alignment.center,
        children: <Widget>[
          widget.graphType == GraphType.SPEED || widget.graphType == GraphType.QOS ?  new Image.asset(widget.graphType.background, scale: widget.scale, color: Colors.black,) : new Image.asset(widget.graphType.background, scale: widget.scale,),
          new ClipPath(
            child: Image.asset(widget.graphType.complete, color: widget.completeColor, scale: widget.scale),
            clipper: ProgressClipper(widget.graphType,widget.value, widget.graphType.start, widget.graphType.total),
          ),
          new Image.asset(ring, color: Colors.black, scale: widget.scale),
          widget.centerLabel
        ],
      ),
    );
  }
}

class ProgressClipper extends CustomClipper<Path> {
  final GraphType graphType;
  final double progress;
  final startAngle;
  final totalAngle;

  ProgressClipper(
      this.graphType,
      this.progress,
      this.startAngle,
      this.totalAngle);

  @override
  ui.Path getClip(ui.Size size) {
    var path = new Path();

    double completeAngle = (progress * totalAngle) / 100;

    double centerX = size.width / 2 ;
    double centerY = size.height / 2;

    double radius = size.width / 2;

    double start = startAngle * mathHelper.radiansPerDegree;
    double complete = completeAngle * mathHelper.radiansPerDegree;

    if(graphType == GraphType.SIGNAL) {
      var oldCenter = centerX;
      centerX = size.width * 0.37;
      centerY = size.height * 0.39;
      radius += (centerX - oldCenter).abs();
    }


    if(graphType == GraphType.SIGNAL || graphType == GraphType.QOS){
      start = (startAngle - completeAngle) * mathHelper.radiansPerDegree;
    }

    //circular sector
    path.addArc(new Rect.fromCircle(center: new Offset(centerX, centerY), radius: radius), start, complete);
    path.lineTo(centerX, centerY);

    path.close();

    return path;
  }

  @override
  bool shouldReclip(ProgressClipper oldClipper) {
    return oldClipper.progress != progress;
  }

}

class ProgressSegment{
  final double startPercent;
  final double finalPercent;
  const ProgressSegment(this.startPercent, this.finalPercent);

  static const INIT = const ProgressSegment(0.0, 13.0);
  static const PING = const ProgressSegment(14.7, 24.9);
  static const DOWN = const ProgressSegment(26.2, 62);
  static const UP = const ProgressSegment(63.5, 100);

  static ProgressSegment getSegment(TaskStatus task){
    if(task.index > TaskStatus.UP.index) return UP;

    if(task.index == TaskStatus.PACKET_LOSS_AND_JITTER.index)
      return Platform.isAndroid ? INIT : PING;

    switch (task){
      case TaskStatus.UP: return UP;
      case TaskStatus.DOWN: return DOWN;
      case TaskStatus.PING: return PING;
      default: return INIT;
    }
  }
}

class PartOfSegment{
  final double startPercent;
  final double finalPercent;
  const PartOfSegment(this.startPercent, this.finalPercent);

  static const PART_I = const PartOfSegment(0.0, 29.0);
  static const PART_II = const PartOfSegment(29.0, 48.0);
  static const PART_III = const PartOfSegment(48.0, 59.0);
  static const PART_IV = const PartOfSegment(59.0, 69.0);
  static const PART_V = const PartOfSegment(69.0, 79.0);
  static const PART_VI = const PartOfSegment(79.0, 84.0);
  static const PART_VII = const PartOfSegment(84.0, 90.0);
  static const PART_VIII = const PartOfSegment(90.0, 96.0);
  static const PART_IX = const PartOfSegment(96.0, 100.0);


  static PartOfSegment getPartOfSegment(int part){
    List<PartOfSegment> values = [PART_I, PART_II, PART_III, PART_IV, PART_V, PART_VI, PART_VII, PART_VIII, PART_IX];

    if(part >= values.length)
      return PART_IX;

    return values[part];
  }
}

class SpeedSegment{
  final startIndexValue;

  final double startPercent;
  final double finalPercent;
  const SpeedSegment(this.startPercent, this.finalPercent, this.startIndexValue);

  static const S1 = const SpeedSegment(0.0, 23.0, 0);
  static const S2 = const SpeedSegment(23.0, 45.0, 1);
  static const S3 = const SpeedSegment(45.0, 67.5, 2);
  static const S4 = const SpeedSegment(67.5, 90.3, 3);
  static const S5 = const SpeedSegment(90.3, 100.0, 4);


  static SpeedSegment getSegment(double value, List<double> values){
    if(value < values[S1.startIndexValue + 1]) return S1;
    else if(value >= values[S1.startIndexValue + 1] && value < values[S2.startIndexValue + 1]) return S2;
    else if(value >= values[S2.startIndexValue + 1] && value < values[S3.startIndexValue + 1]) return S3;
    else if(value >= values[S3.startIndexValue + 1] && value < values[S4.startIndexValue + 1]) return S4;

    return S5;
  }

  static List<SpeedSegment> values()=> [S1, S2, S3, S4, S5];

}

class SpeedGraphType{
  static const overrideTempValue = 10;

  final String ring;
  final List<double> values;
  final double incValue;
  const SpeedGraphType({
    required this.ring,
    required this.values,
    required this.incValue});

  static const TYPE_1 = const SpeedGraphType(ring: "t1.png", values: [0.0, 0.1, 1.0, 10.0, 100.0], incValue: 1.0);
  static const TYPE_2 = const SpeedGraphType(ring: "t2.png", values: [0.0, 40.0, 80.0, 120.0, 160.0], incValue: 4.0);
  static const TYPE_3 = const SpeedGraphType(ring: "t3.png", values: [0.0, 80.0, 160.0, 220.0, 300.0], incValue: 8.0);
  static const TYPE_4 = const SpeedGraphType(ring: "t4.png", values: [0.0, 100.0, 200.0, 300.0, 400.0], incValue: 6.0);
  static const TYPE_5 = const SpeedGraphType(ring: "t5.png", values: [0.0, 150.0, 300.0, 450.0, 600.0], incValue: 15.0);

  static SpeedGraphType getSpeedGraphTypeBySpeed(double speed){
    if(speed < TYPE_1.values[4] + overrideTempValue) return TYPE_1;
    else if(speed < TYPE_2.values[4] + overrideTempValue) return TYPE_2;
    else if(speed < TYPE_3.values[4] + overrideTempValue) return TYPE_3;
    else if(speed < TYPE_4.values[4] + overrideTempValue) return TYPE_4;

    return TYPE_5;
  }

}

class NetworkGraphType {
  final String ring;
  final int min;
  final int max;

  const NetworkGraphType({
    required this.ring,
    required this.min,
    required this.max
  });

  static const MOBILE = const NetworkGraphType(ring: "mobile.png", min: -110, max: -50);
  static const WLAN = const NetworkGraphType(ring: "wlan.png", min: -100, max: -40);
  static const RSRP = const NetworkGraphType(ring: "rsrp.png", min: -130, max: -70);

  static NetworkGraphType getType(SignalType signalType){
    switch (signalType){
      case SignalType.MOBILE: return MOBILE;
      case SignalType.WLAN: return WLAN;
      default: return RSRP;
    }
  }

  static NetworkGraphType getTypeByNetworkType(String cat){
    switch (cat){
      case "LTE" : return RSRP;
      case "WLAN" : return WLAN;
      default: return MOBILE;
    }
  }

}

class GraphType {
  final double start;
  final double total;
  final String background;
  final String ring;
  final String complete;

  const GraphType({
    required this.start,
    required this.total,
    required this.background,
    required this.ring,
    required this.complete
  });

  static const PROGRESS = const GraphType(start: 200.0, total: 290.0, background: "assets/test_gauge_progress_background.png", ring: "assets/ringskala_progress.png", complete: "assets/test_gauge_progress_dynamic.png");
  static const SPEED = const GraphType(start: 0.0, total: 306.0, background: "assets/test_gauge_speed_background.png", ring: "assets/ringskala_speed_", complete: "assets/test_gauge_speed_dynamic.png");
  static const SIGNAL = const GraphType(start: 119, total: 154.0, background: "assets/test_gauge_signal_background.png", ring: "assets/ringskala_signal_", complete: "assets/test_gauge_signal_dynamic.png");
  static const QOS = const GraphType(start: -54.0, total: 305.0, background: "assets/test_gauge_speed_background.png", ring: "assets/ringskala_qos.png", complete: "assets/test_gauge_speed_dynamic.png");
}
