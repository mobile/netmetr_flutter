/*
 *  Copyright (C) 2021 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';

class WarningWidget extends StatelessWidget {
  final String textKey;
  WarningWidget({
    Key? key,
    this.textKey = "noData"
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    TextStyle noDataStyle =  Theme.of(context).textTheme.subtitle1!.copyWith(color: Colors.grey);

    return new Container(
      alignment: Alignment.center,
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Icon(Icons.warning, color: Colors.grey, size: screenSize.width * 0.4,),
          new Padding(padding: const EdgeInsets.only(top: 10.0),
            child: new Text(AppLocalizations.of(context)!.getLocalizedValue(textKey)!, style: noDataStyle,),
          )
        ],
      ),
    );
  }
}
