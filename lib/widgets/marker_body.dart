/*
 * Copyright (C) 2021 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/net/response/test_result_response.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:netmetr_flutter/util/classification.dart';
import 'package:netmetr_flutter/util/widget_helper.dart' as widgetHelper;

class MarkerBody extends StatefulWidget {
  final MarkData? markData;
  final Size dSize;
  MarkerBody({
    Key? key,
    required this.markData,
    required this.dSize
  }) : super(key: key);

  @override
  _MarkerBodyState createState() => _MarkerBodyState();
}

class _MarkerBodyState extends State<MarkerBody> {
  late MarkerSizeData sizeData;

  @override
  void initState() {
    super.initState();
    sizeData = MarkerSizeData._getMarkerSize(widget.dSize.width);
  }

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    TextStyle markTextBold = themeData.textTheme.caption!.copyWith(color: Colors.black, fontWeight: FontWeight.bold, fontSize: sizeData.textSize);
    TextStyle markText = themeData.textTheme.caption!.copyWith(color: Colors.black, fontSize: sizeData.textSize);

    double markHeight = sizeData.emptyMarkHeight;
    if(widget.markData != null){
      markHeight += sizeData.markerDataHeight * (widget.markData!.measurement != null ? widget.markData!.measurement!.length : 0);
      markHeight += sizeData.markerDataHeight * (widget.markData!.net != null ? widget.markData!.net!.length : 0);
    }


    return widget.markData != null ? ConstrainedBox(
      constraints: BoxConstraints(
          maxWidth: sizeData.maxMarkerWidth,
          maxHeight: markHeight
      ),
      child: Container(
        margin: EdgeInsets.all(sizeData.cPadding),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Container(
              width: double.maxFinite,
              color: Colors.grey,
              padding: EdgeInsets.all(sizeData.cPadding),
              child: new Text(widget.markData!.timeString ?? "-", style: markTextBold, maxLines: 1,),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.all(sizeData.cPadding),
              child:  new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Padding(padding: const EdgeInsets.only(top: 1.0),
                    child: new Text(AppLocalizations.of(context)!.getLocalizedValue('measureCAPS')!, style: markTextBold, maxLines: 1),
                  ),
                  widgetHelper.titleDivider(),
                  widget.markData!.measurement != null && widget.markData!.measurement!.isNotEmpty ? new Column(
                    children: widget.markData!.measurement!.map((Measurement measurement){

                      Color classifyColor = TestClassification.classifyColor(measurement.classification);
                      IconData classifyIcon = TestClassification.classifyIcon(measurement.classification);

                      return new Column(
                        children: <Widget>[
                          new Row(
                            children: <Widget>[
                              new Expanded(
                                  child: new Text(measurement.title, style: markText, maxLines: 1)),
                              new Expanded(
                                  child: new Row(
                                    children: <Widget>[
                                      new Icon(classifyIcon, color: classifyColor, size: sizeData.iconSize,),
                                      new Padding(padding: EdgeInsets.only(left: sizeData.textPadding),
                                        child: new Text(measurement.value, style: markText, textAlign: TextAlign.right, maxLines: 1),
                                      )
                                    ],
                                  )
                              )
                            ],
                          ),
                          new Divider(height: sizeData.dividerH,)
                        ],
                      );
                    }).toList(),
                  ) : new Container(),
                  new Text(AppLocalizations.of(context)!.getLocalizedValue('networkCAPS')!, style: markTextBold,),
                  widgetHelper.titleDivider(),
                  widget.markData!.net != null && widget.markData!.net!.isNotEmpty ? new Column(
                    children: widget.markData!.net!.map((ResultData data){
                      bool last = widget.markData!.net!.indexOf(data) == widget.markData!.net!.length - 1;
                      return new Column(
                        children: <Widget>[
                          new Row(
                            children: <Widget>[
                              new Expanded(child: new Text(data.title!, style: markText, maxLines: 1)),
                              new Expanded(child: new Text(data.value, style: markText,textAlign: TextAlign.right, maxLines: 1))
                            ],
                          ),
                          last ? new Container() : new Divider(height: sizeData.dividerH,)
                        ],
                      );
                    }).toList(),
                  ) : new Container(),
                ],
              ),
            ),
            new Container(
                alignment: Alignment.center,
                child: new Image.asset("assets/map_arrow.png", width: sizeData.arrowW, height: sizeData.arrowH, color: Colors.white,)
            )
          ],
        ),
      ),
    ) : new Container();
  }

}

class MarkerSizeData{
  double emptyMarkHeight;
  double markerDataHeight;
  double textSize;
  double iconSize;
  double maxMarkerWidth;
  double arrowW;
  double arrowH;

  double dividerH;
  double cPadding;
  double textPadding;

  MarkerSizeData({
    this.emptyMarkHeight = 80,
    this.markerDataHeight = 30,
    this.textSize = 12.0,
    this.iconSize = 16,
    this.maxMarkerWidth = 250.0,
    this.arrowW = 25,
    this.arrowH = 15,
    this.dividerH = 16,
    this.cPadding = 2.0,
    this.textPadding = 5.0
  });

  static _getMarkerSize(double displayW){
    if(displayW < 400){
      return MarkerSizeData(
        emptyMarkHeight: 20,
        markerDataHeight: 12,
        textSize: 4.0,
        iconSize: 5.0,
        maxMarkerWidth: 100,
        arrowW: 10,
        arrowH: 6,
        dividerH: 5,
        cPadding: 1.0,
        textPadding: 2.0
      );
    }else if(displayW < 600){
      return MarkerSizeData(
          emptyMarkHeight: 30,
          markerDataHeight: 18,
          textSize: 6.0,
          iconSize: 8.0,
          maxMarkerWidth: 120,
          arrowW: 18,
          arrowH: 10,
          dividerH: 8,
          cPadding: 2.0,
          textPadding: 3.0
      );
    }

    return MarkerSizeData();
  }
}