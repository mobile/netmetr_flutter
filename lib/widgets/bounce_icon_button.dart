/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import 'package:flutter/material.dart';


class BounceIconButton extends StatefulWidget {
  final IconData icon;
  final Color colorBack;
  final Color colorIcon;
  final Color colorIconBack;
  final Function onPressed;
  final double iconSize;
  final bool back;

  BounceIconButton({
    Key? key,
    required this.icon,
    required this.onPressed,
    this.colorBack = Colors.white,
    this.colorIcon = Colors.black,
    this.colorIconBack = Colors.white,
    this.iconSize = 24.0,
    this.back = true
  });

  @override
  _BounceIconButtonState createState() => _BounceIconButtonState();
}

class _BounceIconButtonState extends State<BounceIconButton> with SingleTickerProviderStateMixin {
  late AnimationController _controller;


  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
        vsync: this,
        duration: Duration(
          seconds: 1
        ),
        reverseDuration: Duration(
          milliseconds: 500
        )
    )
    ..addListener(() {
      if(mounted) {
        setState(() {

        });
      }
    })
    ..addStatusListener((status) {
      if(status == AnimationStatus.completed){
        _controller.repeat();
      }else if(status == AnimationStatus.dismissed){
        _controller.forward();
      }
    });

    _controller.forward();
  }


  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    double backSize = widget.iconSize * 2;
    double animSize = backSize * _controller.value;

    return GestureDetector(
      onTap: () => widget.onPressed(),
      child: Container(
        width: backSize,
        height: backSize,
        alignment: Alignment.center,
        child: Stack(
          alignment: AlignmentDirectional.center,
          children: [
            Container(
              width: animSize,
              height: animSize,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100.0),
                  color: Color.fromARGB((125 * (1 - _controller.value)).toInt(), widget.colorBack.red, widget.colorBack.green, widget.colorBack.blue)
              ),
            ),
            widget.back ? Container(
              width: widget.iconSize,
              height: widget.iconSize,
              alignment: Alignment.center,
              child: CircleAvatar(
                radius: widget.iconSize * 0.4,
                backgroundColor: Colors.white,
              ),
            ) : Container(),
            Icon(widget.icon, color: widget.colorIcon, size: widget.iconSize,),
          ],
        ),
      ),
    );
  }
}
