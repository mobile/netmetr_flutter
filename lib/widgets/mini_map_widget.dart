/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

const zoom = 15.0;
const markerSize = 45.0;

class MiniMap extends StatefulWidget {
  final double lat;
  final double lon;
  MiniMap({Key? key,
    required this.lat,
    required this.lon
  }) : super(key: key);

  @override
  _MiniMapState createState() => _MiniMapState();
}

class _MiniMapState extends State<MiniMap>  {

  List<Marker> markers = [];
  late Future<String> mapStyle;

  @override
  void initState() {
    super.initState();
    mapStyle = rootBundle.loadString('assets/map/map_style');
    markers.add(Marker(
        markerId: MarkerId("marker"),
        position: LatLng(widget.lat, widget.lon)
    ));

  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child:  FutureBuilder<String>(
        future: mapStyle,
        builder: (_, snapshot) {
          if (snapshot.hasData){
            return GoogleMap(
              initialCameraPosition: CameraPosition(target: LatLng(widget.lat, widget.lon), zoom: zoom),
              myLocationEnabled: false,
              myLocationButtonEnabled: false,
              zoomGesturesEnabled: false,
              zoomControlsEnabled: false,
              markers: markers.toSet(),
               onMapCreated: (controller){
                controller.setMapStyle(snapshot.data);
              },
            );
          } else {
            return Container();
          }
        },
      )
    );
  }

}
