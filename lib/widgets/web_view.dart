/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:netmetr_flutter/widgets/warning_widget.dart';


class AppWebView extends StatefulWidget {
  final String? url;
  AppWebView({
    required this.url
  });

  @override
  _AppWebViewState createState() => _AppWebViewState();
}

class _AppWebViewState extends State<AppWebView> {
  double _progress = 0;

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);

    return SafeArea(
      child: Column(
        children: <Widget>[
          Container(
              padding: EdgeInsets.all(10.0),
              child: _progress < 1.0
                  ? LinearProgressIndicator(value: _progress, valueColor: AlwaysStoppedAnimation<Color>(themeData.colorScheme.secondary),)
                  : Container()
          ),
          Expanded(
            child: Container(
              child: widget.url != null ? InAppWebView(
                initialUrlRequest: new URLRequest(url: Uri.parse(widget.url!)),
                initialOptions: InAppWebViewGroupOptions(
                    crossPlatform: InAppWebViewOptions(
                      //debuggingEnabled: true,
                    )
                ),
                onProgressChanged: (_, int progress){
                  setState(() {
                    _progress = progress / 100;
                  });
                },
              ) : new WarningWidget(textKey: "noData2",),
            ),
          )
        ],
      )
    );
  }
}
