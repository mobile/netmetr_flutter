/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';

class TestClassification{

  static Color classifyColor(int classification){
    switch (classification){
      case 1: return Colors.red;
      case 2: return Colors.yellow;
      case 3: return Colors.green;
      default: return Colors.blueGrey;
    }
  }

  static IconData classifyIcon(int classification){
    switch (classification){
      case 1: return Icons.clear;
      case 2: return Icons.check;
      case 3: return Icons.check;
      default: return Icons.clear;
    }
  }

}

class TrafficClassification{
  final List<int?> threshold;
  final String asset;

  const TrafficClassification({required this.threshold, required this.asset});

  static const NONE = const TrafficClassification(threshold: [0, 1250], asset: "assets/traffic_speed_none.png");
  static const LOW = const TrafficClassification(threshold: [1250, 12500], asset: "assets/traffic_speed_low.png");
  static const MID = const TrafficClassification(threshold: [12500, 125000], asset: "assets/traffic_speed_mid.png");
  static const HIGH = const TrafficClassification(threshold: [125000, null], asset: "assets/traffic_speed_high.png");

  static TrafficClassification classify(final int value){
    if(value < NONE.threshold[1]!) return NONE;
    else if(value >= LOW.threshold[0]! && value < LOW.threshold[1]!) return LOW;
    else if(value >= MID.threshold[0]! && value < MID.threshold[1]!) return MID;

    return HIGH;
  }
}