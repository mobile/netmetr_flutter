/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'widget_helper.dart' as widgetHelper;

const Color _buttonColor = Colors.indigo;

const ERR_SERVER = 'errServer';
const ERR_PARSE_DATA = 'errParseData';
const ERROR = 'error';

enum DialogStatus{
  OK, INFO, WARNING, ERROR
}

Widget _getDialogIcon(DialogStatus status){
  switch (status){
    case DialogStatus.OK: return new Icon(Icons.check_circle, color: Colors.green, size: 50,);
    case DialogStatus.WARNING: return new Icon(Icons.warning, color: Colors.yellow, size: 50,);
    case DialogStatus.ERROR: return new Icon(Icons.error, color: Colors.red, size: 50,);
    default: return new Icon(Icons.info, color: Colors.blueGrey, size: 50,);
  }
}

Future<dynamic> showConnectionError({
  required BuildContext context,
  Function()? ok}){
  return showStatusDialog(context: context, status: DialogStatus.ERROR, title: AppLocalizations.of(context)!.getLocalizedValue(ERROR), message: AppLocalizations.of(context)!.getLocalizedValue(ERR_SERVER)!, ok: ok);
}


Future<dynamic> showParseDataError({
  required BuildContext context,
  Function()? ok}){
  return showStatusDialog(context: context, status: DialogStatus.ERROR, title: AppLocalizations.of(context)!.getLocalizedValue(ERROR), message: AppLocalizations.of(context)!.getLocalizedValue(ERR_PARSE_DATA)!, ok: ok);
}

Future<dynamic> showResponseError({
  required BuildContext context,
  required DialogStatus status,
  required String errorMessage,
  Function()? ok
}){
  return showStatusDialog(context: context, status: status, message: errorMessage, ok: ok, cancelButtonKey: 'confirm');
}

Future<dynamic> showCheckBoxDialog({
  required BuildContext context,
  required DialogStatus status,
  String? title,
  String? message,
  String okButtonKey = 'confirm',
  Function()? ok,
  bool barrierDismissible = true,
  required Function(bool) uncheck,
  required bool check,
  String? checkMessage}){

  return showAlertDialog(
      context: context,
      title: title,
      okButtonKey: okButtonKey,
      ok: ok,
      barrierDismissible: barrierDismissible,
      body: Container(
        width: double.maxFinite,
        child: Column(
          children: [
            Row(
              children: <Widget>[
                Flexible(
                    flex: 2,
                    child: _getDialogIcon(status)
                ),
                Flexible(
                    flex: 8,
                    child: Padding(padding: const EdgeInsets.only(left: 10.0),
                      child: Text(widgetHelper.notNullString(message)),
                    )
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: CheckBoxBody(
                check: check,
                checkMessage: checkMessage!,
                checkListener: uncheck,
              ),
            )
          ],
        )
      ));
}

Future<dynamic> showStatusDialog({
  required BuildContext context,
  required DialogStatus status,
  String? title,
  required String message,
  String? footnote,
  String okButtonKey = 'confirm',
  Function()? ok,
  String cancelButtonKey = 'cancel',
  Function()? cancel,
  bool barrierDismissible = true}){

  return showAlertDialog(
    context: context,
    title: title,
    okButtonKey: okButtonKey,
    ok: ok,
    cancelButtonKey: cancelButtonKey,
    cancel: cancel,
    barrierDismissible: barrierDismissible,
    body: new Container(
      width: double.maxFinite,
      child:  new Row(
        children: <Widget>[
          new Flexible(
              flex: 2,
              child: _getDialogIcon(status)
          ),
          new Flexible(
              flex: 8,
              child: new Padding(padding: const EdgeInsets.only(left: 10.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text(widgetHelper.notNullString(message)),
                    footnote != null && footnote.isNotEmpty ? 
                      new Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: new Text(footnote, style: TextStyle(fontSize: 10.0),),
                      ) : new Container()
                  ],
                ),
              )
          )
        ],
      ),
    )
  );
}

Future<dynamic> showEditDialog({
  required BuildContext context,
  required dynamic value,
  required TextInputType textInputType,
  required String title,
  required bool Function(dynamic) changeValue,
  String okButtonKey = 'confirm_it',
}){

  TextEditingController dialogController = new TextEditingController(text: value.toString());

  return showAlertDialog(
    context: context,
    title: title,
    okButtonKey: okButtonKey,
    cancel: ()=>Navigator.pop(context),
    ok: (){
     bool changed = changeValue(dialogController.text);
     if(changed)Navigator.pop(context);
    },
    body: new Container(
      child: new TextField(
        controller: dialogController,
        keyboardType: textInputType,
        autofocus: true,
      ),
    )
  );
}

Future<dynamic> showAlertDialog({
  required BuildContext context,
  String? title,
  required Widget body,
  String okButtonKey = 'confirm',
  Function()? ok,
  String cancelButtonKey = 'cancel',
  Function()? cancel,
  bool barrierDismissible = true}){
  double maxWidth = MediaQuery.of(context).size.width * 0.7;

  var dialog = new AlertDialog(
    content:  new SingleChildScrollView(
      child: new Column(
        children: <Widget>[
          title != null ? new Container(
            width: double.maxFinite,
            alignment: Alignment.center,
            child: new Text(title, style: new TextStyle(fontWeight: FontWeight.bold),),
          ) : new Container(),
          new Padding(padding: new EdgeInsets.only(top: title != null ? 10.0 : 0.0),
            child: body,
          )
        ],
      ),
    ),
    actions: <Widget>[
      ConstrainedBox(
        constraints: BoxConstraints(maxWidth: maxWidth),
        child: Wrap(
          alignment: WrapAlignment.end,
          children: <Widget>[
            cancel != null ? new ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(_buttonColor)
              ),
              onPressed: cancel,
              child: new Text(widgetHelper.notNullString(AppLocalizations.of(context)!.getLocalizedValue(cancelButtonKey)), style: new TextStyle(color: Colors.white),),
            ) : new Container(),
            new Container(width: 10.0,),
            new ElevatedButton(
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(_buttonColor)
              ),
              onPressed: ok != null ? ok : (){
                Navigator.pop(context);
              },
              child: new Text(widgetHelper.notNullString(AppLocalizations.of(context)!.getLocalizedValue(okButtonKey)), style: new TextStyle(color: Colors.white),),
            ),
            new Container(width: 10.0,),
          ],
        ),
      ),
    ],
  );

  return showDialog(context: context, barrierDismissible: barrierDismissible, builder: (_)=> dialog);
}

class CheckBoxBody extends StatefulWidget {
  final String checkMessage;
  final bool check;
  final Function(bool) checkListener;

  CheckBoxBody({
    Key? key,
    required this.checkMessage,
    required this.check,
    required this.checkListener
  }) : super(key: key);

  @override
  _CheckBoxBodyState createState() => _CheckBoxBodyState();
}

class _CheckBoxBodyState extends State<CheckBoxBody> {

  late bool check;

  @override
  void initState() {
    super.initState();
    check = widget.check;
  }

  @override
  Widget build(BuildContext context) {
    return  Row(
        children: [
          Flexible(
            flex: 1,
            child: Checkbox(
                value: check,
                onChanged: (value){
                  widget.checkListener(value!);

                  setState(() {
                    check = value;
                  });
                }
            )
          ),
          Flexible(
            flex: 4,
            child: Padding(
              padding: const EdgeInsets.only(left: 5.0),
              child: Text(widget.checkMessage),
            )
          )
        ],
    );
  }

}
