/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';


Route _createPageRouteFade(Widget screen){
  return new PageRouteBuilder<Widget>(
      pageBuilder: (context, _, __) => screen,
      transitionDuration: new Duration(milliseconds: 200),
      transitionsBuilder: (_, Animation<double> animation, __, Widget child){
        return FadeTransition(
          opacity: animation,
          child: FadeTransition(
            opacity: new Tween<double>(
                begin: 0.0,
                end: 1.0
            ).animate(animation),
            child: child,
          ),
        );
      }
  );
}

void pushFadeAnimation(BuildContext context, Widget screen){
  Navigator.push(context, _createPageRouteFade(screen));
}

void pushReplaceFadeAnimation(BuildContext context, Widget screen){
  Navigator.pushReplacement(context, _createPageRouteFade(screen));
}
