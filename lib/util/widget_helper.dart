/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/widgets/bounce_icon_button.dart';
import 'dart:math' as math;

import 'network/network_data.dart';
import 'network/network_connection_type.dart';

String notNullString(String? text){
  return text != null ? text : "-";
}

bool useMobileLayout(MediaQueryData queryData){
  double height = queryData.size.height;
  double width = queryData.size.width;
  double smallestDimension = math.min(height, width);
  return smallestDimension < 600;
}

Widget getSmallProgress({color: Colors.blueGrey}){
  return new Container(
    margin: const EdgeInsets.all(5.0),
    width: 20,
    height: 20,
    child: new CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(color), strokeWidth: 2,),
  );
}

Widget titleDivider(){
  return new Container(
    margin: const EdgeInsets.symmetric(vertical: 1.0),
    color: Colors.grey,
    width: double.maxFinite,
    height: 2.0,
  );
}

Widget divider(){
  return  new Divider(color: Colors.grey,);
}

Widget appBarActionsImage(String iconImagePath, double size, Function(int) press, int action){
  return _appBarActions(new Image.asset(iconImagePath, width: size), size, press, action);
}

Widget appBarActionsIcon(IconData icon,double size, Function(int) press, int action){
  return _appBarActions(new Icon(icon, color: Colors.white), size, press, action);
}

Widget appBarBounceActionsIcon(IconData icon,double size, Function(int) press, int action){
  return Container(
    width: size * 2,
    height: size,
    child: BounceIconButton(
      icon: icon,
      onPressed: ()=> press(action),
      colorIcon: Colors.white,
      back: false,
    ),
  );
}

Widget _appBarActions(Widget widget, double size, Function(int) press, int action){
  return new Container(
    width: size,
    height: size,
    child: new IconButton(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 1.0),
        onPressed: ()=> press(action),
        icon: widget
    ),
  );
}

Image getConnectivityImage(NetworkConnectionType networkType, NetworkData networkData, bool useMobileLayout){
  return getConnectivitySignalImage(networkType, networkData.relativeSignal, useMobileLayout ? 2.0 : 0.9);
}

Image getConnectivitySignalImage(NetworkConnectionType networkType, double relativeSignal, double scale){
  String imageName = "assets/signal";
  imageName += networkType == NetworkConnectionType.WIFI ? "_wlan" : "_mobile";
  

  if(relativeSignal == -1 || relativeSignal == 0){
    imageName += "_0";
  }
  else if (relativeSignal < 0.25) {
    imageName += "_25";
  }
  else if (relativeSignal < 0.5) {
    imageName += "_50";
  }
  else if (relativeSignal < 0.75) {
    imageName += "_75";
  }

  imageName += ".png";


  return Image.asset(imageName, color: Colors.black, scale: scale,);
}