/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class LocationData{
  final bool locationPermission;
  final double? latitude;
  final double? longitude;
  final String? latitudeString;
  final String? longitudeString;
  final bool hasAccuracy;
  final double? accuracy;
  final int? satellites;
  final int? age;
  final String? provider;
  final double? altitude;
  final double? speed;
  final bool haveLocationData;

  LocationData({
    this.locationPermission = false,
    this.latitude,
    this.longitude,
    this.latitudeString,
    this.longitudeString,
    this.hasAccuracy = false,
    this.accuracy,
    this.satellites,
    this.age,
    this.provider,
    this.altitude,
    this.speed,
    this.haveLocationData = false
  });

  LocationData.fromJson(Map<String, dynamic> jsonData) : this(
    locationPermission: jsonData["locationPermission"],
    latitude: jsonData["latitude"] + 0.0,
    longitude: jsonData["longitude"] + 0.0,
    latitudeString: jsonData["latitudeString"],
    longitudeString: jsonData["longitudeString"],
    hasAccuracy: jsonData["hasAccuracy"],
    accuracy: jsonData["accuracy"] + 0.0,
    satellites: jsonData["satellites"],
    age: jsonData["age"],
    provider: jsonData["provider"],
    altitude: jsonData["altitude"] + 0.0,
    speed: jsonData["speed"] + 0.0,
    haveLocationData: jsonData["haveLocationData"]
  );

  Map<String, dynamic> toJson() => {
    'lat' : latitude,
    'long' : longitude,
    'provider' : provider,
    'speed' : speed,
    'altitude' : altitude,
    'age' : age,
    'accuracy' : accuracy
  };
}