/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'dart:convert';

Future<String?> getUUID() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("uuid");
}

setUUID(String uuid) async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString("uuid", uuid);
}

Future<String?> getControlServerVersion() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("controlServerVersion");
}

setControlServerVersion(String controlServerVersion) async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString("controlServerVersion", controlServerVersion);
}

Future<String?> getUrlIpv6Check() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("url_ipv6_check");
}

setUrlIpv6Check(String url) async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString("url_ipv6_check", url);
}

Future<String?> getControlIpv4Only() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("control_ipv4_only");
}

setControlIpv4Only(String url) async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString("control_ipv4_only", url);
}

Future<String?> getUrlIpv4Check() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("url_ipv4_check");
}

setUrlIpv4Check(String url) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString("url_ipv4_check", url);
}

Future<String?> getControlIpv6Only() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("control_ipv6_only");
}

setControlIpv6Only(String url) async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString("control_ipv6_only", url);
}

Future<String?> getStatics() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("statistics");
}

setStatistics(String ulr) async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString("statistics", ulr);
}

setFirstMapUse() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool("first_map", true);
}

Future<bool?> getFirstMapUse() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getBool("first_map");
}


setMapFilter(Map<String, dynamic> filter) async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString("map_filter", jsonEncode(filter));
}

Future<Map<String, dynamic>> getMapFilter() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String? data = prefs.getString("map_filter");
  return data != null ? jsonDecode(data) : Map();
}

setMapOptions(Map<String, dynamic>? options) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString("map_options", jsonEncode(options));
}

Future<Map<String, dynamic>> getMapOptions() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String? data = prefs.getString("map_options");
  return  data != null ? jsonDecode(data) : Map();
}

Future<bool?> getConfirmedUserTerms() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getBool("confirmed_user_terms");
}

setConfirmedUserTerms(bool confirmed) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool("confirmed_user_terms", confirmed);
}
//////////////////// DEBUG SETTINGS

setControlServer(String server) async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString("secret_control_server", server);
}

Future<String?> getControlServer() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("secret_control_server");
}

setControlPort(int port) async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setInt("secret_control_port", port);
}

Future<int?> getControlPort() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getInt("secret_control_port");
}

setControlSSL(bool ssl) async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool("secret_control_ssl", ssl);
}

Future<bool?> getControlSSL() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getBool("secret_control_ssl");
}

setQOSSSL(bool ssl) async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool("secret_qos_ssl", ssl);
}

Future<bool?> getQOSSSL() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getBool("secret_qos_ssl");
}

Future<bool?> getFirstRun() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getBool("first_run");
}

setFirstRun(bool firstRun) async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool("first_run", firstRun);
}

Future<bool?> getUncheckDataWarning() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getBool("uncheck_data_warning");
}

setUncheckDataWarning(bool check) async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool("uncheck_data_warning", check);
}

Future<bool?> getConfirmTerminationChecked() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getBool("termination_checked");
}

setTerminationChecked(bool check) async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool("termination_checked", check);
}



