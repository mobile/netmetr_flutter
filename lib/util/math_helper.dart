/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:math';

extension Ex on double {
  double toPrecision(int n) => double.parse(toStringAsFixed(n));
}

double roundDouble(double number, int decimals){
  return number.toPrecision(decimals);
}

int roundToInt(double num, int decimals){
  return num.toPrecision(decimals).toInt();
}

bool checkDoubleValue(double? value){
  return value != null && value != double.infinity && value != double.nan;
}

const double radiansPerDegree = pi / 180;

double degreesToRadians(double degrees) => roundDouble(degrees * radiansPerDegree, 10);
