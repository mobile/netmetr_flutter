/*
 * Copyright (C) 2021 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:netmetr_flutter/app_config.dart';
import 'package:http/http.dart' as http;

class MapTileProvider implements TileProvider{
  final ConfigModel configModel;
  final String mapType;
  final Map<String, dynamic> currentMapOptions;
  final bool needTechnology;
  final int tileSize;

  MapTileProvider({
    required this.configModel,
    required this.mapType,
    required this.currentMapOptions,
    required this.needTechnology,
    required this.tileSize
  });

  @override
  Future<Tile> getTile(int x, int y, int? zoom){
    String params = "?path=$zoom/$x/$y";

    for(String key in currentMapOptions.keys){
      if(key == "technology" && !needTechnology)
        continue;

      dynamic value = currentMapOptions[key];
      params+= "&$key=$value";
    }

    
    var uri = new Uri(
      scheme: 'https',
      port: configModel.port,
      host: configModel.controlHost,
      path: "/RMBTMapServer/tiles/$mapType",
    );

    final client = http.Client();
    final request = http.Request("GET", Uri.parse(uri.toString() + params));
    return client.send(request).then((response){
      return response.stream.toBytes().then((value){
        return Tile(tileSize, tileSize, value);
      });
    });
  }
  
}