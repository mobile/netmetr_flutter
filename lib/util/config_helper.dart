/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:netmetr_flutter/net/basic_info.dart';
import 'package:netmetr_flutter/util/location/location_data.dart';

class ConfigHelper{
  static ConfigHelper? _instance;
  static ConfigHelper? get instance => _instance;

  static init(){
    if(_instance != null){
      print("Config has already been initialized");
      return;
    }

    _instance = new ConfigHelper();
  }

  String uuid;
  BasicInfo? basicInfo;
  LocationData? lastLocation;
  String? versionName;
  int? versionCode;
  bool? mapUsed;
  bool debugMode;
  bool? uncheckDataWarning;

  ConfigHelper({
    this.uuid = "",
    this.basicInfo,
    this.versionName,
    this.versionCode,
    this.mapUsed,
    this.debugMode = false,
    this.uncheckDataWarning
  });

}

