/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class NetworkOverrideType{
  final int original;
  final String? name;
  final String? familyName;

  const NetworkOverrideType({
    required this.original,
    this.name,
    this.familyName
  });

  static const NONE = const NetworkOverrideType(original: 0);
  static const LTE_CA = const NetworkOverrideType(original: 1, name: "LTE-A", familyName: "4G");
  static const LTE_ADVANCED_PRO = const NetworkOverrideType(original: 2, name: "LTE-A Pro", familyName: "5G");
  static const NR_NSA = const NetworkOverrideType(original: 3, name: "NR-NSA", familyName: "5G");
  static const NR_NSA_MMWAVE =const NetworkOverrideType(original: 4, name: "NR-NSA-mmWave", familyName: "5G");

  static NetworkOverrideType getType(int original){
    switch(original){
      case 1: return LTE_CA;
      case 2: return LTE_ADVANCED_PRO;
      case 3: return NR_NSA;
      case 4: return NR_NSA_MMWAVE;
      default: return NONE;
    }
  }

}