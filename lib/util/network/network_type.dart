/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class NetworkType{
  final int id;
  final String name;
  final String? networkFamily;

  const NetworkType({
    required this.id,
    required this.name,
    this.networkFamily
  });

  static const UNKNOWN = const NetworkType(id: 0, name: "UNKNOWN");
  //MOBILE
  static const GPRS = const NetworkType(id: 1, name: "GPRS", networkFamily: "2G");
  static const EDGE = const NetworkType(id: 2, name: "EDGE", networkFamily: "2G");
  static const UMTS = const NetworkType(id: 3, name: "UMTS", networkFamily: "3G");
  static const CDMA = const NetworkType(id: 4, name: "CDMA", networkFamily: "2G");
  static const EVDO_0 = const NetworkType(id: 5, name: "EVDO_0", networkFamily: "2G");
  static const EVDO_A = const NetworkType(id: 6, name: "EVDO_A", networkFamily: "2G");
  static const RTT1x = const NetworkType(id: 7, name: "1xRTT", networkFamily: "2G");
  static const HSDPA = const NetworkType(id: 8, name: "HSDPA", networkFamily: "3G");
  static const HSUPA = const NetworkType(id: 9, name: "HSUPA", networkFamily: "3G");
  static const HSPA = const NetworkType(id: 10, name: "HSPA", networkFamily: "3G");
  static const IDEN = const NetworkType(id: 11, name: "IDEN", networkFamily: "2G");
  static const EVDO_B = const NetworkType(id: 12, name: "EVDO_B", networkFamily: "2G");
  static const LTE = const NetworkType(id: 13, name: "LTE", networkFamily: "4G");
  static const EHRPD = const NetworkType(id: 14, name: "EHRPD", networkFamily: "2G");
  static const HSPAP = const NetworkType(id: 15, name: "HSPA+", networkFamily: "3G");
  static const GSM = const NetworkType(id: 16, name: "GSM", networkFamily: "3G");
  static const TD_SCDMA = const NetworkType(id: 17, name: "TD_SCDMA", networkFamily: "3G");
  static const LTE_CA = const NetworkType(id: 19, name: "LTE_CA", networkFamily: "4G");
  static const NR = const NetworkType(id: 20, name: "NR_SA", networkFamily: "5G");
  static const NR_NSA = const NetworkType(id: 21, name: "NR_NSA", networkFamily: "5G");
  static const NR_SA_MMWAVE =const NetworkType(id: 22, name: "NR_SA_mmWave", networkFamily: "5G");
  static const NR_NSA_MMWAVE =const NetworkType(id: 23, name: "NR_NSA_mmWave", networkFamily: "5G");
  static const LTE_ADVANCED = const NetworkType(id: 50, name: "LTE_A", networkFamily: "4G");
  static const LTE_ADVANCED_PRO = const NetworkType(id: 51, name: "LTE_A Pro", networkFamily: "4G");
  //ANOTHER
  static const IWLAN = const NetworkType(id: 18, name: "IWLAN");
  static const WLAN = const NetworkType(id: 99, name: "WLAN");
  static const ETHERNET = const NetworkType(id: 106, name: "ETHERNET");
  static const BLUETOOTH = const NetworkType(id: 107, name: "BLUETOOTH");

  static List<NetworkType> values = [
    UNKNOWN,GPRS, EDGE, UMTS, CDMA, EVDO_0, EVDO_A, RTT1x, HSDPA, HSUPA, HSPA, IDEN, EVDO_B, LTE, EHRPD,
    HSPAP, GSM, TD_SCDMA, IWLAN, LTE_CA, NR, NR_NSA, NR_SA_MMWAVE, NR_NSA_MMWAVE, LTE_ADVANCED, LTE_ADVANCED_PRO,WLAN, ETHERNET, BLUETOOTH,
  ];

  static NetworkType getType(int? networkID){
    for(NetworkType type in values){
      if(type.id == networkID)
        return type;
    }

    return UNKNOWN;
  }

}