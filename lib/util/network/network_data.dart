/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

enum IpStatus{
  STATUS_NOT_AVAILABLE, NO_ADDRESS, ONLY_LOCAL, CONNECTED_NAT, CONNECTED_NO_NAT
}

enum SignalType{
  NO_SIGNAL, MOBILE, RSRP, WLAN
}

class NetworkData{
  String? ssid;
  double relativeSignal;
  int? signal;
  SignalType signalType;
  int networkID;
  int overrideNetworkType;

  NetworkData({
    this.signalType = SignalType.NO_SIGNAL,
    required this.ssid,
    this.relativeSignal = 0.0,
    required this.signal,
    this.networkID = -1,
    this.overrideNetworkType = 0
  });


  @override
  String toString() {
    switch (signalType){
      case SignalType.MOBILE: return "Mobile";
      case SignalType.RSRP: return "RSRP";
      case SignalType.WLAN: return "WLAN";
      default: return "No Signal";
    }
  }

  NetworkData.fromJson(Map<String, dynamic> jsonData) : this(
    signalType: SignalType.values[jsonData["signalType"]],
    signal: jsonData["signal"],
    ssid: jsonData["extra"],
    relativeSignal: jsonData["relativeSignal"] + 0.0,
    networkID: jsonData["networkID"],
    overrideNetworkType: jsonData.containsKey("overrideNetworkType") ? jsonData["overrideNetworkType"] : 0
  );
}

class NetworkCheckInfo{
  IpStatus? ipV4Status;
  IpStatus? ipV6Status;

  String? publicIpV4;
  String? publicIpV6;

  String? privateIpV4;
  String? privateIpV6;

  NetworkCheckInfo({
    required this.ipV4Status,
    required this.ipV6Status,
    required this.publicIpV4,
    required this.publicIpV6,
    required this.privateIpV4,
    required this.privateIpV6
  });

  NetworkCheckInfo.fromJSON(Map<String, dynamic> jsonData) : this(
    ipV4Status: IpStatus.values[jsonData["status_ipv4"]],
    ipV6Status: IpStatus.values[jsonData["status_ipv6"]],
    publicIpV4: jsonData["public_ipv4"],
    publicIpV6: jsonData["public_ipv6"],
    privateIpV4: jsonData["private_ipv4"],
    privateIpV6: jsonData["private_ipv6"]
  );
}