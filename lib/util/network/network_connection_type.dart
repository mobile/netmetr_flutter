/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:netmetr_flutter/util/network/network_type.dart';

class NetworkConnectionType{
  final int index;
  final String title;

  const NetworkConnectionType({required this.index, required this.title});

  static const NOT_CONNECTED    = const NetworkConnectionType(index: -1,  title: "No connection");
  static const MOBILE           = const NetworkConnectionType(index: 0,   title: "Mobile");
  static const WIFI             = const NetworkConnectionType(index: 1,   title: "Wifi");
  static const WIFI_P2P         = const NetworkConnectionType(index: 13,  title: "Wifi-P2P");
  static const MOBILE_MMS       = const NetworkConnectionType(index: 2,   title: "Mobile_MMS");
  static const MOBILE_SUPL      = const NetworkConnectionType(index: 3,   title: "Mobile_SUPL");
  static const MOBILE_DUN       = const NetworkConnectionType(index: 4,   title: "Mobile_DUN");
  static const MOBILE_HIPRI     = const NetworkConnectionType(index: 5,   title: "Mobile_HIPRI");
  static const MOBILE_FOTA      = const NetworkConnectionType(index: 10,  title: "Mobile_FOTA");
  static const MOBILE_IMS       = const NetworkConnectionType(index: 11,  title: "Mobile_IMS");
  static const MOBILE_CBS       = const NetworkConnectionType(index: 12,  title: "Mobile_CBS");
  static const MOBILE_IA        = const NetworkConnectionType(index: 14,  title: "Mobile_IA");
  static const MOBILE_EMERGENCY = const NetworkConnectionType(index: 0,   title: "Mobile_EMERGENCY");
  static const WIMAX            = const NetworkConnectionType(index: 6,   title: "TYPE_WIMAX");
  static const BLUETOOTH        = const NetworkConnectionType(index: 7,   title: "TYPE_BLUETOOTH");
  static const DUMMY            = const NetworkConnectionType(index: 8,   title: "TYPE_DUMMY");
  static const VPN              = const NetworkConnectionType(index: 17,  title: "VPN");


  static final activeTypes = [MOBILE, WIFI];

  //TODO potřeba?
  static final mobileAnotherTypes = [MOBILE_MMS, MOBILE_SUPL, MOBILE_DUN, MOBILE_HIPRI, MOBILE_FOTA, MOBILE_IMS, MOBILE_CBS, MOBILE_IA, MOBILE_EMERGENCY];
  static final wifiAnotherType = [WIFI_P2P];

  static NetworkConnectionType getType(int index){
    if(index == NOT_CONNECTED.index)
      return NOT_CONNECTED;

    for (NetworkConnectionType type in activeTypes){
      if(type.index == index)
        return type;
    }

    for (NetworkConnectionType type in wifiAnotherType){
      if(type.index == index)
        return type;
    }

    for (NetworkConnectionType type in mobileAnotherTypes){
      if(type.index == index)
        return type;
    }

    return NOT_CONNECTED;
  }

  static String? getNetworkFamily(NetworkType networkType){
    if(networkType == NetworkType.WLAN) return null;
    return networkType.networkFamily == null ? networkType.name : "${networkType.networkFamily}/${networkType.name}";
  }
}