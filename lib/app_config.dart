/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/app_settings.dart';
import 'package:netmetr_flutter/device_info.dart';
import 'package:netmetr_flutter/net/response/map_response.dart';
import 'package:netmetr_flutter/util/network/network_data.dart';
import 'package:netmetr_flutter/util/network/network_connection_type.dart';

import 'util/network/network_type.dart';


class AppConfig extends InheritedWidget{
  final String clientSecret;
  final AppSettings appSettings;
  final DeviceInfo? deviceInfo;
  final ConfigModel configModel;
  final NetworkModel networkModel;

  AppConfig({
    required this.clientSecret,
    required this.configModel,
    required this.networkModel,
    required this.appSettings,
    required this.deviceInfo,
    required Widget child,
  }) : super(child : child);

  static AppConfig? of(BuildContext context){
    return context.dependOnInheritedWidgetOfExactType<AppConfig>();
  }

  String controlServerName(){
    if(appSettings.onlyIpv4) return configModel.controlIpv4Host;
    else return configModel.controlHost;
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;

}

class ConfigModel{
  late String controlHost;
  late String controlIpv4Host;
  late String controlIpv6Host;
  late String checkIpv4Host;
  late String checkIpv6Host;
  late int port;
  late bool controlSSL;
  late bool qosSSL;
  String? statisticsHost;
  String? controlServerVersion;
  late List<String> historyDevices = [];
  late List<String> historyNetworks = [];
  MapAppOptions? mapOptions;

  bool loopModeActive = false;
  bool needSync = false;

  ConfigModel({
    required this.controlHost,
    required this.controlIpv4Host,
    required this.controlIpv6Host,
    required this.checkIpv4Host,
    required this.checkIpv6Host,
    required this.port,
    required this.controlSSL,
    required this.qosSSL,
  });
}

class NetworkModel{

  NetworkConnectionType networkConnectionType;
  NetworkData? networkData;
  NetworkType? networkType;
  NetworkCheckInfo? checkInfo;

  NetworkModel({
    this.networkConnectionType = NetworkConnectionType.NOT_CONNECTED,
  });

}