/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:netmetr_flutter/net/response/abstract_response.dart';
import 'package:netmetr_flutter/util/network/network_data.dart';

class TestResponse extends AbstractResponse{
  TaskStatus? taskStatus;
  var initNano;
  double? pingNano;
  double? downBitPerSec;
  double? upBitPerSec;
  double? progress;
  double? downBitPerSecLog;
  double? upBitPerSecLog;
  int? signal;
  double relativeSignal;
  SignalType signalType;
  var remainingWait;

  JitterStatus jitterStatus;
  JitterData? down;
  JitterData? up;

  TestResponse({
    this.taskStatus,
    this.initNano,
    this.pingNano,
    this.downBitPerSec,
    this.upBitPerSec,
    this.progress,
    this.downBitPerSecLog,
    this.upBitPerSecLog,
    this.signal,
    this.relativeSignal = 0.0,
    this.signalType = SignalType.NO_SIGNAL,
    this.remainingWait,
    this.jitterStatus = JitterStatus.NOT_STARTED,
    this.down,
    this.up
  });

  @override
  TestResponse.fromJson(Map<String, dynamic> jsonData) : this(
    taskStatus: TaskStatus.values[jsonData["status"]],
    initNano: jsonData["initNano"],
    pingNano: 0.0 + jsonData["pingNano"],
    downBitPerSec: 0.0 + jsonData["downBitPerSec"],
    upBitPerSec: 0.0 + jsonData["upBitPerSec"],
    progress: 0.0 + jsonData["progress"],
    downBitPerSecLog: 0.0 + jsonData["downBitPerSecLog"],
    upBitPerSecLog: 0.0 + jsonData["upBitPerSecLog"],
    remainingWait:0.0 +  jsonData["remainingWait"],
    signal:jsonData["signal"],
    signalType: SignalType.values[jsonData["signalType"]],
    jitterStatus: JitterStatus.values[jsonData["jitterStatus"]],
    down: jsonData.containsKey("down") && jsonData["down"] != null ? JitterData.fromJson(jsonData["down"]) : null,
    up: jsonData.containsKey("up") && jsonData["up"] != null ? JitterData.fromJson(jsonData["up"]) : null,
    relativeSignal: jsonData.containsKey("relativeSignal") ? 0.0 + jsonData["relativeSignal"] : 0.0,
  );

}

class JitterData extends AbstractResponse{
  int? meanJitter;
  int totalPacket;
  int numPacket;

  JitterData({
    this.meanJitter,
    this.totalPacket = 0,
    this.numPacket = 0,
  });


  JitterData.fromJson(Map<String, dynamic> jsonData) : this(
    meanJitter: jsonData["meanJitter"],
    totalPacket: jsonData["total"],
    numPacket: jsonData["numPacket"]
  );

}

enum TaskStatus{
  WAIT, INIT, PACKET_LOSS_AND_JITTER, PING, DOWN, UP, INIT_UP, SPEEDTEST_END, QOS_TEST_RUNNING, QOS_END, NDT_RUNNING, NDT_END, END, ERROR, ABORTED
}

enum JitterStatus{
  NOT_STARTED, RUNNING, FINISH, FAIL, DISABLED
}