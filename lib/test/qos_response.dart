/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:netmetr_flutter/net/response/abstract_response.dart';
import 'package:netmetr_flutter/net/response/test_result_qos_reponse.dart';

class QosResponse extends AbstractResponse{
  QosResultType? type;
  double? progress;
  int? value;
  int? target;
  int? firstTest;
  int? lastTest;

  QosResponse({
    this.type,
    this.progress,
    this.value,
    this.target,
    this.firstTest,
    this.lastTest
  });

  String getTestName(){
    switch (type){
      case QosResultType.TRACEROUTE: return "TraceRoute";
      case QosResultType.VOIP: return "Voip";
      case QosResultType.HTTP_PROXY: return "Http proxy";
      case QosResultType.WEBSITE: return "Website";
      case QosResultType.NON_TRANSPARENT_PROXY: return "Non transparent proxy";
      case QosResultType.DNS: return "DNS";
      case QosResultType.TCP: return "TCP";
      case QosResultType.UDP: return "UDP";
      default: return "Unknown";
    }
  }

  @override
  QosResponse.fromJson(Map<String, dynamic> jsonData) : this(
    type: QosResultType.getTypeByOrigin(jsonData["qosType"]),
    progress: 0.0 + jsonData["progress"],
    value: jsonData["value"],
    target: jsonData["target"],
    firstTest: jsonData["firstTest"],
    lastTest: jsonData["lastTest"]
  );
}