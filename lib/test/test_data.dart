/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:netmetr_flutter/net/response/open_data_response.dart';
import 'package:netmetr_flutter/net/response/test_result_qos_reponse.dart';
import 'package:netmetr_flutter/net/response/test_result_response.dart';

enum ResultStatus{
  OK, FAIL, STOPPED
}

class TestData{
  final ResultStatus status;
  final String testUuid;
  TestResultResponse? testResultResponse;
  Map<QosResultType, List<ResultDetailDescription>>? qosDetailDescriptionMap;
  Map<QosResultType, TestDescription>? qosTestDetailMap;
  Map<QosResultType, List<ResultDetail>>? qosResultMap;
  OpenDataResponse? openDataResponse;

  List<ResultData>? testDetails;
  bool loadedDetail;
  bool loadedQoSData;
  bool loadedOpenData;

  final String? error;


  TestData({
    required this.status,
    required this.testUuid,
    this.loadedQoSData = false,
    this.loadedDetail = false,
    this.loadedOpenData = false,
    this.error
  });
}

