
/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:netmetr_flutter/net/response/abstract_response.dart';

class SpeedTestResponse extends AbstractResponse{

  double speedUpload;
  double speedDownload;
  int bytesDownload;
  int nSecDownload;
  int bytesUpload;
  int nSecUpload;
  double totalDownBytes;
  double totalUpBytes;
  double pingNano;

  SpeedTestResponse({
    this.speedUpload = 0.0,
    this.speedDownload = 0.0,
    this.bytesDownload = 0,
    this.nSecDownload = 0,
    this.bytesUpload = 0,
    this.nSecUpload = 0,
    this.totalDownBytes = 0.0,
    this.totalUpBytes = 0.0,
    this.pingNano = 0.0
  });

  SpeedTestResponse.fromJson(Map<String, dynamic> jsonData) : this(
    speedUpload: jsonData['speed_upload'] + 0.0,
    speedDownload: jsonData['speed_download'] + 0.0,
    bytesDownload: jsonData['bytes_download'],
    nSecDownload: jsonData['nsec_download'],
    bytesUpload: jsonData['bytes_upload'],
    nSecUpload: jsonData['nsec_upload'],
    totalDownBytes: jsonData['total_down_bytes'] + 0.0,
    totalUpBytes: jsonData['total_up_bytes'] + 0.0
  );

}