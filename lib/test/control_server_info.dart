/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

enum TaskDataStatus{
  WAIT, RUNNING, FINISHED
}

class TestTask{
  final String errorKey;
  const TestTask({required this.errorKey});

  static const SPEED = const TestTask(errorKey: 'speed_data_error');
  static const QOS = const TestTask(errorKey: 'qos_data_error');
  static const NDT = const TestTask(errorKey: 'ndt_data_error');
  static const values = [SPEED, QOS, NDT];
}

class ControlServerInfo{
  TaskDataStatus? speedDataStatus;
  TaskDataStatus? qosDataStatus;
  TaskDataStatus? ndtDataStatus;
  bool sending;

  Map<TestTask, ResultData>? results;

  ControlServerInfo({
    this.sending = false,
    this.speedDataStatus,
    this.qosDataStatus,
    this.ndtDataStatus,
    this.results,
  });

  String? infoKey(){
    if(speedDataStatus == TaskDataStatus.RUNNING) return "speed_data_info";
    else if(qosDataStatus == TaskDataStatus.RUNNING) return "qos_data_info";
    else if(ndtDataStatus == TaskDataStatus.RUNNING) return "ndt_data_info";

    return null;
  }

  TestTask? sendingTestTask(){
    if(speedDataStatus == TaskDataStatus.RUNNING) return TestTask.SPEED;
    else if(qosDataStatus == TaskDataStatus.RUNNING) return TestTask.QOS;
    else if(ndtDataStatus == TaskDataStatus.RUNNING) return TestTask.NDT;

    return null;
  }

  static Map<TestTask, ResultData> _parseResult(List<dynamic> resultsJson){
    Map<TestTask, ResultData> results = Map();

    for(Map<String, dynamic> result in resultsJson){
      TestTask testTask = TestTask.values[result["test_task"]];
      ResultData resultData = result.containsKey("result_data") ? ResultData.fromJSON(result["result_data"]) : ResultData(status: ResultDataStatus.OK);
      results.putIfAbsent(testTask, () => resultData);
    }
    return results;
  }

  ControlServerInfo.fromJson(Map<String, dynamic> jsonData) : this(
    sending: jsonData["sending_data"],
    speedDataStatus: jsonData.containsKey("speed_status") ? TaskDataStatus.values[jsonData["speed_status"]] : TaskDataStatus.WAIT,
    qosDataStatus: jsonData.containsKey("qos_status") ? TaskDataStatus.values[jsonData["qos_status"]] : TaskDataStatus.WAIT,
    ndtDataStatus: jsonData.containsKey("ndt_status") ? TaskDataStatus.values[jsonData["ndt_status"]] : TaskDataStatus.WAIT,
    results: jsonData.containsKey("results") ? _parseResult(jsonData["results"]) : null

  );

  @override
  String toString() {
    return  "SENDING $sending \n" +
           "SPEED DATA STATUS: $speedDataStatus \n" +
           "QOS DATA STATUS: $qosDataStatus \n" +
          "NDT DATA STATUS: $ndtDataStatus \n";
  }
}


enum ResultDataStatus{
  OK, FAILED
}

class ResultData{
  ResultDataStatus? status;
  String? errMsg;

  ResultData({
    this.status,
    this.errMsg
  });

  ResultData.fromJSON(Map<String, dynamic> jsonData) : this(
    status: ResultDataStatus.values[jsonData["status"]],
    errMsg: jsonData["errMsg"]
  );
}
