/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:netmetr_flutter/net/response/abstract_response.dart';
import 'package:netmetr_flutter/net/response/test_result_qos_reponse.dart';
import 'package:netmetr_flutter/net/response/test_result_response.dart';
import 'package:netmetr_flutter/pages/result_tabs/result_tab.dart';
import 'package:netmetr_flutter/pages/result_tabs/tab_item.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:netmetr_flutter/widgets/warning_widget.dart';
import 'package:url_launcher/url_launcher.dart';

import '../app_config.dart';
import 'help_page.dart';

class OfflineResultsPage extends StatefulWidget {
  final AppConfig config;
  OfflineResultsPage({
    Key? key,
    required this.config
  }) : super (key: key);


  @override
  _OfflineResultsPageState createState() => _OfflineResultsPageState();
}

class _OfflineResultsPageState extends State<OfflineResultsPage> with SingleTickerProviderStateMixin{
  final MethodChannel _resultsChannel = MethodChannel("nic.cz.netmetrflutter/off_result");
  final List<TabItem> _tabs = [TabItem.RESULT, TabItem.QOS];
  late TabController _controller;

  bool _haveData = false;
  bool _error = false;
  _OffData? _offData;

  @override
  void initState() {
    super.initState();
    _controller = TabController(length: 2, vsync: this);
    _downloadData();
  }

  void _downloadData() async{
    _resultsChannel.invokeMethod("getResults")
        .catchError((e){
            print("Error: $e");

            if(mounted){
              setState(() {
                _error = true;
              });
            }

         })
        .then((value){
          if(mounted && value != null) {
            print(value.toString());

            Map<String, dynamic> jsonData = json.decode(value);

            setState(() {
              _offData = _OffData.fromJson(jsonData);
              _haveData = true;
            });
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.getLocalizedValue('results')!),
        bottom: PreferredSize(
            child: Container(
              color: Colors.white,
              width: screenSize.width,
              alignment: Alignment.center,
              child: TabBar(
                  controller: _controller,
                  indicatorColor: Colors.blueGrey,
                  labelColor: Colors.black,
                  tabs: _tabs.map((tab){
                    return  new Tab(text: AppLocalizations.of(context)!.getLocalizedValue(tab.title),);
                  }).toList()
              ),
            ),
            preferredSize: Size.fromHeight(kToolbarHeight)),
        actions: [
          new IconButton(
              icon: new Icon(Icons.share),
              color: Colors.white,
              disabledColor: Colors.grey,
              onPressed: ()=> print("Share")
          ),
          new IconButton(
              icon: new Icon(Icons.help_outline),
              color: Colors.white,
              disabledColor: Colors.grey,
              onPressed: ()=> Platform.isAndroid && widget.config.deviceInfo?.systemOS <= 19 ? launch(AppLocalizations.of(context)!.getLocalizedValue('wwwHelp')!).catchError((error){print("Launch url error $error");}) : Navigator.push(context, new MaterialPageRoute(builder: (_)=> new HelpPage())))
        ],
      ),
      body: _haveData ? TabBarView(
        controller: _controller,
        children: [
          ResultTab(measurement: _offData!.speedData!.measurement, net: _offData!.speedData!.net),
          OffQOSTab(qos: _offData!.qos,)
        ]
      ) : !_error ? Container(
        alignment: Alignment.center,
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.blueGrey),
        ),
      ) : WarningWidget(textKey: "noData2",),
    );
  }
}

class OffQOSTab extends StatefulWidget {
  final Map<QosResultType, List<QosTestResult>>? qos;
  OffQOSTab({
    Key? key,
    this.qos
  }) : super(key: key);

  @override
  _OffQOSTabState createState() => _OffQOSTabState();
}

class _OffQOSTabState extends State<OffQOSTab> {
  Map<QosResultType, int> successMap = {};

  @override
  void initState() {
    super.initState();
    if(widget.qos != null){
      for(QosResultType type in widget.qos!.keys){
        List<QosTestResult> results = widget.qos![type]!;
        for(QosTestResult result in results){
          successMap.update(
              type,
              (value) => result.isSuccess() ? value +=1 : value,
              ifAbsent: ()=> result.isSuccess() ? 1 : 0
          );
        }
      }

      print(successMap.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: widget.qos != null ? Container(
        margin: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 10.0),
        child: ListView.builder(
          itemCount: widget.qos!.length,
          itemBuilder: (_, index){

            QosResultType type = widget.qos!.keys.toList()[index];
            List<QosTestResult>? results = widget.qos![type];
            int? success = successMap[type];

            return _OffQoSItem(
              type: type,
              results: results,
              success: success,
            );
          }
        ),
      ) : new WarningWidget(textKey: "noData2",),
    );
  }

}

class _OffQoSItem extends StatefulWidget {
  final QosResultType type;
  final List<QosTestResult>? results;
  final int? success;

  _OffQoSItem({
   Key? key,
   required this.type,
   required this.results,
   required this.success
  }) : super(key : key);

  @override
  __OffQoSItemState createState() => __OffQoSItemState();
}

class __OffQoSItemState extends State<_OffQoSItem> {
  bool show = false;


  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    TextStyle testTitle = themeData.textTheme.subtitle2!.copyWith(color: Colors.black);

    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.all(5.0),
      child: ExpansionPanelList(
        expansionCallback: (_, bool expand){
          setState(() {
            show = !expand;
          });
        },
        children: <ExpansionPanel>[
          ExpansionPanel(
            isExpanded: show,
            headerBuilder: (_, __) => new Row(
              children: <Widget>[
                new Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(left: 5.0),
                      alignment: Alignment.centerLeft,
                      child: new Text(widget.type.name!),
                    )
                ),
                new Expanded(
                    child: new Container(
                      alignment: Alignment.center,
                      child: new Text("${widget.success}/${widget.results!.length}"),
                    )
                ),
                new Expanded(
                    child: new Container(
                      alignment: Alignment.centerRight,
                      child: widget.success == widget.results!.length ? new Icon(Icons.check, color: Colors.green,) : new Icon(Icons.close, color: Colors.red,),
                    )
                )
              ],
            ),
            body: Container(
              padding: const EdgeInsets.all(5.0),
              child: Column(
                children: widget.results!.map((result){
                  int index = widget.results!.indexOf(result);
                  return Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Expanded(
                                child: new Text("TEST #${index + 1}", style: testTitle,)
                            ),
                            new Expanded(
                              child: new Container(
                                  width: double.maxFinite,
                                  child: result.isSuccess() ? new Icon(Icons.check, color: Colors.green,) : new Icon(Icons.close, color: Colors.red,),
                              ),
                            )
                          ],
                        ),
                        new Text(result.description()),
                        new Divider(
                          color: Colors.black,
                        )
                      ],
                    ),
                  );
                }).toList(),
              ),
            )
          )
        ],
      ),
    );
  }
}



class _OffData extends AbstractResponse{
   _SpeedData? speedData;
    Map<QosResultType, List<QosTestResult>>? qos;

   _OffData({
     this.speedData,
     this.qos
  });

   static Map<QosResultType, List<QosTestResult>>_parseQos(List<dynamic> data){
     Map<QosResultType, List<QosTestResult>> qos = {};

     for(Map<String, dynamic> jsonData in data){
      QosResultType type = QosResultType.getType(jsonData['test_type'])!;
      QosTestResult result = QosTestResult.createInstance(jsonData, type)!;

      qos.update(
          type,
          (value){
            value.add(result);
            return value;
          },
          ifAbsent: () => [result]);

     }

     return qos;
   }

   _OffData.fromJson(Map<String, dynamic> jsonData) : this(
     speedData: _SpeedData.fromJson(jsonData['speed']),
     qos: jsonData.containsKey('qos') ? _parseQos(jsonData['qos']) : null,
   );
}


class _SpeedData extends AbstractResponse{
  List<Measurement>? measurement;
  List<ResultData>? net;

  _SpeedData({
    this.measurement,
    this.net
  });
  
  _SpeedData.fromJson(Map<String, dynamic> jsonData) : this(
    measurement: jsonData.containsKey('measurement') ? AbstractResponse.parseList<Measurement>(jsonData['measurement']) : null,
    net: jsonData.containsKey('net') ? AbstractResponse.parseList<ResultData>(jsonData['net']) : null
  );
}