/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:netmetr_flutter/util/config_helper.dart';
import 'package:netmetr_flutter/util/dialog_helper.dart' as dialogHelper;
import 'package:netmetr_flutter/util/toast_helper.dart' as toastHelper;
import 'package:netmetr_flutter/widgets/loop_test_item.dart';

import '../app_config.dart';

const START_NEW_TEST = "NEW";
const LOOP_FAILED = "FAIL";
const TEST_FINISHED = "TEST_FINISHED";
const ABORTED = "ABORTED";

class LoopIOSPage extends StatefulWidget {
  final AppConfig config;
  LoopIOSPage({Key? key, required this.config}) : super(key: key);

  @override
  _LoopIOSPageState createState() => _LoopIOSPageState();
}

class _LoopIOSPageState extends State<LoopIOSPage> {
  static const loopChannel = const MethodChannel('nic.cz.netmetrflutter/loopChannel');
  static const loopEvent = const EventChannel('nic.cz.netmetrflutter/loopEvent');

  int? maxTest;
  late int runTest;
  late int finishedTest;
  bool running = false;

  bool dialogShowed = false;

  late List<LoopTestItem> loops;

  @override
  void initState() {
    super.initState();

    finishedTest = 0;
    runTest = 0;

    loops = [];
    _addLoopTestItem();

    maxTest = widget.config.appSettings.loopMaxTests;

    _startLoopService();
  }

  _addLoopTestItem(){
    loops.add(new LoopTestItem(
      index: runTest,
      finished: _finishedLoopTest,
      onlySignal: widget.config.appSettings.onlySignal,
      networkType: widget.config.networkModel.networkConnectionType,
      runJitter: widget.config.appSettings.jitter,
      runQoS: widget.config.appSettings.qos,
    ));
  }

  _restart(){
    finishedTest = 0;
    runTest = 0;

    setState(() {
      loops = [];
      _addLoopTestItem();
    });

    _startLoopService();
  }

  @override
  void dispose() {
    _stopLoopService();
    super.dispose();
  }

  Future _startLoopService() async{
    loopChannel.invokeMethod("startLoopService", {
      "uuid": ConfigHelper.instance!.uuid,
      "controlServer": widget.config.controlServerName(),
      "port": widget.config.configModel.port,
      "useSSL": widget.config.configModel.controlSSL,
      "qosSSL" : widget.config.configModel.qosSSL,
      "minDelay" : widget.config.appSettings.loopMinDelay,
      "maxDelay" : widget.config.appSettings.loopMaxDelay,
      "maxMovement" : widget.config.appSettings.loopMovementMeters + 0.0,
      "maxTests" : widget.config.appSettings.loopMaxTests,
      "onlySignal" : widget.config.appSettings.onlySignal,
      "jitter" : widget.config.appSettings.jitter,
      "qos" : widget.config.appSettings.qos
    }).then((_){

      running = true;

      loopEvent.receiveBroadcastStream().listen((value){
        Map<String, dynamic> jsonData = json.decode(value);
        String status = jsonData['status'];

        switch (status){
          case START_NEW_TEST:

            if(mounted) {
              setState(() {
                runTest++;
                _addLoopTestItem();
              });
            }

            break;
          case LOOP_FAILED:
            _checkDialog();
            dialogHelper.showStatusDialog(
              context: context,
              status: dialogHelper.DialogStatus.ERROR,
              title: AppLocalizations.of(context)!.getLocalizedValue('error')!,
              message:  AppLocalizations.of(context)!.getLocalizedValue('testFailed')!,
              ok: (){
                dialogShowed = false;
                _stopLoopService();
                Navigator.pop(context);
                Navigator.pop(context);
              }
            );

            dialogShowed = true;
            break;

          case ABORTED:
            setState(() {
              loops.clear();
            });

            running = false;
            _checkDialog();
            _showTestAbort();
            break;
        }
      });
    });
  }

  _showTestAbort(){
    dialogShowed = true;

    dialogHelper.showStatusDialog(
        context: context,
        status: dialogHelper.DialogStatus.INFO,
        title: AppLocalizations.of(context)!.getLocalizedValue("aborted_title")!,
        message: AppLocalizations.of(context)!.getLocalizedValue("aborted_desc")!,
        okButtonKey: 'but_start_againt',
        cancelButtonKey: 'but_dismiss',
        barrierDismissible: false,
        ok: (){
          dialogShowed = false;

          _restart();
          Navigator.pop(context);
        },
        cancel: (){
          dialogShowed = false;

          Navigator.pop(context);
          Navigator.pop(context);
        }
    );
  }

  _finishedLoopTest(int? index, bool success){
    if(mounted && running) {
      setState(() {
        finishedTest++;
      });
    }

    if(finishedTest == maxTest){
      _stopLoopService();
      Navigator.pop(context, true);
    }
  }

  _checkDialog(){
    if(dialogShowed) {
      Navigator.pop(context);
    }
  }

  Future _stopLoopService() async{
    try{
      running = false;
      await loopChannel.invokeMethod("stopLoopService");
    } on PlatformException catch (e){
    print(e.message);
    }
  }

  _showLoopDetails(){
    AppLocalizations localizations = AppLocalizations.of(context)!;

    toastHelper.showInfoToast(
        "${localizations.getLocalizedValue('test_cnt')}: $maxTest\n"
        "${localizations.getLocalizedValue('movement')}: ${widget.config.appSettings.loopMovementMeters} m\n"
        "${localizations.getLocalizedValue('signal_only')}: ${widget.config.appSettings.onlySignal ? localizations.getLocalizedValue('yes') : localizations.getLocalizedValue('no')}\n"
        "${localizations.getLocalizedValue('min_delay')}: ${widget.config.appSettings.loopMinDelay} s\n"
        "${localizations.getLocalizedValue('max_delay')}: ${widget.config.appSettings.loopMaxTests} s"
    );
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations localizations = AppLocalizations.of(context)!;
    MediaQueryData queryData = MediaQuery.of(context);
    ThemeData themeData = Theme.of(context);
    TextStyle testCounterStyle = themeData.textTheme.headline5!.copyWith(color: Colors.white);

    return new WillPopScope(
      child: new Scaffold(
        appBar: new AppBar(
          title: new Text(localizations.getLocalizedValue('loop_title')!),
          actions: <Widget>[
            new IconButton(icon: new Icon(Icons.info_outline, color: Colors.white,), onPressed: ()=> _showLoopDetails())
          ],
        ),
        body: new SingleChildScrollView(
          child: new Container(
              padding: const EdgeInsets.all(5.0),
              child:  new Column(
                children: loops.map((loopTest)=>loopTest).toList(),
              )
          ),
        ),
        bottomNavigationBar: new Container(
          width: queryData.size.width,
          height: queryData.size.height * 0.12,
          color: themeData.colorScheme.secondary,
          alignment: Alignment.center,
          child: new Text(maxTest == 0 ? "$finishedTest" : "$finishedTest / $maxTest", style: testCounterStyle,),
        ),
      ),
      onWillPop: _onBackPressed);
  }

  Future<bool> _onBackPressed(){
    _checkDialog();

    dialogHelper.showStatusDialog(
        context: context,
        status: dialogHelper.DialogStatus.WARNING,
        title: AppLocalizations.of(context)!.getLocalizedValue('abortTitle')!,
        message: AppLocalizations.of(context)!.getLocalizedValue('sureAbort')!,
        okButtonKey: 'abortIt',
        cancelButtonKey: 'continueTest',
        ok: (){
          _stopLoopService();
          dialogShowed = false;
          Navigator.pop(context);
          Navigator.pop(context);
        },
        cancel: (){
          dialogShowed = false;
          Navigator.pop(context);
        }
    );

    dialogShowed = true;
    return Future.value(false);
  }
}
