/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/net/response/test_result_qos_reponse.dart';

class TabDetailTestPage extends StatefulWidget {
  final List<ResultDetail> results;
  final int startIndex;
  TabDetailTestPage({
    Key? key,
    required this.results,
    this.startIndex = 0}) : super(key: key);

  @override
  _TabDetailTestPageState createState() => _TabDetailTestPageState();
}

class _TabDetailTestPageState extends State<TabDetailTestPage> {

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    TextStyle title = themeData.textTheme.headline6!.copyWith(color: Colors.white);


    return new DefaultTabController(length: widget.results.length,
      initialIndex: widget.startIndex,
      child: new Scaffold(
        appBar: new AppBar(
          title: new Text("QoS", style: title,),
          bottom: new TabBar(
            isScrollable: true,
            tabs: widget.results.map((ResultDetail result){
              return Tab(text: "Test #${widget.results.indexOf(result) + 1}");
            }).toList(),
          ),
        ),
        body: new TabBarView(
            children: widget.results.map((ResultDetail result){
          return new SingleChildScrollView(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                new Container(
                  width: double.maxFinite,
                  padding: const EdgeInsets.all(5.0),
                  color: Colors.black,
                  child: new Text("POPIS", style: title,),
                ),
                new Container(
                  padding: const EdgeInsets.all(5.0),
                  child: new Text(result.testSummary!),
                ),
                new Container(
                  width: double.maxFinite,
                  padding: const EdgeInsets.all(5.0),
                  color: Colors.blueGrey,
                  child: new Text(result.failure == 0 ? "Úspěšný" : "Selhání", style: title,),
                ),
                new Container(
                  width: double.maxFinite,
                  padding: const EdgeInsets.all(5.0),
                  color: result.failure == 0 ? Colors.green : Colors.red,
                  child: new Text(result.resultString!, style: new TextStyle(color: Colors.white),),
                ),
                new Container(
                  width: double.maxFinite,
                  padding: const EdgeInsets.all(5.0),
                  color: Colors.black,
                  child: new Text("DETAILY", style: title,),
                ),
                new Container(
                  padding: const EdgeInsets.all(5.0),
                  child: new Text(result.testDesc!),
                )
              ],
            ),
          );
        }).toList()),
      ),
    );
  }
}
