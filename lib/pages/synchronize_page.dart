/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/app_config.dart';
import 'package:netmetr_flutter/pages/get_sync_code_page.dart';
import 'package:netmetr_flutter/pages/set_sync_code_page.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';

class SynchronizePage extends StatefulWidget {
  final AppConfig appConfig;
  final Function() showHistory;

  SynchronizePage({
    Key? key,
    required this.appConfig,
    required this.showHistory,
  }) : super (key: key);

  @override
  _SynchronizePageState createState() => _SynchronizePageState();
}

class _SynchronizePageState extends State<SynchronizePage> {

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    TextStyle textStyle = themeData.textTheme.bodyText2!.copyWith(color: Colors.black);
    TextStyle buttonStyle = themeData.textTheme.bodyText2!.copyWith(color: Colors.white);
    Color accentColor = themeData.colorScheme.secondary;

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(AppLocalizations.of(context)!.getLocalizedValue('sync')!),
      ),
      body: new Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          new Container(
            margin: const EdgeInsets.all(10.0),
            child: new Image.asset("assets/ic_action_home.png", color: Colors.blueGrey,),
          ),
          new Expanded(
            child:  new Container(
              alignment: Alignment.center,
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Text(AppLocalizations.of(context)!.getLocalizedValue('syncInfo')!,
                    textAlign: TextAlign.center,
                    style: textStyle,
                  ),
                  new Padding(padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: new ElevatedButton(onPressed: ()=> Navigator.push(context, new MaterialPageRoute(builder: (_)=> new GetSyncCodePage(appConfig: widget.appConfig,))),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(accentColor)
                      ),
                      child: new Text(AppLocalizations.of(context)!.getLocalizedValue('getCode')!, style: buttonStyle,),
                    ),
                  ),
                  new Padding(padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: new ElevatedButton(onPressed: ()=> Navigator.push(context, new MaterialPageRoute(builder: (_)=> new SetSyncCodePage(appConfig: widget.appConfig, showHistory: widget.showHistory,))),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(accentColor)
                      ),
                      child: new Text(AppLocalizations.of(context)!.getLocalizedValue('insertCode')!, style: buttonStyle,),
                    ),
                  )
                ],
              ),
            )
          )
        ],
      ),
    );
  }
}
