/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:netmetr_flutter/net/response/test_result_qos_reponse.dart';
import 'package:netmetr_flutter/net/rest_client.dart';
import 'package:netmetr_flutter/pages/offline_results_page.dart';
import 'package:netmetr_flutter/test/client_error.dart';
import 'package:netmetr_flutter/test/control_server_info.dart';
import 'package:netmetr_flutter/test/qos_response.dart';
import 'package:netmetr_flutter/test/test_data.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:netmetr_flutter/util/config_helper.dart';
import 'package:netmetr_flutter/test/test_response.dart';
import 'package:netmetr_flutter/util/location/show_map_data.dart';
import 'package:netmetr_flutter/util/network/network_data.dart';
import 'package:netmetr_flutter/util/network/network_connection_type.dart';
import 'package:netmetr_flutter/util/network/network_type.dart';
import 'package:netmetr_flutter/widgets/chart_widget.dart';
import 'package:netmetr_flutter/widgets/speed_graph.dart';
import 'package:netmetr_flutter/pages/result_test_page.dart';
import 'package:netmetr_flutter/app_config.dart';
import 'package:netmetr_flutter/net/rest_client.dart' as restClient;
import 'package:netmetr_flutter/util/widget_helper.dart' as widgetHelper;
import 'package:netmetr_flutter/util/math_helper.dart' as mathHelper;
import 'package:netmetr_flutter/util/dialog_helper.dart' as dialogHelper;

enum TestStatus{ STOP, RUNNING}

const SERVICE_RUNNING = "running";
const SERVICE_FINISH = "finish";
const SERVICE_ABORTED = "aborted";
const UPDATE_PROGRESS = "update_progress";
const TEST_FAILED = "failed";

const SNACK_DELAY_SEC = 3;

class RunTestPage extends StatefulWidget {
  final Function(ShowMapData) showTestInMap;
  final NetworkData? networkData;
  final AppConfig config;

  RunTestPage({
    required this.networkData,
    required this.config,
    required this.showTestInMap,
  });

  @override
  _RunTestPageState createState() => _RunTestPageState();
}

class _RunTestPageState extends State<RunTestPage> {
  static const serviceChannel =  const MethodChannel('nic.cz.netmetrflutter/serviceChannel');
  static const runTestChannel = const EventChannel('nic.cz.netmetrflutter/runTestReceiver');
  static const infoChannel = const EventChannel('nic.cz.netmetrflutter/infoChannel');

  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;
  bool _fatalError = false;

  static Color speedColor = Colors.green;
  static Color signalColor = Colors.amber;

  final _scaffoldState = GlobalKey<ScaffoldState>();
  Timer? _snackTimer;
  TestTask? _previousTask;
  
  bool serviceRun = true;
  TaskStatus taskStatus = TaskStatus.WAIT;

  double? pingNano;
  double downBitPerSec = 0.0;
  double upBitPerSec = 0.0;
  double progress = 0.0;
  double qosProgress = 0.0;
  int? signal;

  JitterStatus jitterStatus = JitterStatus.NOT_STARTED;
  int? jitter;
  double? packetLoss;

  SignalType signalType = SignalType.NO_SIGNAL;

  bool downFinished = false;

  TestStatus testStatus = TestStatus.STOP;
  TestNetworkData? testNetworkData;
  NetworkType? networkType;

  Map<QosResultType, QosResponse>? qosResponses;

  //chart data
  Map<int, Color> colorChars = { 0: speedColor, 1: signalColor};
  Map<int, List<dynamic>> rowValues = {0: [], 1: []};
  Map<int, List<PartInterval>> rowIntervals = {0: [], 1: []};
  Map<int, List<dynamic>> chartsData = {0: [], 1: []};
  List<double> speedDataProgress = [];
  int rows = 4;
  bool dialogShowed = false;
  bool testFailed = false;
  bool runJitter = true;
  bool runQoS = true;

  //NDT
  bool ndt = false;
  NDTStatus ndtStatus = NDTStatus.NOT_STARTED;
  SpeedGraphType speedGraphType = SpeedGraphType.TYPE_1;

  @override
  void initState() {
    super.initState();
    _initStartData();

    _connectivitySubscription = _connectivity.onConnectivityChanged.listen((ConnectivityResult connectivity) {
      if(mounted && connectivity == ConnectivityResult.none)
        _connectionLost();
    });

    _startService();
  }
  
  

  _restart(){
    serviceRun = true;
    taskStatus = TaskStatus.INIT;

    _fatalError = false;
    pingNano = null;
    downBitPerSec = 0.0;
    upBitPerSec = 0.0;
    progress = 0.0;
    qosProgress = 0.0;
    signal = null;
    signalType = SignalType.NO_SIGNAL;
    testNetworkData = null;
    networkType = null;

    downFinished = false;

    testStatus = TestStatus.STOP;

    qosResponses?.clear();

    colorChars = { 0: Colors.green, 1: Colors.amber};
     rowValues = {0: [], 1: []};
     rowIntervals = {0: [], 1: []};
     chartsData = {0: [], 1: []};
     speedDataProgress = [];

    ndtStatus = NDTStatus.NOT_STARTED;

    _initStartData();

    if(mounted) {
      setState(() {

      });
    }

    _startService();
  }

  _initStartData(){
    qosResponses = new Map();

    var speedSegments = SpeedSegment.values();
    rows = speedSegments.length - 1;

    if(widget.networkData != null) {
      signalType = widget.networkData!.signalType;
      signal = widget.networkData!.signal;

      NetworkGraphType networkGraphType = NetworkGraphType.getType(signalType);

      for(int i = 0; i <= rows; i++){
        if(i == 0) rowValues[1]!.add(networkGraphType.min);
        else if(i == rows) rowValues[1]!.add(networkGraphType.max);
        else rowValues[1]!.add(null);
      }

      rowIntervals[1]!.add(new PartInterval(networkGraphType.min, networkGraphType.max));
    }

    _resetSpeedGraphData(speedGraphType);
  }

  _resetSpeedGraphData(SpeedGraphType st){
    rowValues[0]!.clear();
    rowIntervals[0]!.clear();

    for(SpeedSegment speedSegment in SpeedSegment.values()){
      rowValues[0]!.add(st.values[speedSegment.startIndexValue]);
      if(speedSegment != SpeedSegment.S5) {
        rowIntervals[0]!.add(new PartInterval(st.values[speedSegment.startIndexValue], st.values[speedSegment.startIndexValue + 1]));
      }
    }
  }


  @override
  void dispose() {
    _stopService();
    _hideInfo();
    _connectivitySubscription.cancel();

    if(widget.config.networkModel.networkConnectionType != NetworkConnectionType.NOT_CONNECTED) {
      restClient.checkSettingsTask(widget.config).catchError((error){
        //ignore
      });
    }

    if(_snackTimer != null){
      _snackTimer!.cancel();
    }
    
    super.dispose();
  }

  _checkDialog(){
    if(dialogShowed && !_fatalError){
      dialogShowed = false; 
      Navigator.pop(context);
    }
  }

  Future _continueService() async {
    await serviceChannel.invokeMethod("continueService");
  }

  Future _startService() async {
    try {
      print("--test--");

      serviceRun = true;
      String uuid = ConfigHelper.instance!.uuid;
      String controlServer = widget.config.controlServerName();
      int port = widget.config.configModel.port;
      bool useSSL = widget.config.configModel.controlSSL;
      bool qosSSL = widget.config.configModel.qosSSL;
      bool onlyIpv4 = widget.config.appSettings.onlyIpv4;
      ndt = widget.config.appSettings.ndt;
      runJitter = widget.config.appSettings.jitter;
      runQoS = widget.config.appSettings.qos;

      print("START TEST: $uuid");

      await serviceChannel.invokeMethod("startService", {
        "uuid": uuid,
        "controlServer": controlServer,
        "port": port,
        "useSSL": useSSL,
        "qosSSL" : qosSSL,
        "runNDT" : ndt,
        "onlyIPv4" : onlyIpv4,
        "jitter" : runJitter,
        "qos" : runQoS
      });

      testStatus = TestStatus.RUNNING;

      runTestChannel.receiveBroadcastStream(1).listen((value) {
        if (value != null && serviceRun) {
          Map<String, dynamic> jsonData = json.decode(value);
          String status = jsonData['serviceStatus'];

          switch(status){

            case SERVICE_RUNNING:

              if(jsonData.containsKey('networkData')){
                testNetworkData = TestNetworkData.fromJson(jsonData['networkData']);
                if(testNetworkData!.networkType == NetworkConnectionType.NOT_CONNECTED.index){
                    _connectionLost();
                    break;
                }
                networkType = NetworkType.getType(testNetworkData!.networkType!);
              }

              if(jsonData.containsKey("ndtData")){
                ndtStatus = NDTStatus.values[jsonData["ndtData"]];
              }

              if(jsonData.containsKey('data')) {
                TestResponse testResponse = TestResponse.fromJson(
                    jsonData['data']);

                if (testResponse.taskStatus == TaskStatus.END)
                  _stopService();

                if (testResponse.taskStatus == TaskStatus.UP)
                  downFinished = true;

                if(mounted) {
                  setState(() {
                    if (Platform.isAndroid) {
                      if (taskStatus == TaskStatus.INIT_UP &&
                          testResponse.taskStatus == TaskStatus.UP) {
                        speedDataProgress.clear();
                        chartsData[0]!.clear();
                        chartsData[1]!.clear();
                      }

                      taskStatus = testResponse.taskStatus!;
                      progress = 0.0 + testResponse.progress!;
                    }

                    pingNano = 0.0 + testResponse.pingNano!;
                    downBitPerSec = 0.0 + testResponse.downBitPerSec!;
                    upBitPerSec = 0.0 + testResponse.upBitPerSec!;
                    signal = testResponse.signal;
                    signalType = testResponse.signalType;

                    jitterStatus = testResponse.jitterStatus;

                    if(jitterStatus == JitterStatus.FINISH) {
                      JitterData up = testResponse.up!;
                      JitterData down = testResponse.down!;

                      jitter = _jitter(down, up);
                      packetLoss = _packetLoss(down, up);
                    }

                    if (taskStatus == TaskStatus.DOWN || taskStatus == TaskStatus.UP) {
                      double speedValue = (taskStatus == TaskStatus.DOWN ? downBitPerSec : upBitPerSec) / 1000000.0;

                      SpeedGraphType st = SpeedGraphType.getSpeedGraphTypeBySpeed(speedValue);
                      if(st != speedGraphType){
                        _resetSpeedGraphData(st);
                      }
                      speedGraphType = st;

                      speedDataProgress.add(progress);
                      chartsData[0]!.add(speedValue);
                      chartsData[1]!.add(signal);
                    }
                  });
                }
              }

              if(jsonData.containsKey('qosTest')){
                Map<String, dynamic> qosData = jsonData['qosTest'];

                double sumProgress = 0.0;

                for(var data in qosData.values){
                    QosResponse response = QosResponse.fromJson(data);
                    sumProgress += response.progress!;
                    qosResponses!.update(response.type!, (value){
                      value.progress = response.progress;
                      value.target = response.target;
                      value.value = response.value;
                      value.firstTest = response.firstTest;
                      value.lastTest = response.lastTest;

                      return value;
                    }, ifAbsent: ()=>response);
                }

                if(mounted) {
                  setState(() {
                    if (Platform.isAndroid) {
                      qosProgress = sumProgress / qosData.values.length;
                    }
                  });
                }
              }

              break;
            case UPDATE_PROGRESS:

              if(mounted && jsonData.containsKey('data') && Platform.isIOS){
                Map<String, dynamic> data = jsonData['data'];

                if(mounted) {
                  setState(() {
                    var newStatus = TaskStatus.values[data["status"]];

                    if (taskStatus == TaskStatus.INIT_UP &&
                        newStatus == TaskStatus.UP) {
                      speedDataProgress.clear();
                      chartsData[0]!.clear();
                      chartsData[1]!.clear();
                    }

                    taskStatus = newStatus;

                    if (data.containsKey('progress')) {
                      progress = 0.0 + data['progress'];
                    }

                    if (data.containsKey('qosProgress')) {
                      qosProgress = 0.0 + data['qosProgress'];
                    }
                  });
                }
              }

              break;

            case SERVICE_FINISH:
              print("------------- FINISH --------------");
              if(_snackTimer != null){
                _snackTimer!.cancel();
              }

              _hideInfo();

             if(!jsonData.containsKey('test_uuid')){
               testFailed = true;
               _checkDialog();
               _showTestError(AppLocalizations.of(context)!.getLocalizedValue('final_test_error')!, true, false);
               break;
             }


              if(mounted && jsonData.containsKey("ndtData")){
                setState(() {
                  ndtStatus = NDTStatus.values[jsonData["ndtData"]];
                });
              }

            AppConfig.of(context)!.configModel.historyDevices.add(ConfigHelper.instance!.basicInfo!.device);
            AppConfig.of(context)!.configModel.historyNetworks.add(widget.networkData!.signalType.toString());

            String testUuid = jsonData['test_uuid'];


            TestData testData = new TestData(status: ResultStatus.OK, testUuid: testUuid);
            restClient.testResult(widget.config, testUuid)
                .catchError((error){
                    _checkDialog();

                    testFailed = true;

                    if(error is ResponseError) {
                      _showTestError("ERR CODE: ${error.code} MSG: ${error.description != null && error.description!.isNotEmpty ? error.description : " - "}", true, false);
                    }
                })
                .then((testResponse){
                   if(!mounted || testResponse == null)
                    return;

                  testData.testResultResponse = testResponse;

                  _checkDialog();

                  Navigator.pushReplacement(context, new MaterialPageRoute<ShowMapData>(builder: (_)=> new ResultTestPage(testData: testData, config: widget.config,))).then((ShowMapData? value){
                    if(value != null){
                      widget.showTestInMap(value);
                    }
                  });
            });

            break;

            case TEST_FAILED:
              if(_snackTimer != null){
                _snackTimer!.cancel();
                _hideInfo();
              }

              _checkDialog();
              testFailed = true;

              String error = "-";
              if(jsonData.containsKey('client_error')){
                ClientError clientError = ClientError.fromJson(jsonData['client_error']);
                error = clientError.connectionError! ? AppLocalizations.of(context)!.getLocalizedValue('connection_lost_error')!: clientError.errorMessage!;
              }

              _showTestError(error, false, true);
              _stopService();
              break;

            case SERVICE_ABORTED:
              serviceRun = false;
              testStatus = TestStatus.STOP;
              _checkDialog();
              _showTestAbort();
              break;
          }
        }
      });

      infoChannel.receiveBroadcastStream(2).listen((event) {
        if(event == null)
          return;

        Map<String, dynamic> jsonData = json.decode(event);
        ControlServerInfo serverInfo = ControlServerInfo.fromJson(jsonData);

        if(_snackTimer != null){
          _snackTimer!.cancel();
          _hideInfo();
        }

        if(serverInfo.sending) {
          _previousTask = serverInfo.sendingTestTask()!;
          _snackTimer = Timer(const Duration(seconds: SNACK_DELAY_SEC), () {
            _showInfo(serverInfo.infoKey());
          });
        }else if(_previousTask != null && serverInfo.results!.containsKey(_previousTask)){

          ResultData data = serverInfo.results![_previousTask]!;
          if(data.status == ResultDataStatus.FAILED && mounted){
            _checkDialog();
            _showServerInfoDialog(_previousTask!, data);
          }
        }

      });

    } on PlatformException catch (e) {
      print(e.message);
    }
  }

  _connectionLost(){
    _checkDialog();
    _showTestError(AppLocalizations.of(context)!.getLocalizedValue('connection_lost_error')!, false, true);
    _stopService();

  }

  double _packetLoss(JitterData down, JitterData up){
    return 100.0 - ((down.numPacket + up.numPacket) / (down.totalPacket + up.totalPacket)) * 100.0;
  }

  int? _jitter(JitterData down, JitterData up){
    if(down.meanJitter == null && up.meanJitter == null) return null;
    else if(down.meanJitter == null) return up.meanJitter;
    else if(up.meanJitter == null) return down.meanJitter;

    return ((down.meanJitter! + up.meanJitter!) / 2).round();

  }

  _showServerInfoDialog(TestTask testTask, ResultData data){
    if(_fatalError)
      return;

    AppLocalizations localizations = AppLocalizations.of(context)!;

    dialogShowed = true;
    dialogHelper.showStatusDialog(
        context: context,
        status: dialogHelper.DialogStatus.ERROR,
        barrierDismissible: false,
        title: localizations.getLocalizedValue(testTask.errorKey)!,
        message: "ERR: ${ data.errMsg}",
        footnote: testTask == TestTask.SPEED ? localizations.getLocalizedValue('footnote') : null,
        okButtonKey: 'abortIt',
        cancelButtonKey: 'continueTest',
        ok: (){
          if(!mounted)
            return;

          _stopService();

          Navigator.pop(context);
          Navigator.pop(context);
        },
        cancel: (){
          if(!mounted)
            return;

          _continueService();
          Navigator.pop(context);
        }
    ).catchError((e)=> dialogShowed = false).whenComplete(() => dialogShowed = false);
  }

  _showTestAbort(){
    if(_fatalError)
      return;

    _fatalError = true;

    if(dialogShowed) {
      Navigator.of(context).pop();
    }

    dialogShowed = true;

    dialogHelper.showStatusDialog(
      context: context,
      status: dialogHelper.DialogStatus.INFO,
      title: AppLocalizations.of(context)!.getLocalizedValue("aborted_title")!,
      message: AppLocalizations.of(context)!.getLocalizedValue("aborted_desc")!,
      okButtonKey: 'but_start_againt',
      cancelButtonKey: 'but_dismiss',
      barrierDismissible: false,
      ok: (){
        if(!mounted)
          return;

        _restart();
        Navigator.pop(context);
      },
      cancel: (){
        if(!mounted)
          return;
        Navigator.pop(context);
        Navigator.pop(context);
      }
    ).catchError((e)=> dialogShowed = false).whenComplete(() => dialogShowed = false);
  }

  _showTestError(String msg, bool finish, bool fatal){
    if(_fatalError)
      return;

    _fatalError = fatal;
    dialogShowed = true;

    dialogHelper.showStatusDialog(
        context: context,
        status: dialogHelper.DialogStatus.ERROR,
        barrierDismissible: false,
        title: AppLocalizations.of(context)!.getLocalizedValue('testFailed')!,
        message: msg,
        cancelButtonKey: 'show_data',
        ok: (){
          if(!mounted)
            return;

          Navigator.pop(context);
          Navigator.pop(context);
        },
        cancel: finish ?(){
          if(!mounted)
          return;

          Navigator.pop(context);
          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_)=> OfflineResultsPage(config: widget.config,)));
        } : null
    ).catchError((e)=> dialogShowed = false).whenComplete(() => dialogShowed = false);
  }

  Future _stopService() async{
    if(testStatus == TestStatus.STOP || !serviceRun)
      return;

    try{
      serviceRun = false;
      await serviceChannel.invokeMethod("stopService");
      testStatus = TestStatus.STOP;
    } on PlatformException catch (e){
      print(e.message);
    }
  }

  Future<bool> _onBackPressed(){
    dialogHelper.showStatusDialog(
        context: context,
        status: dialogHelper.DialogStatus.WARNING,
        title: AppLocalizations.of(context)!.getLocalizedValue('abortTitle')!,
        message: AppLocalizations.of(context)!.getLocalizedValue('sureAbort')!,
        okButtonKey: 'abortIt',
        cancelButtonKey: 'continueTest',
        ok: (){
          if(!mounted)
            return;

          _stopService();

          Navigator.pop(context);
          Navigator.pop(context);
        },
        cancel: (){
          if(!mounted)
            return;

          Navigator.pop(context);
        },
    ).catchError((e)=> dialogShowed = false).whenComplete(()=> dialogShowed = false);

    dialogShowed = true;

    return Future.value(false);
  }


  _hideInfo(){
    if(_scaffoldState.currentState != null) {
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
    }
  }

  _showInfo(String? msgKey){
    if(_scaffoldState.currentState != null) {
      ScaffoldMessenger.of(context).showSnackBar( SnackBar(
          backgroundColor: Colors.blueGrey,
          duration: Duration(hours: 1),
          content: InfoWidget(msgKey: msgKey,)
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData = MediaQuery.of(context);
    double screenHeight = queryData.size.height;

    ThemeData themeData = Theme.of(context);
    TextStyle result = themeData.textTheme.subtitle2!.copyWith(color: Colors.black);
    TextStyle textDataStyle = themeData.textTheme.bodyText1!.copyWith(color: Colors.black);
    TextStyle textStyle = new TextStyle(color: Colors.black);

    var down = downBitPerSec / 1000000.0;
    var up = upBitPerSec / 1000000.0;

    double chartWidth = queryData.size.width * 0.85;
    double chartHeight = chartWidth * 0.32;
    double topInfoWidth = queryData.size.width * 0.4;

    return new WillPopScope(
        child: new Scaffold(
          key: _scaffoldState,
          body: new Container(
              margin: const EdgeInsets.only(top: 20.0),
              height: screenHeight,
              padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
              child: new SingleChildScrollView(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Container(
                      alignment: Alignment.centerRight,
                      width: double.maxFinite,
                      child: new Stack(
                        children: <Widget>[
                          new Container(
                            width: topInfoWidth,
                            child: new Column(
                              children: <Widget>[
                                new Row(
                                  children: <Widget>[
                                    new Text(AppLocalizations.of(context)!.getLocalizedValue('init')!, style: textStyle,),
                                    taskStatus.index > TaskStatus.INIT.index ?
                                    new Icon(Icons.check, color: Colors.green,) :
                                    widgetHelper.getSmallProgress()
                                  ],
                                ),
                                runJitter && jitterStatus != JitterStatus.DISABLED ? new Row(
                                  children: <Widget>[
                                    new Text("Jitter", style: textStyle,),
                                    jitterStatus.index > JitterStatus.RUNNING.index ?
                                    jitterStatus.index == JitterStatus.FAIL.index ? new Icon(Icons.close, color: Colors.red,) : new Icon(Icons.check, color: Colors.green,) :
                                    widgetHelper.getSmallProgress()
                                  ],
                                ) : Container(),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Text(
                                            "Ping ${pingNano != null ? "${(pingNano! / 1000000.0).round()} ms" : " - "}",
                                            style: textStyle,
                                            overflow: TextOverflow.clip,
                                            softWrap: true,
                                          ),
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Text(
                                            "Download ${"${taskStatus.index >= TaskStatus.DOWN.index ? down >= 1.0 ? down.round() : mathHelper.roundDouble(down, 2) : " - "} Mbit/s"}",
                                            style: textStyle,
                                            overflow: TextOverflow.clip,
                                            softWrap: true,
                                          ),
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Text(
                                              "Upload ${"${taskStatus.index >= TaskStatus.UP.index ? up >= 1.0 ? up.round() : mathHelper.roundDouble(up, 2) : " - "} Mbit/s" }",
                                              style: textStyle,
                                              overflow: TextOverflow.clip,
                                              softWrap: true
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                          new Container(
                            margin: const EdgeInsets.only(top: 30.0),
                            alignment: Alignment.center,
                            child: new SpeedGraph(
                              currentProgress: progress,
                              taskStatus: taskStatus,
                              downBitPerSec: downBitPerSec,
                              upBitPerSec: upBitPerSec,
                              signal: signal,
                              signalType: signalType,
                              qosProgress: qosProgress,
                              qosEnable: runQoS,
                              speedGraphType: speedGraphType,
                            ),
                          ),
                          Platform.isIOS ? new Container(
                            margin: const EdgeInsets.all(2.0),
                            alignment: Alignment.topRight,
                            child: new IconButton(
                                icon: new Icon(Icons.exit_to_app, color: Colors.black,),
                                onPressed: _onBackPressed
                            ),
                          ) : new Container()
                        ],
                      ),
                    ),
                    _divider(),
                    runJitter && jitterStatus != JitterStatus.DISABLED ?
                    new Column(
                      children: <Widget>[
                        new Container(
                          margin: const EdgeInsets.only(top: 10),
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              new Expanded(
                                  child: new Container(
                                    alignment: Alignment.center,
                                    child: new Column(
                                      children: <Widget>[
                                        new Text("Jitter", style: textDataStyle,),
                                        new Padding(padding: const EdgeInsets.only(top: 5.0),
                                        child: new Text((jitter == null || jitter == -1 || jitter!.isNaN) ? "-" : "${mathHelper.roundDouble(jitter! / 1000000, 2)} ms", style: textStyle,),
                                        )
                                      ],
                                    ),
                                  )
                              ),
                              new Expanded(
                                  child: new Container(
                                    alignment: Alignment.center,
                                    child: new Column(
                                      children: <Widget>[
                                        new Text(AppLocalizations.of(context)!.getLocalizedValue("packet_loss")!, style: textDataStyle,),
                                        new Padding(padding: const EdgeInsets.only(top: 5.0),
                                          child: new Text(mathHelper.checkDoubleValue(packetLoss) && packetLoss != -1 ? "${mathHelper.roundDouble(packetLoss!, 1)} %" : " - ", style: textStyle,),
                                        )
                                      ],
                                    ),
                                  )
                              )
                            ],
                          ),
                        ),
                        _divider(),
                      ],
                    )
                        : new Container(),
                    taskStatus.index <= TaskStatus.SPEEDTEST_END.index ? new Container(
                      padding: const EdgeInsets.only(top: 20.0) ,
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Container(
                            alignment: Alignment.center,
                            child: new ChartWidget(
                              chartsData: chartsData,
                              chartsColor: colorChars,
                              partInterval: rowIntervals,
                              rowValues: rowValues,
                              ratioData: speedDataProgress,
                              rows: rows,
                              width: chartWidth,
                              height: chartHeight,
                            ),
                          ),
                          new Container(
                            padding: const EdgeInsets.only(top: 10.0),
                            width: queryData.size.width,
                            alignment: Alignment.center,
                            child: new Row(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                               Flexible(
                                 child: Row(
                                   children: [
                                     new Container(
                                       width: 10,
                                       height: 10,
                                       color: speedColor,
                                     ),
                                     Expanded(
                                       child: new Container(
                                         margin: const EdgeInsets.only(left: 5.0),
                                         child: new Text(
                                           "${taskStatus.index <= TaskStatus.DOWN.index ? "Download" : "Upload" } (Mbit/s)",
                                           style: textStyle,
                                           overflow: TextOverflow.clip,
                                           softWrap: true,
                                         ),
                                       )
                                     ),
                                   ],
                                 )
                               ),
                                Flexible(
                                  child: Row(
                                    children: [
                                      new Container(
                                        margin: const EdgeInsets.only(left: 10.0),
                                        width: 10,
                                        height: 10,
                                        color: signalColor,
                                      ),
                                      Expanded(
                                        child: new Container(
                                          margin: const EdgeInsets.only(left: 5.0),
                                          child: new Text(
                                            "Signal (dBm)",
                                            style: textStyle,
                                            overflow: TextOverflow.clip,
                                            softWrap: true,
                                          ),
                                        )
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          new Container(
                              padding: const EdgeInsets.only(top: 10.0),
                              alignment: Alignment.center,
                              child: new Text(_getNetworkName(context), style: textStyle,)
                          ),
                          _divider(),
                          new Container(
                            margin: const EdgeInsets.all(5.0),
                            width: double.maxFinite,
                            child: new Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                new Row(
                                    children: [
                                      new Expanded(
                                        flex: 2,
                                        child: new Text("IP: ", textAlign: TextAlign.left, style: textStyle,),
                                      ),
                                      new Expanded(
                                          flex: 8,
                                          child: new Text((testNetworkData != null && testNetworkData!.ip != null && testNetworkData!.ip!.isNotEmpty ? testNetworkData!.ip : " - ")!, textAlign: TextAlign.left, style: textStyle,)
                                      )
                                    ]
                                ),
                                new Row(
                                    children: [
                                      new Expanded(
                                        flex: 2,
                                        child:  new Text("Server: ", textAlign: TextAlign.left, style: textStyle,),
                                      ),
                                      new Expanded(
                                          flex: 8,
                                          child: new Text((testNetworkData != null && testNetworkData!.serverName != null && testNetworkData!.serverName!.isNotEmpty ? testNetworkData!.serverName : " - ")!, textAlign: TextAlign.left, style: textStyle,)
                                      )
                                    ]
                                )
                              ],
                            ),
                          )
                        ],
                      ),

                    ) :
                    runQoS && qosResponses!.isNotEmpty ? new Container(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: new Column(
                        children: <Widget>[
                          ndt ? _ndt(result) : new Container(),
                          new Column(
                            children: qosResponses!.values.map((qos){
                              String name = qos.getTestName();
                              double progress = qos.progress!;

                              return new Container(
                                margin: EdgeInsets.symmetric(horizontal: 5.0),
                                child: new Row(
                                  children: <Widget>[
                                    new Expanded(
                                        child: new Text(name, style: result,)
                                    ),
                                    new Expanded(
                                        child: progress == 100.0 ? new Icon(Icons.check, color: Colors.green,) : new Container(
                                            alignment: Alignment.center,
                                            padding: const EdgeInsets.all(10.0),
                                            child: widgetHelper.getSmallProgress()
                                        )
                                    )
                                  ],
                                ),
                              );
                            }).toList(),
                          )
                        ],
                      ),
                    ) : taskStatus.index >= TaskStatus.NDT_RUNNING.index && ndt ? _ndt(result) : Container()
                  ],
                ),
              ),
          )
        ),
        onWillPop: _onBackPressed
    );
  }

  String _getNetworkName(BuildContext context){
    if(testNetworkData == null)
      return AppLocalizations.of(context)!.getLocalizedValue("test_information")!;

    return "${testNetworkData!.operatorName} ${networkType != null ? networkType!.name : "-"}";
  }

  Widget _ndt(TextStyle resultStyle) => new Column(
    children: <Widget>[
      new Container(
        margin: EdgeInsets.symmetric(horizontal: 5.0),
        child: new Row(
          children: <Widget>[
            new Expanded(
                child: new Text("NDT", style: resultStyle,)
            ),
            new Expanded(
                child: ndtStatus.index > NDTStatus.RUNNING.index ? _getNDTResultIcon() : new Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(10.0),
                    child: widgetHelper.getSmallProgress()
                )
            )
          ],
        ),
      ),
      _divider()
    ],
  );

  Widget _divider() => new Container(
      margin: const EdgeInsets.symmetric(vertical: 5.0),
      child: new Divider(color: Colors.grey,),
    );


  Icon _getNDTResultIcon(){
    switch (ndtStatus){
      case NDTStatus.FINISHED: return new Icon(Icons.check, color: Colors.green);
      case NDTStatus.ERROR: return new Icon(Icons.clear, color: Colors.red,);
      case NDTStatus.ABORTED: return new Icon(Icons.clear, color: Colors.yellow,);
      default: return new Icon(Icons.help, color: Colors.grey,);
    }
  }
}

class InfoWidget extends StatefulWidget {
  final String? msgKey;
  InfoWidget({
    Key? key,
    required this.msgKey
  }) : super(key: key);

  @override
  _InfoWidgetState createState() => _InfoWidgetState();
}

class _InfoWidgetState extends State<InfoWidget> {
  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    TextStyle textStyle = themeData.textTheme.bodyText2!.copyWith(color: Colors.white);

    return Row(
        children: [
          Flexible(
            flex: 1,
            child: widgetHelper.getSmallProgress(color: Colors.white)
          ),
          Flexible(
            flex: 5,
            child:  Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: Text(widget.msgKey != null ? AppLocalizations.of(context)!.getLocalizedValue(widget.msgKey!) ?? "Sending data ..." : "Sending data ...", style: textStyle,),
            )
          )
        ],
      );
  }
}


class TestNetworkData{
  final String? operatorName;
  final int? networkType;
  final String? serverName;
  final String? ip;

  TestNetworkData({
    this.operatorName,
    this.networkType,
    this.serverName,
    this.ip
  });

  TestNetworkData.fromJson(Map<String, dynamic> jsonData) : this(
    operatorName: jsonData["operatorName"],
    networkType: jsonData["networkType"],
    serverName: jsonData["serverName"],
    ip: jsonData["ip"]
  );

  printData(){
    print("OPERATOR NAME: $operatorName\n NETWORKTYPE: $networkType\n SERVER NAME: $serverName\n IP: $ip");
  }
}

enum NDTStatus{
  NOT_STARTED, RUNNING, RESULTS, FINISHED, ERROR, ABORTED
}
