/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/app_config.dart';
import 'package:netmetr_flutter/net/response/test_result_qos_reponse.dart';
import 'package:netmetr_flutter/pages/help_page.dart';
import 'package:netmetr_flutter/pages/result_tabs/detail_tab.dart';
import 'package:netmetr_flutter/pages/result_tabs/graph_tab.dart';
import 'package:netmetr_flutter/pages/result_tabs/location_tab.dart';
import 'package:netmetr_flutter/pages/result_tabs/qos_tab.dart';
import 'package:netmetr_flutter/pages/result_tabs/result_tab.dart';
import 'package:netmetr_flutter/test/test_data.dart';
import 'package:netmetr_flutter/net/rest_client.dart' as restClient;
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:netmetr_flutter/util/location/show_map_data.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

import 'result_tabs/tab_item.dart';

class ResultTestPage extends StatefulWidget {
  final AppConfig config;
  final TestData testData;

  const ResultTestPage({
  Key? key,
  required this.config,
  required this.testData}) : super(key: key);

  @override
  _ResultTestPageState createState() => _ResultTestPageState();
}

class _ResultTestPageState extends State<ResultTestPage> with SingleTickerProviderStateMixin {
  static List<TabItem> tabs = [TabItem.RESULT, TabItem.DETAILS, TabItem.QOS, TabItem.GRAPH, TabItem.LOCATION];
  late TabController _tabController;
  TabItem actualTabItem = TabItem.RESULT;
  late TestData testData;
  late bool locationData;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(length: tabs.length, vsync: this);
    _tabController.addListener(_handleTab);

    testData = widget.testData;
    locationData = testData.testResultResponse!.geoLat != null && testData.testResultResponse!.geoLong != null;

    _downloadTestDetail();
    _downloadQosData();
    _downloadOpenData();
  }

  _handleTab(){
    actualTabItem = tabs[_tabController.index];
    setState(() {

    });
  }

  _downloadOpenData() async{
    restClient.testOpenData(widget.config, testData.testUuid, testData.testResultResponse!.openTestUuid!)
      .catchError((error){
        if(this.mounted) {
          setState(() {
            testData.loadedOpenData = true;
          });
        }
      })
      .then((openData){
        if(openData == null)
          return;

        testData.loadedOpenData = true;
        testData.openDataResponse = openData;

        if(this.mounted){
          setState(() {

          });
        }
      }
    );
  }

  _downloadTestDetail() async{
    restClient.testResultDetail(widget.config, testData.testUuid)
      .catchError((error){
        if(this.mounted) {
          setState(() {
            testData.loadedDetail = true;
          });
        }
      })
      .then((details){
        if( details == null)
          return;

        testData.loadedDetail = true;
        testData.testDetails = details;

        if(this.mounted) {
          setState(() {

          });
        }
    });
  }

  _downloadQosData() async{
    restClient.testResultQoS(widget.config, testData.testUuid)
        .catchError((error){
          if(this.mounted) {
            setState(() {
              testData.loadedQoSData = true;
            });
          }
      })
        .then((qosResponse){
          if(qosResponse == null)
            return;

          Map<QosResultType, List<ResultDetailDescription>> qosDetailDescriptionMap = {};
          Map<QosResultType, TestDescription> qosTestDetailMap = {};
          Map<QosResultType, List<ResultDetail>> qosResultMap = {};


          if(qosResponse.detailDescriptions != null) {
            for (ResultDetailDescription description in qosResponse.detailDescriptions!) {
              qosDetailDescriptionMap.update(description.testType!, (list) {
                list.add(description);
                return list;
              }, ifAbsent: () => [description]);
            }
          }

          if(qosResponse.testDescriptions != null) {
            for (TestDescription description in qosResponse.testDescriptions!) {
              if(description.type!=null) qosTestDetailMap.putIfAbsent(description.type!, () => description);
            }
          }

          if(qosResponse.testResultDetails != null) {
            for (ResultDetail resultDetail in qosResponse.testResultDetails!) {
              if(qosDetailDescriptionMap.containsKey(resultDetail.testType)) {
                for (ResultDetailDescription detailDescription in qosDetailDescriptionMap[resultDetail.testType]!) {
                  if (detailDescription.uid!.contains(resultDetail.uid)) {
                    detailDescription.sum++;

                    if(detailDescription.status == StatusType.FAIL || resultDetail.failure != 0)detailDescription.failure += resultDetail.failure!;
                    else detailDescription.success += resultDetail.success!;

                    resultDetail.resultString = detailDescription.desc;
                  }
                }
              }

              qosResultMap.update(resultDetail.testType!, (list) {
                list.add(resultDetail);
                return list;
              }, ifAbsent: () => [resultDetail]);
            }
          }

          testData.qosDetailDescriptionMap = qosDetailDescriptionMap;
          testData.qosTestDetailMap = qosTestDetailMap;
          testData.qosResultMap = qosResultMap;

          //remove voip
          testData.qosDetailDescriptionMap!.remove(QosResultType.VOIP);
          testData.qosTestDetailMap!.remove(QosResultType.VOIP);
          testData.qosResultMap!.remove(QosResultType.VOIP);

          testData.loadedQoSData = true;

          if(this.mounted) {
            setState(() {

            });
          }
      });
  }

  List<Widget> _getAction(TabItem tabItem){
    switch (tabItem){
      case TabItem.LOCATION:
        return [
          new IconButton(
              icon: new Icon(Icons.map,),
              color: Colors.white,
              disabledColor: Colors.grey,
              onPressed:  locationData ? _showTestInMapFragment : null
          )
        ];
      case TabItem.RESULT:
        return [
          new IconButton(
            icon: new Icon(Icons.share),
            color: Colors.white,
            disabledColor: Colors.grey,
            onPressed: ()=> Share.share("${testData.testResultResponse!.shareText}${testData.testResultResponse!.shareSubject}")
          ),
          new IconButton(
            icon: new Icon(Icons.help_outline),
            color: Colors.white,
            disabledColor: Colors.grey,
            onPressed: ()=> Platform.isAndroid && widget.config.deviceInfo?.systemOS <= 19 ? launch(AppLocalizations.of(context)!.getLocalizedValue('wwwHelp')!).catchError((error){print("Launch url error $error");}) : Navigator.push(context, new MaterialPageRoute(builder: (_)=> new HelpPage())))
        ];
      default: return [];
    }
  }

  _showTestInMapFragment(){
    if(locationData && testData.testResultResponse!.geoLat != null && testData.testResultResponse!.geoLong != null) {
      Navigator.pop(context, new ShowMapData(
          lat: testData.testResultResponse!.geoLat!,
          lon: testData.testResultResponse!.geoLong!));
    }
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(AppLocalizations.of(context)!.getLocalizedValue('results')!),
        actions: _getAction(actualTabItem),
        bottom: PreferredSize(
          child: Container(
            color: Colors.white,
            width: screenSize.width,
            alignment: Alignment.center,
            child: TabBar(
                controller: _tabController,
                indicatorColor: Colors.blueGrey,
                labelColor: Colors.black,
                isScrollable: true,
                tabs: tabs.map((tab){
                  return  new Tab(text: AppLocalizations.of(context)!.getLocalizedValue(tab.title),);
                }).toList()
            ),
          ), 
          preferredSize: Size.fromHeight(kToolbarHeight)),
      ),
      body: new TabBarView(
        controller: _tabController,
        children: [
          new ResultTab(measurement: testData.testResultResponse!.measurement, net: testData.testResultResponse!.net,),
          new DetailTab(loaded: testData.loadedDetail, details: testData.testDetails,),
          new QOSTab(qosDetailDescriptionMap: testData.qosDetailDescriptionMap, qosResultMap: testData.qosResultMap, qosTestDetailMap: testData.qosTestDetailMap, loaded: testData.loadedQoSData,),
          new GraphTab(openData: testData.openDataResponse, loaded: testData.loadedOpenData,),
          new LocationTab(geoLat: testData.testResultResponse!.geoLat, geoLon: testData.testResultResponse!.geoLong, locationString: testData.testResultResponse!.location, showMap: _showTestInMapFragment, locationData: locationData,),
        ])
    );
  }
}
