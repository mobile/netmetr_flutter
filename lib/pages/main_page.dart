/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:netmetr_flutter/net/response/map_response.dart';
import 'package:netmetr_flutter/net/response/settings_response.dart';
import 'package:netmetr_flutter/pages/main_fragments/fragment_state.dart';
import 'dart:async';

import 'package:netmetr_flutter/pages/main_fragments/history_fragment.dart';
import 'package:netmetr_flutter/pages/main_fragments/info_fragment.dart';
import 'package:netmetr_flutter/pages/main_fragments/map_fragment.dart';
import 'package:netmetr_flutter/pages/main_fragments/not_implemented_page.dart';
import 'package:netmetr_flutter/pages/main_fragments/settings_fragment.dart';
import 'package:netmetr_flutter/pages/main_fragments/start_fragment.dart';
import 'package:netmetr_flutter/pages/main_fragments/help_fragment.dart';
import 'package:netmetr_flutter/pages/main_fragments/statistics_fragment.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:netmetr_flutter/util/config_helper.dart';
import 'package:netmetr_flutter/app_config.dart';
import 'package:netmetr_flutter/util/location/show_map_data.dart';
import 'package:netmetr_flutter/util/network/network_data.dart';
import 'package:netmetr_flutter/util/network/network_connection_type.dart';
import 'package:netmetr_flutter/menu/main_menu.dart';
import 'package:netmetr_flutter/net/rest_client.dart' as restClient;
import 'package:netmetr_flutter/util/network/network_type.dart';
import 'package:netmetr_flutter/util/widget_helper.dart' as widgetHelper;
import 'package:netmetr_flutter/util/shared_helper.dart' as sharedHelper;

class MainPage extends StatefulWidget {
  final AppConfig config;

  MainPage({Key? key, required this.config}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  static const netChannel = const EventChannel('nic.cz.netmetrflutter/netStat');
  static const config = const MethodChannel('nic.cz.netmetrflutter/config');
  static const loopChannel = const MethodChannel('nic.cz.netmetrflutter/loopChannel');
  static const loopEvent = const EventChannel('nic.cz.netmetrflutter/loopEvent');

  GlobalKey<ScaffoldState> _key = new GlobalKey<ScaffoldState>();

  SettingsTaskStatus settingsTaskStatus = SettingsTaskStatus.RUNNING;
  bool settingsChecked = false;
  bool mapOptionsFetch = false;
  bool loopModeActive = false;
  double scaleRatio = 1;

  String title = MenuItem.START.title;
  int actualFragmentIndex = MenuItem.START.index;
  Widget? actualFragment;
  GlobalKey<FragmentState> fragmentKey = GlobalKey();

  ShowMapData? lastShowMapData;

  @override
  void initState() {
    super.initState();
    _initNetwork();
  }


  _startLoopService(){
    loopChannel.invokeMethod("startLoopService", {
      "uuid": ConfigHelper.instance!.uuid,
      "controlServer": widget.config.controlServerName(),
      "port": widget.config.configModel.port,
      "useSSL": widget.config.configModel.controlSSL,
      "qosSSL" : widget.config.configModel.qosSSL,
      "minDelay" : widget.config.appSettings.loopMinDelay,
      "maxDelay" : widget.config.appSettings.loopMaxDelay,
      "maxMovement" : widget.config.appSettings.loopMovementMeters + 0.0,
      "maxTests" : widget.config.appSettings.loopMaxTests,
      "onlySignal" : widget.config.appSettings.onlySignal,
      "runNDT" : widget.config.appSettings.ndt,
      "onlyIPv4" : widget.config.appSettings.onlyIpv4,
      "jitter" : widget.config.appSettings.jitter,
      "qos" : widget.config.appSettings.qos
    }).then((_){

      setState(() {
        widget.config.configModel.loopModeActive = true;
      });

      loopEvent.receiveBroadcastStream().listen((status){
        print(status);
        if(status == LOOP_STOP && mounted){
          setState(() {
            widget.config.configModel.loopModeActive = false;
            restClient.checkSettingsTask(widget.config).catchError((error){
              //ignore
            });
          });
        }

      });
    });


  }

  _fetchMapOptions() async{
    restClient.mapOptionsInfo(widget.config)
      .catchError((error){
        //ignore
    })
      .then((MapAppOptions? mapOptions){
        if(mapOptions != null) {
          widget.config.configModel.mapOptions = mapOptions;
          mapOptionsFetch = true;
        }
      }
    );
  }

  _initNetwork(){
    netChannel.receiveBroadcastStream().listen((data){
      if(mounted && data != null) {
        Map<String, dynamic> jsonData = json.decode(data);

        if(jsonData["status"] == "networkChange") {
          Map<String, dynamic> changeData = json.decode(jsonData["data"]);

          NetworkConnectionType networkType = NetworkConnectionType.getType(changeData["networkType"]);
          widget.config.networkModel.networkConnectionType = networkType;

          NetworkData? networkData;
          if (networkType != NetworkConnectionType.NOT_CONNECTED) {

            if (!settingsChecked) {
              _checkSettings();
            }

            if(!mapOptionsFetch){
              _fetchMapOptions();
            }

            networkData = NetworkData.fromJson(json.decode(changeData["data"]));
          }else {
            widget.config.networkModel.checkInfo = null;
            settingsTaskStatus = SettingsTaskStatus.RUNNING;
            settingsChecked = false;
          }

          setState(() {
            widget.config.networkModel.networkData = networkData;
            widget.config.networkModel.networkType = networkData != null ? NetworkType.getType(networkData.networkID) : null;
          });
        }else if(jsonData["status"] == "ipChange"){

          if (settingsChecked && widget.config.networkModel.networkConnectionType != NetworkConnectionType.NOT_CONNECTED) {

            if(widget.config.networkModel.checkInfo != null) {
              setState(() {
                widget.config.networkModel.checkInfo = null;
              });
            }

            _checkIP();
          }

        }
      }
    });
  }

  _checkSettings(){

    if(settingsTaskStatus != SettingsTaskStatus.RUNNING) {
      setState(() {
        settingsTaskStatus = SettingsTaskStatus.RUNNING;
      });
    }

    restClient.checkSettingsTask(widget.config)
        .catchError((error){
        print(error.toString());
        restClient.showRestErrorDialog(context, error);

        if(mounted) {
          setState(() {
            settingsTaskStatus = SettingsTaskStatus.FAILED;
          });
        }
    })
        .then((response){
      if(mounted && response != null) {
        config.invokeMethod("updateConfig", {
          "control_ipv4_only": response.urls["control_ipv4_only"],
          "control_ipv6_only": response.urls["control_ipv6_only"],
          "url_ipv4_check": response.urls["url_ipv4_check"],
          "url_ipv6_check": response.urls["url_ipv6_check"],
          "uuid": ConfigHelper.instance!.uuid
        }).then((val) {
          _checkIP();
        });

        setState(() {
          settingsTaskStatus = SettingsTaskStatus.FINISHED;
          settingsChecked = true;
        });
      }
    });
  }


  _checkIP() {
    if(widget.config.networkModel.networkConnectionType == NetworkConnectionType.NOT_CONNECTED)
      return;

    config.invokeMethod("getCheckIpInfo")
        .catchError((error){
      print("Check IPError: " + error.toString());
    })
        .then((result){
      if(mounted && result != null) {

        setState(() {
          NetworkCheckInfo checkInfo = NetworkCheckInfo.fromJSON(json.decode(result));
          widget.config.networkModel.checkInfo = checkInfo;
        });
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  _changeMobileDataWarning(){
    setState(() {
      print("DataWarning: ${ConfigHelper.instance!.uncheckDataWarning}");
    });
  }

  _getFragment(int index){
    switch (index){
      case 0: actualFragment = new StartFragment(key: fragmentKey,settingsTaskStatus: settingsTaskStatus, showMapData: _showMap, appConfig: widget.config, startLoopService: _startLoopService, showHistory: _showHistory, startSettingTask: _checkSettings, changeMobileDataWaring: _changeMobileDataWarning,); break;
      case 1: actualFragment = new HistoryFragment(key: fragmentKey, appConfig: widget.config, showTestInMap: _showMap, showHistory: _showHistory); break;
      case 2: actualFragment = new MapFragment(key: fragmentKey, appConfig: widget.config, showMapData: lastShowMapData); break;
      case 3: actualFragment = new StatisticsFragment(key: fragmentKey, appConfig: widget.config); break;
      case 4: actualFragment = new HelpFragment(key: fragmentKey,); break;
      case 5: actualFragment = new InfoFragment(key: fragmentKey, appConfig: widget.config); break;
      case 6: actualFragment = new SettingsFragment(key: fragmentKey, appConfig: widget.config, changeControlServer: _changeControlServer, showHistory: _showHistory); break;

      default: actualFragment = new NotImplementedPage(key: fragmentKey);
    }

    lastShowMapData = null;

    return actualFragment;
  }

  _changeControlServer(String? controlServer, int? port, bool? controlSSL, bool? qosSSL){
    sharedHelper.setControlServer(controlServer!);
    sharedHelper.setControlPort(port!);
    sharedHelper.setControlSSL(controlSSL!);
    sharedHelper.setQOSSSL(qosSSL!);

    widget.config.configModel.controlHost = controlServer;
    widget.config.configModel.port = port;
    widget.config.configModel.controlSSL = controlSSL;
    widget.config.configModel.qosSSL = qosSSL;

    setState(() {
      actualFragmentIndex = MenuItem.START.index;
      title = MenuItem.START.title;
      settingsChecked = false;
    });

    _checkSettings();
  }

  _showMap(ShowMapData showMapData){
    lastShowMapData = showMapData;

   _showFragment(MenuItem.MAP);
  }

  _showHistory(){
   _showFragment(MenuItem.HISTORY);
  }

  _showFragment(MenuItem menuItem){
    setState(() {
      actualFragmentIndex = menuItem.index;
      title = AppLocalizations.of(context)!.getLocalizedValue(menuItem.title)!;
    });
  }

  _callAction(int index){
    if(fragmentKey.currentState != null)
      fragmentKey.currentState!.callActionsFunction(index);
  }

  List<Widget> _getAction(int index, final IconThemeData iconTheme){
    const actionPadding = 20.0;

    switch (index){
      case 0: return [
        widget.config.networkModel.networkConnectionType == NetworkConnectionType.MOBILE  && !ConfigHelper.instance!.uncheckDataWarning! ? Padding(padding: const EdgeInsets.only(right: actionPadding),
          child: widgetHelper.appBarBounceActionsIcon(Icons.warning, iconTheme.size!, _callAction, 0),
        ) : Container()
      ];
      case 1: return [
        new Padding(padding: const EdgeInsets.only(right: actionPadding),
          child: widgetHelper.appBarActionsIcon(Icons.find_in_page, iconTheme.size!, _callAction, 0),
        ),
        new Padding(padding: const EdgeInsets.only(right: actionPadding),
          child: widgetHelper.appBarActionsIcon(Icons.find_replace, iconTheme.size!, _callAction, 1),
        )
      ];
      case 2: return[
        new Padding(padding: const EdgeInsets.only(right: actionPadding),
          child: widgetHelper.appBarActionsIcon(Icons.filter_list, iconTheme.size!, _callAction, 0),
        ),
        new Padding(padding: const EdgeInsets.only(right: actionPadding),
          child: widgetHelper.appBarActionsIcon(Icons.settings, iconTheme.size!, _callAction, 1),
        ),
        new Padding(padding: const EdgeInsets.only(right: actionPadding),
          child: widgetHelper.appBarActionsIcon(Icons.info_outline, iconTheme.size!, _callAction, 2),
        ),
      ];
      case 5: return [
        new Padding(padding: const EdgeInsets.only(right: actionPadding),
          child: widgetHelper.appBarActionsImage("assets/ic_action_logo.png", iconTheme.size!, _callAction, 0),
        ),
      ];
      default: return [];
    }
  }

  Future<bool> _onBackPressed(){
    if(actualFragmentIndex != 0 && !_key.currentState!.isDrawerOpen){
      setState(() {
        actualFragmentIndex = MenuItem.START.index;
        title = AppLocalizations.of(context)!.getLocalizedValue(MenuItem.START.title)!;
      });

      return new Future.value(false);
    }

    actualFragment = null;
    return new Future.value(true);
  }

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    TextStyle titleStyle = themeData.textTheme.headline6!.copyWith(color: Colors.white);
    final IconThemeData iconTheme = IconTheme.of(context);
    if(actualFragmentIndex == MenuItem.START.index) title = AppLocalizations.of(context)!.getLocalizedValue(MenuItem.START.title)!; //set title when app starts

    return new WillPopScope(
        child: new Scaffold(
            key: _key,
            appBar: new AppBar(
                title: new Text(title, style: titleStyle,),
                actions: _getAction(actualFragmentIndex, iconTheme)
            ),
            drawer: new Drawer(
                child: MainMenu(
                  config: widget.config,
                  android19: Platform.isAndroid && widget.config.deviceInfo?.systemOS <= 19,
                  changeFragment: (menuItem) {
                    setState(() {
                        actualFragmentIndex = menuItem.index;
                        title = AppLocalizations.of(context)!.getLocalizedValue(menuItem.title)!;
                        actualFragment = _getFragment(actualFragmentIndex);
                        Navigator.of(context).pop();
                    }
                    );
                  },
                )
            ),
            body: new Container(
                child:  Builder(builder: (_){
                  if(actualFragmentIndex <= 6){
                    return _getFragment(actualFragmentIndex);
                  }else{
                    return actualFragment!;
                  }
                })
            )
        ),
        onWillPop: _onBackPressed
    );
  }
}

