/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/app_config.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:netmetr_flutter/net/rest_client.dart' as restClient;

class GetSyncCodePage extends StatefulWidget {
  final AppConfig appConfig;

  const GetSyncCodePage({
    Key? key,
    required this.appConfig}) : super(key: key);

  @override
  _GetSyncCodePageState createState() => _GetSyncCodePageState();
}

class _GetSyncCodePageState extends State<GetSyncCodePage> {
  String? syncCode;
  bool? syncDataLoaded;

  @override
  void initState() {
    super.initState();
    widget.appConfig.configModel.needSync = true;
    syncDataLoaded = false;
    _downloadSyncData();
  }

  _downloadSyncData() async{
    restClient.getSyncCode(widget.appConfig)
        .catchError((error){
      print(error);

      restClient.showRestErrorDialog(context, error);

      if(this.mounted) {
        setState(() {
          syncDataLoaded = true;
        });
      }

    }).then((data){
      if(this.mounted && data != null){
        setState(() {
          syncCode = data;
          syncDataLoaded = true;
        });
      }
    });

  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    ThemeData themeData = Theme.of(context);
    TextStyle titleStyle = themeData.textTheme.headline6!.copyWith(color: Colors.black);
    TextStyle textStyle = themeData.textTheme.bodyText2!.copyWith(color: Colors.black);

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(AppLocalizations.of(context)!.getLocalizedValue('sync')!),
      ),
      body: new Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          new Container(
            margin: const EdgeInsets.all(10.0),
            child: new Image.asset(
              "assets/ic_action_home.png",
              color: Colors.blueGrey,
            ),
          ),
          new Expanded(
            child: Container(
              width: mediaQueryData.size.width,
              height: mediaQueryData.size.height,
              alignment: Alignment.center,
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Padding(
                      padding: const EdgeInsets.only(top: 5.0),
                      child: new Text(
                        AppLocalizations.of(context)!.getLocalizedValue('syncCode')!,
                        textAlign: TextAlign.center,
                        style: titleStyle,
                      )),
                  new Padding(
                      padding: const EdgeInsets.only(top: 25.0),
                      child: new Text((syncCode==null ? AppLocalizations.of(context)!.getLocalizedValue('retreivCode')! : syncCode)!),
                      ),
                  new Padding(
                      padding: const EdgeInsets.only(top: 25.0),
                      child: new Text(
                        AppLocalizations.of(context)!.getLocalizedValue('syncToDo')!,
                        textAlign: TextAlign.center,
                        style: textStyle,
                      ))
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
