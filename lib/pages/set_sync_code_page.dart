/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/app_config.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:netmetr_flutter/net/rest_client.dart' as restClient;
import 'package:netmetr_flutter/util/dialog_helper.dart' as dialogHelper;
import 'package:netmetr_flutter/util/network/network_connection_type.dart';

class SetSyncCodePage extends StatefulWidget {
  final AppConfig appConfig;
  final Function() showHistory;

  const SetSyncCodePage({
    Key? key,
    required this.appConfig,
    required this.showHistory
  }) : super(key: key);

  @override
  _SetSyncCodePageState createState() => _SetSyncCodePageState();
}

class _SetSyncCodePageState extends State<SetSyncCodePage> {
  final syncCodeController = TextEditingController();
  late bool syncDataLoaded;
  String msgTitle = "";
  String msgText = "";
  bool syncSucc = false;

  @override
  void initState() {
    super.initState();

    syncDataLoaded = false;
  }

  _addSyncData(String syncCode) async{
    restClient.newSyncRequest(widget.appConfig, syncCode)
        .catchError((error){
      print(error);

      restClient.showRestErrorDialog(context, error);

      if(this.mounted) {
        setState(() {
          syncDataLoaded = true;
        });
      }

    }).then((data){
      if(this.mounted && data != null){
        setState(() {
          msgTitle = data['sync'][0]['msg_title'];
          msgText = data['sync'][0]['msg_text'];
          syncSucc = data['sync'][0]['success'];
          syncDataLoaded = true;
          if(widget.appConfig.networkModel.networkConnectionType != NetworkConnectionType.NOT_CONNECTED) {
            restClient.checkNewSyncHistory(widget.appConfig).catchError((error){
              //ignore
            });
          }
          dialogHelper.showStatusDialog(
              context: context,
              status: dialogHelper.DialogStatus.INFO,
              title: msgTitle,
              message: msgText,
              barrierDismissible: false,
              cancelButtonKey: 'confirm',
              ok: (){
                if(!syncSucc)Navigator.pop(context);
                else{
                  Navigator.pop(context);
                  _showHistory();
                }
              }
          );
        });
      }
    });

  }

  void _showHistory() {
    Navigator.of(context).pop();
    Navigator.of(context).pop();
    widget.showHistory();
  }

  @override
  void dispose() {
    syncCodeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    ThemeData themeData = Theme.of(context);
    TextStyle titleStyle = themeData.textTheme.headline6!.copyWith(color: Colors.black);
    TextStyle textStyle = themeData.textTheme.bodyText2!.copyWith(color: Colors.black);
    TextStyle buttonStyle = themeData.textTheme.bodyText2!.copyWith(color: Colors.white);
    Color accentColor = themeData.colorScheme.secondary;

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(AppLocalizations.of(context)!.getLocalizedValue('sync')!),
      ),
      body: new Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          new Container(
            margin: const EdgeInsets.all(10.0),
            child: new Image.asset(
              "assets/ic_action_home.png",
              color: Colors.blueGrey,
            ),
          ),
          new Expanded(
            child: SingleChildScrollView(
              child: Container(
                width: mediaQueryData.size.width,
                height: mediaQueryData.size.height,
                alignment: Alignment.center,
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                        child: new Text(
                            AppLocalizations.of(context)!.getLocalizedValue('insertCode')!,
                            style: titleStyle)),
                    new Padding(
                        padding: const EdgeInsets.only(top: 25.0, left: 70.0, right: 70.0),
                        child: new TextField(
                          textAlign: TextAlign.center,
                          decoration: InputDecoration(
                              hintText: AppLocalizations.of(context)!.getLocalizedValue('insertCode'),
                          ),
                          controller: syncCodeController,
                        ),
                    ),
                    new Padding(
                        padding: const EdgeInsets.only(top: 25.0),
                        child: new Text(
                          AppLocalizations.of(context)!.getLocalizedValue('inserCodeInfo')!,
                          style: textStyle,
                        )),
                    new Padding(padding: const EdgeInsets.symmetric(vertical: 25.0),
                      child: new ElevatedButton(
                        onPressed: () {
                          if(syncCodeController.text.length <= 0){
                            dialogHelper.showStatusDialog(
                                context: context,
                                status: dialogHelper.DialogStatus.INFO,
                                title: AppLocalizations.of(context)!.getLocalizedValue('emptyCodeTitle'),
                                message: AppLocalizations.of(context)!.getLocalizedValue('emptyCodeText')!,
                                cancelButtonKey: 'confirm',
                                ok: (){
                                  Navigator.pop(context);
                                }
                            );
                          }
                          else _addSyncData(syncCodeController.text);
                        },
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(accentColor)
                        ),
                        child: new Text(AppLocalizations.of(context)!.getLocalizedValue('insertCode')!, style: buttonStyle,),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
