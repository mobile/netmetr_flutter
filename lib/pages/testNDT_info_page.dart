/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:url_launcher/url_launcher.dart';

class NDTInfoPage extends StatelessWidget {

  _launchUrl(String url){
    launch(url).catchError((error){print("Launch url error $error");});
  }

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    TextStyle titleStyle = themeData.textTheme.headline5!.copyWith(color: Colors.black);
    TextStyle subtitleStyle = themeData.textTheme.subtitle1!.copyWith(color: Colors.black);
    TextStyle buttonStyle = themeData.textTheme.button!.copyWith(color: Colors.white);

    return new Scaffold(
        appBar: new AppBar(
          title: new Text("NDT"),
        ),
        body: new SingleChildScrollView(
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              new Container( //info about NDT tests
                padding: const EdgeInsets.all(15.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Padding(
                      padding: const EdgeInsets.only(top: 15.0),
                      child: new Text("1. NDT Test", style: titleStyle),
                    ),
                    new Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: new Text("1.1", style: subtitleStyle),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 8.0),
                      child: new Text(AppLocalizations.of(context)!.getLocalizedValue('par4_1_part1')!),
                    ),
                    new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part2')!),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part3')!),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part4')!),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: new Text(
                          " - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part5')!),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part6')!),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: new Text(
                          " - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part7')!),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: new Text(
                          " - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part8')!),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part9')!),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part10')!),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part11')!),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part12')!),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: new Text(
                          " - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part13')!),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part14')!),
                    ),
                    new Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: new Text("1.2", style: subtitleStyle),
                    ),
                    new RichText(
                      text: new TextSpan(
                        style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        ),
                        children: <TextSpan>[
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par4_2_part1')),
                          new TextSpan(
                            text: 'The PlanetLab Consortium',
                            style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () { _launchUrl('https://www.planet-lab.org/');
                              },
                          ),
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par4_2_part2')),
                          new TextSpan(
                            text: 'www.netmetr.cz',
                            style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () { _launchUrl(AppLocalizations.of(context)!.getLocalizedValue('wwwNetmetr')!);
                              },
                          ),
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('and')),
                          new TextSpan(
                            text: 'www.measurementlab.net',
                            style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () { _launchUrl("https://www.measurementlab.net/");
                              },
                          ),
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par4_2_part3')),
                          new TextSpan(
                            text: 'CC BY 4.0 ' + AppLocalizations.of(context)!.getLocalizedValue('cz_en')!,
                            style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () { _launchUrl(AppLocalizations.of(context)!.getLocalizedValue('cc_by_4_0_link')!);
                              },
                          ),
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par4_2_part4')),
                        ],
                      ),
                    ),
                    new Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: new Text("1.3", style: subtitleStyle),
                    ),
                    new RichText(
                      text: new TextSpan(
                        style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        ),
                        children: <TextSpan>[
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par4_3')),
                          new TextSpan(
                            text: 'CC BY 4.0 ' + AppLocalizations.of(context)!.getLocalizedValue('cz_en')!,
                            style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () { _launchUrl(AppLocalizations.of(context)!.getLocalizedValue('cc_by_4_0_link')!);
                              },
                          ),
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par4_2_part4')),
                        ],
                      ),
                    ),
                    new Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: new Text("1.4", style: subtitleStyle),
                    ),
                    new Text(
                        AppLocalizations.of(context)!.getLocalizedValue('par4_4')!),
                    new Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: new Text("1.5", style: subtitleStyle),
                    ),
                    new Text(
                        AppLocalizations.of(context)!.getLocalizedValue('par4_5')!),
                  ],
                ),
              ),
              new Container(
                padding: const EdgeInsets.all(15.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: new ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(Theme.of(context).colorScheme.secondary),
                          elevation: MaterialStateProperty.all(4.0),
                          overlayColor: MaterialStateProperty.all(Colors.blueGrey)
                        ),
                        onPressed: () {
                          Navigator.pop(context, false);
                        },
                        child: new Container(
                            child: new Text(AppLocalizations.of(context)!.getLocalizedValue('decline')!, style: buttonStyle)),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: new ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(Theme.of(context).colorScheme.secondary),
                            elevation: MaterialStateProperty.all(4.0),
                            overlayColor: MaterialStateProperty.all(Colors.blueGrey)
                        ),
                        onPressed: (){
                          Navigator.pop(context, true);
                        },
                        child: new Container(
                            child: new Text(AppLocalizations.of(context)!.getLocalizedValue('agree')!, style: buttonStyle)),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
    );
  }
}


