/*
 * Copyright (C) 2022 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:netmetr_flutter/util/shared_helper.dart' as sharedHelper;

const margin = 10.0;
const bottomSheetH = 130.0;

class WarningTerminationPage extends StatefulWidget {
  final bool confirmedUserTerms;
  const WarningTerminationPage({
    required this.confirmedUserTerms,
    Key? key}) : super(key: key);

  @override
  _WarningTerminationPageState createState() => _WarningTerminationPageState();
}

class _WarningTerminationPageState extends State<WarningTerminationPage> {
  bool terminationCheck = false;


  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    TextStyle? p1Style = themeData.textTheme.bodyText2?.copyWith(fontWeight: FontWeight.bold);
    TextStyle? p2Style = themeData.textTheme.bodyText2?.copyWith();
    TextStyle? urlStyle = themeData.textTheme.bodyText2?.copyWith(decoration: TextDecoration.underline, color: Colors.blueAccent);

    TextStyle? buttonStyle = themeData.textTheme.button?.copyWith(fontSize: 18, color: Colors.white);

    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.getLocalizedValue("warning_termination_title") ?? "-"),
      ),
      body: Container(
          margin: const EdgeInsets.all(margin),
          child:  SingleChildScrollView(
            child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Text(AppLocalizations.of(context)!.getLocalizedValue("warning_termination_desc_p1") ?? "-", style: p1Style,),
              Padding(padding: const EdgeInsets.only(top: margin)),
              Text(AppLocalizations.of(context)!.getLocalizedValue("warning_termination_desc_p2") ?? "-", style: p2Style,),
              Padding(padding: const EdgeInsets.only(top: margin)),
              Row(
                children: [
                  Text(AppLocalizations.of(context)!.getLocalizedValue("more_information") ?? "-", style: p2Style,),
                  TextButton(onPressed: ()=> _launchUrl('https://www.nic.cz/page/4305/sdruzeni-cznic-ukonci-projekt-netmetr/'),
                      child: Text(AppLocalizations.of(context)!.getLocalizedValue("click_url") ?? "-", style: urlStyle,)
                  )
                ],
              ),
              Padding(padding: const EdgeInsets.only(top: margin)),
              Row(
                children: [
                  Flexible(
                      flex: 1,
                      child: Checkbox(
                          value: terminationCheck,
                          onChanged: (value){
                            terminationCheck = value!;
                            sharedHelper.setTerminationChecked(terminationCheck);
                            setState(() {
                              terminationCheck = value;
                            });
                          }
                      )
                  ),
                  Flexible(
                      flex: 4,
                      child: Padding(
                        padding: const EdgeInsets.only(left: margin),
                        child: Text(AppLocalizations.of(context)?.getLocalizedValue("not_display_again") ?? "-",),
                      )
                  )
                ],
              ),
              const Padding(padding: EdgeInsets.only(top: margin)),
              ElevatedButton(
                onPressed: ()=> Navigator.of(context).pushReplacementNamed(widget.confirmedUserTerms ? '/main' : '/firstAppRun'),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(AppLocalizations.of(context)!.getLocalizedValue("understand_button") ?? "-",style: buttonStyle),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _launchUrl(String ulr){
    launch(ulr).catchError((error) {
      print("Launch url error $error");
    });
  }
}

