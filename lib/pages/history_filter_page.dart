/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/app_config.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';

class HistoryFilterPage extends StatefulWidget {
  final AppConfig appConfig;
  final FilterData? previous;
  HistoryFilterPage({
    Key? key,
    required this.appConfig,
    this.previous
  }) : super (key: key);

  @override
  _HistoryFilterPageState createState() => _HistoryFilterPageState();
}

class _HistoryFilterPageState extends State<HistoryFilterPage> {

  late FilterData filterData;

  @override
  void initState() {
    super.initState();
    if(widget.previous == null) {
      filterData = new FilterData();

      Map<String, bool> devices = new Map();
      Map<String, bool> networks = new Map();

      for(String device in widget.appConfig.configModel.historyDevices){
        devices.putIfAbsent(device, ()=> true);
      }

      for(String network in widget.appConfig.configModel.historyNetworks){
        networks.putIfAbsent(network, ()=> true);
      }

      filterData.devices = devices;
      filterData.networks = networks;

    }else{
      filterData = widget.previous!;
    }

  }

  Widget _mapDataWidget(Map<String, bool> data, TextStyle textStyle){
    return new Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: data.keys.map((String title){
        bool check = data[title]!;

        return new Container(
          width: double.maxFinite,
          child: new TextButton(
            onPressed: (){
              setState(() {
                data[title] = !data[title]!;
              });
            },
            child: new Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new Flexible(
                  flex: 3,
                  child: new Text(title, style: textStyle,),
                ),
                new Flexible(
                    flex: 1,
                    child: new Checkbox(value: check, onChanged: (value){
                      setState(() {
                        data[title] = value ?? false;
                      });
                    })
                )
              ],
            )),
        );
      }).toList()
    );
  }

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    TextStyle titleStyle = themeData.textTheme.subtitle2!.copyWith(color: Colors.black);
    TextStyle textStyle = themeData.textTheme.bodyText2!.copyWith(color: Colors.black);
    TextStyle buttonStyle = themeData.textTheme.bodyText2!.copyWith(color: Colors.white);

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(AppLocalizations.of(context)!.getLocalizedValue('filter')!),
      ),
      body: new SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.all(10.0),
          child: new Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              new Text(AppLocalizations.of(context)!.getLocalizedValue('deviceCAPS')!, style: titleStyle,),
              new Padding(padding: const EdgeInsets.symmetric(vertical: 5.0),
                child: new Divider(color: Colors.black,),
              ),
              new Padding(padding: const EdgeInsets.symmetric(vertical: 5.0),
                child: _mapDataWidget(filterData.devices, textStyle),
              ),
              new Padding(padding: const EdgeInsets.symmetric(vertical: 5.0),
                child: new Text(AppLocalizations.of(context)!.getLocalizedValue('netTypeCAPS')!, style: titleStyle,),
              ),
              new Padding(padding: const EdgeInsets.symmetric(vertical: 5.0),
                child: new Divider(color: Colors.black,),
              ),
              new Padding(padding: const EdgeInsets.symmetric(vertical: 5.0),
                child:  _mapDataWidget(filterData.networks, textStyle),
              ),
              new Padding(padding: const EdgeInsets.symmetric(vertical: 5.0),
                child: new Text(AppLocalizations.of(context)!.getLocalizedValue('numOfResCAPS')!, style: titleStyle,),
              ),
              new Padding(padding: const EdgeInsets.symmetric(vertical: 5.0),
                child: new Divider(color: Colors.black,),
              ),
              new Padding(padding: const EdgeInsets.symmetric(vertical: 5.0),
                child: new TextButton(
                  onPressed: (){
                    setState(() {
                      filterData.first25 = !filterData.first25;
                    });
                  },
                  child: new Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Flexible(
                        flex: 3,
                        child: new Text(AppLocalizations.of(context)!.getLocalizedValue('show25Newest')!, style: textStyle,),
                      ),
                      new Flexible(
                          flex: 1,
                          child: new Padding(padding: const EdgeInsets.only(left: 5.0),
                            child: new Checkbox(value: filterData.first25, onChanged: (check){
                              print("25: $check");
                              setState(() {
                                filterData.first25 = check ?? false;
                              });
                            }),
                          ))
                    ],
                  )
                )
              ),
              new Container(
                padding: const EdgeInsets.symmetric(vertical: 5.0),
                alignment: Alignment.center,
                child: new ElevatedButton(onPressed: (){
                  Navigator.pop(context, filterData);
                },
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(themeData.colorScheme.secondary)
                  ),
                  child: new Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Icon(Icons.arrow_back, color: Colors.white,),
                      new Padding(padding: const EdgeInsets.only(left: 10.0, top: 15.0, bottom: 15.0),
                        child: new Text(AppLocalizations.of(context)!.getLocalizedValue('applyFiltr')!, style: buttonStyle,),
                      )
                    ],
                  )
                ),
              )
            ],
          ),
        ),
      )
    );
  }


}


class FilterData{
  bool first25;
  Map<String, bool> devices = {};
  Map<String, bool> networks = {};

  FilterData({
    this.first25 = true});

  List<String> activeDevices(){
    List<String> active = [];
    for(String device in devices.keys){
      if(devices[device]!){
        active.add(device);
      }
    }

    return active;
  }

  List<String> activeNetworks(){
    List<String> active = [];
    for(String network in networks.keys){
      if(networks[network]!){
        active.add(network);
      }
    }

    return active;
  }
}