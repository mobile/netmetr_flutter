/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/pages/main_fragments/fragment_state.dart';

class NotImplementedPage extends StatefulWidget {
  NotImplementedPage({Key? key}) : super (key: key);

  @override
  _NotImplementedPageState createState() => _NotImplementedPageState();

}

class _NotImplementedPageState extends FragmentState<NotImplementedPage> {
  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    TextStyle textStyle = Theme.of(context).textTheme.headline4!.copyWith(color: Colors.blueGrey);

    return new SingleChildScrollView(
      child: Container(
        width: mediaQueryData.size.width,
        height: mediaQueryData.size.height,
        alignment: Alignment.center,
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Icon(Icons.not_interested, color: Colors.blueGrey, size: mediaQueryData.size.width * 0.6,),
            new Padding(padding: const EdgeInsets.only(top: 15.0),
                child: new Text("Not implemented", style: textStyle,)
            )
          ],
        ),
      ),
    );
  }

  @override
  callActionsFunction(int actionIndex) {
    //ignore
  }
}

