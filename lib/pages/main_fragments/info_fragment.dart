/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:netmetr_flutter/app_config.dart';
import 'package:netmetr_flutter/pages/main_fragments/fragment_state.dart';
import 'package:netmetr_flutter/pages/term_of_use_page.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:netmetr_flutter/util/config_helper.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:get_version/get_version.dart';

class InfoFragment extends StatefulWidget {
  final AppConfig appConfig;

  InfoFragment({
    Key? key,
    required this.appConfig
  }) : super(key: key);

  @override
  _InfoFragmentState createState() => _InfoFragmentState();
}

class _InfoFragmentState extends FragmentState<InfoFragment> {
  String _projectVersion = '';
  String _projectName = '';

  @override
  initState() {
    super.initState();
    initPlatformState();
  }

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    TextStyle titleStyle = themeData.textTheme.headline5!.copyWith(color: Colors.black);
    TextStyle subtitleStyle = themeData.textTheme.subtitle1!.copyWith(color: Colors.black);

    return new SingleChildScrollView(
      child: Container(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: new Image.asset(AppLocalizations.of(context)!.getLocalizedValue('logoCef')!, width: 400,),
            ),
            new Container(
              padding: const EdgeInsets.all(15.0),
              child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Padding(
                        padding: const EdgeInsets.only(bottom: 15.0),
                        child: new Text("Netmetr", style: titleStyle)),
                    new Text(AppLocalizations.of(context)!.getLocalizedValue('version')!, style: subtitleStyle),
                    new Text(_projectName),
                    new Text(_projectVersion),
                  ]),
            ),
            new Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: new Divider(
                color: Colors.black,
              ),
            ),
            new Container(
              padding: const EdgeInsets.all(15.0),
              child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Padding(
                        padding: const EdgeInsets.only(bottom: 15.0),
                        child: new Text(AppLocalizations.of(context)!.getLocalizedValue('clientUUD')!, style: subtitleStyle)),
                    new Text(
                      ConfigHelper.instance!.uuid,
                      textAlign: TextAlign.left,
                    ),
                  ]),
            ),
            new Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: new Divider(
                color: Colors.black,
              ),
            ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child:  TextButton(
              onPressed: () => _launchUrl(AppLocalizations.of(context)!.getLocalizedValue('wwwNetmetr')!),
              child: new Container(
                width: double.maxFinite,
                child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      new Padding(
                          padding: const EdgeInsets.only(bottom: 15.0),
                          child: new Text("Web", style: subtitleStyle)),
                      new Text(AppLocalizations.of(context)!.getLocalizedValue('wwwNetmetr')!),
                    ]),
              ),
            ),
          ),
            new Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: new Divider(
                color: Colors.black,
              ),
            ),
            new Container(
              padding: const EdgeInsets.all(15.0),
              child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Padding(
                        padding: const EdgeInsets.only(bottom: 15.0),
                        child: new Text("E-Mail", style: subtitleStyle)),
                    new Text("netmetr@labs.nic.cz"),
                  ]),
            ),
            new Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: new Divider(
                color: Colors.black,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child:  new TextButton(
                onPressed: () => Navigator.push(context, new MaterialPageRoute(builder: (_)=> new TermOfUsePage())),
                child: new Container(
                  width: double.maxFinite,
                  child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Padding(
                            padding: const EdgeInsets.only(bottom: 15.0),
                            child: new Text(AppLocalizations.of(context)!.getLocalizedValue('privacy')!,
                                style: subtitleStyle)),
                        new Text(AppLocalizations.of(context)!.getLocalizedValue('privacyText')!),
                      ]),
                ),
              ),
            ),
            new Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: new Divider(
                color: Colors.black,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: TextButton(
                onPressed: () => _launchUrl('https://gitlab.labs.nic.cz/mobile/netmetr_flutter'),
                child: new Container(
                  width: double.maxFinite,
                  child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Padding(
                            padding: const EdgeInsets.only(bottom: 15.0),
                            child: new Text(AppLocalizations.of(context)!.getLocalizedValue('gitRep')!, style: subtitleStyle)),
                        new Text('https://gitlab.labs.nic.cz/mobile/netmetr_flutter'),
                      ]),
                ),
              ),
            ),
            new Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: new Divider(
                color: Colors.black,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: TextButton(
                onPressed: () => _launchUrl('https://labs.nic.cz/'),
                child: new Container(
                width: double.maxFinite,
                child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Padding(
                          padding: const EdgeInsets.only(bottom: 15.0),
                          child: new Text(AppLocalizations.of(context)!.getLocalizedValue('devel')!, style: subtitleStyle)),
                      new Text("CZ.NIC Labs"),
                      new Text('https://labs.nic.cz/'),
                    ]),
                ),
              ),
            ),
            new Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: new Divider(
                color: Colors.black,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: TextButton(
                onPressed: () => _launchUrl('http://www.specure.com/'),
                child: new Container(
                  width: double.maxFinite,
                  child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Padding(
                          padding: const EdgeInsets.only(bottom: 15.0),
                          child: new Text(AppLocalizations.of(context)!.getLocalizedValue('devel')! + " AT", style: subtitleStyle),
                        ),
                        new Text("Specure GmbH"),
                        new Text('http://www.specure.com/'),
                      ]),
                ),
              ),
            ),
            new Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: new Divider(
                color: Colors.black,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: TextButton(
                onPressed: () => _launchUrl('https://www.apache.org/licenses/LICENSE-2.0'),
                child: new Container(
                  width: double.maxFinite,
                  child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Padding(
                            padding: const EdgeInsets.only(bottom: 15.0),
                            child: new Text("Google Play", style: subtitleStyle)),
                        new Text(AppLocalizations.of(context)!.getLocalizedValue('googleLegal')!),
                      ]),
                ),
              ),
            ),
            new Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: new Divider(
                color: Colors.black,
              ),
            ),
            new Container(
              padding: const EdgeInsets.all(15.0),
              child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Padding(
                        padding: const EdgeInsets.only(bottom: 15.0),
                        child: new Text(AppLocalizations.of(context)!.getLocalizedValue('controlServer')!,
                            style: subtitleStyle)),
                    new Text(widget.appConfig.configModel.controlServerVersion ?? AppLocalizations.of(context)!.getLocalizedValue('noControlServer')!),
                  ]),
            ),
          ],
        ),
      ),
    );
  }

  _launchUrl(String ulr){
    launch(ulr).catchError((error) {
      print("Launch url error $error");
    });
  }

  @override
  callActionsFunction(int actionIndex) {
    _launchUrl(AppLocalizations.of(context)!.getLocalizedValue('wwwNic')!);
  }

  initPlatformState() async {
    String projectVersion;
    try {
      projectVersion = await GetVersion.projectVersion;
    } on PlatformException {
      projectVersion = 'Failed to get project version.';
    }

    String projectName;
    try {
      projectName = await GetVersion.appName;
    } on PlatformException {
      projectName = 'Failed to get app name.';
    }

    if (!mounted) return;

    setState(() {
      _projectVersion = projectVersion;
      _projectName = projectName;
    });
  }
}
