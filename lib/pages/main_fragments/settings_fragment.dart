/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:netmetr_flutter/app_config.dart';
import 'package:netmetr_flutter/app_settings.dart';
import 'package:netmetr_flutter/pages/main_fragments/fragment_state.dart';
import 'package:netmetr_flutter/pages/synchronize_page.dart';
import 'package:netmetr_flutter/pages/testNDT_info_page.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:netmetr_flutter/util/config_helper.dart';
import 'package:netmetr_flutter/util/widget_helper.dart' as widgetHelper;
import 'package:netmetr_flutter/util/dialog_helper.dart' as dialogHelper;
import 'package:netmetr_flutter/util/toast_helper.dart' as toastHelper;

class SettingsFragment extends StatefulWidget {
  final AppConfig appConfig;
  final Function() showHistory;
  final Function(String?, int?, bool?, bool?) changeControlServer;
  SettingsFragment({Key? key,
    required this.appConfig,
    required this.changeControlServer,
    required this.showHistory,
  }) : super (key: key);

  @override
  _SettingsFragmentState createState() => _SettingsFragmentState();
}

class _SettingsFragmentState extends FragmentState<SettingsFragment> {
  static const settingsMethod = const MethodChannel("nic.cz.netmetrflutter/settings");

  late bool debugMode;

  late AppSettings appSettings;

  //debug mode
  late bool controlSSL;
  late bool qosSSL;
  late String controlServer;
  late int port;

  @override
  void initState() {
    super.initState();
    debugMode = ConfigHelper.instance!.debugMode;

    appSettings = widget.appConfig.appSettings;

    //debug mode
    controlSSL = widget.appConfig.configModel.controlSSL;
    qosSSL = widget.appConfig.configModel.qosSSL;
    controlServer = widget.appConfig.configModel.controlHost;
    port = widget.appConfig.configModel.port;

  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations localizations = AppLocalizations.of(context)!;

    ThemeData themeData = Theme.of(context);
    TextStyle titleStyle = themeData.textTheme.subtitle1!.copyWith(color: Colors.black, fontWeight: FontWeight.bold);
    TextStyle subTitle = themeData.textTheme.subtitle1!.copyWith(color: Colors.black, fontWeight: FontWeight.bold);
    TextStyle textStyle = themeData.textTheme.bodyText2!.copyWith(color: Colors.black);


    return new SingleChildScrollView(
      child: new Container(
        margin: const EdgeInsets.all(5.0),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Text(localizations.getLocalizedValue('settings')!, style: subTitle,),
            widgetHelper.titleDivider(),
            _settingsPart(titleStyle, textStyle, localizations),
            new Text(localizations.getLocalizedValue('loop_title')!, style: subTitle,),
            widgetHelper.titleDivider(),
            _loopPart(context, titleStyle, textStyle, localizations),
            debugMode ? new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Text(localizations.getLocalizedValue('controler_server_set')!, style:  subTitle,),
                widgetHelper.titleDivider(),
                _debugControlPart(titleStyle, textStyle, themeData.colorScheme.secondary, localizations)
              ],
            ) : new Container()
          ],
        ),
      ),
    );
  }

  Widget _loopPart(BuildContext context, TextStyle titleStyle, TextStyle textStyle, AppLocalizations localizations){
    return new Container(
      margin: const EdgeInsets.all(5.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _checkBoxItem(
            title: localizations.getLocalizedValue('loop_mode_title')!,
            subText: localizations.getLocalizedValue('loop_mode_sum')!,
            value: appSettings.loopMode,
            changeValue: (value){
              setState(() {
                appSettings.loopMode = value!;
                appSettings.saveLoopMode();
              });
            },
            titleStyle: titleStyle,
            textStyle: textStyle
          ),
          widgetHelper.divider(),
          _checkBoxItem(
            title: localizations.getLocalizedValue('signal_only')!,
            subText: localizations.getLocalizedValue('signal_only_sum')!,
            value: appSettings.onlySignal,
            changeValue: (value){
              setState(() {
                appSettings.onlySignal = value!;
                appSettings.saveOnlySignal();
              });
            },
            titleStyle: titleStyle,
            textStyle: textStyle,
            enable: appSettings.loopMode
          ),
          widgetHelper.divider(),
          _pressItem(
            title: localizations.getLocalizedValue('loop_max_tests')!,
            subText: localizations.getLocalizedValue('loop_max_tests_sum')!,
            pressFunction: ()=> _showNumberEditDialogs(localizations.getLocalizedValue('loop_max_tests')!, appSettings.loopMaxTests, (value){appSettings.loopMaxTests = value; appSettings.saveLoopMaxTests();}, int.tryParse),
            titleStyle: titleStyle,
            textStyle: textStyle,
            enable: appSettings.loopMode
          ),
          widgetHelper.divider(),
          _pressItem(
            title: localizations.getLocalizedValue('loop_min_delay')!,
            subText: localizations.getLocalizedValue('loop_min_delay_sum')!,
            pressFunction: ()=> _showNumberEditDialogs(localizations.getLocalizedValue('loop_min_delay')!, appSettings.loopMinDelay, (value){appSettings.loopMinDelay = value; appSettings.saveLoopMinDelay();}, int.tryParse),
            titleStyle: titleStyle,
            textStyle: textStyle,
            enable: appSettings.loopMode
          ),
          widgetHelper.divider(),
          _pressItem(
            title: localizations.getLocalizedValue('loop_max_delay')!,
            subText: localizations.getLocalizedValue('loop_max_delay_sum')!,
            pressFunction: ()=> _showNumberEditDialogs(localizations.getLocalizedValue('loop_max_delay')!, appSettings.loopMaxDelay, (value){appSettings.loopMaxDelay = value; appSettings.saveLoopMaxDelay();}, int.tryParse),
            titleStyle: titleStyle,
            textStyle: textStyle,
            enable: appSettings.loopMode
          ),
          widgetHelper.divider(),
          _pressItem(
            title: localizations.getLocalizedValue('loop_movement')!,
            subText: localizations.getLocalizedValue('loop_movement_sum')!,
            pressFunction: ()=> _showNumberEditDialogs(localizations.getLocalizedValue('loop_movement')!, appSettings.loopMovementMeters, (value){ appSettings.loopMovementMeters = value + 0.0; appSettings.saveLoopMovementMeters();}, double.tryParse),
            titleStyle: titleStyle,
            textStyle: textStyle,
            enable: appSettings.loopMode
          )
        ],
      ),
    );
  }

  Widget _settingsPart(TextStyle titleStyle, TextStyle textStyle, AppLocalizations localizations){
    return new Container(
      margin: const EdgeInsets.all(5.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _checkBoxItem(
            title: localizations.getLocalizedValue('set_jitter_title')!,
            subText: localizations.getLocalizedValue('set_jitter_description')!,
            value: appSettings.jitter,
            changeValue: (value){
              setState(() {
                appSettings.jitter = value!;
                appSettings.saveJitter();
              });
            },
            titleStyle: titleStyle,
            textStyle: textStyle),
          widgetHelper.divider(),
          _checkBoxItem(
              title: localizations.getLocalizedValue('set_qos_title')!,
              subText: localizations.getLocalizedValue('set_qos_description')!,
              value: appSettings.qos,
              changeValue: (value){
                setState(() {
                  appSettings.qos = value!;
                  appSettings.saveQoS();
                });
              },
              titleStyle: titleStyle,
              textStyle: textStyle),
          widgetHelper.divider(),
          Platform.isAndroid ? _checkBoxItem(
            title: "NDT",
            subText: localizations.getLocalizedValue('ndt_sum')!,
            value: appSettings.ndt,
            titleStyle: titleStyle,
            textStyle: textStyle,
            changeValue: (value){
              Navigator.push(context, new MaterialPageRoute<bool>(builder: (_) => new NDTInfoPage())).then((value) {
                if (value != null) {
                  setState(() {
                    appSettings.ndt = value;
                    appSettings.saveNDT();
                  });
                }
              });
            },
          ) : new Container(),
          widgetHelper.divider(),
          _checkBoxItem(
            title: localizations.getLocalizedValue('ipv4_only')!,
            subText: localizations.getLocalizedValue('ipv4_only_sum')!,
            value: appSettings.onlyIpv4,
            changeValue: (value){
              setState(() {
                appSettings.onlyIpv4 = value!;
                appSettings.saveOnlyIpv4();
              });
            },
            titleStyle: titleStyle,
            textStyle: textStyle,
          ),
          widgetHelper.divider(),
          _pressItem(
            title: localizations.getLocalizedValue('location_settings')!,
            subText:localizations.getLocalizedValue('location_settings_sum')!,
            pressFunction: ()=> settingsMethod.invokeMethod("showSettings"),
            titleStyle: titleStyle,
            textStyle: textStyle
          ),
          widgetHelper.divider(),
          _pressItem(
            title: localizations.getLocalizedValue('sync')!,
            subText: localizations.getLocalizedValue('sync_sum')!,
            pressFunction: ()=> Navigator.push(context, new MaterialPageRoute(builder: (_)=> new SynchronizePage(appConfig: widget.appConfig, showHistory: widget.showHistory))),
            titleStyle: titleStyle,
            textStyle: textStyle)
        ],
      ),
    );
  }

  Widget _debugControlPart(TextStyle titleStyle, TextStyle textStyle, Color accentColor, AppLocalizations localizations){
    return new Container(
      padding: const EdgeInsets.all(5.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _pressItem(
            title: "Control Server",
            subText: localizations.getLocalizedValue('control_host_sum')!,
            pressFunction: ()=> _showTextEditDialogs("Control Server", controlServer, (value)=> controlServer = value),
            titleStyle: titleStyle,
            textStyle: textStyle
          ),
          widgetHelper.divider(),
          _pressItem(
            title: "Control Port",
            subText: localizations.getLocalizedValue('control_port_sum')!,
            pressFunction: ()=>_showNumberEditDialogs("Control Port", port, (value)=> port = value, int.tryParse),
            titleStyle: titleStyle,
            textStyle: textStyle
          ),
          widgetHelper.divider(),
          _checkBoxItem(
            title: "Control Server SSL",
            subText: localizations.getLocalizedValue('control_ssl_sum')!,
            value: controlSSL,
            changeValue: (value){
              setState(() {
                controlSSL = value!;
              });
            },
            titleStyle: titleStyle,
            textStyle: textStyle
          ),
          widgetHelper.divider(),
          _checkBoxItem(
            title: "QoS SSL",
            subText: localizations.getLocalizedValue('qos_ssl_sum')!,
            value: qosSSL,
            changeValue: (value){
              setState(() {
                qosSSL = value!;
              });
            },
            titleStyle: titleStyle,
            textStyle: textStyle
          ),
          widgetHelper.divider(),
          new Container(
            margin: const EdgeInsets.all(10.0),
            alignment: Alignment.center,
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(accentColor),
                textStyle: MaterialStateProperty.all(TextStyle(color: Colors.white))
              ),

              onPressed: (){

                if(controlServer != widget.appConfig.configModel.controlHost || port != widget.appConfig.configModel.port || qosSSL != widget.appConfig.configModel.qosSSL || controlSSL != widget.appConfig.configModel.controlSSL){
                  widget.changeControlServer(controlServer, port, controlSSL, qosSSL);
                }

              },
              child: new Text(localizations.getLocalizedValue('save_settings')!),
            ),
          )

        ],
      ),
    );
  }

  Widget _pressItem({
    required String title,
    required String subText,
    required Function() pressFunction,
    required TextStyle titleStyle,
    required TextStyle textStyle,
    bool enable = true
  }){
    return TextButton(
      onPressed: enable ? pressFunction : null,
      child: new Container(
        width: double.maxFinite,
        padding: const EdgeInsets.symmetric(vertical: 10.0),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Text(title, style: enable ? titleStyle : titleStyle.copyWith(color: Colors.grey),),
            new Padding(padding: const EdgeInsets.only(top: 5.0),
                child: new Text(subText, style: enable ? textStyle : textStyle.copyWith(color: Colors.grey),)
            )
          ],
        ),
      )
    );
  }

  Widget _checkBoxItem({
    required String title,
    required String subText,
    required bool value,
    required Function(bool?)? changeValue,
    required TextStyle titleStyle,
    required TextStyle textStyle,
    bool enable = true
  }){

    return new TextButton(
        onPressed: enable ?(){
          changeValue!(!value);
        } : null,
        child: new Container(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Flexible(
                  flex: 3,
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(title, style: enable ? titleStyle : titleStyle.copyWith(color: Colors.grey),),
                      new Padding(padding: const EdgeInsets.only(top: 5.0),
                          child: new Text(subText, style: enable ? textStyle : textStyle.copyWith(color: Colors.grey), )
                      )
                    ],
                  )
              ),
              new Flexible(
                  child:  new Container(
                    margin: const EdgeInsets.only(left: 5.0),
                    alignment: Alignment.centerRight,
                    child: new Checkbox(value: value, onChanged: enable ? changeValue : null,)
                  )
              )
            ],
          ),
        )
    );
  }

  _showNumberEditDialogs(String title, dynamic value, Function(dynamic) changeValue, num? Function(String) tryParse){
    dialogHelper.showEditDialog(
        context: context,
        value: value,
        textInputType: TextInputType.number,
        title: title,
        changeValue: (value){
          num? newValue = tryParse(value);

          if(newValue != null){
            changeValue(newValue);
            return true;
          }

          _badFormatToast();
          return false;
        }
    );
  }

  _showTextEditDialogs(String title, String value, Function(String) changeValue){
    dialogHelper.showEditDialog(
      context: context,
      value: value,
      textInputType: TextInputType.text,
      title: title,
      changeValue: (newValue){
        changeValue(newValue);
        return true;
      }
    );
  }

  _badFormatToast(){
    toastHelper.showErrorToast(AppLocalizations.of(context)!.getLocalizedValue('bad_format')!);
  }

  @override
  callActionsFunction(int actionIndex) {
    //ignore
  }
}
