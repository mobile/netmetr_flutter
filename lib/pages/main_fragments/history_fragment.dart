/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:netmetr_flutter/app_config.dart';
import 'package:netmetr_flutter/net/response/history_response.dart';
import 'package:netmetr_flutter/net/rest_client.dart' as restClient;
import 'package:netmetr_flutter/pages/history_filter_page.dart';
import 'package:netmetr_flutter/pages/main_fragments/fragment_state.dart';
import 'package:netmetr_flutter/pages/synchronize_page.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:netmetr_flutter/util/location/show_map_data.dart';
import 'package:netmetr_flutter/util/network/network_connection_type.dart';
import 'package:netmetr_flutter/util/widget_helper.dart' as widgetHelper;
import 'package:netmetr_flutter/pages/result_test_page.dart';
import 'package:netmetr_flutter/test/test_data.dart';

const double SIZE_MAX = 70;
const double COLUMN_CNT = 6;

class HistoryFragment extends StatefulWidget {
  final AppConfig appConfig;
  final Function(ShowMapData) showTestInMap;
  final Function() showHistory;

  HistoryFragment({
    Key? key,
    required this.appConfig,
    required this.showTestInMap,
    required this.showHistory,
  }) : super(key: key);

  @override
  _HistoryFragmentState createState()=> _HistoryFragmentState();

}

class _HistoryFragmentState extends FragmentState<HistoryFragment> {

  late bool historyLoaded;
  List<HistoryResponse> historyResponses = [];
  FilterData? activeFilter;

  @override
  void initState() {
    super.initState();
    historyLoaded = false;
    _downloadAndCheckHistory();
  }

  @override
  callActionsFunction(int actionIndex) {

    switch (actionIndex){
      case 0: showSettings(); break;
      case 1: showSynchronizePage(); break;
      default: print("not implemented");
    }
  }

  showSynchronizePage(){
    Navigator.push(context, new MaterialPageRoute(builder: (_)=> new SynchronizePage(appConfig: widget.appConfig, showHistory: widget.showHistory))).then((data){
      setState(() {
        historyLoaded = false;
      });

      _downloadAndCheckHistory();
    });
  }

  showSettings(){
    Navigator.push(context, new MaterialPageRoute(builder: (_)=> new HistoryFilterPage(appConfig: widget.appConfig, previous: activeFilter,))).then((data){
      if(data != null && data is FilterData){
        activeFilter = data;

        setState(() {
          historyLoaded = false;
        });


        _downloadAndCheckHistory();
      }
    });
  }

  _downloadAndCheckHistory() async{
    if(widget.appConfig.configModel.needSync == true && widget.appConfig.networkModel.networkConnectionType != NetworkConnectionType.NOT_CONNECTED) {
      restClient.checkNewSyncHistory(widget.appConfig).catchError((error){}).then((syncData){
        _downloadHistory();
      });
    } else {
      _downloadHistory();
    }

  }

  _downloadHistory() async{
    bool first25 = true;
    List<String> devices = [];
    List<String> networks = [];
    if(activeFilter == null){
      devices = widget.appConfig.configModel.historyDevices;
      networks = widget.appConfig.configModel.historyNetworks;
    }else{
      first25 = activeFilter!.first25;
      devices = activeFilter!.activeDevices();
      networks = activeFilter!.activeNetworks();
    }

    restClient.historyTask(widget.appConfig, devices, networks, first25)
        .catchError((error){
      print(error);

      if(this.mounted) {
        restClient.showRestErrorDialog(context, error);

        setState(() {
          historyResponses = [];
          print("HISTORY: ${historyResponses.length}");
          historyLoaded = true;
        });
      }

    }).then((data){

      if(this.mounted && data != null){
        setState(() {
          historyResponses = data;
          print("HISTORY: ${historyResponses.length}");

          historyLoaded = true;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData = MediaQuery.of(context);
    ThemeData themeData = Theme.of(context);
    TextStyle titleStyle = new TextStyle(color: Colors.white);
    TextStyle textStyle = themeData.textTheme.caption!.copyWith(color: Colors.black);

    double deviceWidth = queryData.size.width;
    double columnSize = deviceWidth / COLUMN_CNT;
    if(deviceWidth < SIZE_MAX * COLUMN_CNT)
      columnSize = SIZE_MAX;

    return Container(
      child: historyLoaded ? Container(
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          primary: true,
          dragStartBehavior: DragStartBehavior.down,
          physics: ClampingScrollPhysics(),
          child: Column(
            children: [
              SizedBox(
                   width: columnSize * COLUMN_CNT,
                   child: Container(
                     color: Colors.black,
                     padding: const EdgeInsets.all(5.0),
                     child:  Row(
                       children: <Widget>[
                         Expanded(child: Text(AppLocalizations.of(context)!.getLocalizedValue('device')!, style: titleStyle, textAlign: TextAlign.center,)),
                         Expanded(child: Text(AppLocalizations.of(context)!.getLocalizedValue('access')!, style: titleStyle, textAlign: TextAlign.center)),
                         Expanded(child: Text(AppLocalizations.of(context)!.getLocalizedValue('date')!, style: titleStyle, textAlign: TextAlign.center)),
                         Expanded(child: Text("Down \n Mbit/s", style: titleStyle, textAlign: TextAlign.center)),
                         Expanded(child: Text("Up \n Mbit/s", style: titleStyle, textAlign: TextAlign.center)),
                         Expanded(child: Text("Ping \n ms", style: titleStyle, textAlign: TextAlign.center))
                       ],
                     ),
                   ),
             ),
              Expanded(
                  child: SizedBox(
                    width: columnSize * COLUMN_CNT,
                    child: new Container(
                      width: double.maxFinite,
                      height: double.infinity,
                      child: ListView.builder(
                          itemCount: historyResponses.length,
                          itemBuilder: (BuildContext context, int index){
                            HistoryResponse data = historyResponses[index];
                            return new Column(
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                new TextButton(
                                    onPressed: (){
                                      restClient.testResult(widget.appConfig, data.testUuid).catchError((error)=>restClient.showRestErrorDialog(context, error))
                                          .then((response){
                                        if(response == null)
                                          return ;

                                        TestData testData = new TestData(status: ResultStatus.OK, testUuid: data.testUuid);
                                        testData.testResultResponse = response;
                                        Navigator.push(context, new MaterialPageRoute<ShowMapData>(builder: (_)=> new ResultTestPage(testData: testData, config: widget.appConfig,))).then((mapData){
                                          if(mapData != null){
                                            widget.showTestInMap(mapData);
                                          }
                                        });
                                      });
                                    },
                                    child: new Container(
                                      child: new Row(
                                        children: <Widget>[
                                          new Expanded(child: new Text(widgetHelper.notNullString(data.model), style: textStyle, textAlign: TextAlign.center,)),
                                          new Expanded(child: new Text(widgetHelper.notNullString(data.networkType), style: textStyle, textAlign: TextAlign.center)),
                                          new Expanded(child: new Text(widgetHelper.notNullString(data.timeString), style: textStyle, textAlign: TextAlign.center)),
                                          new Expanded(child: new Text(widgetHelper.notNullString(data.speedDownload), style: textStyle, textAlign: TextAlign.center)),
                                          new Expanded(child: new Text(widgetHelper.notNullString(data.speedUpload), style: textStyle, textAlign: TextAlign.center)),
                                          new Expanded(child: new Text(widgetHelper.notNullString(data.ping), style: textStyle, textAlign: TextAlign.center))
                                        ],
                                      ),
                                    )
                                ),
                                new Divider(color: Colors.blueGrey,)
                              ],
                            );
                          }
                      ),
                    ),
                  )
              )
            ],
          ),
        ),
      ) : Container(
        alignment: Alignment.center,
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.blueGrey),
        ),
      ),
    );
  }

}
