/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:netmetr_flutter/app_config.dart';
import 'package:netmetr_flutter/net/basic_info.dart';
import 'package:netmetr_flutter/net/response/settings_response.dart';
import 'package:netmetr_flutter/pages/loop_ios_page.dart';
import 'package:netmetr_flutter/pages/main_fragments/fragment_state.dart';
import 'package:netmetr_flutter/pages/run_test_page.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:netmetr_flutter/util/config_helper.dart';
import 'package:netmetr_flutter/util/location/location_data.dart';
import 'package:netmetr_flutter/util/location/show_map_data.dart';
import 'package:netmetr_flutter/util/math_helper.dart' as mathHelper;
import 'package:netmetr_flutter/util/network/network_type.dart';
import 'package:netmetr_flutter/util/widget_helper.dart' as widgetHelper;
import 'package:netmetr_flutter/util/network/network_data.dart';
import 'package:netmetr_flutter/util/network/network_connection_type.dart';
import 'package:netmetr_flutter/util/classification.dart';
import 'package:netmetr_flutter/util/dialog_helper.dart' as dialogHelper;
import 'package:netmetr_flutter/widgets/bounce_icon_button.dart';
import 'package:permission_handler/permission_handler.dart' as permissionHandler;
import 'package:netmetr_flutter/util/shared_helper.dart' as sharedHelper;


const LOOP_START = "loop start";
const LOOP_STOP = "loop stop";
const LOOP_ACTIVE = "loop active";

class StartFragment extends StatefulWidget {
  final AppConfig appConfig;
  final SettingsTaskStatus settingsTaskStatus;
  final Function() startLoopService;
  final Function(ShowMapData) showMapData;
  final Function() showHistory;
  final Function() startSettingTask;
  final Function() changeMobileDataWaring;

  StartFragment({Key? key,
    required this.settingsTaskStatus,
    required this.showMapData,
    required this.appConfig,
    required this.startLoopService,
    required this.showHistory,
    required this.startSettingTask,
    required this.changeMobileDataWaring
  }) : super (key: key);

  @override
  _StartFragmentState createState() => _StartFragmentState();
}

class _StartFragmentState extends FragmentState<StartFragment> {
  static const statChannel = const MethodChannel('nic.cz.netmetrflutter/statChannel');

  late Timer _timer;
  BasicInfo? basicInfo;

  bool cpuOS = false;
  double? cpuUsed = -1.0;
  MemInfo? memInfo = new MemInfo();
  DataType? showedDataDetail;

  int trafficDown = 0;
  int trafficUp = 0;
  TrafficClassification lastDownTraffic = TrafficClassification.NONE;
  TrafficClassification lastUpTraffic = TrafficClassification.NONE;

  LocationData? locationData;

  NetworkData? networkData;
  NetworkType? networkType;
  NetworkConnectionType? networkConnectionType;
  NetworkCheckInfo? checkIpResponse;

  @override
  void initState() {
    super.initState();
    _timer = new Timer.periodic(new Duration(milliseconds: 1000), _updateStat);
  }

  @override
  callActionsFunction(int actionIndex){
      dialogHelper.showCheckBoxDialog(
          context: context,
          status: dialogHelper.DialogStatus.WARNING,
          check: ConfigHelper.instance!.uncheckDataWarning!,
          barrierDismissible: false,
          title: AppLocalizations.of(context)!.getLocalizedValue('data_warning_title')!,
          message: AppLocalizations.of(context)!.getLocalizedValue('data_warning_message')!,
          checkMessage: AppLocalizations.of(context)!.getLocalizedValue('not_display_again')!,
          uncheck: (check) {
            ConfigHelper.instance!.uncheckDataWarning = check;
            sharedHelper.setUncheckDataWarning(check);
          },
      ).whenComplete(widget.changeMobileDataWaring);
  }

  _startTestErrorDialog(BuildContext context){
    dialogHelper.showAlertDialog(
      context: context,
      body: Container(
        alignment: Alignment.center,
        child: Column(
          children: [
            Text(AppLocalizations.of(context)!.getLocalizedValue("android_mobile_network_info")!),
            Padding(
              padding: const EdgeInsets.only(top: 5),
              child: ElevatedButton(
                onPressed: () {  Navigator.pop(context);
                permissionHandler.openAppSettings(); },
                child: Text(AppLocalizations.of(context)!.getLocalizedValue("set_permissions")!),
              ),
            )
          ],
        )
      ),
      cancelButtonKey: "back",
      cancel: ()=> Navigator.pop(context),
      okButtonKey: "continue_anyway",
      ok: (){
        Navigator.pop(context);
        _startTest(context);
      }
    );
  }

  _startTest(BuildContext context) {
    if(networkConnectionType != NetworkConnectionType.NOT_CONNECTED && widget.settingsTaskStatus == SettingsTaskStatus.FINISHED){

      if(!widget.appConfig.appSettings.loopMode) {
        Navigator.push(context, MaterialPageRoute(builder: (_) =>
        new RunTestPage(
          networkData: networkData,
          config: AppConfig.of(context)!,
          showTestInMap: widget.showMapData,
        )));
      }else{

        if(Platform.isAndroid) {
          widget.startLoopService();
        }else{

          Navigator.push(context, MaterialPageRoute<bool>(builder: (_) => new LoopIOSPage(config: widget.appConfig,))).then((value){
            if(value != null && value){
              widget.showHistory();
            }
          });

        }
      }
    }
  }

  @override
  void dispose() {
    print("---------- dispose ------------");
    _timer.cancel();
    super.dispose();
  }

  Future<MemInfo?>_getMemInfo() async{
    try{
      var result = await statChannel.invokeMethod("getMemInfo");

      var free = result['free_memory'];
      var total = result['total_memory'];

      var percent = mathHelper.roundDouble(100.0 - ((free / total) * 100.0), 3);

      return new MemInfo(percent: percent,free: free, total: total);

    } on PlatformException catch (e){
      print(e.message);
    }

    return null;
  }


  _updateStat(Timer t) async {
    cpuUsed = await _getCpuInfo();
    memInfo = await _getMemInfo();
    _updateBackgroundProcess();
    _updateLocation();

    if(networkConnectionType != NetworkConnectionType.NOT_CONNECTED) {

      var data = await _getNetworkSignal();

      try {
        if (networkData != null && data != null) {
          networkData!.relativeSignal = data["relativeSignal"];
          networkData!.signalType = SignalType.values[data["signalType"]];
          networkData!.signal = data["signal"];
        }
      }catch (e){
        print(e);
      }

    }

    if(this.mounted) {
      setState(() {

      });
    }

  }

  _updateLocation() async{
    try{
      var result  =  await statChannel.invokeMethod("getLocation");

      if(result != null){
        locationData = LocationData.fromJson(json.decode(result));
        ConfigHelper.instance!.lastLocation = locationData;
      }


    } on PlatformException catch (e){
      print(e.message);
    }
  }

  _updateBackgroundProcess() async{
    try{
      var result  =  await statChannel.invokeMethod("getTraffic");

      trafficDown = result["down"];
      trafficUp = result["up"];

      lastDownTraffic = TrafficClassification.classify(trafficDown);
      lastUpTraffic = TrafficClassification.classify(trafficUp);

    } on PlatformException catch (e){
      print(e.message);
    }
  }

  Future<dynamic> _getNetworkSignal() async{
    try{
      return await statChannel.invokeMethod("getNetworkSignal");
    } on PlatformException catch (e){
      print(e.message);
    }

    return null;
  }

  Future<double> _getCpuInfo() async{
    try{
      var cpus = await statChannel.invokeMethod("getCpu");

      if(cpus == null) {

        if(widget.appConfig.deviceInfo?.systemOS is int){
          cpuOS = widget.appConfig.deviceInfo?.systemOS >= 26;
        }

        return -1.0;
      }

      if(Platform.isAndroid){
        if (cpus != null && cpus.length > 0) {
          double total = 0.0;
          for (double cpu in cpus) {
            total += cpu;
          }
          return mathHelper.roundDouble(total / cpus.length * 100.0, 3);
        }
      }else {
        return mathHelper.roundDouble(cpus + 0.0, 3);
      }
    } on PlatformException catch (e){
      print(e.message);
    }

    return 0.0;
  }

  _showCpuErrorInfo(BuildContext context){
    dialogHelper.showAlertDialog(
      context: context,
      body: new Container(
        alignment: Alignment.center,
        child: new Text(AppLocalizations.of(context)!.getLocalizedValue('cpu_err_msg')!),
      )
    );
  }

  _showNetworkErrorDialog(BuildContext context, String errorKey){
    dialogHelper.showAlertDialog(
        context: context,
        body: new Container(
          alignment: Alignment.center,
          child: new Text(AppLocalizations.of(context)!.getLocalizedValue(errorKey)!),
        ),
        okButtonKey: 'set_permissions',
        ok: (){
          Navigator.pop(context);
          permissionHandler.openAppSettings();
        },
        cancelButtonKey: 'back',
        cancel: ()=> Navigator.pop(context)
    );
  }

  _ssid(NetworkData? data, NetworkConnectionType? type){
    if(_haveSSID(data) ) return data!.ssid;

    return type == NetworkConnectionType.WIFI ? "wifi" : "mobile";
  }

  bool _haveSSID(NetworkData? data){
    if(data == null) return false;

    return data.ssid != null && data.ssid!.isNotEmpty && data.ssid != "null";
  }

  _showSSIDInfo(){
    if(Platform.isIOS || _haveSSID(networkData)) return false;


    return widget.appConfig.deviceInfo?.systemOS > 27 && networkConnectionType == NetworkConnectionType.WIFI && (locationData == null || !locationData!.haveLocationData || !locationData!.locationPermission);
  }

  _showMobileNetworkTypeError(){
    return networkType != null && networkConnectionType == NetworkConnectionType.MOBILE && networkType == NetworkType.UNKNOWN;
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    ThemeData themeData = Theme.of(context);
    Color accentColor = themeData.colorScheme.secondary;
    TextStyle titleDataDetailStyle = themeData.textTheme.headline6!.copyWith(color: Colors.white);
    TextStyle subStyle = themeData.textTheme.subtitle1!.copyWith(color: Colors.black);
    TextStyle correctSubStyle = themeData.textTheme.subtitle1!.copyWith(color: Colors.green);

    networkData = widget.appConfig.networkModel.networkData;
    networkType = widget.appConfig.networkModel.networkType;
    networkConnectionType = widget.appConfig.networkModel.networkConnectionType;
    checkIpResponse = widget.appConfig.networkModel.checkInfo;

    bool useMobileLayout = widgetHelper.useMobileLayout(mediaQueryData);
    double networkImageSize =  mediaQueryData.size.width * (useMobileLayout ? 0.4 : 0.42);
    bool loopModeActive = widget.appConfig.configModel.loopModeActive;
    String? networkFamily = networkType != null ? NetworkConnectionType.getNetworkFamily(networkType!) : null;
    bool wifiSSIDError = _showSSIDInfo();
    bool mobileNetworkError = _showMobileNetworkTypeError();

    return new WillPopScope(
        child: new Scaffold(
          body: new Container(
              width: mediaQueryData.size.width,
              height: mediaQueryData.size.height,
              color: Colors.white,
              child: new Stack(
                children: <Widget>[
                  new SingleChildScrollView(
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Stack(
                              children: <Widget>[
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    new Container(
                                      margin: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                                      alignment: Alignment.center,
                                      child: networkConnectionType == NetworkConnectionType.NOT_CONNECTED ? new Icon(Icons.signal_wifi_off, color: Colors.blueGrey, size: networkImageSize,) : widgetHelper.getConnectivityImage(networkConnectionType!, networkData!, useMobileLayout),
                                    ),
                                    networkConnectionType != NetworkConnectionType.NOT_CONNECTED ?
                                    new Container(
                                      alignment: Alignment.center,
                                      child: new Text(networkFamily == null ? _ssid(networkData, networkConnectionType) : "${_ssid(networkData, networkConnectionType)} $networkFamily", style: subStyle,) ,
                                    ): new Container(),
                                  ],
                                ),
                                networkData != null && (wifiSSIDError || mobileNetworkError) ? Positioned(
                                    bottom: 0,
                                    right: 5,
                                    child: BounceIconButton(
                                      icon: Icons.error,
                                      onPressed: ()=> _showNetworkErrorDialog(context, wifiSSIDError ? "android_ssid_info" : "android_mobile_network_info"),
                                      colorBack: Colors.red,
                                      colorIcon: Colors.red,
                                      iconSize: 35,
                                    )
                                ) : Container(),
                              ],
                            ),
                            new Padding(padding: const EdgeInsets.only(top: 10.0),
                              child: new Divider(color: Colors.black,),
                            ),
                            new Row(
                              children: <Widget>[
                                new Expanded(
                                    child: new TextButton(
                                        onPressed: (){
                                          setState(() {
                                            showedDataDetail = DataType.CPU_MEMORY;
                                          });
                                        },
                                        child: new Container(
                                          padding: const EdgeInsets.symmetric(vertical: 15.0),
                                          child: new Table(
                                            children: [
                                              new TableRow(
                                                  children: [
                                                    new Text(AppLocalizations.of(context)!.getLocalizedValue('processor')!, textAlign: TextAlign.end, style: subStyle,),
                                                    new Text(cpuUsed != -1 ? "${mathHelper.roundDouble(cpuUsed!, 2)} %" : "-", textAlign: TextAlign.center, style: correctSubStyle,) //TODO when fail
                                                  ]
                                              ),
                                              new TableRow(
                                                  children: [
                                                    new Text(AppLocalizations.of(context)!.getLocalizedValue('memory')!, textAlign: TextAlign.end, style: subStyle,),
                                                    new Padding(padding: const EdgeInsets.only(left: 5.0),
                                                        child: new Text("${mathHelper.roundDouble(memInfo!.percent, 2)} %", textAlign: TextAlign.center, style: correctSubStyle,) //TODO when fail
                                                    )
                                                  ]
                                              )
                                            ],
                                          ),
                                        )
                                    )
                                ),
                                new Expanded(
                                    child: new TextButton(
                                        onPressed: (){
                                          if(checkIpResponse == null)
                                            return;

                                          showedDataDetail = DataType.IP;
                                        },
                                        child: new Container(
                                          padding: const EdgeInsets.symmetric(vertical: 15.0),
                                          child: new Table(
                                            children: [
                                              new TableRow(
                                                  children: [
                                                    new Text("IPv4", textAlign: TextAlign.end, style: subStyle,),
                                                    new Container(
                                                        alignment: Alignment.center,
                                                        child: checkIpResponse != null ? _getIconByStat(checkIpResponse!.ipV4Status) : networkConnectionType != NetworkConnectionType.NOT_CONNECTED ? widgetHelper.getSmallProgress() : _getIconByStat(IpStatus.STATUS_NOT_AVAILABLE) //TODO when fail
                                                    )
                                                  ]
                                              ),
                                              new TableRow(
                                                  children: [
                                                    new Text("IPv6", textAlign: TextAlign.end, style: subStyle,),
                                                    new Container(
                                                        alignment: Alignment.center,
                                                        child: checkIpResponse != null ? _getIconByStat(checkIpResponse!.ipV6Status) : networkConnectionType != NetworkConnectionType.NOT_CONNECTED ? widgetHelper.getSmallProgress() : _getIconByStat(IpStatus.STATUS_NOT_AVAILABLE) //TODO when fail
                                                    )
                                                  ]
                                              )
                                            ],
                                          ),
                                        )
                                    )
                                )
                              ],
                            ),
                            new Padding(padding: const EdgeInsets.only(bottom: 10.0),
                              child: new Divider(color: Colors.black),
                            ),
                            new Row(
                              children: <Widget>[
                                new Expanded(
                                    child: new TextButton(
                                        onPressed: (){
                                          showedDataDetail = DataType.TRAFFIC;
                                        },
                                        child: new Column(
                                          children: <Widget>[
                                            new Row(
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: <Widget>[
                                                new Container(
                                                  margin: const EdgeInsets.symmetric(vertical: 20.0),
                                                  child: new Image.asset(lastDownTraffic.asset, color: Colors.black, scale: 2.5,),
                                                ),
                                                new Container(
                                                  margin: const EdgeInsets.only(left: 20.0, top: 20.0, bottom: 20.0),
                                                  child: new Transform.rotate(
                                                    angle: mathHelper.degreesToRadians(180),
                                                    child: new Image.asset(lastUpTraffic.asset, color: Colors.black, scale: 2.5),
                                                  ),
                                                )
                                              ],
                                            ),
                                            new Container(
                                              margin: const EdgeInsets.all(5.0),
                                              alignment: Alignment.bottomCenter,
                                              child: new Text(AppLocalizations.of(context)!.getLocalizedValue('backwork')!, style: subStyle,),
                                            )
                                          ],
                                        )
                                    )
                                ),
                                new Expanded(
                                    child: new TextButton(
                                        onPressed: (){
                                          setState(() {
                                            showedDataDetail = DataType.LOCATION;
                                          });
                                        },
                                        child: new Column(
                                          children: <Widget>[
                                            new Container(
                                              margin: const EdgeInsets.symmetric(horizontal: 5.0, vertical: 20.0),
                                              child: new Icon(_isLocationEnabled(locationData) ? Icons.my_location : Icons.location_disabled, color: Colors.black,),
                                            ),
                                            new Container(
                                              margin: const EdgeInsets.all(5.0),
                                              alignment: Alignment.bottomCenter,
                                              child: new Text(AppLocalizations.of(context)!.getLocalizedValue('location')!, style: subStyle,),
                                            )
                                          ],
                                        )
                                    )
                                )
                              ],
                            ),
                            new Divider(color: Colors.black)
                          ]
                      )
                  ),
                  showedDataDetail == null ? new Container() : new Center(child: _getDataDetail(showedDataDetail!, titleDataDetailStyle),)
                ],
              )
          ),
          bottomNavigationBar: new ElevatedButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.resolveWith<Color>((states){
                if(states.contains(MaterialState.disabled))
                  return loopModeActive ? Colors.blueGrey : Colors.grey;

                return accentColor;
              }),

            ),
            onPressed: (networkConnectionType != NetworkConnectionType.NOT_CONNECTED && widget.settingsTaskStatus != SettingsTaskStatus.RUNNING) && !loopModeActive ? (){
              if(widget.settingsTaskStatus == SettingsTaskStatus.FINISHED) !mobileNetworkError ? _startTest(context) : _startTestErrorDialog(context);
              else widget.startSettingTask();
            }: null,
            child: new Container(
              alignment: Alignment.center,
              height: mediaQueryData.size.height * 0.12,
              child: networkConnectionType == NetworkConnectionType.NOT_CONNECTED ? new Text(" - "): widget.settingsTaskStatus != SettingsTaskStatus.RUNNING ?
              new Text(!loopModeActive ? _startButtonText(context) : AppLocalizations.of(context)!.getLocalizedValue('testIsRunningCAPS')!, style: new TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 25.0),)
                  : new CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.white),),
            ),
          ),
        ),
        onWillPop: onBackPressed);

  }

  String _startButtonText(BuildContext context){
    return  widget.settingsTaskStatus == SettingsTaskStatus.FINISHED ? 'START' : AppLocalizations.of(context)!.getLocalizedValue('repeatSetting')!;
  }

  Future<bool> onBackPressed(){
    if(showedDataDetail != null){

      if(this.mounted) {
        setState(() {
          showedDataDetail = null;
        });
      }

      return Future.value(false);
    }

    return Future.value(true);
  }

  Icon _getIconByStat(IpStatus? status){
    switch (status){
      case IpStatus.NO_ADDRESS: return new Icon(Icons.close, color: Colors.red,);
      case IpStatus.ONLY_LOCAL: return new Icon(Icons.check, color: Colors.yellow,);
      case IpStatus.CONNECTED_NAT: return new Icon(Icons.check, color: Colors.yellow,);
      case IpStatus.CONNECTED_NO_NAT: return new Icon(Icons.check, color: Colors.green,);
      default:
        return new Icon(Icons.not_interested, color: Colors.blueGrey,);
    }
  }


  Widget _getDataDetail(DataType type, TextStyle titleStyle){
    return new Container(
      width: double.maxFinite,
      margin: const EdgeInsets.symmetric(horizontal: 10.0),
      color: const Color.fromARGB(170, 0, 0, 0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          new Container(
            alignment: Alignment.center,
            child: type == DataType.CPU_MEMORY && Platform.isAndroid ? new Container(
                  width: double.infinity,
                  child: new Stack(
                    children: <Widget>[
                      new Container(alignment: Alignment.center,
                        child: new Text(AppLocalizations.of(context)!.getLocalizedValue(type.title)!, style: titleStyle,),
                      ),
                      cpuUsed == null || cpuUsed == -1 ? new Positioned.fill(
                          child: new Container(
                            alignment: Alignment.topRight,
                            child: new IconButton(
                              icon: new Icon(Icons.info, color: Colors.white,),
                              onPressed: ()=> _showCpuErrorInfo(context)
                            ),
                          )
                      ) : Container()
                    ],
                  ),
            ) : new Text(AppLocalizations.of(context)!.getLocalizedValue(type.title)!, style: titleStyle,),
          ),
          new Builder(builder: (BuildContext context){
            switch (type){
              case DataType.CPU_MEMORY:

                return new Container(
                  child: new Table(
                    children: [
                      new TableRow(
                        children: [
                          new Container(
                            margin: const EdgeInsets.symmetric(vertical: 5.0),
                            child: new Text(AppLocalizations.of(context)!.getLocalizedValue('procusage')!,textAlign: TextAlign.end ,style: new TextStyle(color: Colors.white),),
                          ),
                          new Container(
                            margin: const EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
                            child: new Text(cpuUsed != -1 ? "${mathHelper.roundDouble(cpuUsed!, 2)} %" : "-", textAlign: TextAlign.left, style: new TextStyle(color: Colors.white),),
                          )
                        ]
                      ),
                      new TableRow(
                        children: [
                          new Container(
                            margin: const EdgeInsets.symmetric(vertical: 5.0),
                            child: new Text(AppLocalizations.of(context)!.getLocalizedValue('memusage')!,textAlign: TextAlign.end ,style: new TextStyle(color: Colors.white),),
                          ),
                          new Container(
                            margin: const EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
                            child: new Text("${mathHelper.roundDouble(memInfo!.percent, 2)} %", textAlign: TextAlign.left, style: new TextStyle(color: Colors.white),),
                          )
                        ]
                      ),
                      new TableRow(
                          children: [
                            new Container(
                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              child: new Text(AppLocalizations.of(context)!.getLocalizedValue('freeMem')!,textAlign: TextAlign.end ,style: new TextStyle(color: Colors.white),),
                            ),
                            new Container(
                              margin: const EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
                              child: new Text("${mathHelper.roundDouble(memInfo!.free / 1000, 2)} MB", textAlign: TextAlign.left, style: new TextStyle(color: Colors.white),),
                            )
                          ]
                      ),
                      new TableRow(
                          children: [
                            new Container(
                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              child: new Text(AppLocalizations.of(context)!.getLocalizedValue('memusage')!,textAlign: TextAlign.end ,style: new TextStyle(color: Colors.white),),
                            ),
                            new Container(
                              margin: const EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
                              child: new Text("${mathHelper.roundDouble(memInfo!.total / 1000, 2)} MB", textAlign: TextAlign.left, style: new TextStyle(color: Colors.white),),
                            )
                          ]
                      )
                    ],
                  ),
                );

              case DataType.TRAFFIC:

                return new Container(
                  child: new Table(
                    children: [
                      new TableRow(
                          children: [
                            new Container(
                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              child: new Text("Upload",textAlign: TextAlign.end ,style: new TextStyle(color: Colors.white),),
                            ),
                            new Container(
                              margin: const EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
                              child: new Text("${trafficUp / 125000} Mbit/s", textAlign: TextAlign.left, style: new TextStyle(color: Colors.white),),
                            )
                          ]
                      ),
                      new TableRow(
                          children: [
                            new Container(
                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              child: new Text("Download",textAlign: TextAlign.end ,style: new TextStyle(color: Colors.white),),
                            ),
                            new Container(
                              margin: const EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
                              child: new Text("${trafficDown / 125000} Mbit/s", textAlign: TextAlign.left, style: new TextStyle(color: Colors.white),),
                            )
                          ]
                      ),
                    ],
                  ),
                );

              case DataType.LOCATION:

                return new Container(
                  child: _isLocationEnabled(locationData) ?
                    new Container(
                      child: new Table(
                        children: [
                          new TableRow(
                              children: [
                                new Container(
                                  margin: const EdgeInsets.symmetric(vertical: 5.0),
                                  child: new Text(AppLocalizations.of(context)!.getLocalizedValue('position')!,textAlign: TextAlign.end ,style: new TextStyle(color: Colors.white),),
                                ),
                                new Container(
                                  margin: const EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
                                  child: new Text(_getLocationText(locationData!), textAlign: TextAlign.left, style: new TextStyle(color: Colors.white),),
                                )
                              ]
                          ),
                          new TableRow(
                              children: [
                                new Container(
                                  margin: const EdgeInsets.symmetric(vertical: 5.0),
                                  child: new Text(AppLocalizations.of(context)!.getLocalizedValue('accuracy')!,textAlign: TextAlign.end ,style: new TextStyle(color: Colors.white),),
                                ),
                                new Container(
                                  margin: const EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
                                  child: new Text(locationData!.hasAccuracy ? "+/- ${mathHelper.roundDouble(locationData!.accuracy!, 2)} m" : AppLocalizations.of(context)!.getLocalizedValue('notAvailable')!, textAlign: TextAlign.left, style: new TextStyle(color: Colors.white),),
                                )
                              ]
                          ),
                          new TableRow(
                              children: [
                                new Container(
                                  margin: const EdgeInsets.symmetric(vertical: 5.0),
                                  child: new Text(AppLocalizations.of(context)!.getLocalizedValue('updated')!,textAlign: TextAlign.end ,style: new TextStyle(color: Colors.white),),
                                ),
                                new Container(
                                  margin: const EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
                                  child: new Text(locationData!.age! < 1000 ? "< 1s" : "${mathHelper.roundDouble((locationData!.age! / 1000.0), 2)} s", textAlign: TextAlign.left, style: new TextStyle(color: Colors.white),),
                                )
                              ]
                          ),
                          Platform.isAndroid ? new TableRow(
                              children: [
                                new Container(
                                  margin: const EdgeInsets.symmetric(vertical: 5.0),
                                  child: new Text(AppLocalizations.of(context)!.getLocalizedValue('source')!,textAlign: TextAlign.end ,style: new TextStyle(color: Colors.white),),
                                ),
                                new Container(
                                  margin: const EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
                                  child: new Text(_getProviderText(locationData!), textAlign: TextAlign.left, style: new TextStyle(color: Colors.white),),
                                )
                              ]
                          ) : new TableRow( children: [ new Container(), new Container()]),
                          new TableRow(
                              children: [
                                new Container(
                                  margin: const EdgeInsets.symmetric(vertical: 5.0),
                                  child: new Text(AppLocalizations.of(context)!.getLocalizedValue('altitude')!,textAlign: TextAlign.end ,style: new TextStyle(color: Colors.white),),
                                ),
                                new Container(
                                  margin: const EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
                                  child: new Text(locationData!.altitude != null && locationData!.altitude! > 0 ? "${mathHelper.roundDouble(locationData!.altitude!, 2)} m" : AppLocalizations.of(context)!.getLocalizedValue('notAvailable')!, textAlign: TextAlign.left, style: new TextStyle(color: Colors.white),),
                                )
                              ]
                          ),
                        ],
                      )
                    ) :
                    new Container(
                      margin: const EdgeInsets.all(10.0),
                      child: new Text(locationData == null ? AppLocalizations.of(context)!.getLocalizedValue('noData3')! : AppLocalizations.of(context)!.getLocalizedValue('noAuthorization')!, style: new TextStyle(color: Colors.white),),
                    ),
                );

              case DataType.IP:

                return new Container(
                    child: new Table(
                      children: [
                        new TableRow(
                            children: [
                              new Container(
                                margin: const EdgeInsets.symmetric(vertical: 5.0),
                                child: new Text(AppLocalizations.of(context)!.getLocalizedValue('private')! + " IPv4",textAlign: TextAlign.end ,style: new TextStyle(color: Colors.white),),
                              ),
                              new Container(
                                margin: const EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
                                child: new Text(widgetHelper.notNullString(checkIpResponse?.privateIpV4 ?? "-"), textAlign: TextAlign.left, style: new TextStyle(color: Colors.white),),
                              )
                            ]
                        ),
                        new TableRow(
                            children: [
                              new Container(
                                margin: const EdgeInsets.symmetric(vertical: 5.0),
                                child: new Text(AppLocalizations.of(context)!.getLocalizedValue('public')! + " IPv4",textAlign: TextAlign.end ,style: new TextStyle(color: Colors.white),),
                              ),
                              new Container(
                                margin: const EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
                                child: new Text(widgetHelper.notNullString(checkIpResponse?.publicIpV4 ?? "-"), textAlign: TextAlign.left, style: new TextStyle(color: Colors.white),),
                              )
                            ]
                        ),
                        new TableRow(
                            children: [
                              new Container(
                                margin: const EdgeInsets.symmetric(vertical: 5.0),
                                child: new Text(AppLocalizations.of(context)!.getLocalizedValue('private')! + " IPv6",textAlign: TextAlign.end ,style: new TextStyle(color: Colors.white),),
                              ),
                              new Container(
                                margin: const EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
                                child: new Text(widgetHelper.notNullString(checkIpResponse?.privateIpV6 ?? "-"), textAlign: TextAlign.left, style: new TextStyle(color: Colors.white),),
                              )
                            ]
                        ),
                        new TableRow(
                            children: [
                              new Container(
                                margin: const EdgeInsets.symmetric(vertical: 5.0),
                                child: new Text(AppLocalizations.of(context)!.getLocalizedValue('public')! + " IPv6",textAlign: TextAlign.end ,style: new TextStyle(color: Colors.white),),
                              ),
                              new Container(
                                margin: const EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
                                child: new Text(widgetHelper.notNullString(checkIpResponse?.publicIpV6 ?? "-"), textAlign: TextAlign.left, style: new TextStyle(color: Colors.white),),
                              )
                            ]
                        ),
                      ],
                    ),
                );


            }

            return new Container();
          }),
          new Container(
            alignment: Alignment.centerRight,
            child: TextButton(
              onPressed: (){
                setState(() {
                  showedDataDetail = null;
                });
              },
              child: new Icon(Icons.arrow_back_ios, color: Colors.white,)),
          )
        ],
      ),
    );
  }

  String _getProviderText(LocationData locationData){
    if(locationData.provider == null) return "-";
    else if(locationData.provider == "network") return "Síť";
    else if (locationData.provider == "gps") return "GPS";

    return locationData.provider!;
  }

  bool _isLocationEnabled(LocationData? locationData){
    return locationData != null && locationData.locationPermission && !(locationData.latitude == null || locationData.longitude == null || locationData.latitudeString == null || locationData.longitudeString == null);
  }

  String _getLocationText(LocationData locationData){
    if(locationData.latitude == null || locationData.longitude == null || locationData.latitudeString == null || locationData.longitudeString == null)
      return AppLocalizations.of(context)!.getLocalizedValue('notAvailable')!;


    try {
      var latitude = locationData.latitudeString!.split(":");
      var longitude = locationData.longitudeString!.split(":");

      latitude[1] = latitude[1].replaceFirst(",", ".");
      longitude[1] = longitude[1].replaceFirst(",", ".");

      double lat = mathHelper.roundDouble(double.parse(latitude[1]), 3);
      double long = mathHelper.roundDouble(double.parse(longitude[1]), 3);


      return (locationData.latitude! >= 0 ? "N" : "S") + " " +
          latitude[0] + "°$lat' " +
          (locationData.longitude! >= 0 ? "E" : "W") + " " +
          longitude[0] + "°$long'";
    } catch(e){
      print("Parse location error: $e");
      return AppLocalizations.of(context)!.getLocalizedValue('notAvailable')!;
    }
  }

}

class DataType{
  final String title;

  const DataType({required this.title});

  static const CPU_MEMORY = const DataType(title: "cpuMemUsage");
  static const IP = const DataType(title: "IP");
  static const TRAFFIC = const DataType(title: "backwork");
  static const LOCATION = const DataType(title: "location");
}

class MemInfo{
  var percent;
  var free;
  var total;
  MemInfo({this.percent = 0.0, this.free = 0.0, this.total = 0.0});
}