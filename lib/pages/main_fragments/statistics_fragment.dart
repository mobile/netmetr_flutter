/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/app_config.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:netmetr_flutter/widgets/web_view.dart';

class StatisticsFragment extends StatefulWidget {
  final AppConfig appConfig;

  StatisticsFragment({
    Key? key,
    required this.appConfig
  }) : super(key: key);

  @override
  _StatisticsFragmentState createState() => _StatisticsFragmentState();
}

class _StatisticsFragmentState extends State<StatisticsFragment> {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: AppWebView(url: (widget.appConfig.configModel.statisticsHost != null) ? widget.appConfig.configModel.statisticsHost : AppLocalizations.of(context)!.getLocalizedValue('wwwStatistics'))
    );
  }
}
