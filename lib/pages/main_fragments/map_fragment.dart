/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:netmetr_flutter/app_config.dart';
import 'package:netmetr_flutter/net/response/map_response.dart';
import 'package:netmetr_flutter/net/response/test_result_response.dart';
import 'package:netmetr_flutter/pages/main_fragments/fragment_state.dart';
import 'package:netmetr_flutter/pages/map_filter.dart';
import 'package:netmetr_flutter/pages/map_settings.dart';
import 'package:netmetr_flutter/util/map_tile_provider.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:netmetr_flutter/util/config_helper.dart';
import 'package:netmetr_flutter/util/location/location_data.dart';
import 'package:netmetr_flutter/util/location/show_map_data.dart';
import 'package:netmetr_flutter/net/rest_client.dart' as restClient;
import 'package:netmetr_flutter/util/shared_helper.dart' as sharedHelper;
import 'package:netmetr_flutter/util/toast_helper.dart' as toastHelper;
import 'dart:ui' as ui;

import 'package:netmetr_flutter/widgets/marker_body.dart';


const default_map_lat = 49.600170585239695;
const default_map_lng = 15.509453192353249;
const default_map_zoom = 6.0;
const default_map_zoom_location = 16.0;

const map_type_change_zoom = 13;

const mark_show_size = 45.0;

const zoom_max = 20.0;
const zoom_min = 2.0;
const tile_size = 512;

const mapIconSize = 20.0;

const optionsMapDefault = "all/download";

const overlayPrefix = "om_";

class MapFragment extends StatefulWidget {
  final AppConfig appConfig;
  final ShowMapData? showMapData;

  MapFragment({Key? key,
  required this.appConfig,
  this.showMapData
  }) : super(key: key);

  @override
  _MapFragmentState createState() => _MapFragmentState();
}

class _MapFragmentState extends FragmentState<MapFragment> {
  GoogleMapController? mapController;
  MapAppOptions? mapOptions;
  TileOverlay? _tileOverlay;

  int _tileID = 0;

  GlobalKey _markKey = GlobalKey();

  List<Marker> markers = [];
  List<MarkData> marksData = [];

  Map<String, dynamic> currentMapOptions = new Map();
  Map<String, dynamic> currentFilter = new Map();
  Map<String, String> currentMapTitles = new Map();

  LatLng? startLocation;
  ShowMapData? showMapData;

  MapMeasurementsType actualMapType = MapMeasurementsType.POINTS;
  double? actualZoom;

  int pointParameter = 8;
  bool normalType = true;
  bool overlayAuto = true;
  bool needTechnology = true;
  bool loadShared = false;

  bool canZoomIn = true;
  bool canZoomOut = true;

  String? _mapStyle;

  @override
  void initState() {
    super.initState();
    
    rootBundle.loadString('assets/map/map_style').then((value) => _mapStyle = value);
    
    mapOptions = widget.appConfig.configModel.mapOptions;
    showMapData = widget.showMapData;

    if(widget.showMapData == null) {
      if (_userLocation()) {
        startLocation = new LatLng(ConfigHelper.instance!.lastLocation!.latitude!, ConfigHelper.instance!.lastLocation!.longitude!);
      } else {
        actualMapType = MapMeasurementsType.HEAT_MAP;
      }
    }else{
      startLocation = new LatLng(widget.showMapData!.lat, widget.showMapData!.lon);
      markers.add(Marker(
        markerId: MarkerId("showMark"),
        position: startLocation!
      ));
    }

    currentMapTitles.putIfAbsent("_SAT", () => "VZHLED MAPY: Normální");
    currentMapTitles.putIfAbsent("_OVERLAY", () => "VRSTVA MAPY: Automatická");

    if(mapOptions!=null) {
      if (!ConfigHelper.instance!.mapUsed!) {
        currentMapOptions["highlight"] = ConfigHelper.instance!.uuid;
        currentMapOptions["overlay_type"] = actualMapType.desc;
        currentMapOptions["map_options"] = optionsMapDefault;
        currentMapOptions["point_diameter"] = pointParameter;

        currentFilter.putIfAbsent("_SAT", () => "NOSAT");
        currentFilter.putIfAbsent("_OVERLAY", () => "AUTO");
        currentMapOptions.putIfAbsent("_SAT", () => "NOSAT");
        currentMapOptions.putIfAbsent("_OVERLAY", () => "AUTO");

        for (MapData mapData in mapOptions!.mapFilters["mobile"]!) {
          for (Options options in mapData.options) {
            if (options.isDefault && options.key != null &&
                options.value != null) {
              currentMapOptions[options.key!] = options.value;
              currentFilter[options.key!] = options.value;
              currentMapTitles[options.key!] =
                  mapData.title + ": ${options.optionsData["summary"]}";
              break;
            }
          }
        }

        sharedHelper.setMapOptions(currentMapOptions);
        sharedHelper.setMapFilter(currentFilter);
        sharedHelper.setFirstMapUse();
        ConfigHelper.instance!.mapUsed = true;
        _setTileOverlay();
      } else {
        loadShared = true;
        _loadSavedOptions();
      }
    }


  }

  bool _userLocation(){
    LocationData? locationData = ConfigHelper.instance!.lastLocation;
    return locationData != null && locationData.locationPermission && locationData.haveLocationData && locationData.latitude != null && locationData.longitude != null;
  }

  _setTileOverlay(){
    mapController?.clearTileCache(TileOverlayId("$overlayPrefix$_tileID"));
    _tileID++;
    final TileOverlay tileOverlay = TileOverlay(
      tileOverlayId: TileOverlayId("$overlayPrefix$_tileID"),
      tileSize: tile_size,
      tileProvider: MapTileProvider(
          configModel: widget.appConfig.configModel,
          mapType: actualMapType.desc,
          currentMapOptions: currentMapOptions,
          needTechnology: needTechnology,
          tileSize: tile_size
      ),
    );
    setState(() {
      _tileOverlay = tileOverlay;
    });
  }

  _loadSavedOptions() async{

    if(ConfigHelper.instance!.mapUsed!){
      currentFilter = await sharedHelper.getMapFilter();
      currentMapOptions = await sharedHelper.getMapOptions();

      needTechnology = _needTechnologyKey(currentMapOptions["map_options"]);
      normalType = currentMapOptions["_SAT"] == "NOSAT";
      overlayAuto = currentMapOptions["_OVERLAY"] == "AUTO";

      if(!overlayAuto) {
        actualMapType = _mapTypeByDesc(currentMapOptions["_OVERLAY"]);
      }

      currentMapTitles["_SAT"] = AppLocalizations.of(context)!.getLocalizedValue('mapAppearCAPS')! + ": ${normalType ? AppLocalizations.of(context)!.getLocalizedValue('normal') : AppLocalizations.of(context)!.getLocalizedValue('static')}";
      currentMapTitles["_OVERLAY"] = AppLocalizations.of(context)!.getLocalizedValue('mapOverlayCAPS')! + ": ${overlayAuto ? AppLocalizations.of(context)!.getLocalizedValue('automatic') : actualMapType.title}";

      _setTilesByFilter();
      _setTileOverlay();
      setState(() {
        loadShared = false;
      });
    }

  }

  _setTilesByFilter(){
    List<String> split = currentMapOptions["map_options"].split('/');

    if(split.isNotEmpty){
      String type = split[0];
      type = type != "all" ? type : "mobile";

      for (MapData mapData in mapOptions!.mapFilters[type]!){
        for(Options options in mapData.options){
          if(currentFilter.containsKey(options.key) && currentFilter[options.key] == options.value){
            currentMapTitles[options.key!] = mapData.title + ": ${options.optionsData["summary"]}";
          }
        }
      }
    }
  }

  _updateFilterInOptions(){

    for(String key in currentFilter.keys){
      var value = currentFilter[key];
      currentMapOptions.update(key, (data){
        data = value;
        return data;
      }, ifAbsent: ()=> value);

      if(key == "_SAT"){
        normalType = value == "NOSAT";
        String satValue = AppLocalizations.of(context)!.getLocalizedValue('mapAppearCAPS')! + ": ${normalType ? AppLocalizations.of(context)!.getLocalizedValue('normal') : AppLocalizations.of(context)!.getLocalizedValue('static')}";
        currentMapTitles.update(key, (data){
          data = satValue;
          return data;
        }, ifAbsent: ()=> satValue);

      }else if(key == "_OVERLAY"){
        overlayAuto = value == "AUTO";
        actualMapType = !overlayAuto ? _mapTypeByDesc(value) : actualZoom != null && actualZoom! >= map_type_change_zoom ? MapMeasurementsType.POINTS : MapMeasurementsType.HEAT_MAP;
        currentMapOptions["overlay_type"] = actualMapType.desc;

        String overlayValue = AppLocalizations.of(context)!.getLocalizedValue('mapOverlayCAPS')! + ": ${overlayAuto ? AppLocalizations.of(context)!.getLocalizedValue('automatic') : actualMapType.title}";
        currentMapTitles.update(key, (data){
          data = overlayValue;
          return data;
        }, ifAbsent: ()=> overlayValue);
      }

    }

    _setTilesByFilter();
    _setTileOverlay();

    sharedHelper.setMapFilter(currentFilter);
    sharedHelper.setMapOptions(currentMapOptions);
  }

  MapMeasurementsType _mapTypeByDesc(String desc){
    switch (desc){
      case "HEATMAP": return MapMeasurementsType.HEAT_MAP;
      //case "REGIONS": return MapType.REGIONS;
      default: return MapMeasurementsType.POINTS;
    }
  }

  _changeMapType(MapMeasurementsType mapType){
    if(!mounted || currentMapOptions.isEmpty)
      return;

    currentMapOptions["overlay_type"] = mapType.desc;
    actualMapType = mapType;
    _setTileOverlay();
  }


  _mapClick(double lat, double lon, double zoom){
    restClient.mapMarker(widget.appConfig, lat, lon, zoom.round(), currentMapOptions, needTechnology)
      .catchError((error){

        toastHelper.showErrorToast(AppLocalizations.of(context)!.getLocalizedValue("map_detail_error")!);

    }).then((MarkData? markData){

      if(markData != null && mapController != null) {
        mapController?.getVisibleRegion().then((bounds){
          var diff = bounds.northeast.latitude - bounds.southwest.latitude;
          mapController?.moveCamera(CameraUpdate.newLatLngZoom(LatLng(markData.lat + diff / 5, markData.lon), zoom)).then((value){
            markers.clear();
            marksData.clear();

            setState(() {
              marksData.add(markData);
            });
            _createMarker(markData);
          });
        });


      }else{
        if(mounted) {
          setState(() {
            _clearMarker();
          });
        }
      }

    });
  }

  _addMarker(Uint8List byteData, MarkData markData){
    if(mounted){
      setState(() {
        markers.add(Marker(
          markerId: MarkerId("marker"),
          icon: BitmapDescriptor.fromBytes(byteData),
          position: LatLng(markData.lat, markData.lon)
        ));
      });
    }
  }

  _clearMarker(){
    if(mounted){
      setState(() {
        markers.clear();
        marksData.clear();
      });
    }
  }

  _createMarker(MarkData markData){
    Future.delayed(Duration(milliseconds: 40), () async {
      if(_markKey.currentContext == null)
        return;

      try {
        RenderRepaintBoundary renderRepaintBoundary = _markKey.currentContext!.findRenderObject() as RenderRepaintBoundary;
        ui.Image image = await renderRepaintBoundary.toImage(pixelRatio: 3.0);
        ByteData? byteData = await image.toByteData(format: ui.ImageByteFormat.png);

        if (byteData != null) {
          _addMarker(byteData.buffer.asUint8List(), markData);
        }
      }catch (e){
        print("MAP ERROR: " + e.toString());
      }
    });
  }


  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData = MediaQuery.of(context);

    Set<TileOverlay> overlays = <TileOverlay>{
      if (_tileOverlay != null) _tileOverlay!,
    };
    
    LatLng centerLocation = startLocation ?? new LatLng(default_map_lat, default_map_lng);
    double zoom = startLocation != null ? default_map_zoom_location : default_map_zoom;

    bool userLocation = _userLocation();

    return Container(
      child: new Stack(
        children: <Widget>[
          marksData.isNotEmpty ?
              RepaintBoundary(
                key: _markKey,
                child: MarkerBody(
                  markData: marksData[0],
                  dSize: Size(queryData.size.width * queryData.devicePixelRatio, queryData.size.height * queryData.devicePixelRatio),
                ),) : Container(),
          !loadShared ? GoogleMap(
            initialCameraPosition: CameraPosition(target: centerLocation, zoom: zoom),
            myLocationEnabled: userLocation,
            myLocationButtonEnabled: userLocation,
            tileOverlays: overlays,
            mapType: normalType ? MapType.normal : MapType.satellite,
            onMapCreated: (controller){
              mapController=controller;
              mapController?.setMapStyle(_mapStyle);

            },
            onTap: (position){
              mapController?.getZoomLevel().then((zoom) => _mapClick(position.latitude, position.longitude, zoom));
            },
            onCameraMove: (move){
              actualZoom = move.zoom;
              if(overlayAuto){
                MapMeasurementsType type = move.zoom >= map_type_change_zoom ? MapMeasurementsType.POINTS : MapMeasurementsType.HEAT_MAP;
                if(type.desc != actualMapType.desc){
                  _changeMapType(type);
                }
              }
            },
            markers: markers.toSet(),
          ): new Container(),
        ],
      ),
    );
  }

  @override
  callActionsFunction(int actionIndex) {
    switch (actionIndex){
      case 0: Navigator.push(context, new MaterialPageRoute<Map<String, dynamic>>(builder: (_)=> new MapFilter(mapFilters: mapOptions != null ? mapOptions!.mapFilters : null, actualMapOptions: currentMapOptions.isNotEmpty ? currentMapOptions["map_options"] : null, currentFilter: currentFilter,))).then((Map<String, dynamic>? mapFilterData){
       if(currentFilter.isEmpty)
         return;

        setState(() {
         markers.clear();
         marksData.clear();

         currentFilter = mapFilterData!;
         _updateFilterInOptions();
       });

      }); break;
      case 1: Navigator.push(context, new MaterialPageRoute<String>(builder: (_)=> new MapSettings(mapTypes: mapOptions != null ? mapOptions!.mapTypes : null, lastOptions: currentMapOptions.isNotEmpty ? currentMapOptions["map_options"] : null,))).then((String? newOptions){

        if(currentMapOptions.isEmpty)
          return;

        if(newOptions != currentMapOptions["map_options"]){
          needTechnology = _needTechnologyKey(newOptions);
          currentMapOptions["map_options"] = newOptions;

          sharedHelper.setMapOptions(currentMapOptions);

          _clearMarker();
          _setTileOverlay();
        }

      }); break;
      case 2:

        String info = "";

        for(String value in currentMapTitles.values){
          if(info.length > 0)
            info += "\n";

          info += value;
        }

        toastHelper.showInfoToast(info);

        break;
    }
  }


  bool _needTechnologyKey(String? options){
    List<String> split = options!.split('/');
    if(split.isNotEmpty){
      String type = split[0];
      return !(type == "wifi" || type == "browser");
    }

    return true;
  }
}


class MapMeasurementsType{
  final String desc;
  final String title;
  const MapMeasurementsType(this.desc, this.title);

  static const MapMeasurementsType POINTS = const MapMeasurementsType("points", "Body");
  static const MapMeasurementsType HEAT_MAP = const MapMeasurementsType("heatmap", "Heat mapa");
}

