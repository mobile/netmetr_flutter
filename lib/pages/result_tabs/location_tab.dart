/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/widgets/mini_map_widget.dart';
import 'package:netmetr_flutter/widgets/warning_widget.dart';

class LocationTab extends StatefulWidget {
  final double? geoLat;
  final double? geoLon;
  final String? locationString;
  final Function()? showMap;
  final bool locationData;

  LocationTab({Key? key,
    required this.geoLat,
    required this.geoLon,
    required this.locationString,
    required this.showMap,
    required this.locationData,
  }) : super(key: key);

  @override
  _LocationTabState createState() => _LocationTabState();
}

class _LocationTabState extends State<LocationTab> with AutomaticKeepAliveClientMixin{
  @override
  Widget build(BuildContext context) {
    super.build(context);

    MediaQueryData queryData = MediaQuery.of(context);
    ThemeData themeData = Theme.of(context);
    TextStyle textStyle = themeData.textTheme.subtitle1!.copyWith(color: Colors.black);

    double miniMapSize = queryData.size.width * 0.7;

    return widget.locationData ? new Container(
      width: queryData.size.width,
      height: queryData.size.height,
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Flexible(
            flex: 2,
            child: new Container(
              alignment: Alignment.center,
              child: new TextButton(
                onPressed: widget.showMap,
                child: new Container(
                  width: miniMapSize,
                  height: miniMapSize,
                  child:  new MiniMap(
                      lat: widget.geoLat ?? 0,
                      lon: widget.geoLon ?? 0
                    )
                  ),
                ),
            ),
          ),
          new Flexible(
            child: new Container(
              alignment: Alignment.topCenter,
              margin: const EdgeInsets.only(top: 20),
              child: new Text(widget.locationString!, style:  textStyle, textAlign: TextAlign.center,),
            )
          )
        ],
      ),
    ) : new WarningWidget();
  }

  @override
  bool get wantKeepAlive => true;
}
