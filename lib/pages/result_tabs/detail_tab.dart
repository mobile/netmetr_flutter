/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/net/response/test_result_response.dart';
import 'package:netmetr_flutter/widgets/warning_widget.dart';

class DetailTab extends StatefulWidget {
  final bool loaded;
  final List<ResultData>? details;

  DetailTab({
    Key? key,
    required this.loaded,
    required this.details
  }) : super (key : key);

  @override
  _DetailTabState createState() => _DetailTabState();
}

class _DetailTabState extends State<DetailTab> with AutomaticKeepAliveClientMixin {
  TextStyle textStyle = new TextStyle(color: Colors.black);

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      child: widget.loaded ?
      widget.details == null || widget.details!.isEmpty ? new WarningWidget(textKey: "noData2",)
          : new SingleChildScrollView(
            child:  new Table(
              border: new TableBorder(horizontalInside: new BorderSide(color: Colors.black)),
              children: widget.details!.map((detail){
                return new TableRow(
                    children: [
                      new Container(
                        margin: const EdgeInsets.only(left: 5.0),
                        padding: const EdgeInsets.symmetric(vertical: 5.0),
                        child: new Text(detail.title!, style: textStyle,),
                      ),
                      new Container(
                        margin: const EdgeInsets.only(right: 5.0),
                        padding: const EdgeInsets.symmetric(vertical: 5.0),
                        child:  new Text("${detail.value}", style: textStyle,),
                      )
                    ]
                );
              }).toList()
            ),
      ) : new Container(
        alignment: Alignment.center,
        child: new CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(Colors.blueGrey),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
