/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'package:netmetr_flutter/net/response/open_data_response.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:netmetr_flutter/widgets/chart_widget.dart';
import 'package:netmetr_flutter/widgets/speed_graph.dart';
import 'package:netmetr_flutter/widgets/warning_widget.dart';

class GraphTab extends StatefulWidget {
  final OpenDataResponse?  openData;
  final bool loaded;

  GraphTab({
    Key? key,
    required this.openData,
    required this.loaded
  }) : super(key: key);

  @override
  _GraphTabState createState() => _GraphTabState();
}

class _GraphTabState extends State<GraphTab> with AutomaticKeepAliveClientMixin{
  final graphHeight = 150.0;

  //Speed chart data
  int chartRows = 4;

  //Download chart data
  Map<int, Color> downColorChars = {0: Colors.green};
  Map<int, List<dynamic>> downChartsData = {0: []};
  List<double> downSpeedDataProgress = [];
  Map<int, List<PartInterval>> downRowIntervals = {0: []};
  Map<int, List<dynamic>> downRowValues = {0: []};

  //Upload chart data
  Map<int, Color> uploadColorChars = {0: Colors.lightBlue};
  Map<int, List<dynamic>> uploadChartsData = {0: []};
  List<double> uploadSpeedDataProgress = [];
  Map<int, List<PartInterval>> upRowIntervals = {0: []};
  Map<int, List<dynamic>> upRowValues = {0: []};

  //Signal chartData
  Map<int, Color> signalColorChars = {0: Colors.amber};
  Map<int, List<dynamic>> signalRowValues = {0: []};
  Map<int, List<PartInterval>> signalRowIntervals = {0: []};
  Map<int, Color> signalBackgroundParts = {0 : Colors.green, 1 : Colors.lightBlue};
  Map<int, PartInterval> backgroundParts = {};
  Map<int, List<dynamic>> signalChartsData = {0: []};
  List<double> signalSpeedDataProgress = [];

  bool downloadData = false;
  bool uploadData = false;


  @override
  void initState() {
    super.initState();

    if(widget.loaded && widget.openData != null) {
      OpenDataResponse openData = widget.openData!;

      var speedSegments = SpeedSegment.values();
      chartRows = speedSegments.length - 1;


      if (openData.curve != null && openData.curve!.download != null && openData.curve!.download!.isNotEmpty) {
        downloadData = true;
        double maxMBS = 0;
        dynamic timeElapsedMS = openData.curve!.download!.last.timeElapsed;
        for (SpeedData speedData in openData.curve!.download!) {
          double bitPerSec = (speedData.bytesTotal! * 8000) /
              speedData.timeElapsed!;
          double dataProgress = speedData.timeElapsed! / timeElapsedMS;
          double mbs = bitPerSec / 1000000.0;
          if(mbs > maxMBS){
            maxMBS = mbs;
          }
          downChartsData[0]!.add(mbs);
          downSpeedDataProgress.add(dataProgress);
        }

        SpeedGraphType downGT = SpeedGraphType.getSpeedGraphTypeBySpeed(maxMBS);
        for (SpeedSegment speedSegment in speedSegments) {
          downRowValues[0]!.add(downGT.values[speedSegment.startIndexValue]);
          if (speedSegment != SpeedSegment.S5) {
            downRowIntervals[0]!.add(
                new PartInterval(downGT.values[speedSegment.startIndexValue], downGT.values[speedSegment.startIndexValue + 1]));
          }
        }
      }

      if (openData.curve != null && openData.curve!.upload != null && openData.curve!.upload!.isNotEmpty) {
        uploadData = true;
        double maxMBS = 0;
        dynamic timeElapsedMS = openData.curve!.upload!.last.timeElapsed;
        for (SpeedData speedData in openData.curve!.upload!) {
          if(speedData.bytesTotal == null || speedData.timeElapsed == null){
            continue;
          }

          double bitPerSec = (speedData.bytesTotal! * 8000) / speedData.timeElapsed!;
          double dataProgress = speedData.timeElapsed! / timeElapsedMS;
          double mbs = bitPerSec / 1000000.0;
          if(mbs > maxMBS){
            maxMBS = mbs;
          }
          uploadChartsData[0]!.add(mbs);
          uploadSpeedDataProgress.add(dataProgress);
        }

        SpeedGraphType upGT = SpeedGraphType.getSpeedGraphTypeBySpeed(maxMBS);
        for (SpeedSegment speedSegment in speedSegments) {
          upRowValues[0]!.add(upGT.values[speedSegment.startIndexValue]);
          if (speedSegment != SpeedSegment.S5) {
            upRowIntervals[0]!.add(
                new PartInterval(upGT.values[speedSegment.startIndexValue], upGT.values[speedSegment.startIndexValue + 1]));
          }
        }
      }


      if (openData.curve != null && openData.curve!.signal != null && openData.curve!.signal!.isNotEmpty) {
        var speedGraphType = NetworkGraphType.getTypeByNetworkType(openData.curve!.signal!.first.networkType!);

        for(int i = 0; i <= chartRows; i++){
          if(i == 0) signalRowValues[0]!.add(speedGraphType.min);
          else if(i == chartRows) signalRowValues[0]!.add(speedGraphType.max);
          else signalRowValues[0]!.add(null);
        }

        signalRowIntervals[0]!.add(new PartInterval(speedGraphType.min, speedGraphType.max));
        
        double maxUploadTime = (openData.timeUlMS ?? 0) + (openData.durationUploadMS ?? 0);
        double totalTimeElapsed = math.max(maxUploadTime, (openData.curve!.signal!.last.timeElapsed ?? 0) + 0.0);

        //download back part
        if(downloadData && openData.timeDlMS != null && openData.durationDownloadMS != null) backgroundParts.putIfAbsent(0, ()=> new PartInterval(openData.timeDlMS! / totalTimeElapsed, (openData.timeDlMS! + openData.durationDownloadMS!) / totalTimeElapsed));

        //upload back part
        if(uploadData && openData.timeUlMS != null && openData.durationUploadMS != null) backgroundParts.putIfAbsent(1, ()=> new PartInterval(openData.timeUlMS! / totalTimeElapsed, (openData.timeUlMS! + openData.durationUploadMS!) / totalTimeElapsed));

        int? lastStrength;
        for (SignalData signalData in openData.curve!.signal!) {
          if(signalData.timeElapsed == null){
            continue;
          }

          int? newStrength = speedGraphType == NetworkGraphType.RSRP ? signalData.lteRSRP : signalData.signalStrength;
          var progress = openData.curve!.signal!.length == 1 ? 0.0 : signalData.timeElapsed! / totalTimeElapsed;


          if(lastStrength != null && lastStrength != newStrength){
            signalChartsData[0]!.add(lastStrength);
            signalSpeedDataProgress.add(progress);
          }

          signalChartsData[0]!.add(newStrength);
          signalSpeedDataProgress.add(progress);

          lastStrength = newStrength;
        }

        //draw signal to end
        if(signalSpeedDataProgress.last < 1.0 && lastStrength != null){
          signalChartsData[0]!.add(lastStrength);
          signalSpeedDataProgress.add(1.0);
        }
      }
    }

  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    ThemeData themeData = Theme.of(context);
    TextStyle titleText = themeData.textTheme.caption!.copyWith(color: Colors.black);
    TextStyle emptyText = themeData.textTheme.subtitle2!.copyWith(color: Colors.black);

    MediaQueryData queryData = MediaQuery.of(context);

    double chartWidth = queryData.size.width * 0.85;
    double chartHeight = chartWidth * 0.32;

    return widget.loaded ?
      widget.openData == null ? new WarningWidget(textKey: "noData2",) :
      new SingleChildScrollView(
        child: new Container(
          margin: const EdgeInsets.all(10.0),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              new Padding(padding: const EdgeInsets.symmetric(vertical: 15.0),
                child: new Text("Download (Mbit/s)", style: titleText,)
              ),
              downloadData ? new ChartWidget(
                chartsData: downChartsData,
                rowValues: downRowValues,
                chartsColor: downColorChars,
                ratioData: downSpeedDataProgress,
                partInterval: downRowIntervals,
                width: chartWidth,
                height: chartHeight,
              ) : new Center(
                child: new Text(AppLocalizations.of(context)!.getLocalizedValue('noData2')!, textAlign: TextAlign.center, style: emptyText,),
              ),
              new Padding(padding: const EdgeInsets.only(top: 30.0, bottom: 15.0),
                child: new Text("Upload (Mbit/s)", style: titleText,),
              ),
              uploadData ? new ChartWidget(
                chartsData: uploadChartsData,
                rowValues: upRowValues,
                chartsColor: uploadColorChars,
                ratioData: uploadSpeedDataProgress,
                partInterval: upRowIntervals,
                width: chartWidth,
                height: chartHeight,
              ): new Center(
                child: new Text(AppLocalizations.of(context)!.getLocalizedValue('noData2')!, textAlign: TextAlign.center, style: emptyText,),
              ),
              new Padding(padding: const EdgeInsets.only(top: 30.0, bottom: 15.0),
                child: new Text(AppLocalizations.of(context)!.getLocalizedValue('signal')! + " (-dBm)", style: titleText,),
              ),
              widget.openData!.curve!.signal!.isNotEmpty ? new ChartWidget(
                chartsData: signalChartsData,
                rowValues: signalRowValues,
                chartsColor: signalColorChars,
                ratioData: signalSpeedDataProgress,
                partInterval: signalRowIntervals,
                width: chartWidth,
                height: chartHeight,
                minValue: 0,
                maxValue: 40,
                useBackgroundFillParts: true,
                backgroundParts: backgroundParts,
                backgroundPartsColor: signalBackgroundParts,
              ):  new WarningWidget(textKey: "noData2",),
            ],
          ),
        )
      ): new Container(
        alignment: Alignment.center,
        child: new CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(Colors.blueGrey),
        ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
