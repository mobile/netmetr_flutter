/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/net/response/test_result_response.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:netmetr_flutter/util/classification.dart';
import 'package:netmetr_flutter/util/widget_helper.dart' as widgetHelper;


class ResultTab extends StatefulWidget {
  final List<Measurement>? measurement;
  final List<ResultData>? net;
  ResultTab({Key? key, required this.measurement, required this.net}) : super(key: key);

  @override
  _ResultTabState createState() => _ResultTabState();
}

class _ResultTabState extends State<ResultTab> with AutomaticKeepAliveClientMixin{

  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    super.build(context);

    ThemeData themeData = Theme.of(context);
    TextStyle title = themeData.textTheme.subtitle2!.copyWith(color: Colors.white);
    TextStyle dataStyle = new TextStyle(color: Colors.black, fontSize: themeData.textTheme.subtitle2!.fontSize);

    Container _getTestRow(String title, String dataValue, Color color){
      return Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: Colors.black
            )
          )
        ),
        child: IntrinsicHeight(
          child:  Row(
              children: [
                Flexible(
                  flex: 1,
                  child: Container(
                    width: 25.0,
                    color: color,
                  ),
                ),
                Flexible(
                  flex: 3,
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                        child: Text(title, style: dataStyle,),
                      )
                  ),
                ),
                Flexible(
                    flex: 2,
                    child: Container(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      alignment: Alignment.centerLeft,
                      child: new Text(dataValue, style: dataStyle,),
                    )
                )
              ]
          ),
        ),
      );
    }

    return new Scaffold(
      body: new SingleChildScrollView(
        child: new Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              new Container(
                width: double.maxFinite,
                padding: const EdgeInsets.all(5.0),
                color: Colors.black,
                child: new Text(AppLocalizations.of(context)!.getLocalizedValue('measureCAPS')!, style: title,),
              ),
              new Column(
                children: widget.measurement!.map((measurement){
                  return _getTestRow(measurement.title, measurement.value, TestClassification.classifyColor(measurement.classification));
                }).toList()
              ),
              new Container(
                width: double.maxFinite,
                padding: const EdgeInsets.all(5.0),
                color: Colors.black,
                child: new Text(AppLocalizations.of(context)!.getLocalizedValue('networkCAPS')!, style: title,),
              ),
              new Padding(padding: const EdgeInsets.only(top: 5.0)),
              widget.net!= null && widget.net!.isNotEmpty ?  Column(
                children:  widget.net!.map((ResultData data){
                  return Container(
                    decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              color: Colors.black
                          )
                      )
                    ),
                    child: Row(
                      children: [
                        Expanded(
                          child: Container(
                            padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 5.0),
                            alignment: Alignment.centerLeft,
                            margin: const EdgeInsets.only(left: 25.0),
                            child: Text(data.title!, style: dataStyle,),
                          )
                        ),
                        Expanded(
                          child: Container(
                            padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 5.0),
                            alignment: Alignment.centerLeft,
                            child: Text(widgetHelper.notNullString(data.value), style: dataStyle,),
                          )
                        )
                      ],
                    ),
                  );
                }).toList()
              ) : new Container()
            ],
          ),
        ),
      )
    );

  }

  @override
  bool get wantKeepAlive => true;

}
