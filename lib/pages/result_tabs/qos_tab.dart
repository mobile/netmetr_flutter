/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/net/response/test_result_qos_reponse.dart';
import 'package:netmetr_flutter/pages/tab_detail_test_page.dart';
import 'package:netmetr_flutter/util/widget_helper.dart' as widgetHelper;
import 'package:netmetr_flutter/widgets/warning_widget.dart';

class QOSTab extends StatefulWidget {

  final Map<QosResultType, List<ResultDetailDescription>>? qosDetailDescriptionMap;
  final Map<QosResultType, List<ResultDetail>>? qosResultMap;
  final Map<QosResultType, TestDescription>? qosTestDetailMap;

  final bool loaded;

  QOSTab({
    Key? key,
    required this.qosDetailDescriptionMap,
    required this.qosResultMap,
    required this.qosTestDetailMap,
    required this.loaded,
  }) : super(key: key);

  @override
  _QOSTabState createState() => _QOSTabState();

}

class _QOSTabState extends State<QOSTab> with AutomaticKeepAliveClientMixin{
  @override
  Widget build(BuildContext context) {
    super.build(context);

    Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      body: new Container(
        width: screenSize.width,
        height: screenSize.height,
        child: widget.loaded ? new Container(
            margin: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 10.0),
            child: widget.qosTestDetailMap == null || widget.qosTestDetailMap!.isEmpty ?
            new WarningWidget(textKey: "noData2",) : new ListView.builder(
                  itemCount: widget.qosDetailDescriptionMap!.length,
                  itemBuilder: (_, int index){
                    QosResultType type = widget.qosDetailDescriptionMap!.keys.elementAt(index);
                    List<ResultDetail> result = widget.qosResultMap![type]!;

                    return new QosItem(
                      description: widget.qosDetailDescriptionMap![type],
                      result: result,
                      testDescription: widget.qosTestDetailMap![type],
                    );
                  }
            )
        ) : new Container(
          alignment: Alignment.center,
          child: new CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(Colors.blueGrey),
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class QosItem extends StatefulWidget {
  QosItem({Key? key,
    required this.testDescription,
    required this.description,
    required this.result,
  }) : super (key: key);

  final TestDescription? testDescription;
  final List<ResultDetailDescription>? description;
  final List<ResultDetail> result;


  @override
  _QosItemState createState() => _QosItemState();
}

class _QosItemState extends State<QosItem> {
  bool show = false;

  late ResultDetailDescription detailDescription;

  late int success;
  late int failure;
  late int sum;


  @override
  void initState() {
    super.initState();

    success = 0;
    failure = 0;
    sum = 0;

    sum = widget.result.length;
    for(ResultDetailDescription description in widget.description!){
      success += description.success;
      failure += description.failure;
    }
  }

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    TextStyle title = themeData.textTheme.headline6!.copyWith(color: Colors.white);
    TextStyle testTitle = themeData.textTheme.subtitle2!.copyWith(color: Colors.black);

    return Container(
      alignment: Alignment.center,
      child: new Container(
        margin: const EdgeInsets.all(5.0),
        child:  new Container(
          child: new ExpansionPanelList(
            expansionCallback: (_, bool expand){
              setState(() {
                show = !expand;
              });
            },
            children: <ExpansionPanel>[
              new ExpansionPanel(
                  isExpanded: show,
                  headerBuilder: (_, __){
                    return new Row(
                      children: <Widget>[
                        new Expanded(
                            child: Container(
                              margin: const EdgeInsets.only(left: 5.0),
                              alignment: Alignment.centerLeft,
                              child: new Text(widget.testDescription!.name!),
                            )
                        ),
                        new Expanded(
                            child: new Container(
                              alignment: Alignment.center,
                              child: new Text("$success/$sum"),
                            )
                        ),
                        new Expanded(
                            child: new Container(
                              alignment: Alignment.centerRight,
                              child: failure == 0 ? new Icon(Icons.check, color: Colors.green,) : new Icon(Icons.close, color: Colors.red,),
                            )
                        )
                      ],
                    );
                  },
                  body: new Container(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Container(
                          width: double.maxFinite,
                          padding: const EdgeInsets.all(5.0),
                          color: Colors.black,
                          child: new Text("DETAIL", style: title,),
                        ),
                        new Container(
                          padding: const EdgeInsets.all(5.0),
                          child: new Text(widgetHelper.notNullString(widget.testDescription!.desc)),
                        ),
                        new Container(
                          width: double.maxFinite,
                          padding: const EdgeInsets.all(5.0),
                          color: Colors.black,
                          child: new Text("TESTY", style: title,),
                        ),
                        new Container(
                          padding: const EdgeInsets.all(5.0),
                          child: new Column(
                            children: widget.result.map((ResultDetail result){
                              int index = widget.result.indexOf(result);

                              return new Container(
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    new Row(
                                      mainAxisSize: MainAxisSize.max,
                                      children: <Widget>[
                                        new Expanded(
                                          child: new Text("TEST #${index + 1}", style: testTitle,)
                                        ),
                                        new Expanded(
                                          child: new Container(
                                            width: double.maxFinite,
                                            child: new Row(
                                              children: [
                                                result.success! > result.failure! ? new Icon(Icons.check, color: Colors.green,) : new Icon(Icons.close, color: Colors.red,),
                                                new IconButton(icon: new Icon(Icons.arrow_forward, color: Colors.black,), onPressed: ()=> Navigator.push(context, new MaterialPageRoute(builder: (_)=> new TabDetailTestPage(results: widget.result, startIndex: index,))))
                                              ]
                                            )
                                          ),
                                        )
                                      ],
                                    ),
                                    new Text(result.testSummary!),
                                    new Divider(
                                      color: Colors.black,
                                    )
                                  ],
                                ),
                              );
                            }).toList(),
                          ),
                        )
                      ],
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
