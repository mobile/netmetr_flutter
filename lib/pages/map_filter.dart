/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/net/response/map_response.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:netmetr_flutter/util/widget_helper.dart' as widgetHelper;

class MapFilter extends StatefulWidget {
  final String actualMapOptions;
  final Map<String, List<MapData>>? mapFilters;
  final Map<String, dynamic>? currentFilter;

  MapFilter({Key? key, required this.actualMapOptions, required this.mapFilters, required this.currentFilter}) : super(key: key);

  @override
  _MapFilterState createState() => _MapFilterState();
}

class _MapFilterState extends State<MapFilter> {

  late Map<String, dynamic> _mapFilterData;
  late List<MapData> _filters;

  @override
  void initState() {
    super.initState();
    _mapFilterData = new Map();
    _filters = [];

    if(widget.currentFilter != null) {
      _mapFilterData.addAll(widget.currentFilter!);
      _filters.add(new MapData(
          title: "mapAppearCAPS",
          options: [
            new Options(optionsData: {
              "title": "normView",
              "summary": "normViewInfo",
              "default": true,
            }, key: "_SAT", value: "NOSAT"),
            new Options(optionsData: {
              "title": "satView",
              "summary": "satViewInfo"
            }, key: "_SAT", value: "SAT")
          ],
          translate: true
      ));

      _filters.add(new MapData(
        title: "mapOverlayCAPS",
        options: [
          new Options(optionsData: {
            "title": "automatic",
            "summary": "automaticInfo",
            "default": true,
          }, key: "_OVERLAY", value: "AUTO"),
          new Options(optionsData: {
            "title": "heatMap",
            "summary": "heatMapInfo"
          }, key: "_OVERLAY", value: "HEATMAP"),
          new Options(optionsData: {
            "title": "points",
            "summary": "pointsInfo"
          }, key: "_OVERLAY", value: "POINTS"),
          /*new Options(optionsData: {
            "title": "regions",
            "summary": "-"
          }, key: "_OVERLAY", value: "REGIONS"),*/
        ],
        translate: true,
      ));

      List<String> split = widget.actualMapOptions.split('/');
      if (split.isNotEmpty) {
        String type = split[0];
        type = type != "all" ? type : "mobile";
        if (widget.mapFilters!.containsKey(type)) {
          _filters.addAll(widget.mapFilters![type]!);
        }
      }
    }
  }



  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData = MediaQuery.of(context);
    ThemeData themeData = Theme.of(context);
    TextStyle titleStyle = themeData.textTheme.subtitle1!.copyWith(color: Colors.black, fontWeight: FontWeight.bold);
    TextStyle subheadStyle = themeData.textTheme.bodyText2!.copyWith(color: Colors.black, fontWeight: FontWeight.bold);
    TextStyle textStyle = themeData.textTheme.bodyText2!.copyWith(color: Colors.black);

    return new WillPopScope(
      child: new Scaffold(
        appBar: new AppBar(
          title: new Text(AppLocalizations.of(context)!.getLocalizedValue('mapFilter')!),
        ),
        body: (_filters.isNotEmpty) ? new ListView.builder(
          padding: const EdgeInsets.all(10.0),
          itemCount: _filters.length,
          itemBuilder: (__, int index){
            MapData mapData = _filters[index];

            return new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Text(mapData.translate==true ? AppLocalizations.of(context)!.getLocalizedValue(mapData.title)! : mapData.title, style: subheadStyle,),
                widgetHelper.titleDivider(),
                new Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: mapData.options.map((Options options){
                    int index = mapData.options.indexOf(options);
                    var title = options.optionsData.containsKey("title") ? (mapData.translate==true ? AppLocalizations.of(context)!.getLocalizedValue(options.optionsData["title"]) : options.optionsData["title"]) : "-";
                    var summary = options.optionsData.containsKey("summary") ? (mapData.translate==true ? AppLocalizations.of(context)!.getLocalizedValue(options.optionsData["summary"]) : options.optionsData["summary"]) : "-";
                    var value = options.value;

                    return Container(
                      margin: const EdgeInsets.symmetric(vertical: 10.0),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new TextButton(
                              onPressed: (){
                                setState(() {
                                  _mapFilterData[options.key!] = value;
                                });
                              },
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  new Flexible(
                                    flex: 3,
                                    child: new Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.max,
                                      children: <Widget>[
                                        new Text(title, style: titleStyle,),
                                        new Padding(padding: const EdgeInsets.only(top: 5.0),
                                            child: new Text(summary, style: textStyle,)
                                        ),
                                      ],
                                    )
                                  ),
                                  new Flexible(
                                    flex: 1,
                                    child: new Container(
                                      margin: const EdgeInsets.only(left: 5.0),
                                      alignment: Alignment.centerRight,
                                      child: new Radio(value: value, groupValue: _mapFilterData[options.key], onChanged: (dynamic value){
                                        setState(() {
                                          _mapFilterData[options.key!] = value;
                                        });
                                      }),
                                    )
                                  )
                                ],
                              )
                          ),
                          index  == mapData.options.length -1 ? new Container() : widgetHelper.divider()
                        ],
                      ),
                    );

                  }).toList(),
                )
              ],
            );
          }
        ) : new Container(
          alignment: Alignment.center,
          child: new Icon(Icons.block, size:  queryData.size.width / 3, color: Colors.blueGrey,),
        ),
      ),
      onWillPop: _onBackPressed);
  }

  Future<bool> _onBackPressed(){
    Navigator.pop(context, _mapFilterData);
    return new Future.value(false);
  }
}

class MapFilterData{

}