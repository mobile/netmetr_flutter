/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/net/response/map_response.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:netmetr_flutter/util/widget_helper.dart' as widgetHelper;

class MapSettings extends StatefulWidget {
  final List<MapData>? mapTypes;
  final String lastOptions;
  MapSettings({Key? key, required this.mapTypes, required this.lastOptions}) : super(key: key);

  @override
  _MapSettingsState createState() => _MapSettingsState();
}

class _MapSettingsState extends State<MapSettings> {
  String? _selectedOptions;

  @override
  void initState() {
    super.initState();
    _selectedOptions = widget.lastOptions;
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData = MediaQuery.of(context);
    ThemeData themeData = Theme.of(context);
    TextStyle titleStyle = themeData.textTheme.subtitle1!.copyWith(color: Colors.black, fontWeight: FontWeight.bold);
    TextStyle subheadStyle = themeData.textTheme.bodyText2!.copyWith(color: Colors.black, fontWeight: FontWeight.bold);
    TextStyle textStyle = themeData.textTheme.bodyText2!.copyWith(color: Colors.black);

    return new WillPopScope(
      child: new Scaffold(
        appBar: new AppBar(
          title: new Text(AppLocalizations.of(context)!.getLocalizedValue('chooseMap')!),
        ),
        body: widget.mapTypes != null ? new ListView.builder(
            padding: const EdgeInsets.all(10.0),
            itemCount: widget.mapTypes!.length,
            itemBuilder: (__,  int index){
              MapData mapData = widget.mapTypes![index];

              return new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(mapData.title, style: subheadStyle,),
                  widgetHelper.titleDivider(),
                  new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: mapData.options.map((Options options){
                      int index = mapData.options.indexOf(options);
                      var title = options.optionsData.containsKey("title") ? options.optionsData["title"] : "-";
                      var summary = options.optionsData.containsKey("summary") ? options.optionsData["summary"] : "-";
                      var optionsMap = options.optionsData.containsKey("map_options") ? options.optionsData["map_options"] : "$index";

                      return Container(
                        margin: const EdgeInsets.symmetric(vertical: 10.0),
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new TextButton(
                                onPressed: (){
                                  setState(() {
                                    _selectedOptions = optionsMap;
                                  });
                                },
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    new Flexible(
                                      flex: 3,
                                      child: new Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          new Text(title, style: titleStyle,),
                                          new Padding(padding: const EdgeInsets.only(top: 5.0),
                                            child: new Text(summary, style: textStyle,),
                                          ),
                                        ],
                                      )
                                    ),
                                    new Flexible(
                                      flex: 1,
                                      child: new Container(
                                        margin: const EdgeInsets.only(left: 5.0),
                                        alignment: Alignment.centerRight,
                                        child: new Radio(value: optionsMap, groupValue: _selectedOptions, onChanged: (dynamic value){
                                          setState(() {
                                            _selectedOptions = value;
                                          });
                                        }),
                                      )
                                    )
                                  ],
                                )
                            ),
                            index  == mapData.options.length -1 ? new Container() : widgetHelper.divider()
                          ],
                        ),
                      );
                    }).toList(),
                  )
                ],
              );
            }
        ) : new Container(
          alignment: Alignment.center,
          child: new Icon(Icons.block, size: queryData.size.width / 3, color: Colors.blueGrey,),
        ),
      ), 
      onWillPop: _onBackPressed);
  }

  Future<bool> _onBackPressed(){
    Navigator.pop(context, _selectedOptions);
    return new Future.value(false);
  }
}
