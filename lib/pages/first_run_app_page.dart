/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:io';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:netmetr_flutter/app_config.dart';
import 'package:netmetr_flutter/app_settings.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:netmetr_flutter/util/shared_helper.dart' as sharedHelper;

class FirstRunAppPage extends StatefulWidget {
  final AppConfig config;

  FirstRunAppPage({Key? key, required this.config}) : super(key: key);

  @override
  _FirstRunAppPageState createState() => _FirstRunAppPageState();
}

class _FirstRunAppPageState extends State<FirstRunAppPage> {
  late AppSettings appSettings;

  @override
  void initState() {
    super.initState();
    appSettings = widget.config.appSettings;
  }

  _launchUrl(String url){
    launch(url).catchError((error){
      print("Launch url error $error");
    });
  }

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    TextStyle titleStyle =
        themeData.textTheme.headline5!.copyWith(color: Colors.black);
    TextStyle subtitleStyle =
        themeData.textTheme.subtitle1!.copyWith(color: Colors.black);
    TextStyle subheadStyle =
        themeData.textTheme.subtitle2!.copyWith(color: Colors.black);
    TextStyle buttonStyle =
        themeData.textTheme.button!.copyWith(color: Colors.white);

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(AppLocalizations.of(context)!.getLocalizedValue('termsOfUseNetmetr')!),
      ),
      body: new SingleChildScrollView(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            new Container(
              padding: const EdgeInsets.all(15.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Padding(
                      padding: const EdgeInsets.only(top: 15.0),
                      child: new Text("1. " + AppLocalizations.of(context)!.getLocalizedValue('introductoryProvision')!,
                          style: titleStyle)),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("1.1", style: subtitleStyle),
                  ),
                  new RichText(
                    text: new TextSpan(
                      style: new TextStyle(
                        fontSize: 14.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par1_1_part1')),
                        new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par1_1_part2'), style: new TextStyle(fontWeight: FontWeight.bold)),
                        new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par1_1_part3')),
                      ],
                    ),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new RichText(
                      text: new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par_info_1'), style: new TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold, color: Colors.black)),
                    ),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("1.2", style: subtitleStyle),
                  ),
                  new RichText(
                    text: new TextSpan(
                      style: new TextStyle(
                        fontSize: 14.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par1_2')),
                        new TextSpan(
                          text: 'https://gitlab.labs.nic.cz/mobile/netmetr_flutter',
                          style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () { _launchUrl('https://gitlab.labs.nic.cz/mobile/netmetr_flutter');
                            },
                        ),
                        new TextSpan(
                            text: "."
                        )
                      ],
                    ),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("1.3", style: subtitleStyle),
                  ),
                  new Text(
                    AppLocalizations.of(context)!.getLocalizedValue('par1_3')!),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("1.4", style: subtitleStyle),
                  ),
                  new RichText(
                    text: new TextSpan(
                      style: new TextStyle(
                        fontSize: 14.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par1_4_part1')),
                        new TextSpan(
                          text: 'www.netmetr.cz',
                          style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () { _launchUrl(AppLocalizations.of(context)!.getLocalizedValue('wwwNetmetr')!);
                            },
                        ),
                        new TextSpan(text: ', '),
                        new TextSpan(
                          text: 'www.measurementlab.net',
                          style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () { _launchUrl("https://www.measurementlab.net/");
                            },
                        ),
                        new TextSpan(
                            text: AppLocalizations.of(context)!.getLocalizedValue('par1_4_part2')
                        ),
                        new TextSpan(
                            text: AppLocalizations.of(context)!.getLocalizedValue('par1_4_part3')
                        )
                      ],
                    ),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("1.5", style: subtitleStyle),
                  ),
                  new Text(
                    AppLocalizations.of(context)!.getLocalizedValue('par1_5')!),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("1.6", style: subtitleStyle),
                  ),
                  new Text(
                    AppLocalizations.of(context)!.getLocalizedValue('par1_6')!),
                ],
              ),
            ),
            new Container(
              padding: const EdgeInsets.all(15.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Padding(
                    padding: const EdgeInsets.only(top: 15.0),
                    child: new Text("2. " + AppLocalizations.of(context)!.getLocalizedValue('basicTest')!, style: titleStyle),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("2.1", style: subtitleStyle),
                  ),
                  new Text(AppLocalizations.of(context)!.getLocalizedValue('par2_1_part1')!),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part2')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part3')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part4')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part5')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part6')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part7')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part8')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part9')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part10')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part11')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part12')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part13')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part14')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part15')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part16')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part17')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part18')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new RichText(
                      text: new TextSpan(
                        style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        ),
                        children: <TextSpan>[
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par2_1_part19')),
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par2_1_part20'), style: new TextStyle(fontWeight: FontWeight.bold)),
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par2_1_part21')),
                          new TextSpan(
                            text: AppLocalizations.of(context)!.getLocalizedValue('netmetr_open_data_link'),
                            style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () { _launchUrl(AppLocalizations.of(context)!.getLocalizedValue('netmetr_open_data_link')!);
                              },
                          ),
                          new TextSpan(
                            text: AppLocalizations.of(context)!.getLocalizedValue('par2_1_part22')
                          )
                        ],
                      ),
                    ),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("2.2", style: subtitleStyle),
                  ),
                  new Text(
                    AppLocalizations.of(context)!.getLocalizedValue('par2_2')!),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("2.3", style: subtitleStyle),
                  ),
                  new Text(
                    AppLocalizations.of(context)!.getLocalizedValue('par2_3')!),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("2.4", style: subtitleStyle),
                  ),
                  new Text(
                      AppLocalizations.of(context)!.getLocalizedValue('par2_4')!),
                ],
              ),
            ),
            new Container(
              padding: const EdgeInsets.all(15.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Padding(
                    padding: const EdgeInsets.only(top: 15.0),
                    child: new Text("3. " + AppLocalizations.of(context)!.getLocalizedValue('dataPrivacy')!,
                        style: titleStyle),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text(
                        AppLocalizations.of(context)!.getLocalizedValue('par3_0_1')!)
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text(
                        AppLocalizations.of(context)!.getLocalizedValue('par3_0_2')!)
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("3.1 " + AppLocalizations.of(context)!.getLocalizedValue('par3_1_header')!, style: subtitleStyle),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text(
                        AppLocalizations.of(context)!.getLocalizedValue('par3_1')!),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("3.1.1 " + AppLocalizations.of(context)!.getLocalizedValue('basicTest')!, style: subheadStyle),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part2')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part3')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part4')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part5')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part6')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part7')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part8')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part9')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part10')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part11')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part12')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part13')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part14')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part15')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part16')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part17')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par2_1_part18')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("3.1.2 " + AppLocalizations.of(context)!.getLocalizedValue('par3_1_2_header')!, style: subheadStyle),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part2')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part3')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part4')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part5')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part6')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part7')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part8')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part9')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part10')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part11')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part12')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part13')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part14')!,
                        style: new TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("3.1.3 " + AppLocalizations.of(context)!.getLocalizedValue('par3_1_3_header')!, style: subheadStyle),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par3_1_3_1')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par3_1_3_2')!),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("3.2 " + AppLocalizations.of(context)!.getLocalizedValue('par3_2_header')!, style: subtitleStyle),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, top: 10.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par3_2_1')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par3_2_2')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par3_2_3')!),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("3.3 " + AppLocalizations.of(context)!.getLocalizedValue('par3_3_header')!, style: subtitleStyle),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("3.3.1 " + AppLocalizations.of(context)!.getLocalizedValue('basicTest')!, style: subheadStyle),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new RichText(
                      text: new TextSpan(
                        style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        ),
                        children: <TextSpan>[
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par3_3_1_1')),
                          new TextSpan(
                            text: 'CC BY 4.0 ' + AppLocalizations.of(context)!.getLocalizedValue('cz_en')!,
                            style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () { _launchUrl(AppLocalizations.of(context)!.getLocalizedValue('cc_by_4_0_link')!);
                              },
                          ),
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par3_3_1_2')),
                          new TextSpan(
                            text: AppLocalizations.of(context)!.getLocalizedValue('netmetr_open_data_link'),
                            style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () { _launchUrl(AppLocalizations.of(context)!.getLocalizedValue('netmetr_open_data_link')!);
                              },
                          ),
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par3_3_1_3')),
                          new TextSpan(
                              text: AppLocalizations.of(context)!.getLocalizedValue('par3_3_1_4'), style: new TextStyle(fontWeight: FontWeight.bold)
                          )
                        ],
                      ),
                    ),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("3.3.2 " + AppLocalizations.of(context)!.getLocalizedValue('par3_1_2_header')!, style: subheadStyle),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new RichText(
                      text: new TextSpan(
                        style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        ),
                        children: <TextSpan>[
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par3_3_2_1_1')),
                          new TextSpan(
                            text: 'The PlanetLab Consortium',
                            style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () { _launchUrl('https://www.planet-lab.org/');
                              },
                          ),
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par3_3_2_1_2')),
                          new TextSpan(
                            text: 'www.measurementlab.net',
                            style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () { _launchUrl("https://www.measurementlab.net/");
                              },
                          ),
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par3_3_2_1_3')),
                          new TextSpan(
                            text: 'CC BY 4.0 ' + AppLocalizations.of(context)!.getLocalizedValue('cz_en')!,
                            style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () { _launchUrl(AppLocalizations.of(context)!.getLocalizedValue('cc_by_4_0_link')!);
                              },
                          ),
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par3_3_2_1_4')),
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par3_3_2_1_5'), style: new TextStyle(fontWeight: FontWeight.bold)),
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par3_3_2_1_6')),
                        ],
                      ),
                    ),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text(AppLocalizations.of(context)!.getLocalizedValue('par3_3_2_2')!),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text(AppLocalizations.of(context)!.getLocalizedValue('par3_3_2_3')!),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("3.4 " + AppLocalizations.of(context)!.getLocalizedValue('par3_4_header')!, style: subtitleStyle),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text(AppLocalizations.of(context)!.getLocalizedValue('par3_4_1')!),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text(AppLocalizations.of(context)!.getLocalizedValue('par3_4_2')!),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("3.5 " + AppLocalizations.of(context)!.getLocalizedValue('par3_5_header')!, style: subtitleStyle),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text(AppLocalizations.of(context)!.getLocalizedValue('par3_5_1')!),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text(AppLocalizations.of(context)!.getLocalizedValue('par3_5_2')!),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text(AppLocalizations.of(context)!.getLocalizedValue('par3_5_3')!),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text(AppLocalizations.of(context)!.getLocalizedValue('par3_5_4')!),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("3.6", style: subtitleStyle),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new RichText(
                      text: new TextSpan(
                        style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        ),
                        children: <TextSpan>[
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par3_6_1_1')),
                          new TextSpan(
                            text: AppLocalizations.of(context)!.getLocalizedValue('principles_forPPD'),
                            style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () { _launchUrl(AppLocalizations.of(context)!.getLocalizedValue('wwwPrinciples_forPPD')!);
                              },
                          ),
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par3_6_1_2')),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new RichText(
                      text: new TextSpan(
                        style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        ),
                        children: <TextSpan>[
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par3_6_2')),
                          new TextSpan(
                            text: 'www.netmetr.cz',
                            style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () { _launchUrl(AppLocalizations.of(context)!.getLocalizedValue('wwwNetmetr')!);
                              },
                          ),
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('and')),
                          new TextSpan(
                            text: 'www.measurementlab.net',
                            style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () { _launchUrl("https://www.measurementlab.net/");
                              },
                          ),
                          new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('.')),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            (Theme.of(context).platform == TargetPlatform.android) ? new Container( //info about NDT tests
              padding: const EdgeInsets.all(15.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Padding(
                    padding: const EdgeInsets.only(top: 15.0),
                    child: new Text("4. NDT Test", style: titleStyle),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("4.1", style: subtitleStyle),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: new Text(AppLocalizations.of(context)!.getLocalizedValue('par4_1_part1')!),
                  ),
                  new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part2')!),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part3')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part4')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part5')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part6')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part7')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part8')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part9')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part10')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part11')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part12')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(
                        " - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part13')!),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Text(" - " + AppLocalizations.of(context)!.getLocalizedValue('par4_1_part14')!),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("4.2", style: subtitleStyle),
                  ),
                  new RichText(
                    text: new TextSpan(
                      style: new TextStyle(
                        fontSize: 14.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par4_2_part1')),
                        new TextSpan(
                          text: 'The PlanetLab Consortium',
                          style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () { _launchUrl('https://www.planet-lab.org/');
                            },
                        ),
                        new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par4_2_part2')),
                        new TextSpan(
                          text: 'www.netmetr.cz',
                          style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () { _launchUrl(AppLocalizations.of(context)!.getLocalizedValue('wwwNetmetr')!);
                            },
                        ),
                        new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('and')),
                        new TextSpan(
                          text: 'www.measurementlab.net',
                          style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () { _launchUrl("https://www.measurementlab.net/");
                            },
                        ),
                        new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par4_2_part3')),
                        new TextSpan(
                          text: 'CC BY 4.0 ' + AppLocalizations.of(context)!.getLocalizedValue('cz_en')!,
                          style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () { _launchUrl(AppLocalizations.of(context)!.getLocalizedValue('cc_by_4_0_link')!);
                            },
                        ),
                        new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par4_2_part4')),
                      ],
                    ),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("4.3", style: subtitleStyle),
                  ),
                  new RichText(
                    text: new TextSpan(
                      style: new TextStyle(
                        fontSize: 14.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par4_3')),
                        new TextSpan(
                          text: 'CC BY 4.0 ' + AppLocalizations.of(context)!.getLocalizedValue('cz_en')!,
                          style: new TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () { _launchUrl(AppLocalizations.of(context)!.getLocalizedValue('cc_by_4_0_link')!);
                            },
                        ),
                        new TextSpan(text: AppLocalizations.of(context)!.getLocalizedValue('par4_2_part4')),
                      ],
                    ),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("4.4", style: subtitleStyle),
                  ),
                  new Text(
                    AppLocalizations.of(context)!.getLocalizedValue('par4_4')!),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Text("4.5", style: subtitleStyle),
                  ),
                  new Text(
                    AppLocalizations.of(context)!.getLocalizedValue('par4_5')!),
                  CheckboxListTile(
                    title: Text(AppLocalizations.of(context)!.getLocalizedValue('acceptNDT')!, style: themeData.textTheme.bodyText2!.copyWith(color: Colors.black)),
                    value: appSettings.ndt,
                    onChanged: (value){
                      setState(() {
                        appSettings.ndt = value!;
                        appSettings.saveNDT();
                      });
                    },
                    controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                  )
                ],
              ),
            ) : new Container(),
            new Container(
              padding: const EdgeInsets.all(15.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: new ElevatedButton(
                      style: ButtonStyle(
                        elevation: MaterialStateProperty.all(4.0),
                        backgroundColor: MaterialStateProperty.all( Theme.of(context).colorScheme.secondary),
                        overlayColor: MaterialStateProperty.all(Colors.blueGrey)
                      ),
                      onPressed: () => exit(0),
                      child: new Container(
                          child: new Text(AppLocalizations.of(context)!.getLocalizedValue('decline')!, style: buttonStyle)),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: new ElevatedButton(
                      style: ButtonStyle(
                          elevation: MaterialStateProperty.all(4.0),
                          backgroundColor: MaterialStateProperty.all( Theme.of(context).colorScheme.secondary),
                          overlayColor: MaterialStateProperty.all(Colors.blueGrey)
                      ),
                      onPressed: (){
                        sharedHelper.setConfirmedUserTerms(true); //terms confirmed
                        Navigator.of(context).pushReplacementNamed('/main');
                      },
                      child: new Container(
                          child: new Text(AppLocalizations.of(context)!.getLocalizedValue('agree')!, style: buttonStyle)),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
