/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:netmetr_flutter/app_config.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:url_launcher/url_launcher.dart';

class MenuItem {
  final int index;
  final String title;
  final String iconPath;
  final bool useWebView;

  const MenuItem({
    required this.index,
    required this.title,
    required this.iconPath,
    this.useWebView = false,
  });

  static const START = const MenuItem(index: 0, title: 'startPage', iconPath: "assets/ic_action_home.png");
  static const HISTORY = const MenuItem(index: 1, title: "history", iconPath: "assets/ic_action_history.png");
  static const MAP = const MenuItem(index: 2, title: "map", iconPath: "assets/ic_action_map.png");
  static const STAT = const MenuItem(index: 3, title: "statistics", iconPath: "assets/ic_action_stat.png", useWebView: true);
  static const HELP = const MenuItem(index: 4, title: "help", iconPath: "assets/ic_action_help.png", useWebView: true);
  static const INFO = const  MenuItem(index: 5, title: "info", iconPath: "assets/ic_action_about.png");
  static const SETTINGS = const MenuItem(index: 6, title: "settings", iconPath: "assets/ic_action_settings.png");


  static String? getURL(AppConfig config, BuildContext context, MenuItem item){
    switch (item){
      case STAT: return (config.configModel.statisticsHost != null) ? config.configModel.statisticsHost : AppLocalizations.of(context)!.getLocalizedValue('wwwStatistics');
      case HELP: return AppLocalizations.of(context)!.getLocalizedValue('wwwHelp');
      default: return null;
    }
  }
}

class MainMenu extends StatelessWidget {
  late final AppConfig config;
  late final Function(MenuItem) changeFragment;
  final bool android19;
  MainMenu({
    Key? key,
    required this.config,
    required this.changeFragment,
    this.android19 = false
  }) : super (key: key);

  final menuItems = [
    MenuItem.START,
    MenuItem.HISTORY,
    MenuItem.MAP,
    MenuItem.STAT,
    MenuItem.HELP,
    MenuItem.INFO,
    MenuItem.SETTINGS
  ];


  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    TextStyle titleBlack = theme.textTheme.headline6!.copyWith(color: Colors.black);
    TextStyle titleWhite = theme.textTheme.headline6!.copyWith(color: Colors.white);

    Color accent = theme.colorScheme.secondary;

    return Container(
      color: accent,
      child: ListView.builder(
        itemCount: menuItems.length + 1,
        itemBuilder: (_, index){
          if(index == 0){

            return new Container(
              width: double.maxFinite,
              color: Colors.white,
              child: new DrawerHeader(
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      new Image.asset("assets/app_icon.png"),
                      new Padding(padding: const EdgeInsets.only(top: 15.0),
                        child: new Text("Netmetr", style: titleBlack,),
                      ),
                    ],
                  )
              ),
            );

          }else{
            var item = menuItems[index - 1];
            return new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new TextButton(onPressed: (){
                  if(android19 && item.useWebView){
                    launch(MenuItem.getURL(config, context, item)!);
                    Navigator.pop(context);
                  }else {
                    changeFragment.call(item);
                  }
                },
                  child: new Container(
                    width: double.maxFinite,
                    child: new Row(
                      children: <Widget>[
                        Image.asset(item.iconPath, color: Colors.white,),
                        new Padding(padding: const EdgeInsets.only(left: 10.0),
                          child: new Text(AppLocalizations.of(context)!.getLocalizedValue(item.title)!, style: titleWhite,),
                        )
                      ],
                    ),
                  )
                ),
                new Divider(color: Colors.white,)
              ],
            );
          }
        }
      ),
    );
  }
}
