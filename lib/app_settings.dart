/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:shared_preferences/shared_preferences.dart';

const default_max_tests = 100;
const default_min_delay = 30;
const default_max_delay = 900;
const default_movement_meters = 250.0;

class AppSettings{
  bool qos;
  bool jitter;
  bool ndt;
  bool onlyIpv4;

  bool loopMode;
  bool onlySignal;
  int loopMaxTests;
  int loopMinDelay; //sec
  int loopMaxDelay; //sec
  double loopMovementMeters; //meters

  AppSettings({
    this.qos = true,
    this.jitter = true,
    this.ndt = false,
    this.onlyIpv4 = false,
    this.loopMode = false,
    this.onlySignal = false,
    this.loopMaxTests = default_max_tests,
    this.loopMinDelay = default_min_delay,
    this.loopMaxDelay = default_max_delay,
    this.loopMovementMeters = default_movement_meters
  });

  load() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    qos = preferences.getBool("qos") ?? true;
    jitter = preferences.getBool("jitter") ?? true;

    ndt = preferences.getBool("ndt") ?? false;
    onlyIpv4 = preferences.getBool("ipv4_only") ?? false;

    loopMode = preferences.getBool("loop_mode") ?? false;
    onlySignal = preferences.getBool("signal_only") ?? false;

    loopMaxTests =  preferences.getInt("loop_max_tests") ?? default_max_tests;
    loopMinDelay =  preferences.getInt("loop_min_delay") ?? default_min_delay;
    loopMaxDelay =  preferences.getInt("loop_max_delay") ?? default_max_delay;
    loopMovementMeters =  preferences.getDouble("loop_movement_meters_d") ?? default_movement_meters;

  }

  saveLoopMaxTests() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setInt("loop_max_tests", loopMaxTests);
  }

  saveLoopMinDelay() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setInt("loop_min_delay", loopMinDelay);
  }

  saveLoopMaxDelay() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setInt("loop_max_delay", loopMaxDelay);
  }

  saveLoopMovementMeters() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setDouble("loop_movement_meters_d", loopMovementMeters);
  }

  saveQoS() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool("qos", qos);
  }

  saveJitter() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool("jitter", jitter);
  }

  saveNDT() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool("ndt", ndt);
  }

  saveOnlyIpv4() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool("ipv4_only", onlyIpv4);
  }

  saveLoopMode() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool("loop_mode", loopMode);
  }

  saveOnlySignal() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool("signal_only", onlySignal);
  }
}