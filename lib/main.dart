/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:netmetr_flutter/device_info.dart';
import 'package:netmetr_flutter/pages/first_run_app_page.dart';
import 'package:netmetr_flutter/app_settings.dart';
import 'package:netmetr_flutter/pages/warning_termination_page.dart';
import 'package:netmetr_flutter/util/app_localizations.dart';
import 'package:netmetr_flutter/util/config_helper.dart';
import 'package:netmetr_flutter/pages/main_page.dart';
import 'package:netmetr_flutter/net/basic_info.dart';
import 'package:netmetr_flutter/app_config.dart';
import 'package:netmetr_flutter/util/shared_helper.dart' as sharedHelper;
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:sentry/sentry.dart';
import 'main.reflectable.dart';
import 'package:device_info/device_info.dart';

final buildTypeTags = "build_type";
final platformTags = "platform";
final modelTags = "model";
final hardwareTags = "hardware";
final systemOSTags = "systemOS";
final systemNameTags = "systemName";

BasicInfo? basicInfo;
String uuid = "";
String? versionName;
int? versionCode;
bool? mapUsed;
bool confirmedUserTerms = false;
bool debugMode = false;
bool? uncheckDataWarning;
bool terminationChecked = false;

void main() async {
  initializeReflectable();
  WidgetsFlutterBinding.ensureInitialized();

  //Build Config
  const config = const MethodChannel('nic.cz.netmetrflutter/config');

  await sharedHelper.getUUID().then((value) => uuid = value != null ? value : "");
  await sharedHelper.getFirstMapUse().then((value)=> mapUsed = value != null ? value : false);
  await sharedHelper.getConfirmedUserTerms().then((value)=> confirmedUserTerms = value != null ? value : false);
  await sharedHelper.getUncheckDataWarning().then((value)=> uncheckDataWarning = value != null ? value : false);
  await sharedHelper.getConfirmTerminationChecked().then((value)=> terminationChecked = value != null ? value : false);

  basicInfo = BasicInfo.fromJson(json.decode(await config.invokeMethod('getBasicInfo')));
  Map<String, dynamic> buildConfig = json.decode(await config.invokeMethod("getBuildConfig"));

  versionName = await config.invokeMethod('getVersionName');
  versionCode = await config.invokeMethod('getVersionCode');

  //App Settings
  AppSettings settings = new AppSettings();
  await settings.load();

  debugMode = isInDebugMode;

  print("VersionName: $versionName VersionCode: $versionCode");

  //Device Info
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  DeviceInfo? dInfo;
  if(Platform.isAndroid){
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    dInfo = DeviceInfo(
        model: androidInfo.model,
        hardware: androidInfo.manufacturer,
        systemOS: androidInfo.version.sdkInt,
        systemName: "Android " + androidInfo.version.release);
  }else if(Platform.isIOS){
    IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    dInfo = new DeviceInfo(
        model: iosInfo.utsname.machine,
        hardware: iosInfo.name,
        systemOS: iosInfo.systemVersion,
        systemName: iosInfo.systemName);

    print(dInfo.toString());
  }

  //Debug control server
  String controlHostDev = buildConfig['control_host_dev'];
  int port = buildConfig['port'];
  bool controlSSL = buildConfig['control_ssl'];
  bool qosSSL = buildConfig['qos_ssl'];

  if(debugMode){
    await sharedHelper.getControlServer().then((serverShared) => controlHostDev = serverShared != null ? serverShared : controlHostDev);
    await sharedHelper.getControlPort().then((portShared) => port = portShared != null ? portShared : port);
    await sharedHelper.getControlSSL().then((value) => controlSSL = value != null ? value : controlSSL);
    await sharedHelper.getQOSSSL().then((value) => qosSSL = value != null ? value : qosSSL);
  }

  // Sentry init
  final String sentryDSN = buildConfig['sentry_dsn'];
  final String sentryURL = buildConfig['sentry_url'];

  await Sentry.init(
          (options) => options
            ..dsn = "https://" + sentryDSN + "@" + sentryURL + (isInDebugMode ? "/38" : "/37")
            ..release = versionName,
          appRunner: (){
            Sentry.configureScope((scope) => scope.setTag(buildTypeTags, debugMode ? "Debug" : "Release"));
            Sentry.configureScope((scope) => scope.setTag(platformTags, Platform.isAndroid ? "Android" : Platform.isIOS ? "iOS" : "unknown"));
            Sentry.configureScope((scope) => scope.setTag(modelTags, dInfo != null && dInfo.model != null ? dInfo.model! : "unknown"));
            Sentry.configureScope((scope) => scope.setTag(hardwareTags, dInfo != null && dInfo.hardware != null ? dInfo.hardware! : "unknown"));
            Sentry.configureScope((scope) => scope.setTag(systemOSTags, dInfo != null && dInfo.systemOS != null ? dInfo.systemOS!.toString() : "unknown"));
            Sentry.configureScope((scope) => scope.setTag(systemNameTags, dInfo != null && dInfo.systemName != null ? dInfo.systemName! : "unknown"));

            runZonedGuarded(() async{
              var configuredApp = new AppConfig(
                  clientSecret: buildConfig["client_secret"],
                  configModel: new ConfigModel(
                    controlHost: controlHostDev,
                    controlIpv4Host: buildConfig["control_ipv4_host"],
                    controlIpv6Host: buildConfig["control_ipv6_host"],
                    checkIpv4Host: buildConfig["check_ipv4_host"],
                    checkIpv6Host: buildConfig["check_ipv6_host"],
                    port: port,
                    controlSSL: controlSSL,
                    qosSSL: qosSSL,
                  ),
                  networkModel: new NetworkModel(),
                  appSettings: settings,
                  deviceInfo: dInfo,
                  child: new NetmetrApp());

              SystemChrome.setPreferredOrientations([ DeviceOrientation.portraitUp]);
              SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
                  statusBarColor: Colors.black26
              ));

              runApp(configuredApp);
            }, (error, stackTrace) async{
             _catchError(error, stackTrace);
             exit(0);

            });
          }
  );

  FlutterError.onError = (errorDetails){
    if (errorDetails.stack != null) {
      _catchError(errorDetails.exception, errorDetails.stack!);
    }
  };
}

void _catchError(Object error, StackTrace stackTrace) async{
  print('Caught error: $error');
  print('Send error to sentry...');

  final SentryId response = await Sentry.captureException(error, stackTrace:  stackTrace);
  print("Crash successfully send on sentry ID: $response");

}

bool get isInDebugMode {
  bool inDebugMode = false;
  assert(inDebugMode = true);
  return inDebugMode;
}

class NetmetrApp extends StatefulWidget {

  @override
  _NetmetrAppState createState() => _NetmetrAppState();
}

class _NetmetrAppState extends State<NetmetrApp> {


  @override
  void initState() {
    super.initState();

    //init config
    ConfigHelper.init();
    ConfigHelper.instance!.basicInfo = basicInfo;
    ConfigHelper.instance!.versionCode = versionCode;
    ConfigHelper.instance!.versionName = versionName;
    ConfigHelper.instance!.uuid = uuid;
    ConfigHelper.instance!.mapUsed = mapUsed;
    ConfigHelper.instance!.debugMode = debugMode;
    ConfigHelper.instance!.uncheckDataWarning = uncheckDataWarning;
  }


  @override
  Widget build(BuildContext context) {
    AppConfig config = AppConfig.of(context)!;
    return new MaterialApp(
        title: 'Netmetr',
        localizationsDelegates: [
          const AppLocalizationsDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en', ''),
          const Locale('cs', ''),
        ],
      theme: new ThemeData(
        colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.indigo).copyWith(secondary: Colors.indigo),
      ),
      home: terminationChecked ? (confirmedUserTerms ? new MainPage(config: config) : FirstRunAppPage(config: config)) : WarningTerminationPage(confirmedUserTerms: confirmedUserTerms,),
      routes:  <String, WidgetBuilder>{
        '/main': (BuildContext context) =>  new MainPage(config: config),
        '/firstAppRun': (BuildContext context) =>  new FirstRunAppPage(config: config),
        '/warning_termination' : (BuildContext context) => WarningTerminationPage(confirmedUserTerms: confirmedUserTerms,)
      }
    );
  }

}


