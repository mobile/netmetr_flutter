
/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class BasicInfo{
  String platform;
  String osVersion;
  String? apiLevel;
  String device;
  String model;
  String? product;
  String language;
  String? timeZone;
  String? softwareRevision;

  int softwareVersionCode;
  String softwareVersionName;
  String? type;

  bool userServerSelection;

  BasicInfo({
    required this.platform,
    required this.osVersion,
    required this.apiLevel,
    required this.device,
    required this.model,
    required this.product,
    required this.language,
    required this.timeZone,
    required this.softwareRevision,
    required this.softwareVersionCode,
    required this.softwareVersionName,
    required this.type,
    this.userServerSelection = true
  });

  BasicInfo.fromJson(Map<String, dynamic> data): this(
    platform: data['platform'],
    osVersion: data['os_version'],
    apiLevel: data['api_level'],
    device: data['device'],
    model: data['model'],
    product: data['product'],
    language: data['language'],
    timeZone: data['timezone'],
    softwareRevision: data['softwareRevision'],
    softwareVersionCode: data['softwareVersionCode'],
    softwareVersionName: data['softwareVersionName'],
    type: data['type'],
  );

  Map<String, dynamic> toJson() =>
      {
        'platform' : platform,
        'os_version' : osVersion,
        'api_level' : apiLevel,
        'device' : device,
        'model' : model,
        'product' : product,
        'language' : language,
        'timezone' : timeZone,
        'softwareRevision': softwareRevision,
        'softwareVersionCode': softwareVersionCode,
        'softwareVersionName': softwareVersionName,
        'type': type,
        'user_server_selection' : userServerSelection
      };

}