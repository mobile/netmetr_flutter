/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:netmetr_flutter/net/response/abstract_response.dart';

class HistoryResponse extends AbstractResponse{
  final String testUuid;
  final int? testTypeId;
  final int? time;
  final String? timezone;
  final String? ping;
  final String? speedUpload;
  final String? pingShortest;
  final String? speedDownload;
  final int? speedDownloadClassification;
  final int? speedUploadClassification;
  final int? pingClassification;
  final int? pingShortestClassification;
  final String? model;
  final String? timeString;
  final String? networkType;

  HistoryResponse({
    required this.testUuid,
    this.testTypeId,
    this.time,
    this.timezone,
    this.ping,
    this.speedUpload,
    this.pingShortest,
    this.speedDownload,
    this.speedDownloadClassification,
    this.speedUploadClassification,
    this.pingClassification,
    this.pingShortestClassification,
    this.model,
    this.timeString,
    this.networkType
  });

  @override
  HistoryResponse.fromJson(Map<String, dynamic> jsonData) : this(
    testUuid: jsonData["test_uuid"],
    testTypeId: jsonData["test_type_id"],
    timezone: jsonData["timezone"],
    ping: jsonData["ping"],
    speedUpload: jsonData["speed_upload"],
    pingShortest: jsonData["ping_shotest"],
    speedDownload: jsonData["speed_download"],
    speedDownloadClassification: jsonData["speed_download_classification"],
    pingClassification: jsonData["ping_classification"],
    pingShortestClassification: jsonData["ping_shortest_classification"],
    model: jsonData["model"],
    time: jsonData["time"],
    timeString: jsonData["time_string"],
    networkType: jsonData["network_type"],
    speedUploadClassification: jsonData["speed_upload_classification"]
  );
}