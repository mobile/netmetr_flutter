/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:netmetr_flutter/reflector.dart';
import 'package:netmetr_flutter/net/response/abstract_response.dart';

class MapAppOptions extends AbstractResponse{
  List<MapData> mapTypes;
  Map<String, List<MapData>> mapFilters;

  MapAppOptions({
    required this.mapTypes,
    required this.mapFilters});

  static Map<String, List<MapData>> _parseMapData(Map<String, dynamic> mapData){
    Map<String, List<MapData>> value = Map();

    for(String key in mapData.keys){
      value.putIfAbsent(key, ()=> []);
      var data = mapData[key];
      for(Map<String, dynamic> json in data){
        value[key]!.add(MapData.fromJson(json));
      }

    }

    return value;
  }

  @override
  MapAppOptions.fromJson(Map<String,dynamic> jsonData) : this(
    mapTypes: AbstractResponse.parseList<MapData>(jsonData["mapTypes"]),
    mapFilters: _parseMapData(jsonData["mapFilters"])
  );

}

@reflector
class MapData extends AbstractResponse{
  String title;
  List<Options> options;
  bool translate;

  MapData({
    required this.title,
    required this.options,
    this.translate = false
  });

  @override
  MapData.fromJson(Map<String, dynamic> jsonData) : this(
    title: jsonData["title"],
    options: AbstractResponse.parseList<Options>(jsonData["options"])
  );
}

@reflector
class Options extends AbstractResponse{
  String? key;
  dynamic value;
  bool isDefault;
  Map<String, dynamic> optionsData;
  Options({
    required this.optionsData,
    this.key,
    this.value,
    this.isDefault = false
  });

  @override
  Options.fromJson(Map<String, dynamic> jsonData) : this(
    optionsData: jsonData
  );
}