/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:netmetr_flutter/net/response/abstract_response.dart';
import 'package:netmetr_flutter/reflector.dart';

class OpenDataResponse extends AbstractResponse{
  SpeedCurve? curve;
  double? timeUlMS;
  double? timeDlMS;
  double? durationDownloadMS;
  double? durationUploadMS;
  String? networkType;


  OpenDataResponse({
    this.curve,
    this.timeUlMS,
    this.timeDlMS,
    this.durationDownloadMS,
    this.durationUploadMS,
    this.networkType
  });

  @override
  OpenDataResponse.fromJson(Map<String, dynamic> jsonData) : this (
    curve: SpeedCurve.fromJson(jsonData['speed_curve']),
    timeUlMS: jsonData['time_ul_ms'],
    timeDlMS: jsonData['time_dl_ms'],
    durationDownloadMS: jsonData['duration_download_ms'],
    durationUploadMS: jsonData['duration_upload_ms'],
    networkType: jsonData['network_type']
  );
}

class SpeedCurve extends AbstractResponse{
  List<SpeedData>? download;
  List<SpeedData>? upload;
  List<SignalData>? signal;

  SpeedCurve({
    this.download,
    this.upload,
    this.signal
  });

  @override
  SpeedCurve.fromJson(Map<String, dynamic> jsonData) : this(
    download: AbstractResponse.parseList<SpeedData>(jsonData['download']),
    upload: AbstractResponse.parseList<SpeedData>(jsonData['upload']),
    signal: AbstractResponse.parseList<SignalData>(jsonData['signal'])
  );
}

@reflector
class SpeedData extends AbstractResponse{
  int? timeElapsed;
  int? bytesTotal;

  SpeedData({
    this.timeElapsed,
    this.bytesTotal
  });

  @override
  SpeedData.fromJson(Map<String, dynamic> jsonData) : this (
    timeElapsed: jsonData['time_elapsed'],
    bytesTotal: jsonData['bytes_total']
  );
}

@reflector
class SignalData extends AbstractResponse{
  int? timeElapsed;
  int? lteRSRQ;
  int? lteRSRP;
  String? catTechnology;
  int? signalStrength;
  String? networkType;

  SignalData({
    this.timeElapsed,
    this.lteRSRQ,
    this.lteRSRP,
    this.catTechnology,
    this.signalStrength,
    this.networkType
  });

  @override
  SignalData.fromJson(Map<String, dynamic> jsonData) : this (
    timeElapsed: jsonData['time_elapsed'],
    lteRSRQ: jsonData['lte_rsrq'],
    lteRSRP: jsonData['lte_rsrp'],
    catTechnology: jsonData['cat_technology'],
    signalStrength: jsonData['signal_strength'],
    networkType: jsonData['network_type']
  );
}