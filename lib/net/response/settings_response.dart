/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:netmetr_flutter/reflector.dart';
import 'package:netmetr_flutter/net/response/abstract_response.dart';

class SettingsResponse extends AbstractResponse{
  Map<String, String> urls;
  List<TestType>? qosTestTypeDesc;
  String? controlServerVersion;
  History? history;
  String? uuid;

  SettingsResponse({
    required this.urls,
    this.qosTestTypeDesc,
    this.controlServerVersion,
    this.history,
    this.uuid,
  });

  static Map<String, String> parseUrls(Map<String, dynamic> urlsData){
    Map<String,String> urls = {};

    urlsData.forEach((key, value){
      urls.putIfAbsent(key, () => value);
    });
    return urls;
  }


  static String parseVersions(Map<String, dynamic> versionData){
    return versionData["control_server_version"];
  }

  SettingsResponse.fromJson(Map<String, dynamic> jsonData) : this(
    uuid : jsonData["uuid"],
    urls : parseUrls(jsonData["urls"]),
    qosTestTypeDesc: AbstractResponse.parseList<TestType>(jsonData["qostesttype_desc"]),
    controlServerVersion: parseVersions(jsonData["versions"]),
    history: jsonData.containsKey("history") ? History.fromJson(jsonData["history"]) : null
  );

}

@reflector
class TestType extends AbstractResponse{
  String? name;
  String? type;

  TestType({this.name, this.type});
  
  TestType.fromJson(Map<String, dynamic> jsonData) : this(
    name: jsonData['name'],
    type: jsonData['test_type'],
  );
}

class History extends AbstractResponse{
  String? uuid;
  List<String>? devices;
  List<String>? networks;

  History({
    this.uuid,
    this.devices,
    this.networks});


  static List<String> parseList(List<dynamic> listData){
    List<String> parsed = [];
    for(dynamic data in listData){
      parsed.add(data);
    }

    return parsed;
  }

  History.fromJson(Map<String, dynamic> jsonData) : this(
    uuid: jsonData["uuid"],
    devices: parseList(jsonData["devices"]),
    networks:  parseList(jsonData["networks"])
  );
}

enum SettingsTaskStatus{
  RUNNING, FINISHED, FAILED
}