/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:netmetr_flutter/net/response/abstract_response.dart';
import 'package:netmetr_flutter/reflector.dart';
import 'package:reflectable/reflectable.dart';

class TestResultQoSResponse extends AbstractResponse{
  List<ResultDetail>? testResultDetails;
  String? evaluation;
  List<ResultDetailDescription>? detailDescriptions;
  EvalTimes? evalTimes;
  List<TestDescription>? testDescriptions;

  TestResultQoSResponse({
    this.testResultDetails,
    this.evaluation,
    this.detailDescriptions,
    this.evalTimes,
    this.testDescriptions
  });

  @override
  TestResultQoSResponse.fromJson(Map<String, dynamic> jsonData) : this(
    testResultDetails:  AbstractResponse.parseList<ResultDetail>(jsonData['testresultdetail']),
    evaluation: jsonData['evaluation'],
    detailDescriptions: AbstractResponse.parseList<ResultDetailDescription>(jsonData['testresultdetail_desc']),
    evalTimes: EvalTimes.fromJson(jsonData['eval_times']),
    testDescriptions: AbstractResponse.parseList<TestDescription>(jsonData['testresultdetail_testdesc'])
  );
}


@reflector
class ResultDetail extends AbstractResponse{
  QosResultType? testType;
  int uid;
  Map<String, dynamic>? testResultKeyMap;
  int? nnTestUid;
  String? testSummary;
  int? success;
  int? failure;
  int testUid;
  int? qosTestUid;
  String? testDesc;
  QosTestResult? result;
  String? resultString;

  ResultDetail({
    this.testType,
    required this.uid,
    this.testResultKeyMap,
    this.nnTestUid,
    this.testSummary,
    this.success,
    this.failure,
    required this.testUid,
    this.qosTestUid,
    this.testDesc,
    this.result
  });

  @override
  ResultDetail.fromJson(Map<String, dynamic> jsonData) : this(
    testType: QosResultType.getType(jsonData['test_type']),
    uid: jsonData['uid'],
    testResultKeyMap: jsonData['test_result_key_map'],
    nnTestUid: jsonData['nn_test_uid'],
    testSummary: jsonData['test_summary'],
    success: jsonData['success_count'],
    failure: jsonData['failure_count'],
    testUid: jsonData['test_uid'],
    qosTestUid: jsonData['qos_test_uid'],
    testDesc: jsonData['test_desc'],
    result: QosTestResult.createInstance(jsonData['result'], QosResultType.getType(jsonData['test_type']))
  );

}

@reflector
class ResultDetailDescription extends AbstractResponse{
  List<dynamic>? uid;
  QosResultType? testType;
  String? key;
  StatusType? status;
  String? desc;

  int sum;
  int success;
  int failure;

  ResultDetailDescription({
    this.uid,
    this.testType,
    this.key,
    this.status,
    this.desc,
    this.sum = 0,
    this.success = 0,
    this.failure = 0
  });

  @override
  ResultDetailDescription.fromJson(Map<String, dynamic> jsonData) : this (
    uid: jsonData['uid'],
    testType: QosResultType.getType(jsonData['test']),
    key: jsonData['key'],
    status: StatusType.getStatus(jsonData['status']),
    desc: jsonData['desc']
  );
}

@reflector
class EvalTimes extends AbstractResponse{
  int? eval;
  int? full;

  EvalTimes({this.eval, this.full});

  @override
  EvalTimes.fromJson(Map<String, dynamic> jsonData) : this(
    eval: jsonData['eval'],
    full: jsonData['full']
  );
}

@reflector
class TestDescription extends AbstractResponse{
  String? name;
  QosResultType? type;
  String? desc;

  TestDescription({
    this.name,
    this.type,
    this.desc
  });

  @override
  TestDescription.fromJson(Map<String, dynamic> jsonData) : this(
    name: jsonData['name'],
    type: QosResultType.getType(jsonData['test_type']),
    desc: jsonData['desc']
  );
}


@reflector
abstract class QosTestResult extends AbstractResponse{
  @override
  QosTestResult.fromJson(Map<String, dynamic> jsonData);
  bool isSuccess();
  String description();

  static const JSON_START_TIME_NS = "start_time_ns";
  static const JSON_DURATION_NS = "duration_ns";

  int? startTimeNS;
  int? durationNS;

  QosTestResult({
    this.startTimeNS,
    this.durationNS
  });

  static QosTestResult? createInstance(Map<String, dynamic>? jsonData, QosResultType? resultType){
    if(resultType == null || resultType.type == null) {
      print("Error cant create instance by null result type");
      return null;
    }

    ClassMirror classMirror = reflector.reflectType(resultType.type!) as ClassMirror;
    return classMirror.newInstance("fromJson", [jsonData]) as QosTestResult;
  }

  String nullableString(String? value){
    return value != null ? value : "Unknown";
  }
}

@reflector
class WebSiteResult extends QosTestResult{
  static const JSON_OBJECTIVE_TIMEOUT = "website_objective_timeout";
  static const JSON_RESULT_DURATION = "website_result_duration";
  static const JSON_RESULT_TX_BYTES = "website_result_tx_bytes";
  static const JSON_RESULT_STATUS = "website_result_status";
  static const JSON_RESULT_RX_BYTES = "website_result_rx_bytes";
  static const JSON_OBJECTIVE_URL = "website_objective_url";
  static const JSON_RESULT_INFO = "website_result_info";


  dynamic timeOut;
  int? resultDuration;
  int? resultTXBytes;
  dynamic resultStatus;
  int? resultRXBytes;
  String? objectiveUrl;
  String? resultInfo;

  WebSiteResult({
    this.timeOut,
    this.resultDuration,
    int? durationNS,
    this.resultTXBytes,
    this.resultStatus,
    this.resultRXBytes,
    int? startTimeNS,
    this.objectiveUrl,
    this.resultInfo
  }) : super(durationNS: durationNS, startTimeNS: startTimeNS);

  @override
  WebSiteResult.fromJson(Map<String, dynamic> jsonData) : this(
      timeOut: jsonData[JSON_OBJECTIVE_TIMEOUT],
      resultDuration: jsonData[JSON_RESULT_DURATION],
      durationNS: jsonData[QosTestResult.JSON_DURATION_NS],
      resultTXBytes: jsonData[JSON_RESULT_TX_BYTES],
      resultStatus: jsonData[JSON_RESULT_STATUS],
      resultRXBytes: jsonData[JSON_RESULT_RX_BYTES],
      startTimeNS: jsonData[QosTestResult.JSON_START_TIME_NS],
      objectiveUrl: jsonData[JSON_OBJECTIVE_URL],
      resultInfo: jsonData[JSON_RESULT_INFO]
  );

  @override
  bool isSuccess() {
    int status = (resultStatus is String) ? int.parse(resultStatus) : resultStatus;
    return status < 300;
  }

  @override
  String description() {
    return "Target: ${objectiveUrl != null && objectiveUrl!.isNotEmpty ? objectiveUrl : "unknown"}";
  }

}

@reflector
class HttpProxyResult extends QosTestResult{
  static const JSON_OBJECTIVE_URL = "http_objective_url";
  static const JSON_RESULT_DURATION = "http_result_duration";
  static const JSON_RESULT_HEADER = "http_result_header";
  static const JSON_HTTP_RESULT_LENGTH = "http_result_length";
  static const JSON_HTTP_OBJECTIVE_RANGE = "http_objective_range";
  static const JSON_HTTP_RESULT_HASH = "http_result_hash";
  static const JSON_RESULT_STATUS = "http_result_status";

  String? objectiveUrl;
  int? resultDuration;
  String? resultHeader;
  String? resultLength;
  String? objectiveRange;
  String? resultHash;
  int? resultStatus;

  HttpProxyResult({
    this.objectiveUrl,
    this.resultDuration,
    this.resultHeader,
    int? durationNS,
    this.resultLength,
    this.objectiveRange,
    int? startTimeNS,
    this.resultHash,
    required this.resultStatus
  }) : super(durationNS: durationNS, startTimeNS: startTimeNS);

  @override
  HttpProxyResult.fromJson(Map<String, dynamic> jsonData) : this(
      objectiveUrl: jsonData[JSON_OBJECTIVE_URL],
      resultDuration: jsonData[JSON_RESULT_DURATION],
      resultHeader: jsonData[JSON_RESULT_HEADER],
      durationNS: jsonData[QosTestResult.JSON_DURATION_NS],
      resultLength: jsonData[JSON_RESULT_HEADER],
      objectiveRange: jsonData[JSON_HTTP_OBJECTIVE_RANGE],
      startTimeNS: jsonData[QosTestResult.JSON_START_TIME_NS],
      resultHash: jsonData[JSON_HTTP_RESULT_HASH],
      resultStatus: jsonData[JSON_RESULT_STATUS]
  );

  @override
  bool isSuccess() {
   return resultStatus != null && resultStatus! < 300;
  }

  @override
  String description() {
    return "Target: $objectiveUrl";
  }

}

@reflector
class DNSResult extends QosTestResult{
  static const JSON_RESULT_ENTRIES = "dns_result_entries";
  static const JSON_OBJECTIVE_TIMEOUT = "dns_objective_timeout";
  static const JSON_DNS_RECORD = "dns_objective_dns_record";
  static const JSON_OBJECTIVE_HOST = "dns_objective_host";
  static const JSON_OBJECTIVE_RESOLVER = "dns_objective_resolver";
  static const JSON_RESULT_INFO = "dns_result_info";
  static const JSON_RESULT_DURATION = "dns_result_duration";
  static const JSONT_ENTRIES_FOUND = "dns_result_entries_found";
  static const JSON_RESULT_STATUS = "dns_result_status";


  List<dynamic>? resultEntries;
  int? objectiveTimeOut;
  String? record;
  String? host;
  String? resolver;
  String? resultInfo;
  String? resultStatus;
  int? resultDuration;

  DNSResult({
    this.resultEntries,
    this.objectiveTimeOut,
    int? startTimeNS,
    this.record,
    int? durationNS,
    this.host,
    this.resolver,
    this.resultInfo,
    this.resultStatus,
    this.resultDuration,
  }): super(durationNS: durationNS, startTimeNS: startTimeNS);

  @override
  DNSResult.fromJson(Map<String, dynamic> jsonData) : this(
      resultEntries: jsonData[JSON_RESULT_ENTRIES],
      objectiveTimeOut: jsonData[JSON_OBJECTIVE_TIMEOUT],
      startTimeNS: jsonData[QosTestResult.JSON_START_TIME_NS],
      record: jsonData[JSON_DNS_RECORD],
      durationNS: jsonData[QosTestResult.JSON_DURATION_NS],
      host: jsonData[JSON_OBJECTIVE_HOST],
      resolver: jsonData[JSON_OBJECTIVE_RESOLVER],
      resultInfo: jsonData[JSON_RESULT_INFO],
      resultStatus: jsonData[JSON_RESULT_STATUS],
      resultDuration: jsonData[JSON_RESULT_DURATION],
  );

  @override
  bool isSuccess() {
    return resultStatus == "NOERROR";
  }

  @override
  String description() {
    return "Target:  $host \n Entry: $record \n Resolver: $resolver";
  }

}

@reflector
class TCPResult extends QosTestResult{
  static const TCP_OBJECTIVE_TIMEOUT = "tcp_objective_timeout";
  static const TCP_RESULT_OUT_RESPONSE = "tcp_result_out_response";
  static const TCP_RESULT_OUT = "tcp_result_out";
  static const TCP_OBJECTIVE_OUT_PORT = "tcp_objective_out_port";

  int? objectiveTimeout;
  String? resultOutResponse;
  String? resultOut;
  int? objectiveOutPort;

  TCPResult({
    int? durationNS,
    int? startTimeNS,
    this.objectiveTimeout,
    this.resultOutResponse,
    this.resultOut,
    this.objectiveOutPort

  }) : super(durationNS: durationNS, startTimeNS: startTimeNS);

  @override
  TCPResult.fromJson(Map<String, dynamic> jsonData) : this (
    durationNS: jsonData[QosTestResult.JSON_DURATION_NS],
    startTimeNS: jsonData[QosTestResult.JSON_START_TIME_NS],
    objectiveTimeout: jsonData[TCP_OBJECTIVE_TIMEOUT],
    resultOutResponse: jsonData[TCP_RESULT_OUT_RESPONSE],
    resultOut: jsonData[TCP_RESULT_OUT],
    objectiveOutPort: jsonData[TCP_OBJECTIVE_OUT_PORT]
  );

  @override
  bool isSuccess() {
    return resultOut == "OK";
  }

  @override
  String description() {
    return "TCP outgoing, port: $objectiveOutPort";
  }

}

@reflector
class UDPResult extends QosTestResult{
  static const UDP_RESULT_OUT_NUM_PACKETS = "udp_result_out_num_packets";
  static const UDP_RESULT_OUT_RESPONSE_NUM_PACKETS = "udp_result_out_response_num_packets";
  static const UDP_OBJECTIVE_OUT_NUM_PACKETS = "udp_objective_out_num_packets";
  static const UDP_RESULT_OUT_PACKET_LOSS_RATE = "udp_result_out_packet_loss_rate";
  static const UDP_OBJECTIVE_OUT_PORT = "udp_objective_out_port";
  static const UDP_OBJECTIVE_DELAY = "udp_objective_delay";
  static const UDP_OBJECTIVE_TIMEOUT = "udp_objective_timeout";

  int? resultNumPackets;
  int? responseNumPackets;
  int? objectiveNumPackets;
  String? packetLossRate;
  int? port;
  int? delay;
  int? timeout;

  UDPResult({
    int? durationNS,
    int? startTimeNS,
    this.resultNumPackets,
    this.responseNumPackets,
    this.objectiveNumPackets,
    this.packetLossRate,
    this.port,
    this.delay,
    this.timeout
  }): super(durationNS: durationNS, startTimeNS: startTimeNS);

  @override
  UDPResult.fromJson(Map<String, dynamic> jsonData) : this (
    durationNS: jsonData[QosTestResult.JSON_DURATION_NS],
    startTimeNS: jsonData[QosTestResult.JSON_START_TIME_NS],
    resultNumPackets: jsonData[UDP_RESULT_OUT_NUM_PACKETS],
    responseNumPackets: jsonData[UDP_RESULT_OUT_RESPONSE_NUM_PACKETS],
    objectiveNumPackets: jsonData[UDP_OBJECTIVE_OUT_NUM_PACKETS],
    packetLossRate: jsonData[UDP_RESULT_OUT_PACKET_LOSS_RATE],
    port: jsonData[UDP_OBJECTIVE_OUT_PORT],
    delay: jsonData[UDP_OBJECTIVE_DELAY],
    timeout: jsonData[UDP_OBJECTIVE_TIMEOUT]
  );

  @override
  bool isSuccess() {
    return packetLossRate == "0";
  }

  @override
  String description() {
    return "UDP outgoing port: $port \n num of packets: $objectiveNumPackets";
  }

}

@reflector
class NonTransparentProxyResult extends QosTestResult{
  static const NON_TRANSPARENT_PROXY_RESULT = "nontransproxy_result";
  static const NON_TRANSPARENT_PROXY_OBJECTIVE_REQUEST = "nontransproxy_objective_request";
  static const NON_TRANSPARENT_PROXY_OBJECTIVE_TIMEOUT = "nontransproxy_objective_timeout";
  static const NON_TRANSPARENT_PROXY_OBJECTIVE_PORT = "nontransproxy_objective_port";
  static const NON_TRANSPARENT_PROXY_RESULT_RESPONSE = "nontransproxy_result_response";

  String? result;
  String? objectiveRequest;
  int? objectiveTimeout;
  int? objectivePort;
  String? resultResponse;


  NonTransparentProxyResult({
    int? durationNS,
    int? startTimeNS,
    this.result,
    this.objectiveRequest,
    this.objectiveTimeout,
    this.objectivePort,
    this.resultResponse
  }) : super(durationNS: durationNS, startTimeNS: startTimeNS);

  @override
  NonTransparentProxyResult.fromJson(Map<String, dynamic> jsonData) : this (
    durationNS: jsonData[QosTestResult.JSON_DURATION_NS],
    startTimeNS: jsonData[QosTestResult.JSON_START_TIME_NS],
    result: jsonData[NON_TRANSPARENT_PROXY_RESULT],
    objectiveRequest: jsonData[NON_TRANSPARENT_PROXY_OBJECTIVE_REQUEST],
    objectiveTimeout: jsonData[NON_TRANSPARENT_PROXY_OBJECTIVE_TIMEOUT],
    objectivePort: jsonData[NON_TRANSPARENT_PROXY_OBJECTIVE_PORT],
    resultResponse: jsonData[NON_TRANSPARENT_PROXY_RESULT_RESPONSE]
  );

  @override
  bool isSuccess() {
    return result == "OK";
  }

  @override
  String description() {
    return "Port: $objectivePort \n Request: $objectiveRequest";
  }

}

class QosResultType{
  final String? name;
  final Type? type;

  const QosResultType({this.name, this.type});

  static const TRACEROUTE = const QosResultType(name: "traceroute");
  static const VOIP = const QosResultType(name: "voip");
  static const HTTP_PROXY = const QosResultType(name: "http_proxy", type: HttpProxyResult);
  static const WEBSITE = const QosResultType(name: "website", type: WebSiteResult);
  static const NON_TRANSPARENT_PROXY = const QosResultType(name: "non_transparent_proxy", type: NonTransparentProxyResult);
  static const DNS = const QosResultType(name: "dns", type: DNSResult);
  static const TCP = const QosResultType(name: "tcp", type: TCPResult);
  static const UDP = const QosResultType(name: "udp", type: UDPResult);


  static QosResultType? getType(String name){
    name = name.toLowerCase();
    var types = [TRACEROUTE, VOIP, HTTP_PROXY, WEBSITE, NON_TRANSPARENT_PROXY, DNS, TCP, UDP];

    for(var type in types){
      if(type.name == name)
        return type;
    }

    return null;
  }

  static QosResultType getTypeByOrigin(int origin){
    var types = [TRACEROUTE, VOIP, HTTP_PROXY, WEBSITE, NON_TRANSPARENT_PROXY, DNS, TCP, UDP];
    return types[origin];
  }
}

class StatusType{
  final String desc;
  const StatusType({required this.desc});

  static const OK = const StatusType(desc: 'ok');
  static const FAIL = const StatusType(desc: 'fail');

  static StatusType getStatus(String desc){
    if(OK.desc == desc) return OK;
    else return FAIL;
  }
}