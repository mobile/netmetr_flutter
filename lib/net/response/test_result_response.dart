/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'package:netmetr_flutter/net/response/abstract_response.dart';
import 'package:netmetr_flutter/reflector.dart';

class TestResultResponse extends AbstractResponse{
  String? shareText;
  int? testTypeId;
  String? timezone;
  List<Measurement>? measurement;
  double? geoLong;
  double? geoLat;
  String? shareSubject;
  String? openTestUuid;
  String? location;
  String? openUuid;
  int? time;
  String? timeString;
  List<ResultData>? net;
  int? networkType;

  TestResultResponse({
    this.shareText,
    this.testTypeId,
    this.timezone,
    this.measurement,
    this.geoLong,
    this.geoLat,
    this.shareSubject,
    this.openTestUuid,
    this.location,
    this.openUuid,
    this.time,
    this.timeString,
    this.net,
    this.networkType
  });

  @override
  TestResultResponse.fromJson(Map<String, dynamic> jsonData) : this(
    shareText : jsonData['share_text'],
    testTypeId: jsonData['test_type_id'],
    timezone: jsonData['timezone'],
    measurement: AbstractResponse.parseList<Measurement>(jsonData['measurement']),
    geoLong: jsonData['geo_long'],
    geoLat: jsonData['geo_lat'],
    shareSubject: jsonData['share_subject'],
    openTestUuid: jsonData['open_test_uuid'],
    location: jsonData['location'],
    openUuid: jsonData['open_uuid'],
    time: jsonData['time'],
    timeString: jsonData['time_string'],
    net: AbstractResponse.parseList<ResultData>(jsonData['net']),
    networkType: jsonData['network_type']
  );

}

@reflector
class MarkData extends AbstractResponse{
  String? openTestUuid;
  double lat;
  double lon;
  String? timeString;
  List<Measurement>? measurement;
  List<ResultData>? net;

  MarkData({
    this.openTestUuid,
    this.lat = 0,
    this.lon = 0,
    this.timeString,
    this.measurement,
    this.net
  });

  @override
  MarkData.fromJson(Map<String, dynamic> jsonData) : this(
    openTestUuid: jsonData["open_test_uuid"],
    lat: jsonData["lat"],
    lon: jsonData["lon"],
    timeString: jsonData["time_string"],
    measurement: AbstractResponse.parseList<Measurement>(jsonData["measurement"]),
    net: AbstractResponse.parseList<ResultData>(jsonData["net"])
  );
}

@reflector
class Measurement extends AbstractResponse{
  String title;
  int classification;
  String value;

  Measurement({
    required this.title,
    required this.classification,
    required this.value
  });

  @override
  Measurement.fromJson(Map<String, dynamic> jsonData) : this(
    title: jsonData['title'] ?? "",
    classification: jsonData['classification'] ?? 0,
    value: jsonData['value'] ?? ""
  );
}

@reflector
class ResultData extends AbstractResponse{
  String? title;
  dynamic value;

  ResultData({
    this.title,
    this.value
  });

  @override
  ResultData.fromJson(Map<String, dynamic> jsonData) : this(
    title: jsonData['title'],
    value: jsonData['value']
  );
}