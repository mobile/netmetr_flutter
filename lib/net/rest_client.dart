/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'dart:async';
import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:crypto/src/digest_sink.dart';
import 'package:flutter/material.dart';
import 'dart:io';

import 'package:netmetr_flutter/net/basic_info.dart';
import 'package:netmetr_flutter/net/response/abstract_response.dart';
import 'package:netmetr_flutter/net/response/history_response.dart';
import 'package:netmetr_flutter/net/response/map_response.dart';
import 'package:netmetr_flutter/net/response/open_data_response.dart';
import 'package:netmetr_flutter/net/response/test_result_qos_reponse.dart';
import 'package:netmetr_flutter/net/response/test_result_response.dart';
import 'package:netmetr_flutter/util/config_helper.dart';
import 'package:netmetr_flutter/net/response/settings_response.dart';
import 'package:netmetr_flutter/app_config.dart';
import 'package:netmetr_flutter/util/network/network_connection_type.dart';
import 'package:netmetr_flutter/util/shared_helper.dart' as sharedHelper;
import 'package:netmetr_flutter/util/dialog_helper.dart' as dialogHelper;

const String _clientName = "Open-RMBT";
const String _controlPath = "/RMBTControlServer";
const String _mapPath = "/RMBTMapServer";

const String _requestTestResult = "/testresult";
const String _requestTestResultDetail = "/testresultdetail";
const String _requestTestResultQoS = "/qosTestResult";
const String _requestTestOpenData = "/opentests/";

const String _mapOptionsPath = "/tiles/info";
const String _mapMarkerPath = "/tiles/markers";

const String _requestSettings = "/settings";
const String _requestHistory = "/history";
const String _requestTime = "/time";
const String _requestSync = "/sync";

const int _historyLimit = 25;


////////////////////////// CLIENT HELPER


Uri _getUri(String path, String request, AppConfig config){
  return new Uri(scheme: config.configModel.controlSSL ? "https" : "http",
      port: config.configModel.port,
      host: config.configModel.controlHost,
      path: path + request
  );
}


String _prepareClientSecret({required String? uuid, String clientName = _clientName ,required String? versionName, required int? versionCode, required int time, required String clientSecret}){
  String input = "${uuid}_${clientName}_${versionName}_${versionCode}_${time}_$clientSecret";
  var bytes = utf8.encode(input);

  var ds = new DigestSink();
  var s = sha256.startChunkedConversion(ds);
  s.add(bytes);
  s.close();

  return ds.value.toString();
}

Future<ResponseData> _getUrl(AppConfig config, String requestPath) async{
  HttpClient client = new HttpClient();

  Uri uri = _getUri(_controlPath, requestPath, config);

  print("run task: ${uri.toString()}");

  HttpClientRequest request = await client.getUrl(uri);
  request.headers.set('Referer', uri.toString());

  HttpClientResponse response = await request.close().timeout(new Duration(milliseconds: 20000), onTimeout: ()=> throw Exception("Time out"));
  String responseData = await response.transform(utf8.decoder).join().timeout(new Duration(milliseconds: 20000), onTimeout: ()=> throw Exception("Time out"));
  print("statusCode: ${response.statusCode}");

  client.close();
  return new ResponseData(statusCode: response.statusCode, data: responseData);
}

Future<ResponseData> _sendRequest(AppConfig config, String url, String requestPath, Map<String, dynamic> data) async{
  HttpClient client = new HttpClient();

  Uri uri = _getUri(url, requestPath, config);

  print("run task: ${uri.toString()}");

  HttpClientRequest request = await client.postUrl(uri);
  request.headers.set('content-type', 'application/json;charset=UTF-8');
  request.headers.set('endcoding', 'utf-8');

  request.add(utf8.encode(json.encode(data)));

  HttpClientResponse response = await request.close().timeout(new Duration(milliseconds: 20000), onTimeout: ()=> throw Exception("Time out"));
  String responseData = await response.transform(utf8.decoder).join().timeout(new Duration(milliseconds: 20000), onTimeout: ()=> throw Exception("Time out"));
  print("statusCode: ${response.statusCode}");

  client.close();
  return new ResponseData(statusCode: response.statusCode, data: responseData);
}

Map<String, dynamic> basicRequest(ConfigHelper? configHelper, bool location){
  Map<String, dynamic> requestData = {};
  requestData.addAll(configHelper!.basicInfo!.toJson());

  if(location && configHelper.lastLocation != null){
    requestData.addAll({
      'location' : configHelper.lastLocation!.toJson()
    });
  }

  return requestData;
}

Map<String, dynamic> parseResponse(ResponseData response) {
  StatusCodes status = StatusCodes.getStatus(response.statusCode);

  if(status == StatusCodes.SERVER_ERROR)
    throw ResponseError(code: status.code, description: status.description);

  Map<String,dynamic> responseData = json.decode(response.data);

  if(status != StatusCodes.SUCCESS)
    throw ResponseError(code: status.code, description: status.description, errorData: responseData['error']);

  if(responseData.containsKey('error') && responseData['error'] != null) {
    List<dynamic> error = responseData['error'];
    if(error.isNotEmpty) {
      throw ResponseError(code: StatusCodes.RESPONSE_ERROR.code, description: StatusCodes.RESPONSE_ERROR.description, errorData: error);
    }
  }
  return responseData;
}

////////////////////////// REQUESTS

Future<int> getTime(BasicInfo? basicInfo, AppConfig config) async {
   return _sendRequest(config, _controlPath, _requestTime, basicInfo!.toJson())
      .catchError((e)=> throw new ResponseError(code: StatusCodes.SERVER_ERROR.code, description: StatusCodes.SERVER_ERROR.description, errorData: e))
      .then((response){
        try{
          Map<String,dynamic> responseData = json.decode(response.data);
          return responseData['time'];
        }catch(error){
          throw error;
        }
      });
}

Future<String?> getSyncCode(AppConfig config) async {
  if(config.networkModel.networkConnectionType == NetworkConnectionType.NOT_CONNECTED)
    throw new ResponseError(code: StatusCodes.NOT_CONNECTED.code);

  ConfigHelper? configHelper = ConfigHelper.instance;
  Map<String, dynamic> requestData = basicRequest(configHelper, true);

  requestData.addAll({
    'uuid': configHelper!.uuid,
  });

  return _sendRequest(config, _controlPath, _requestSync, requestData)
      .catchError((e){
        print(e);
    })
      .then((response){
    Map<String,dynamic> responseData = json.decode(response.data);
    return responseData['sync'][0]['sync_code'];
  });
}

Future<Map<String,dynamic>?> newSyncRequest(AppConfig config, String syncCode) async {
  if(config.networkModel.networkConnectionType == NetworkConnectionType.NOT_CONNECTED)
    throw new ResponseError(code: StatusCodes.NOT_CONNECTED.code);

  ConfigHelper? configHelper = ConfigHelper.instance;
  Map<String, dynamic> requestData = basicRequest(configHelper, true);

  requestData.addAll({
    'uuid': configHelper!.uuid,
    'sync_code': syncCode,
  });

  return _sendRequest(config, _controlPath, _requestSync, requestData)
      .catchError((e){
    print(e);
  })
      .then((response){
    Map<String,dynamic> responseData = json.decode(response.data);
    return responseData;
  });
}

Future<List<HistoryResponse>?> historyTask(AppConfig config, List<String> devices, List<String> networks, bool first25) async{
  if(config.networkModel.networkConnectionType == NetworkConnectionType.NOT_CONNECTED)
    throw new ResponseError(code: StatusCodes.NOT_CONNECTED.code);

  ConfigHelper? configHelper = ConfigHelper.instance;

  Map<String, dynamic> requestData = basicRequest(configHelper, true);

  requestData.addAll({
    'uuid' : configHelper!.uuid,
    'devices' : devices,
    'networks' : networks
  });

  if(first25) {
    requestData['result_limit'] = _historyLimit;
  }

  return _sendRequest(config, _controlPath, _requestHistory, requestData)
      .catchError((e)=> throw new ResponseError(code: StatusCodes.SERVER_ERROR.code, description: StatusCodes.SERVER_ERROR.description, errorData: e))
      .then((ResponseData response){

        try{
          Map<String, dynamic> data = parseResponse(response);

          List<HistoryResponse> history = [];
          for(var historyData in data['history']){
            print(historyData);
            history.add(new HistoryResponse.fromJson(historyData));
          }

          print(data);
          return history;

        }catch(error){
          throw error;
        }
    }
  );
}

Future<TestResultQoSResponse?> testResultQoS(AppConfig config, String testUuid) async{
  if(config.networkModel.networkConnectionType == NetworkConnectionType.NOT_CONNECTED)
    throw new ResponseError(code: StatusCodes.NOT_CONNECTED.code);

  ConfigHelper? configHelper = ConfigHelper.instance;
  Map<String, dynamic> requestData = basicRequest(configHelper, true);

  requestData.addAll({
    'uuid': configHelper!.uuid,
    'test_uuid': testUuid
  });

  return _sendRequest(config, _controlPath, _requestTestResultQoS, requestData)
    .catchError((e)=> throw new ResponseError(code: StatusCodes.SERVER_ERROR.code, description: StatusCodes.SERVER_ERROR.description, errorData: e))
    .then((response){

      try{

        Map<String, dynamic> parsedResponse = parseResponse(response);
        if(parsedResponse.keys.length == 2){
          print("not qos data");
          //return null
        }

        TestResultQoSResponse qoSResponse = TestResultQoSResponse.fromJson(parsedResponse);

        return qoSResponse;
      }catch (e){
        throw e;
      }

  });
}

Future<TestResultResponse?> testResult(AppConfig config, String testUuid) async{
  if(config.networkModel.networkConnectionType == NetworkConnectionType.NOT_CONNECTED)
    throw new ResponseError(code: StatusCodes.NOT_CONNECTED.code);

  ConfigHelper? configHelper = ConfigHelper.instance;
  Map<String, dynamic> requestData = basicRequest(configHelper, true);

  requestData.addAll({
    'uuid': configHelper!.uuid,
    'test_uuid': testUuid
  });

  return _sendRequest(config, _controlPath, _requestTestResult, requestData)
      .catchError((e)=> throw new ResponseError(code: StatusCodes.SERVER_ERROR.code, description: StatusCodes.SERVER_ERROR.description, errorData: e))
      .then((response){

    try{

      Map<String, dynamic> parsedResponse = parseResponse(response);
      List<dynamic> responseData = parsedResponse['testresult'];

      TestResultResponse testResultResponse = TestResultResponse.fromJson(responseData[0]);
      print(testResultResponse);

      return testResultResponse;

    }catch (e){
      throw e;
    }

  }
  );
}


Future<List<ResultData>?> testResultDetail(AppConfig config, String testUuid) async{
  if(config.networkModel.networkConnectionType == NetworkConnectionType.NOT_CONNECTED)
    throw new ResponseError(code: StatusCodes.NOT_CONNECTED.code);

  ConfigHelper? configHelper = ConfigHelper.instance;
  Map<String, dynamic> requestData = basicRequest(configHelper, true);

  requestData.addAll({
    'uuid': configHelper!.uuid,
    'test_uuid': testUuid
  });


  return _sendRequest(config, _controlPath, _requestTestResultDetail, requestData)
    .catchError((e)=> throw new ResponseError(code: StatusCodes.SERVER_ERROR.code, description: StatusCodes.SERVER_ERROR.description, errorData: e))
    .then((response){

     try{

       Map<String, dynamic> responseData = parseResponse(response);
       List<ResultData> details = AbstractResponse.parseList<ResultData>(responseData['testresultdetail']);
       return details;

     }catch (e){
       throw e;
     }

    }
  );
}

Future<OpenDataResponse?> testOpenData(AppConfig config, String testUuid, String openTestUuid) async{
  if(config.networkModel.networkConnectionType == NetworkConnectionType.NOT_CONNECTED)
    throw new ResponseError(code: StatusCodes.NOT_CONNECTED.code);

  return _getUrl(config, _requestTestOpenData + openTestUuid)
      .catchError((e)=> throw new ResponseError(code: StatusCodes.SERVER_ERROR.code, description: StatusCodes.SERVER_ERROR.description, errorData: e))
      .then((response){

    try{

      Map<String, dynamic> responseData = parseResponse(response);
      OpenDataResponse openData = OpenDataResponse.fromJson(responseData);
      return openData;

    }catch (e){
      throw e;
    }

  }
  );
}

Future<SettingsResponse?> checkNewSyncHistory(AppConfig config) async{
  if(config.networkModel.networkConnectionType == NetworkConnectionType.NOT_CONNECTED)
    throw new ResponseError(code: StatusCodes.NOT_CONNECTED.code);

  ConfigHelper? configHelper = ConfigHelper.instance;

  int time;
  try{
    time = await getTime(configHelper!.basicInfo, config);
  }catch(error){
    throw error;
  }

  Map<String, dynamic> requestData = basicRequest(configHelper, false);

  requestData.addAll({
    'uuid' : configHelper.uuid,
    'name' : _clientName,
    'version_name' : configHelper.versionName,
    'version_code' : configHelper.versionCode,
    'time' : time,
    'client_secret' : _prepareClientSecret(
        uuid: configHelper.uuid,
        versionName: configHelper.versionName,
        versionCode: configHelper.versionCode,
        time: time,
        clientSecret: config.clientSecret
    ),
    'terms_and_conditions_accepted_version' : 1,
    'terms_and_conditions_accepted' : true
  });


  return _sendRequest(config, _controlPath, _requestSettings, requestData)
      .catchError((e)=> throw new ResponseError(code: StatusCodes.SERVER_ERROR.code, description: StatusCodes.SERVER_ERROR.description, errorData: e))
      .then((response){
    try{
      Map<String,dynamic> responseData = parseResponse(response);

      List<dynamic> settings = responseData['settings'];
      SettingsResponse settingsResponse = SettingsResponse.fromJson(settings[0]);

      if(settingsResponse.history != null) {
        config.configModel.historyNetworks = settingsResponse.history!.networks ?? [];
        config.configModel.historyDevices = settingsResponse.history!.devices ?? [];
      }
      config..configModel.needSync = false;


      return settingsResponse;

    }catch(error){
      throw error;
    }

  });
}


Future<SettingsResponse?> checkSettingsTask(AppConfig config) async{
  if(config.networkModel.networkConnectionType == NetworkConnectionType.NOT_CONNECTED)
    throw new ResponseError(code: StatusCodes.NOT_CONNECTED.code);

  ConfigHelper? configHelper = ConfigHelper.instance;

  int time;
  try{
    time = await getTime(configHelper!.basicInfo, config);
  }catch(error){
    throw error;
  }

  Map<String, dynamic> requestData = basicRequest(configHelper, false);

  requestData.addAll({
      'uuid' : configHelper.uuid,
      'name' : _clientName,
      'version_name' : configHelper.versionName,
      'version_code' : configHelper.versionCode,
      'time' : time,
      'client_secret' : _prepareClientSecret(
        uuid: configHelper.uuid,
        versionName: configHelper.versionName,
        versionCode: configHelper.versionCode,
        time: time,
        clientSecret: config.clientSecret
      ),

      'terms_and_conditions_accepted_version' : 1, //TODO proč?
      'terms_and_conditions_accepted' : true
  });


  return _sendRequest(config, _controlPath, _requestSettings, requestData)
      .catchError((e)=> throw new ResponseError(code: StatusCodes.SERVER_ERROR.code, description: StatusCodes.SERVER_ERROR.description, errorData: e))
      .then((response){
        try{
          Map<String,dynamic> responseData = parseResponse(response);

          List<dynamic> settings = responseData['settings'];
          SettingsResponse settingsResponse = SettingsResponse.fromJson(settings[0]);


          if(settingsResponse.uuid != null) {
            String uuid = settingsResponse.uuid!;

            ConfigHelper.instance!.uuid = uuid;
            sharedHelper.setUUID(uuid);
          }

          if(settingsResponse.urls.containsKey("url_ipv6_check") && settingsResponse.urls["url_ipv6_check"] != null) {
            config.configModel.checkIpv6Host = settingsResponse.urls["url_ipv6_check"]!;
            sharedHelper.setUrlIpv6Check( config.configModel.checkIpv6Host);
          }

          if(settingsResponse.urls.containsKey("control_ipv4_only") && settingsResponse.urls["control_ipv4_only"] != null) {
            config.configModel.controlIpv4Host = settingsResponse.urls["control_ipv4_only"]!;
            sharedHelper.setControlIpv4Only(config.configModel.controlIpv4Host);
          }

          if(settingsResponse.urls.containsKey("url_ipv4_check") && settingsResponse.urls["url_ipv4_check"] != null) {
            config.configModel.checkIpv4Host = settingsResponse.urls["url_ipv4_check"]!;
            sharedHelper.setUrlIpv4Check(config.configModel.checkIpv4Host);
          }

          if(settingsResponse.urls.containsKey("control_ipv6_only") && settingsResponse.urls["control_ipv6_only"] != null) {
            config.configModel.controlIpv6Host = settingsResponse.urls["control_ipv6_only"]!;
            sharedHelper.setControlIpv6Only(config.configModel.controlIpv6Host);
          }

          if(settingsResponse.urls.containsKey("statistics") && settingsResponse.urls["statistics"] != null) {
            config.configModel.statisticsHost = settingsResponse.urls["statistics"];
            sharedHelper.setStatistics(config.configModel.statisticsHost!);
          }

          sharedHelper.setControlServerVersion(settingsResponse.controlServerVersion!);

          if(settingsResponse.history != null) {
            config.configModel.historyNetworks = settingsResponse.history!.networks ?? [];
            config.configModel.historyDevices = settingsResponse.history!.devices ?? [];
          }
          config.configModel.controlServerVersion = settingsResponse.controlServerVersion;



          return settingsResponse;

        }catch(error){
          throw error;
        }

      });
}

Future<MapAppOptions?> mapOptionsInfo(AppConfig config) async{
  if(config.networkModel.networkConnectionType == NetworkConnectionType.NOT_CONNECTED)
    throw new ResponseError(code: StatusCodes.NOT_CONNECTED.code);

  String? language = ConfigHelper.instance!.basicInfo!.language;
  Map<String, dynamic> requestData = Map();
  requestData.putIfAbsent("language", ()=>language);


  return _sendRequest(config, _mapPath, _mapOptionsPath, requestData)
    .catchError((e)=> throw new ResponseError(code: StatusCodes.SERVER_ERROR.code, description: StatusCodes.SERVER_ERROR.description, errorData: e))
    .then((response){
      try{
        Map<String,dynamic> responseData = parseResponse(response);
        MapAppOptions mapFilter = MapAppOptions.fromJson( responseData["mapfilter"]);

        //add filter keys and values
        for(List<MapData> filters in mapFilter.mapFilters.values){
          for(MapData mapData in filters){
            for(Options options in mapData.options){

              Map<String, dynamic> optionsData = new Map();
              optionsData.addAll(options.optionsData);

              optionsData.remove("title");
              optionsData.remove("summary");

              if(optionsData.containsKey("default")) {
                options.isDefault = true;
                optionsData.remove("default");
              }

              if (optionsData.isNotEmpty){

                String key = optionsData.keys.toList()[0];
                options.key = key == "provider" ? "operator" : key;
                options.value = optionsData[key];
              }
            }
          }
        }

        print(mapFilter);
        return mapFilter;
      }catch(error){
        throw error;
      }
  });
}

Future<MarkData?> mapMarker(AppConfig config, double lat, double lon, int zoom, Map<String, dynamic>? mapOptions, bool needTechnology) async{
  if(config.networkModel.networkConnectionType == NetworkConnectionType.NOT_CONNECTED)
    throw new ResponseError(code: StatusCodes.NOT_CONNECTED.code);

  Map<String, dynamic> requestData = Map();

  //Lang
  String? language = ConfigHelper.instance!.basicInfo!.language;

  //Coordinates
  Map<String, dynamic> coords ={
    'lat' : lat,
    'lon' : lon,
    'z' : zoom,
    'size' : 20
  };

  //Options and filter
  Map<String, dynamic> filter = new Map();
  Map<String, dynamic> options = new Map();

  for(String key in mapOptions!.keys){
    if(key == "_OVERLAY" || (key == "technology" && !needTechnology))
      continue;

    String value = mapOptions[key].toString();

    if(value.length > 0){
      if(key == "map_options") options.putIfAbsent(key, ()=>value);
      else filter.putIfAbsent(key, ()=>value);
    }
  }

  requestData.putIfAbsent("language", ()=>language);
  requestData.putIfAbsent("coords", ()=> coords);
  requestData.putIfAbsent("filter", ()=> filter);
  requestData.putIfAbsent("options", ()=>options);

  return _sendRequest(config, _mapPath, _mapMarkerPath, requestData)
    .catchError((e)=> throw new ResponseError(code: StatusCodes.SERVER_ERROR.code, description: StatusCodes.SERVER_ERROR.description, errorData: e))
    .then((response){
      try{

        Map<String,dynamic> responseData = parseResponse(response);

        if(responseData.isEmpty){
          return null;
        }

        List<MarkData> markList = AbstractResponse.parseList<MarkData>(responseData["measurements"]);

        print("MARKS(${markList.length}): ${markList.toString()}");

        return markList.isNotEmpty ? markList[0] : null;

      }catch(error){
        throw error;
      }
  });

}


////////////////////////// RESPONSES

showRestErrorDialog(BuildContext context,dynamic error){
  if(error is ResponseError){
    if(error.code == StatusCodes.RESPONSE_ERROR.code){
      dialogHelper.showResponseError(context: context, status: dialogHelper.DialogStatus.ERROR, errorMessage: error.getErrorString());
    }else if(error.code == StatusCodes.NOT_CONNECTED.code){
      dialogHelper.showStatusDialog(context: context, status:  dialogHelper.DialogStatus.INFO, message: "Nejste připojen k internetu");
    }else{
      dialogHelper.showParseDataError(context: context);
    }

  }else{
    dialogHelper.showConnectionError(context: context);
  }
}

class ResponseData{
  int statusCode;
  var data;

  ResponseData({required this.statusCode, this.data});
}

class ResponseError{
  int code;
  String? description;
  List<dynamic>? errorData;

  ResponseError({required this.code, this.description, this.errorData});

  String getErrorString(){
    if(errorData == null) return "";

    String errorString = "";
    for(dynamic error in errorData!){
      errorString += error.toString();
    }

    return errorString;
  }
}

class StatusCodes{
  final int code;
  final String description;

  const StatusCodes(this.code, this.description);

  static const NOT_CONNECTED = const StatusCodes(-1, "not connected");
  static const SUCCESS = const StatusCodes(200, "request ok");
  static const RESPONSE_ERROR = const StatusCodes(300, "error in response data");
  static const BAD_REQUEST = const StatusCodes(400, "bad request");
  static const FORBIDDEN = const StatusCodes(403, "forbidden");
  static const CONFLICT = const StatusCodes(409, "conflict");
  static const SERVER_ERROR = const StatusCodes(500, "internal server error");


  static getStatus(int code){
    //TODO upravit az bude jasné co může dorazit (dokumentace ?)

    if(code >= 200 && code <= 226) return SUCCESS;
    else if(code >= 400 && code <= 450){

      switch (code){
        case 403: return FORBIDDEN;
        case 409: return CONFLICT;
        default: return BAD_REQUEST;
      }
    }

    return SERVER_ERROR;
  }
}